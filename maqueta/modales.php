<div class="contactar-modal clearfix" id="contactar-modal"> 
      <hgroup>
        <h2>Contactar entrenador</h2>
        <div class="e10"></div>
        <h3 class="camarillo">Lorem Ipsum Dolor</h3>
      </hgroup>
      <div class="e10 clear"></div>
      <div class="col-left50 pull-left inline">
        <figure> <img src="assets/img/perfil-medico.png" ></figure>
      </div>

      <div class="col-left50 pull-right inline">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris</p>
        <form class="formulario">
          <textarea placeholder="Contactar "></textarea>
          <div class="clear"></div>
          <button class="pull-right boton" type="submit">Contactar</button>
        </form>
      </div>      
</div>

<div class="directorio-modal clearfix" id="directorio-modal">
  <h2>Lorem Ipsum Dolor</h2>
  <div class="e10"></div>
    <div class="col-left50 pull-left inline">
      <img src="assets/img/perfil-medico.png" >
      <div class="e20"></div>
      <p class="phone">314 5759887</p>
    </div>

    <div class="col-left50 pull-left inline">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris</p>

      <ul class="lista">
        <li>Lorem ipsum Dolor Sit Amet</li>
        <li>Lorem ipsum Dolor Sit Amet</li>
        <li>Lorem ipsum Dolor Sit Amet</li>
        <li>Lorem ipsum Dolor Sit Amet</li>
        <li>Lorem ipsum Dolor Sit Amet</li>
      </ul>
    </div>
</div>

<div class="reportar-modal clearfix" id="reportar-modal"> 
  <h2>Reporta tu Actividad</h2>
  <div class="e20"></div>
  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris</p>
    <form class="formulario">
      <div class="e10"></div>   
      <input type="text" placeholder="Peso actual">

      <div class="col-left50 pull-left clearfix">
        <select class="select_dd w100" id="sel1">
          <option value="">Ejercicio realizado</option>
          <option value="">Lorem ipsum dolor </option>
          <option value="">Lorem ipsum dolor </option>
          <option value="">Lorem ipsum dolor </option>
        </select>
      </div>
      <div class="col-left50 pull-right clearfix">
        <select class="select_dd w100" id="sel2">
          <option value="">Minutos de actividad</option>
          <option value="">Lorem ipsum dolor </option>
          <option value="">Lorem ipsum dolor </option>
          <option value="">Lorem ipsum dolor </option>
        </select>
      </div>
       <button class="pull-right boton" type="submit">Reportar</button>
    </form>
</div>

<div class="conoce-modal" id="conoce-modal"> 
  <h2>Conoce tu Avance</h2>
  <div class="e20"></div>
  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor...</p>

  <figure><img src="assets/img/curva.gif"></figure>
  <form class="formulario">
    <div class="e20"></div>
    <input type="text" placeholder="Calorías quemadas proyectadas" value="Calorías quemadas proyectadas" readonly>
    <input type="text" placeholder="Calorías quemadas reales " value="Calorías quemadas reales" readonly>
    <input type="text" placeholder="Calorías no quemadas" value="Calorías no quemadas " readonly>
    <div class="clear e10"></div>
  </form>
</div>
