
<!-- Scripts -->

<script language="javascript" type="text/javascript" src="assets/js/lib/jquery-1.8.3.min.js"></script>  <!--jquery-->

<!-- Columnas
  ================================================== -->
<script language="javascript" type="text/javascript" src="assets/js/lib/columnas/js/columnas.js"></script> <!--goofy  columnas-->
<script language="javascript" type="text/javascript" src="assets/js/lib/columnas/js/xylem.js"></script>  <!--alexander mueve columnas-->
<script language="javascript" type="text/javascript" src="assets/js/lib/columnas/js/velvetFoundation.js"></script>  <!--velvet mueve columnas-->

 <!-- Estilizar
================================================== -->
<script type="text/javascript" src="assets/js/lib/dd/jquery.dd.js"></script>

 <!-- Scroll
================================================== -->
<script type="text/javascript" src="assets/js/lib/jscrollpane/js/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" src="assets/js/lib/scroll/start_stop_scroll.js"></script>

 <!-- File
================================================== -->
<script type="text/javascript" src="assets/js/lib/customfile/filecustom.js"></script>

<!-- Slider
================================================== -->
<script type="text/javascript" src="assets/js/lib/bxslider/js/jquery.bxslider.js"></script>

<!-- Bootstrap
  ================================================== -->
<script src="assets/js/lib/bootstrap/js/bootstrap-fileupload.js"></script> 

  <!-- Fancybox
================================================== -->

<script type="text/javascript" src="assets/js/lib/source/jquery.fancybox.js"></script>

  <!-- Add Button helper (this is optional) -->
<script type="text/javascript" src="assets/js/lib/source/helpers/jquery.fancybox-buttons.js"></script>

  <!-- Add Thumbnail helper (this is optional) -->
<script type="text/javascript" src="assets/js/lib/source/helpers/jquery.fancybox-thumbs.js"></script>

  <!-- Media (this is optional) -->
<script type="text/javascript" src="assets/js/lib/source/helpers/jquery.fancybox-media.js"></script>


  <!-- UI
================================================== -->

<script src="assets/js/lib/ui/jquery-ui-1.7.custom.min.js"></script>

<!-- Acciones
================================================== -->
<script src="assets/js/actions.js"></script>

<?php include("llamados.php"); ?>

</body>
</html>

