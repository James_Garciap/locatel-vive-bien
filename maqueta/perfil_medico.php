<section>
  <article>
    <div class="fondos-left" style="background: #EE4E11;">
      <div class="mask-left clearfix"> <a href="#actualizar-modal" class="actualizar pull-right llama_modal">Actualizar información</a>
        <h1>Mi perfil</h1>
        <div class="e10"></div>
        <div class="col-left50 pull-left inline">
          <div class="miperfil-img pull_left inline">
            <figure><img src="assets/img/perfil-medico.png"></figure>
            <span>13</span> </div>
          <div class="e20"></div>
          <div class="fondo-articulos pull_left">
            <h2 class="ribon-verde">Mi Información</h2>
            <div class="e10"></div>
            <p><strong>Lorem Ipsum Dolor</strong></p>
            <p>Lorem Ipsum Dolor Sit Amet</p>
            <div class="e10 clear"></div>
          </div>
        </div>
        <div class="col-left50 pull-right inline">
          <div class="fondo-blanco">
            <h2 class="ribon-negro ">Mi estado: Sobrepeso</h2>
            <div class="e5 clear"></div>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
            <p class="text_plan">Plan a largo plazo.</p>
            <p class="text_plan"><strong>Fecha de inicio:</strong> 23/09/2013</p>
            <p class="text_plan"><strong>Estado:</strong> Activo</p>
          </div>
        </div>
      </div>
    </div>
  </article>
</section>
