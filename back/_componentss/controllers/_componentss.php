<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.co
     * @company Imaginamos S.A.S | Todos los derechos reservados
     * @date Wed, 15 Jan 2014 15:25:48
     */

                        

class _componentss extends Back_Controller {
	protected $admin_area = true;
	public $model = 'components';
	public $name = 'componentss';
	public $title = 'Componentss';


	public function __construct() {
		parent::__construct();
		$this->menu();
		$this->migas($this->current_menu);
		$this->add_modular_assets('js', 'autoload');
	}

	public function menu() {
		return $this->current_menu['Componentss'] = cms_url('componentss');
	}

	public function index() {
		$data['pag'] = $this->session->userdata('page_'.  $this->name);
		$this->session->set_userdata('page_'.$this->name, '1');
		$data['title_page'] = $this->title;
		$data['pag'] = 1;
		$data['migas'] = $this->miga;
		$data['pag_content'] = $this->contents();
		$data['pag_content_title'] = ucwords ($this->title);
		return $this->build('index', $data);
	}

	public function contents() {
		$data['path_delete'] = cms_url($this->name.'/delete');
		$data['path_add'] = cms_url($this->name.'/form');
		$data['path_edit'] = cms_url($this->name.'/form');
		$data['path_list'] = cms_url($this->name.'/lista');
		$data['mpag_content'] = $this->lista();
		$data['pag'] = 1;
		$this->session->set_userdata('page_'.$this->name, '1');
		return $this->buildajax('control', $data);
	}

	public function form() {
		$obj = new $this->model();
		$id = $this->_post('id');
		if(!empty($id))
		  $obj->join_related('component_category')->join_related('users')->get_by_id($id);
		$data['form_content'] = "";
		$data['form_content'] .= $this->inputHidden($obj->id, "id");
		$var_component_category_id = new components();
		$data['form_content'] .= $this->combox($obj->component_category_id,$var_component_category_id->get_component_category_list(),"component_category_id", "Tipo de componente", "", $obj->is_rule("component_category_id","required"), "span3");
		$data['form_content'] .= $this->input($obj->name, "name",$obj->get_rule("name","max_length") ,"Nombre", "Maximo ".$obj->get_rule("name","max_length")." caracteres", $obj->is_rule("name","required"), "span3");
		$data['form_content'] .= $this->input($obj->description, "description",$obj->get_rule("description","max_length") ,"Descripción", "Maximo ".$obj->get_rule("description","max_length")." caracteres", $obj->is_rule("description","required"), "span3");
		$data['form_content'] .= $this->input($obj->parent, "parent",$obj->get_rule("parent","max_length") ,"Relacionado..", "Maximo ".$obj->get_rule("parent","max_length")." caracteres", $obj->is_rule("parent","required"), "span3");

		$data['direct_form'] = $this->name.'/add';
		return $this->buildajax('control/form', $data);
	}

	public function lista() {
		$obj = new $this->model();
		$data['datos'] = $obj->join_related('component_category')->join_related('users')->get();
		$data['direct_form'] = $this->name.'/delete';
		return $this->buildajax('control/lista', $data);
	}

	public function add() {
		$error = false;
		$msg = "";
		$obj = new $this->model();
		$obj->get_by_id($this->_post('id'));
		if(!$obj->exists())
		$obj->id = "";
		$this->loadVar($obj);

				//		AÑADIENDO ID DEL USUARIO QUE CREA EL COMPONENTE
				//-------------------------------------------------------------
				$obj->users_id = $this->current_user->id;
				//echo "<pre>";print_r($this->current_user->id);die("</pre>");
				//-------------------------------------------------------------

		
		if (!$obj->save()) {
			$error = true;
			$msg = $obj->error->string;
			$this->deleteImg($this->data_id_obj_path($obj));
		}
		if ($error)
			$this->set_alert_session("Error guardando datos," . $msg, 'error');
		else
			$this->set_alert_session("Datos Guardados con exito...!", 'info');
		redirect(cms_url($this->name));
	}

	public function delete() {
		$error = false;
		$obj = new $this->model();
		$obj->get_by_id($this->_post('value'));
		$msg = "";
		if ($obj->exists()) {
			$id_file = $this->data_id_obj_path($obj);
			if (!$obj->delete()){
				$error = true;
				$msg = $obj->error->string;
			}
			$this->delete_files($this->data_file_path($obj));
			$this->deleteImg($id_file);
		} else {
			$error = true;
			$msg = "No existe item...!";
		}
		if ($error)
			$this->set_alert('Error al eliminar datos' . ', ' . $msg, 'error');
		else{
			$this->set_alert_session("Datos eliminados con éxito...!", 'info');
		}
		return $this->render_json(!$error);
	}

}
?>