<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.co
     * @company Imaginamos S.A.S | Todos los derechos reservados
     * @date Wed, 15 Jan 2014 15:25:49
     */

                        

class _users_doc_profiles extends Back_Controller {
	protected $admin_area = true;
	public $model = 'users_doc_profile';
	public $name = 'users_doc_profiles';
	public $title = 'Users Doc Profiles';


	public function __construct() {
		parent::__construct();
		$this->menu();
		$this->migas($this->current_menu);
		$this->add_modular_assets('js', 'autoload');
	}

	public function menu() {
		return $this->current_menu['Users Doc Profiles'] = cms_url('users_doc_profiles');
	}

	public function index() {
		$data['pag'] = $this->session->userdata('page_'.  $this->name);
		$this->session->set_userdata('page_'.$this->name, '1');
		$data['title_page'] = $this->title;
		$data['pag'] = 1;
		$data['migas'] = $this->miga;
		$data['pag_content'] = $this->contents();
		$data['pag_content_title'] = ucwords ($this->title);
		return $this->build('index', $data);
	}

	public function contents() {
		$data['path_delete'] = cms_url($this->name.'/delete');
		$data['path_add'] = cms_url($this->name.'/form');
		$data['path_edit'] = cms_url($this->name.'/form');
		$data['path_list'] = cms_url($this->name.'/lista');
		$data['mpag_content'] = $this->lista();
		$data['pag'] = 1;
		$this->session->set_userdata('page_'.$this->name, '1');
		return $this->buildajax('control', $data);
	}

	public function form() {
		$obj = new $this->model();
		$id = $this->_post('id');
		if(!empty($id))
		  $obj->join_related('users')->get_by_id($id);
		$data['form_content'] = "";
		$data['form_content'] .= $this->inputHidden($obj->id, "id");
		$data['form_content'] .= $this->input($obj->profession, "profession",$obj->get_rule("profession","max_length") ,"Profesión", "Maximo ".$obj->get_rule("profession","max_length")." caracteres", $obj->is_rule("profession","required"), "span3");
		$data['form_content'] .= $this->input($obj->school, "school",$obj->get_rule("school","max_length") ,"Universidad", "Maximo ".$obj->get_rule("school","max_length")." caracteres", $obj->is_rule("school","required"), "span3");
		$data['form_content'] .= $this->input($obj->profesional_register, "profesional_register",$obj->get_rule("profesional_register","max_length") ,"Tarjeta profesional", "Maximo ".$obj->get_rule("profesional_register","max_length")." caracteres", $obj->is_rule("profesional_register","required"), "span3");
		$data['form_content'] .= $this->input($obj->specialty_school, "specialty_school",$obj->get_rule("specialty_school","max_length") ,"Universidad (especialización)", "Maximo ".$obj->get_rule("specialty_school","max_length")." caracteres", $obj->is_rule("specialty_school","required"), "span3");
		$data['form_content'] .= $this->input($obj->medical_register, "medical_register",$obj->get_rule("medical_register","max_length") ,"Registro médico", "Maximo ".$obj->get_rule("medical_register","max_length")." caracteres", $obj->is_rule("medical_register","required"), "span3");
		$data['form_content'] .= $this->input($obj->society, "society",$obj->get_rule("society","max_length") ,"Sociedad a la que pertenece", "Maximo ".$obj->get_rule("society","max_length")." caracteres", $obj->is_rule("society","required"), "span3");
		$data['form_content'] .= $this->input($obj->work_company, "work_company",$obj->get_rule("work_company","max_length") ,"Empresa donde trabaja", "Maximo ".$obj->get_rule("work_company","max_length")." caracteres", $obj->is_rule("work_company","required"), "span3");
		$data['form_content'] .= $this->input($obj->address, "address",$obj->get_rule("address","max_length") ,"Dirección de contacto", "Maximo ".$obj->get_rule("address","max_length")." caracteres", $obj->is_rule("address","required"), "span3");
		$data['form_content'] .= $this->input($obj->average_consulting_cost, "average_consulting_cost",$obj->get_rule("average_consulting_cost","max_length") ,"Costo promedio de consulta", "Maximo ".$obj->get_rule("average_consulting_cost","max_length")." caracteres", $obj->is_rule("average_consulting_cost","required"), "span3");

		$data['direct_form'] = $this->name.'/add';
		return $this->buildajax('control/form', $data);
	}

	public function lista() {
		$obj = new $this->model();
		$data['datos'] = $obj->join_related('users')->get();
		$data['direct_form'] = $this->name.'/delete';
		return $this->buildajax('control/lista', $data);
	}

	public function add() {
		$error = false;
		$msg = "";
		$obj = new $this->model();
		$obj->get_by_id($this->_post('id'));
		if(!$obj->exists())
		$obj->id = "";
		$this->loadVar($obj);
		if (!$obj->save()) {
			$error = true;
			$msg = $obj->error->string;
			$this->deleteImg($this->data_id_obj_path($obj));
		}
		if ($error)
			$this->set_alert_session("Error guardando datos," . $msg, 'error');
		else
			$this->set_alert_session("Datos Guardados con exito...!", 'info');
		redirect(cms_url($this->name));
	}

	public function delete() {
		$error = false;
		$obj = new $this->model();
		$obj->get_by_id($this->_post('value'));
		$msg = "";
		if ($obj->exists()) {
			$id_file = $this->data_id_obj_path($obj);
			if (!$obj->delete()){
				$error = true;
				$msg = $obj->error->string;
			}
			$this->delete_files($this->data_file_path($obj));
			$this->deleteImg($id_file);
		} else {
			$error = true;
			$msg = "No existe item...!";
		}
		if ($error)
			$this->set_alert('Error al eliminar datos' . ', ' . $msg, 'error');
		else{
			$this->set_alert_session("Datos eliminados con éxito...!", 'info');
		}
		return $this->render_json(!$error);
	}

}
?>