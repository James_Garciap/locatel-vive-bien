<?php echo form_open_multipart(cms_url($direct_form), array("id" => "validate_field_types", "novalidate" => "novalidate")) ?>
<div class="row-fluid">
   
    <?php echo $form_content ?>
    
    <div class="formSep">
        <button type="submit" id="Guardar" class="btn btn-success" onclick="$('.fileupload').fileupload();">Guardar Cambios</button>
    </div>
</div>
<?php echo form_close() ?>
<br>
<h3>Asistentes</h3>

	<?php
	if($calendar_assistants)
		{
			?>
			<table border="1" cellspacing="0" bordercolor="#cccccc">
				<tr bgcolor="#eeeeee">
					<th><h5>Nombre</h5></th>
					<th><h5>Email</h5></th>
					<th><h5>acciones</h5></th>
				</tr>
			<?php

			foreach ($calendar_assistants as $key) 
				{
					echo "<tr id='tr-".$key->id."-".$calendar_id."'><td>".$key->first_name." ".$key->last_name."</td><td>".$key->email."<td/><td><a href='delete_assistant?users_id=".$key->id."&calendar_id=".$calendar_id."'>Eliminar</a></td></tr>";
				}
			?>
			</table>
			<?php
		}
	else
		{
			echo "No se han registrado asistentes";
		}
	?>
<script type="text/javascript">
function delete_assistant(users_id, calendar_id)
        {
            $("#tr"+users_id+"-"+calendar_id).load('cms/calendars/delete_assistant?users_id=' + users_id+"&calendar_id="+calendar_id, function()
            {
                $("#tr"+users_id+"-"+calendar_id).fadeOut(400);
            });

        }
</script>
