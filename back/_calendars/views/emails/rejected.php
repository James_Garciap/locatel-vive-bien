<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        <table width="632" border="0" align="center" background="<?php echo cdn_imaginamos('images/bg/bg_mail_new_user.jpg') ?>">
            <tr>
                <td height="522">
                    <table width="55%" border="0" align="center">
                        <tr>
                            <td>
                                <p style="margin-top:20px;">
                                    Su solicitud para el evento <?php echo $event_name?> ha sido rechazada<br><br>
                                    Para información adicional y comunicarse con un consultor, puede escribir a :
                                    <?php echo $email_admin?>
                                    <br><br>
                                </p>
                                <p>
                                    <br><br>
                                    Cordialmente,<br>Staff <a href="http://imaginamos.com" target="_blank">imaginamos.com</a>
                                    <br><br>
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
