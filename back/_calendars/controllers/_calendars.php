<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.co
     * @company Imaginamos S.A.S | Todos los derechos reservados
     * @date Wed, 12 Feb 2014 12:01:28
     */

                        

class _calendars extends Back_Controller {
	protected $admin_area = true;
	public $model = 'calendar';
	public $name = 'calendars';
	public $title = 'Calendars';


	public function __construct() {
		parent::__construct();
		$this->menu();
		$this->migas($this->current_menu);
		$this->add_modular_assets('js', 'autoload');
	}

	public function menu() {
		return $this->current_menu['Calendars'] = cms_url('calendars');
	}

	public function index() {
		$data['pag'] = $this->session->userdata('page_'.  $this->name);
		$this->session->set_userdata('page_'.$this->name, '1');
		$data['title_page'] = $this->title;
		$data['pag'] = 1;
		$data['migas'] = $this->miga;
		$data['pag_content'] = $this->contents();
		$data['pag_content_title'] = ucwords ($this->title);
		return $this->build('index', $data);
	}

	public function contents() {
		$data['path_delete'] = cms_url($this->name.'/delete');
		$data['path_add'] = cms_url($this->name.'/form');
		$data['path_edit'] = cms_url($this->name.'/form');
		$data['path_list'] = cms_url($this->name.'/lista');
		$data['mpag_content'] = $this->lista();
		$data['pag'] = 1;
		$this->session->set_userdata('page_'.$this->name, '1');
		return $this->buildajax('control', $data);
	}

	public function form() {
		$obj = new $this->model();
		$id = $this->_post('id');
		if(!empty($id))
		  $obj->join_related('users')->join_related('imagen')->get_by_id($id);
		$data['form_content'] = "";
		$data['form_content'] .= $this->inputHidden($obj->id, "id");
		$data['form_content'] .= $this->input($obj->name, "name",$obj->get_rule("name","max_length") ,"Nombre del evento", "Maximo ".$obj->get_rule("name","max_length")." caracteres", $obj->is_rule("name","required"), "span3");
		$data['form_content'] .="<br><br><br><br>Descripción";
		$data['form_content'] .= $this->text($obj->description, "description",$obj->get_rule("description","max_length") ,"Descripción", "Maximo ".$obj->get_rule("description","max_length")." caracteres", $obj->is_rule("description","required"), "span3");
		//$data['form_content'] .= $this->input($obj->date, "date",$obj->get_rule("date","max_length") ,"Fecha", "Maximo ".$obj->get_rule("date","max_length")." caracteres", $obj->is_rule("date","required"), "span3");
		//$data['form_content'] .= $this->input($obj->imagen_id, "imagen_id",$obj->get_rule("imagen_id","max_length") ,"Imagen", "Maximo ".$obj->get_rule("imagen_id","max_length")." caracteres", $obj->is_rule("imagen_id","required"), "span3");
		$data['form_content'] .= $this->imagen($obj->imagen_id,$obj->imagen_path, "Imagen", "", false, $obj->is_rule("imagen_id","required"), "span3");

		$data['direct_form'] = $this->name.'/add';
		$assistants = new assistants();
		$assistants->select('users_id')->where('calendar_id', $obj->id)->get();
		$c=0;
		if($assistants->result_count()>0)
			{
				foreach ($assistants as $key) 
					{
						$users[$c] = new users();
						$users[$c]->where('id', $key->users_id)->get();
						$c++;
					}
				$data['calendar_assistants'] = $users;
			}
		else
			{
				$data['calendar_assistants'] = false;		
			}
		$data['calendar_id'] = $obj->id;
		return $this->buildajax('control/form', $data);
	}

	public function lista() {
		$obj = new $this->model();
		$data['datos'] = $obj->join_related('users')->join_related('imagen')->get();
		$data['direct_form'] = $this->name.'/delete';
		return $this->buildajax('control/lista', $data);
	}

	public function add() {
		$error = false;
		$msg = "";
		$obj = new $this->model();
		$obj->get_by_id($this->_post('id'));
		if(!$obj->exists())
		$obj->id = "";
		$this->loadVar($obj);

		$obj->users_id = $this->current_user->id;
		$obj->date = date("Y-m-d");

		if (!$obj->save()) {
			$error = true;
			$msg = $obj->error->string;
			$this->deleteImg($this->data_id_obj_path($obj));
		}
		if ($error)
			$this->set_alert_session("Error guardando datos," . $msg, 'error');
		else
			$this->set_alert_session("Datos Guardados con exito...!", 'info');
		redirect(cms_url($this->name));
	}

	public function delete() {
		$error = false;
		$obj = new $this->model();
		$obj->get_by_id($this->_post('value'));
		$msg = "";
		if ($obj->exists()) {

			//===========================
			$assistants = new assistants();
			$assistants->where('calendar_id', $obj->id)->get();
			if($assistants->result_count()>0)
				{
					$this->set_alert('El evento no se puede eliminar por que tiene usuarios asociados');
					return $this->render_json(!$error);
				}
			//===========================

			$id_file = $this->data_id_obj_path($obj);
			if (!$obj->delete()){
				$error = true;
				$msg = $obj->error->string;
			}
			$this->delete_files($this->data_file_path($obj));
			$this->deleteImg($id_file);
		} else {
			$error = true;
			$msg = "No existe item...!";
		}
		if ($error)
			$this->set_alert('Error al eliminar datos' . ', ' . $msg, 'error');
		else{
			$this->set_alert_session("Datos eliminados con éxito...!", 'info');
		}
		return $this->render_json(!$error);
	}


	//------------------
	function delete_assistant()
		{
			$users_id = $_GET['users_id'];
			$calendar_id = $_GET['calendar_id'];

			//---- Trayendo data del evento
			$calendar = new calendar();
			$calendar->where('id', $_GET['calendar_id'])->get();
			//-----------------------------

			$assistants = new assistants();
			$assistants->where(array('users_id'=>$users_id, 'calendar_id'=>$calendar_id))->get();
			$assistants->delete_all();


			//---------- MENSAJE DE ALERTA PARA USUARIOS DESACTIVADOS --------------------
				$this->load->library('email');
                $this->email->clear();
                $this->email->protocol = 'smtp';
                $this->_data = array(
                   'email_admin' => "cms@imaginamos.com",
                   'event_name' => $calendar->name,
                );
                $html = $this->view('emails/rejected');
                $this->email->from('imaginamos.prueba@gmail.com', 'CMS Imaginamos');
                $this->email->to($obj->email);
                $this->email->subject('Solicitud rechazada');
                $this->email->message($html);
		        if(!$this->email->send())
		        	{
		        		die("No se ha podido notificar por email al usuario");
		        	}
				//---------- MENSAJE DE ALERTA PARA USUARIOS DESACTIVADOS --------------------

			redirect(cms_url('calendars'));
		}

}
?>