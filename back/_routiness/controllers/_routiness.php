<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.co
     * @company Imaginamos S.A.S | Todos los derechos reservados
     * @date Wed, 15 Jan 2014 15:25:48
     */

                        

class _routiness extends Back_Controller {
	protected $admin_area = true;
	public $model = 'routines';
	public $name = 'routiness';
	public $title = 'Routiness';


	public function __construct() {
		parent::__construct();
		$this->menu();
		$this->migas($this->current_menu);
		$this->add_modular_assets('js', 'autoload');
	}

	public function menu() {
		return $this->current_menu['Routiness'] = cms_url('routiness');
	}

	public function index() {
		$data['pag'] = $this->session->userdata('page_'.  $this->name);
		$this->session->set_userdata('page_'.$this->name, '1');
		$data['title_page'] = $this->title;
		$data['pag'] = 1;
		$data['migas'] = $this->miga;
		$data['pag_content'] = $this->contents();
		$data['pag_content_title'] = ucwords ($this->title);
		return $this->build('index', $data);
	}

	public function contents() {
		$data['path_delete'] = cms_url($this->name.'/delete');
		$data['path_add'] = cms_url($this->name.'/form');
		$data['path_edit'] = cms_url($this->name.'/form');
		$data['path_list'] = cms_url($this->name.'/lista');
		$data['mpag_content'] = $this->lista();
		$data['pag'] = 1;
		$this->session->set_userdata('page_'.$this->name, '1');
		return $this->buildajax('control', $data);
	}

	public function form() {
		$obj = new $this->model();
		$id = $this->_post('id');
		if(!empty($id))
		  $obj->join_related('routines_categories')->get_by_id($id);
		$data['form_content'] = "";
		$data['form_content'] .= $this->inputHidden($obj->id, "id");
		$var_routines_categories_id = new routines();
		$data['form_content'] .= $this->combox($obj->routines_categories_id,$var_routines_categories_id->get_routines_categories_list(),"routines_categories_id", "Categoríar", "", $obj->is_rule("routines_categories_id","required"), "span3");
		$data['form_content'] .= $this->input($obj->name, "name",$obj->get_rule("name","max_length") ,"Nombre", "Maximo ".$obj->get_rule("name","max_length")." caracteres", $obj->is_rule("name","required"), "span3");
		$data['form_content'] .= $this->input($obj->description, "description",$obj->get_rule("description","max_length") ,"Descripción", "Maximo ".$obj->get_rule("description","max_length")." caracteres", $obj->is_rule("description","required"), "span3");
		
		//========================================== Añadiendo dependencias =============================================//
		//----------------------- RUTINA -------------------------
		//----------------------- ETAPAS -------------------------
		//echo "<pre>";print_r($_POST);echo "</pre>";die("-");
		$stage = new stages();
		$stage->where('routines_id', $obj->id)->order_by('id asc')->get();
		$c = 1;
		$data['form_content_2'] = "";
		foreach ($stage as $key) 
			{
				$data['form_content_2'] .="<a onclick=\"$('#stage_".$c."').toggle('slow');\" style=\"cursor:pointer\">
					    	<h5>Etapa ".$c." + </h5>
					    </a>
					    <div  class=\"formSep\" id=\"stage_".$c."\" style=\"display:none\">
		    	<form action=\"http://localhost/locatel/vivebien/cms/stagess/add\" method=\"post\" accept-charset=\"utf-8\" id=\"validate_field_types\" novalidate=\"novalidate\" enctype=\"multipart/form-data\" style=\"opacity: 1;\">";
				$data['form_content_2'] .= $this->input($key->name, "name",$key->get_rule("name","max_length") ,"Nombre de la etapa", "Maximo ".$key->get_rule("name","max_length")." caracteres", $key->is_rule("name","required"), "span3");
				$data['form_content_2'] .= $this->inputHidden($key->id, "id");
				$data['form_content_2'] .= $this->inputHidden($obj->id, "routines_id");
				//$data['form_content_2_keys'] .= $this->input($key->type, "type",$key->get_rule("type","max_length") ,"Tipo", "Maximo ".$key->get_rule("type","max_length")." caracteres", $key->is_rule("type","required"), "span3");
				$data['form_content_2'] .= $this->input($key->description, "description",$key->get_rule("description","max_length") ,"Detalles", "Maximo ".$key->get_rule("description","max_length")." caracteres", $key->is_rule("description","required"), "span3");
				
				$data['form_content_2'] .= "<div class=\"formSep\">
			         <button type=\"submit\" id=\"Guardar\" class=\"btn btn-success\" onclick=\"$('.fileupload').fileupload();\">Guardar Cambios</button>
			    </div>
		    	</form>
		    	<div class=\"formSep\"></div>";
		    	// --------------------- SEMANAS ----------------------
		    	$week = new weeks();
		    	$week->join_related('phases');
		    	$week->where('stages_id', $key->id)->order_by('id asc')->get();
		    	$c2 = 1;
		    	foreach ($week as $key2)
			    	{
			    		$data['form_content_2'] .= "
			    		<a onclick=\"$('#stage_".$c."-week_".$c2."').toggle('slow');\" style=\"cursor:pointer\">
					    	<h5>Semana ".$c2." + </h5>
					    </a>
				    	<div  class=\"formSep\" id=\"stage_".$c."-week_".$c2."\" style=\"display:none\">
				    		<form action=\"http://localhost/locatel/vivebien/cms/weekss/add\" method=\"post\" accept-charset=\"utf-8\" id=\"validate_field_types\" novalidate=\"novalidate\" enctype=\"multipart/form-data\" style=\"opacity: 1;\">
			    		";
						$data['form_content_2'] .= $this->inputHidden($key2->id, "id");
						$data['form_content_2'] .= $this->inputHidden($key->id, "stages_id");
						$data['form_content_2'] .= $this->input($key2->name, "name",$key2->get_rule("name","max_length") ,"Detalles", "Maximo ".$key2->get_rule("name","max_length")." caracteres", $key2->is_rule("name","required"), "span3");
						$var_phases_id = new weeks();
						$data['form_content_2'] .= $this->combox($key2->phases_id,$var_phases_id->get_phases_list(),"phases_id", "Tipo de fase", "", $key2->is_rule("phases_id","required"), "span3");
						$data['form_content_2'] .= "
						<div class=\"formSep\">
						         <button type=\"submit\" id=\"Guardar\" class=\"btn btn-success\" onclick=\"$('.fileupload').fileupload();\">Guardar Cambios</button>
						    </div>
				    		</form>
				    	<div class=\"formSep\"></div>";
				    	//------------------------ SESIONES
				    	$workout = new workouts();
				    	$workout->where('weeks_id', $key2->id)->order_by('id asc')->get();
				    	$c3 =1;
				    	foreach ($workout as $key3)
					    	{
					    		$data['form_content_2'] .= "
					    		<a onclick=\"$('#stage_".$c."-week_".$c2."-workout_".$c3."').toggle('slow');\" style=\"cursor:pointer\">
							    	<h5>Sesión ".$c3." + </h5>
							    </a>
						    	<div  class=\"formSep\" id=\"stage_".$c."-week_".$c2."-workout_".$c3."\" style=\"display:none\">
						    		<form action=\"http://localhost/locatel/vivebien/cms/workoutss/add\" method=\"post\" accept-charset=\"utf-8\" id=\"validate_field_types\" novalidate=\"novalidate\" enctype=\"multipart/form-data\" style=\"opacity: 1;\">
					    		";
					    		$data['form_content_2'] .= $this->inputHidden($key3->id, "id");
					    		$data['form_content_2'] .= $this->inputHidden($key2->id, "weeks_id");
					    		$data['form_content_2'] .= $this->input($key3->name, "name",$key3->get_rule("name","max_length") ,"Nombre", "Maximo ".$key3->get_rule("name","max_length")." caracteres", $key3->is_rule("name","required"), "span3");
								$data['form_content_2'] .= $this->input($key3->description, "description",$key3->get_rule("description","max_length") ,"Detalles", "Maximo ".$key3->get_rule("description","max_length")." caracteres", $key3->is_rule("description","required"), "span3");
								$data['form_content_2'] .= $this->imagen($key3->imagen_id,$key3->imagen_path, "Imagen", "", false, $key3->is_rule("imagen_id","required"), "span3");		
								
								$data['form_content_2'] .="
								<div class=\"formSep\">
									         <button type=\"submit\" id=\"Guardar\" class=\"btn btn-success\" onclick=\"$('.fileupload').fileupload();\">Guardar Cambios</button>
									    </div>
						    		</form>
						    	<div class=\"formSep\"></div>
						    	</div>
								";
								$c3++;
					    	}
						$data['form_content_2'] .="</div>";
						$c2++;	
			    	}
				$data['form_content_2'] .="</div>";
				$c++;
			}

		//echo "<pre>";print_r($data['form_content']);die("</pre>--");




		$data['direct_form'] = $this->name.'/add';
		return $this->buildajax('control/form', $data);
	}

	public function lista() {
		$obj = new $this->model();
		$data['datos'] = $obj->join_related('routines_categories')->get();
		$data['direct_form'] = $this->name.'/delete';
		return $this->buildajax('control/lista', $data);
	}

	public function add() {
		//-------- custom---------------
		//$this->create_dependencies();
		$flag = 0;
		//-------- custom---------------
		$error = false;
		$msg = "";
		$obj = new $this->model();
		$obj->get_by_id($this->_post('id'));
		if(!$obj->exists())
			{
				$obj->id = "";
				$flag = true;
			}
		$this->loadVar($obj);
		if (!$obj->save()) {
			$error = true;
			$msg = $obj->error->string;
			$this->deleteImg($this->data_id_obj_path($obj));
		}
		if ($error)
			$this->set_alert_session("Error guardando datos," . $msg, 'error');
		else
			$this->set_alert_session("Datos Guardados con exito...!", 'info');
		//-------------- CUSTOM------------
		if($flag)
		{
			if(!$this->create_dependencies($obj->id))
			{
				$msg = $obj->error->string;
				$this->set_alert_session("Error creando dependencias...," . $msg, 'error');
			}	
		}
		
		//-------------- CUSTOM------------
		redirect(cms_url($this->name));
	}

	public function delete() {


		$error = false;
		$obj = new $this->model();
		$obj->get_by_id($this->_post('value'));
		$id = $obj->id;
		$msg = "";
		if ($obj->exists()) {

			$id_file = $this->data_id_obj_path($obj);

			//---------- Eliminando depende3ncias --------------------
			$stages = new stages();
			$stages->where('routines_id', $id)->get();
			foreach ($stages as $key)
				{
					$week = new weeks();
					$week->where('stages_id', $key->id)->get();
					foreach ($week as $key2)
						{
							$workout = new workouts();
							$workout->where('weeks_id', $key2->id)->get();
							foreach ($workout as $key3)
								{
									$key3->delete();
								}
							$key2->delete();
						}
					$key->delete();
				}
			//---------- Eliminando depende3ncias --------------------
			if (!$obj->delete()){

				
				$error = true;
				$msg = $obj->error->string;
			}

			$this->delete_files($this->data_file_path($obj));
			$this->deleteImg($id_file);
		} else {
			$error = true;
			$msg = "No existe item...!";
		}
		if ($error)
			$this->set_alert('Error al eliminar datos' . ', ' . $msg, 'error');
		else{
			$this->set_alert_session("Datos eliminados con éxito...!", 'info');
		}
		return $this->render_json(!$error);
	}

	//----------------- CUSTOM -------------------
	public function create_dependencies($id)
		{
			$c = 1;
			while ($c <= 6) 
				{
					$stage = new stages();
					$stage->routines_id = $id;
					$stage->name = 'Etapa '.$c;
					if(!$stage->save())
						{
							$msg = $stage->error->string;
							$this->set_alert('Error al crear dependencias (etapas)' . ', ' . $msg, 'error');
							return false;
						}
					else
						{
							$c2 = 1;
							while ($c2 <= 4)
								{
									$week = new weeks();
									$week->stages_id = $stage->id;
									$week->phases_id = 1;
									$week->name = 'Semana '.$c2;
									if(!$week->save())
										{
											$msg = $week->error->string;
											$this->set_alert('Error al crear dependencias (semanas)' . ', ' . $msg, 'error');
											return false;
										}
									else
										{
											$c3 = 1;
											while ($c3 <= 7)
												{
													$workout = new workouts();
													$workout->weeks_id = $week->id;
													$workout->name = 'Sesión '.$c3;
													if(!$workout->save())
														{
															$msg = $workout->error->string;
															$this->set_alert('Error al crear dependencias (sesiones)' . ', ' . $msg, 'error');
															return false;
														}
													$c3++;
												}
										}
									$c2++;
								}
						}
					$c++;
				}

		return true;
		}
	//----------------- CUSTOM -------------------

}
?>