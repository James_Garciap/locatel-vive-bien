<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.co
     * @company Imaginamos S.A.S | Todos los derechos reservados
     * @date Tue, 18 Feb 2014 16:06:20
     */

                        

class _userss extends Back_Controller {
	protected $admin_area = true;
	public $model = 'users';
	public $name = 'userss';
	public $title = 'Userss';


	public function __construct() {
		parent::__construct();
		$this->menu();
		$this->migas($this->current_menu);
		$this->add_modular_assets('js', 'autoload');
	}

	public function menu() {
		return $this->current_menu['Userss'] = cms_url('userss');
	}

	public function index() {
		$data['pag'] = $this->session->userdata('page_'.  $this->name);
		$this->session->set_userdata('page_'.$this->name, '1');
		$data['title_page'] = $this->title;
		$data['pag'] = 1;
		$data['migas'] = $this->miga;
		$data['pag_content'] = $this->contents();
		$data['pag_content_title'] = ucwords ($this->title);
		return $this->build('index', $data);
	}

	public function contents() {
		$data['path_delete'] = cms_url($this->name.'/delete');
		$data['path_add'] = cms_url($this->name.'/form');
		$data['path_edit'] = cms_url($this->name.'/form');
		$data['path_list'] = cms_url($this->name.'/lista');
		$data['mpag_content'] = $this->lista();
		$data['pag'] = 1;
		$this->session->set_userdata('page_'.$this->name, '1');
		return $this->buildajax('control', $data);
	}

	public function form() {
		$obj = new $this->model();
		$id = $this->_post('id');
		if(!empty($id))
		  $obj->where(array('email <>'=>'admin@imaginamos.com', 'email <>'=>'cms@imaginamos.com'))->get_by_id($id);
		$data['form_content'] = "";
		$data['form_content'] .= $this->inputHidden($obj->id, "id");
		$data['form_content'] .= $this->input($obj->first_name, "first_name",$obj->get_rule("first_name","max_length") ,"Fecha de nacimiento", "Maximo ".$obj->get_rule("first_name","max_length")." caracteres", $obj->is_rule("first_name","required"), "span3");
		$data['form_content'] .= $this->input($obj->last_name, "last_name",$obj->get_rule("last_name","max_length") ,"Fecha de nacimiento", "Maximo ".$obj->get_rule("last_name","max_length")." caracteres", $obj->is_rule("last_name","required"), "span3");
		$data['form_content'] .= $this->input($obj->email, "email",$obj->get_rule("email","max_length") ,"Fecha de nacimiento", "Maximo ".$obj->get_rule("email","max_length")." caracteres", $obj->is_rule("email","required"), "span3");
		//$data['form_content'] .= $this->input($obj->birthday, "birthday",$obj->get_rule("birthday","max_length") ,"Fecha de nacimiento", "Maximo ".$obj->get_rule("birthday","max_length")." caracteres", $obj->is_rule("birthday","required"), "span3");
		//$data['form_content'] .= $this->input($obj->height, "height",$obj->get_rule("height","max_length") ,"Altura", "Maximo ".$obj->get_rule("height","max_length")." caracteres", $obj->is_rule("height","required"), "span3");
		//$data['form_content'] .= $this->input($obj->weight, "weight",$obj->get_rule("weight","max_length") ,"Peso", "Maximo ".$obj->get_rule("weight","max_length")." caracteres", $obj->is_rule("weight","required"), "span3");
		$var_activity_level_id = new users();
		$data['form_content'] .= $this->combox($obj->activity_level_id,$var_activity_level_id->get_activity_level_list(),"activity_level_id", "Nivel de actividad física que realizas", "", $obj->is_rule("activity_level_id","required"), "span3");
		$var_cities_id = new users();
		$data['form_content'] .= $this->combox($obj->cities_id,$var_cities_id->get_cities_list(),"cities_id", "Ciudad", "", $obj->is_rule("cities_id","required"), "span3");

		$data['direct_form'] = $this->name.'/add';
		return $this->buildajax('control/form', $data);
	}

	public function lista() {
		$obj = new $this->model();
		$data['datos'] = $obj->where(array('email <>'=>'cms@imaginamos.com', 'status'=>1))->get();
		foreach ($obj as &$key)
			{
				//echo "<pre>";print_r($key->status);echo "</pre>";
				$cities = new cities;
				$cities->where('id', $key->cities_id)->get();
				$key->cities_name = $cities->name;
				$profile = new users_doc_profile();
				$profile->select('profession, medical_register, specialty')->where('users_id', $key->id)->get();
				$key->profession = $profile->profession;
				$key->medical_register = $profile->medical_register;
				$key->specialty = $profile->specialty;
			}
		$data['direct_form'] = $this->name.'/delete';
		return $this->buildajax('control/lista', $data);
	}

	public function add() {
		$error = false;
		$msg = "";
		$obj = new $this->model();
		$obj->get_by_id($this->_post('id'));
		if(!$obj->exists())
		$obj->id = "";
		$this->loadVar($obj);
		if (!$obj->save()) {
			$error = true;
			$msg = $obj->error->string;
			$this->deleteImg($this->data_id_obj_path($obj));
		}
		if ($error)
			$this->set_alert_session("Error guardando datos," . $msg, 'error');
		else
			$this->set_alert_session("Datos Guardados con exito...!", 'info');
		redirect(cms_url($this->name));
	}

	public function delete() {
		$error = false;
		$obj = new $this->model();
		$obj->get_by_id($this->_post('value'));
		//------- --CUSTOM CODE
		$obj->status = 0;
		$obj->full_name =0;
		//---------------------
		$msg = "";
		if ($obj->exists()) {
			$id_file = $this->data_id_obj_path($obj);
			if (!$obj->save()){
				$error = true;
				$msg = $obj->error->string;
			}
			$this->delete_files($this->data_file_path($obj));
			$this->deleteImg($id_file);
		} else {
			$error = true;
			$msg = "No existe item...!";
		}
		if ($error)
			$this->set_alert('Error al eliminar datos' . ', ' . $msg, 'error');
		else
			{

				//---------- MENSAJE DE ALERTA PARA USUARIOS DESACTIVADOS --------------------
				$this->load->library('email');
                $this->email->clear();
                $this->email->protocol = 'smtp';
                $this->_data = array(
                   'email_admin' => "cms@imaginamos.com"
                );
                $html = $this->view('emails/inactivate');
                $this->email->from('imaginamos.prueba@gmail.com', 'CMS Imaginamos');
                $this->email->to($obj->email);
                $this->email->subject('Cuenta desactivada');
                $this->email->message($html);
		        if(!$this->email->send())
		        	{
		        		die("No se ha podido notificar por email al usuario");
		        	}
				//---------- MENSAJE DE ALERTA PARA USUARIOS DESACTIVADOS --------------------
				$this->set_alert_session("Datos eliminados con éxito...!", 'info');
			}
		return $this->render_json(!$error);
	}

}
?>