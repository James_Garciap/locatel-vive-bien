<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <div class="desplegableu pull-right" style="margin-top: 7px;">
                <a class="goog-text-highlight btn-seccion dropdown-toggle" data-toggle="dropdown" href="#" style="">
                    <i class="icon-user icon-large"></i>
                </a>
                <a class="btn-seccion dropdown-toggle" data-toggle="dropdown" href="#"  >
                    <span class="icon-chevron-down icon-large"></span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="<?php echo cms_url('login/logout') ?>">
                            <i class="icon-remove"></i>Cerrar Sesión
                        </a>
                    </li>
                </ul>
            </div>
            <div id="fade-menu" class="pull-left">
                <ul class="clearfix" id="mobile-nav">
                        <li>
                            <a href="javascript:void(0)">Home</a>
                            <ul>
                                <li>
                                    <a href="javascript:void(0)">Contacto</a>
                                    <ul>
                                        <li>
                                            <a href="<?php echo cms_url('contactos/redes') ?>">Redes Sociales</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo cms_url('contactos/contactos') ?>">Datos de Contacto</a>
                                        </li>
                                    </ul>
                                </li>
                           </ul>
                       </li>   


                       <!-- ========================================================== --> 


                       <li>
                            <a href="javascript:void(0)">Componentes</a>
                            <ul>
                                <li>
                                    <a href="<?php echo cms_url('static_textss') ?>">Textos estáticos</a>
                                </li>
                                <li>
                                    <a href="<?php echo cms_url('tipss') ?>">Tips</a>
                                </li>
                                <li>
                                    <a href="<?php echo cms_url('recipess') ?>">Recetas</a>
                                </li>
                                <li>
                                    <a href="<?php echo cms_url('calendars') ?>">Eventos</a>
                                </li>
                                <li>
                                    <a href="<?php echo cms_url('postss') ?>">Artículos</a>
                                </li>
                                 <li>
                                    <a href="<?php echo cms_url('sliders') ?>">Slider del menú Home</a>
                                </li>
                           </ul>
                       </li> 


                        <!-- ========================================================== -->                     
                        
                     <!-- ========================================================== --> 


                       <li>
                            <a href="javascript:void(0)">Menús</a>
                            <ul>
                                                                       
                                <li>
                                    <a href="<?php echo cms_url('frontmenus') ?>">Menús</a>
                                </li>
                                <li>
                                    <a href="<?php echo cms_url('frontmenu_itemss') ?>">Items</a>
                                </li>
                                  
                           </ul>
                       </li> 


                        <!-- ========================================================== -->


                          <!-- ========================================================== --> 


                       <li>
                            <a href="javascript:void(0)">Dietas</a>
                            <ul>
                                                                       
                                <li>
                                    <a href="<?php echo cms_url('diets_categoriess') ?>">Categorías</a>
                                </li>
                                <li>
                                    <a href="<?php echo cms_url('dietss') ?>">Dietas</a>
                                </li>
                                <li>
                                    <a href="<?php echo cms_url('meals_types') ?>">Tipos de comidas</a>
                                </li>
                                <li>
                                    <a href="<?php echo cms_url('mealss') ?>">Comidas</a>
                                </li>
                                  
                           </ul>
                       </li> 


                        <!-- ========================================================== -->

                         <!-- ========================================================== --> 

                       <li>
                            <a href="javascript:void(0)">Rutinas</a>
                            <ul>
                                                                       
                                <li>
                                    <a href="<?php echo cms_url('routiness') ?>">Rutinas</a>
                                </li>
                                <li>
                                    <a href="<?php echo cms_url('stagess') ?>">Etapas</a>
                                </li>
                                <li>
                                    <a href="<?php echo cms_url('weekss') ?>">Semanas</a>
                                </li>
                                <li>
                                    <a href="<?php echo cms_url('phasess') ?>">Fases</a>
                                </li>
                                <li>
                                    <a href="<?php echo cms_url('workoutss') ?>">sesiones</a>
                                </li>
                           </ul>
                       </li> 

                        <!-- ========================================================== -->

                       <li>
                            <a href="javascript:void(0)">Listas</a>

                            <ul>
                                <li>
                                    <a href="javascript:void(0)">Registro de usuario</a>
                                    <ul>
                                        <li>
                                            <a href="<?php echo cms_url('activity_levels') ?>">Niveles de actividad física</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo cms_url('hobbiess') ?>">Hobbies</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo cms_url('specialtiess') ?>">Áreas de especialidad</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo cms_url('epss') ?>">EPS</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo cms_url('opening_hourss') ?>">Horarios de atención</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo cms_url('citiess') ?>">Ciudades</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo cms_url('prepaid_healths') ?>">Medicina prepagada</a>
                                        </li>
                                    </ul>
                                </li>



                                <li>
                                    <a href="javascript:void(0)">Formula médica</a>
                                    <ul>
                                        <li>
                                            <a href="<?php echo cms_url('unity_types') ?>">Tipo de unidad</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo cms_url('no_unitss') ?>">Número de unidades</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo cms_url('periodicitys') ?>">Periodicidad de la fórmula</a>
                                        </li>
                                    </ul>
                                </li>
                           </ul>
                       </li> 


                       <!-- ========================================================== -->
                       <!-- ========================================================== -->
                       <li>
                            <a href="<?php echo cms_url('coachess') ?>">Entrenadores</a>
                       </li>
                       <!-- ========================================================== -->



                    <?php if (true === $is_superadmin) : ?>
                        <li>
                            <a href="javascript:void(0)">Configuraciones</a>
                            <ul>
                                <li>
                                    <a id="change_pass" href="javascript:void(0);">Cambiar Contraseña</a>
                                </li>
                                <li>
                                    <a href="<?php echo cms_url('admin/administradores') ?>">Administradores</a>
                                </li>
                                <li>
                                    <a href="<?php echo cms_url('userss') ?>">Usuarios</a>
                                </li>
                                
                                <?php if (has_perm('cms_admin_perms.view')) { ?>
                                    <li>
                                        <a href="<?php echo cms_url('perms') ?>">Permisos</a>
                                    </li>
                                <?php } ?>
                                <?php if (has_perm('cms_admin_perms.view')) { ?>
                                <li>
                                  <a href="javascript:void(0)">Desarrollador</a>
                                <ul>
                                    <li>
                                        <a href="<?php echo cms_url('generator_models') ?>">Generador de Modelos</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo cms_url('generator_modules') ?>">Generador de Modulos</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo cms_url('backup_db') ?>">Generador de Backup</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo cms_url('generator_front_modules') ?>">Generador de Front END</a>
                                    </li>
                                    
                                </ul> 
                                </li>  
                                <?php } ?>    
                                   
                                <?php if (has_perm('cms_config_oauth.view')) { ?>
                                    <li>
                                        <a href="<?php echo cms_url('users/config_oauth') ?>">Config OAuth</a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</div>