<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.co
     * @company Imaginamos S.A.S | Todos los derechos reservados
     * @date Tue, 25 Feb 2014 16:15:13
     */

                        

class _workoutss extends Back_Controller {
	protected $admin_area = true;
	public $model = 'workouts';
	public $name = 'workoutss';
	public $title = 'Workoutss';


	public function __construct() {
		parent::__construct();
		$this->menu();
		$this->migas($this->current_menu);
		$this->add_modular_assets('js', 'autoload');
	}

	public function menu() {
		return $this->current_menu['Workoutss'] = cms_url('workoutss');
	}

	public function index() {
		$data['pag'] = $this->session->userdata('page_'.  $this->name);
		$this->session->set_userdata('page_'.$this->name, '1');
		$data['title_page'] = $this->title;
		$data['pag'] = 1;
		$data['migas'] = $this->miga;
		$data['pag_content'] = $this->contents();
		$data['pag_content_title'] = ucwords ($this->title);
		return $this->build('index', $data);
	}

	public function contents() {
		$data['path_delete'] = cms_url($this->name.'/delete');
		$data['path_add'] = cms_url($this->name.'/form');
		$data['path_edit'] = cms_url($this->name.'/form');
		$data['path_list'] = cms_url($this->name.'/lista');
		$data['mpag_content'] = $this->lista();
		$data['pag'] = 1;
		$this->session->set_userdata('page_'.$this->name, '1');
		return $this->buildajax('control', $data);
	}

	public function form() {

		$obj = new $this->model();
		$objImagen = new workouts_imagen();
		$imagen = new imagen();
		$id = $this->_post('id');
		$html = "";

		$row = $objImagen->where("workouts_id", $id)->get();

		foreach($row as $value){

			$imagen_id = $value->imagen_id;
			echo $imagen_id;
			$rowImg = $imagen->query("SELECT * FROM cms_imagen WHERE id = $imagen_id");
			$html .= "<tr>"; 
			foreach ($rowImg as $value2) {

					//echo $value2->path."<br>";
					$path = dirname(cms_url())."/".substr($value2->path, 2);
					$html .= "<td><img src='{$path}' width='150px'></td>";

			}
		}
		

		if(!empty($id))
		  $obj->join_related('weeks')->get_by_id($id);
		$data['form_content'] = "";
		$data['form_content'] .= $this->inputHidden($obj->id, "id");
		$data['form_content'] .= $this->input($obj->name, "name",$obj->get_rule("name","max_length") ,"Título", "Maximo ".$obj->get_rule("name","max_length")." caracteres", $obj->is_rule("name","required"), "span3");
		$data['form_content'] .= $this->input($obj->description, "description",$obj->get_rule("description","max_length") ,"Descripción", "Maximo ".$obj->get_rule("description","max_length")." caracteres", $obj->is_rule("description","required"), "span3");
		$data['form_content'] .= $this->input($obj->status, "status",$obj->get_rule("status","max_length") ,"Imagen", "Maximo ".$obj->get_rule("status","max_length")." caracteres", $obj->is_rule("status","required"), "span3");
		$var_weeks_id = new workouts();
		$data['form_content'] .= $this->combox($obj->weeks_id,$var_weeks_id->get_weeks_list(),"weeks_id", "Semana", "", $obj->is_rule("weeks_id","required"), "span3");
		$data['form_content'] .= $this->imagen($objImagen->imagen_id, $objImagen->imagen_path, "Imagen", "", false, $objImagen->is_rule("imagen_id","required"), "span3");
		$data["form_content"] .= "<table class='table pull-rigth'><caption>Imagenes subidad para esta session</caption>".$html."</tr></table>";

		$data['direct_form'] = $this->name.'/add';
		return $this->buildajax('control/form', $data);
	}

	public function lista() {
		$obj = new $this->model();
		$data['datos'] = $obj->join_related('weeks')->get();
		$data['direct_form'] = $this->name.'/delete';
		return $this->buildajax('control/lista', $data);
	}

	public function add() {

		$imagen = new imagen();
		$saveImagen = new imagen();
		$workout_imagen = new workouts_imagen();
		$obj = new $this->model();

		$error = false;
		$msg = "";
		
		$obj->get_by_id($this->_post('id'));
		if(!$obj->exists())
		$obj->id = "";
		$this->loadVar($obj);

		if (!$obj->save()) {
			$error = true;
			$msg = $obj->error->string;
			//$this->deleteImg($this->data_id_obj_path($obj));
		}

		$uploadedfileload = "true";
		$uploadedfile_size = $_FILES['imagen_path']['size'];

		$file_name = $_FILES['imagen_path']['name'];
		$add = "./uploads/workout_imagen_{$this->_post('id')}_$file_name";

		/**
		 * Se trae de manera terible el lastInsertId();
		 */
		$objImagen = $imagen->query("SELECT max(id) as 'id' FROM cms_imagen");
		$lastInsertId = "";

		foreach($objImagen as $value)
			$lastInsertId = $value->id;


		$saveImagen->path = (string)$add;
		$saveImagen->name = "";
		if($saveImagen->save()){
			move_uploaded_file($_FILES['imagen_path']['tmp_name'], $add);
		}

		#Validación de datos con imagenes repetidas
		$roWorkOutImagen = $workout_imagen
		->where("imagen_id", $lastInsertId)
		->get();

		
		$workout_imagen->imagen_id = $lastInsertId;
		$workout_imagen->workouts_id = $this->_post('id');
		$workout_imagen->save();
		

		if ($error)
			$this->set_alert_session("Error guardando datos," . $msg, 'error');
		else
			$this->set_alert_session("Datos Guardados con exito...!", 'info');
		redirect(cms_url($this->name));
	}

	public function delete() {
		$error = false;
		$obj = new $this->model();
		$obj->get_by_id($this->_post('value'));
		$msg = "";
		if ($obj->exists()) {
			$id_file = $this->data_id_obj_path($obj);
			if (!$obj->delete()){
				$error = true;
				$msg = $obj->error->string;
			}
			$this->delete_files($this->data_file_path($obj));
			$this->deleteImg($id_file);
		} else {
			$error = true;
			$msg = "No existe item...!";
		}
		if ($error)
			$this->set_alert('Error al eliminar datos' . ', ' . $msg, 'error');
		else{
			$this->set_alert_session("Datos eliminados con éxito...!", 'info');
		}
		return $this->render_json(!$error);
	}

}
?>