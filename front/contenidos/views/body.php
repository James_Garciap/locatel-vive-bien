
<?php //include("front/header/views/body.php"); ?>
<?php //include("front/menu/views/body.php"); ?>
<?php include("front/headerint/views/body.php"); ?>
<?php include("front/home/views/partials/modales_registro.php"); ?>
<?php include("front/modales/views/body.php"); ?>


<div class="dieta_izq">
  <?php include("front/mi_dieta/views/body.php"); ?>
</div>

<div class="dieta_der">
  <?php include("front/mi_dieta2/views/body.php"); ?>
</div>

<div class="rutina_izq">
  <?php include("front/mi_rutina/views/body.php"); ?>
</div>

<div class="rutina_der">
  <?php include("front/mi_rutina2/views/body.php"); ?>
</div>



<!-- contenido -->

  <div  id="container">

      <!-- left column is the master -->
      <div id="leftColumn">

      <div class="cont1 content_panel scrollable cont_contenidos" id="cont1">
        <?php include("front/perfil_medico/views/body.php"); ?>
      </div>

      <div class="cont2 content_panel scrollable cont_contenidos" id="cont2">
        <?php include("front/directorio_medico/views/body.php"); ?>
      </div>

      <div class="cont3 content_panel scrollable cont_contenidos" id="cont3">
        <?php include("front/mi_plan/views/body.php"); ?>
      </div>

      <div class="cont1 content_panel scrollable cont_contenidos" id="cont4">
        <?php include("front/tips_recetas/views/body.php"); ?>
      </div>

      <div class="cont2 content_panel scrollable cont_contenidos" id="cont5">
        <?php include("front/noticias/views/body.php"); ?>
      </div>

      <div class="cont3 content_panel scrollable cont_contenidos" id="cont6">
        <?php include("front/formula_medica/views/body.php"); ?>
      </div>

      <div class="cont1 content_panel scrollable cont_contenidos" id="cont7">
        <?php include("front/eventos/views/body.php"); ?>
      </div>
      <?php
      if($is_doc)
      {
      ?>
      <div class="cont2 content_panel scrollable cont_contenidos" id="cont8">
        <?php include("front/ingreso_articulos/views/body.php"); ?>
      </div>
      <?php
      }
      ?>
      <div class="cont3 content_panel scrollable cont_contenidos" id="cont9">
        <?php include("front/ciudad_locatel/views/body.php"); ?>
      </div>

      </div>

      <div id="rightColumn">
        
      <div class="cont3 content_panel scrollable cont_contenidos" id="cont9-1">
        <?php include("front/ciudad_locatel2/views/body.php"); ?>
      </div>
      <?php
      if($is_doc)
      {
      ?>
      <div class="cont2 content_panel scrollable cont_contenidos" id="cont8-1">
        <?php include("front/ingreso_articulos2/views/body.php"); ?>
      </div>
      <?php
      }
      ?>
      <div class="cont1 content_panel scrollable cont_contenidos" id="cont7-1">
        <?php include("front/eventos2/views/body.php"); ?>
      </div>

      <div class="cont3 content_panel scrollable cont_contenidos" id="cont5-1">
        <?php include("front/formula_medica2/views/body.php"); ?>
      </div>

      <div class="cont2 content_panel scrollable cont_contenidos" id="cont5-1">
        <?php include("front/noticias2/views/body.php"); ?>
      </div>

      <div class="cont1 content_panel scrollable cont_contenidos" id="cont4-1">
        <?php include("front/tips_recetas2/views/body.php"); ?>
      </div>

      <div class="cont3 content_panel scrollable cont_contenidos" id="cont3-1">
        <?php include("front/mi_plan2/views/body.php"); ?>
      </div>

      <div class="cont2 content_panel scrollable cont_contenidos" id="cont2-1"> 
        <?php include("front/directorio_medico2/views/body.php"); ?>
      </div>

      <div class="cont1 content_panel scrollable cont_contenidos" id="cont1-1">
        <?php include("front/perfil_medico2/views/body.php"); ?>
      </div>


      </div>

  </div>

  <?php //include("front/footer/views/body.php"); ?>
  <?php //include("front/footer2/views/body.php"); ?>


