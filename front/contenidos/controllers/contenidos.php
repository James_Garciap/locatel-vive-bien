<?php

/**
 * @author Elbert Tous
 * @email elbert.tous@imaginamos.com
 * @copyright Imaginamos S.A.S | Todos los derechos reservados
 * @version 1-050004 - elbert.tous@imaginamos.com
 */
class Contenidos extends Front_Controller {

	public function __construct() {
		parent::__construct();
	}
	
	public function index() {
		
		if(!$this->ion_auth->logged_in())
		{
			header("location: ".base_url());
		}
		// ===============================   LISTAS  =========================================
		$specialties = new specialties();
		$this->_data['specialties_options'] = $specialties->get();
		$eps = new eps();
		$this->_data['eps_options'] = $eps->get();
		$cities = new cities();
		$this->_data['cities_id_options'] = $cities->get();
		$this->_data['reported_activity_options'] = $this->reported_activity_weeks();
		// =============================== / LISTAS  =========================================

		/**
		 *
		 * Lista desplegables de Modulo de formula medica para combo tipo unidad,
		 * Se hace consulta al modelo y se asigna a la vista.
		 *
		 * @author Julian Molina - julian.molina@imagina.co
		 * @version 1.0, 12 feb 2014
		 * @copyright Imaginamos S,A
		*/
		$tipo_unidad = new unity_type();
		$this->_data['formula_medica_tipo_unidad'] = $tipo_unidad->get();

		/**
		 *
		 * Lista desplegables de Modulo de formula medica para combo periodicidad,
		 * Se hace consulta al modelo y se asigna a la vista.
		 *
		 * @author Julian Molina - julian.molina@imagina.co
		 * @version 1.0 - 12 feb 2014
		 * @copyright Imaginamos S,A
		*/
		$periodicidad= new periodicity();
		$this->_data['formula_medica_periodicidad'] = $periodicidad->get();

		/**
		 *
		 * Lista desplegables de Modulo de reportar mis rutinas, combo de minutos
		 *
		 * @author Julian Molina - julian.molina@imagina.co
		 * @version 1.0 - 17 feb 2014
		 * @copyright Imaginamos S,A
		*/
		$minutos = new activity_minutes();
		$this->_data['rutina_minutos'] = $minutos->get();

		/**
		 *
		 * Lista desplegables de Modulo de reportar mis rutinas, combo de ejercicios
		 *
		 * @author Julian Molina - julian.molina@imagina.co
		 * @version 1.0 - 17 feb 2014
		 * @copyright Imaginamos S,A
		*/
		$routines = new workouts();
		$this->_data['rutina_ejercicios'] = $routines->get();

		/**
		 *
		 * Lista desplegables de Modulo de articulos
		 *
		 * @author Julian Molina - julian.molina@imagina.co
		 * @version 1.0 - 17 feb 2014
		 * @copyright Imaginamos S,A
		*/
		$post = new posts();
		$this->_data['articulos_perfil'] = $post->get();

		// =============================== DIRECTORIO MÃ‰DICO =========================================
		$medical_directory_data = $this->medical_directory(false, '0, 4');
		$this->_data['doctor_directory_users'] = $medical_directory_data['users'];
		$this->_data['navigation'] = $medical_directory_data['navigation'];
		
		/**
		 *
		 * Lista desplegables de Modulo de articulos
		 *
		 * @author Julian Molina - julian.molina@imagina.co
		 * @version 1.0 - 25 feb 2014
		 * @copyright Imaginamos S,A
		*/
		$prepaid_health = new prepaid_health();
		$this->_data['prepaid_health'] = $prepaid_health->get();

		/**
		 *
		 * Lista desplegables de Modulo de Rutinas
		 *
		 * @author Julian Molina - julian.molina@imagina.co
		 * @version 1.0 - 25 feb 2014
		 * @copyright Imaginamos S,A
		*/
		$routines = new users_routines();
		$workouts_imagen = new workouts_imagen();

		$id_usuario = $this->session->userdata("user_id");

		$workouts = $routines->query("SELECT wo.id FROM 
									  	cms_users_routines u
									  INNER JOIN
										cms_stages s 
									  ON s.routines_id = u.routines_id
									  INNER JOIN cms_weeks w
									  ON w.stages_id = s.id
									  INNER JOIN cms_workouts wo
									  ON wo.weeks_id = w.id
									  WHERE u.users_id = $id_usuario		
									 ");

		
		
		$this->_data['workouts'] = $workouts;
		$this->_data['workouts_imagen_array'] = $imagen_session;
		
		// =============================== /DIRECTORIO MÃ‰DICO =========================================

		// ===============================   MI DIETA  =========================================
		$user = $this->ion_auth->user()->row();
		$users_diets = new users_diets();
		$users_diets->where('users_id', $user->id)->get();
		$diets = new diets();
		$diets->where('id', $users_diets->diets_id)->get();
		$meals = new meals();
		$meals->where('diets_id', $diets->id);
		$meals->order_by("id", 'asc');
		//echo "<pre>";print_r($meals->diets_id);die("</pre>");
		$this->_data['mi_dieta_meals'] = $meals->get();
		$this->_data['mi_dieta_description'] = $diets->description;
		$this->_data['mi_dieta_tip'] = $this->get_tip(false);
		// =============================== / MI DIETA  =========================================

		//====================== Mi pefil ======================
		$users = new users();
		$users->where("id", $user->id)->get();
		$image = new imagen();
		$image->where("id", $users->imagen_id)->get();
		$users_doc_profile = new users_doc_profile();
		$users_doc_profile->where("users_id", $user->id)->get();
		if($users_doc_profile->id)
		{
			$this->_data['is_doc'] = true;
		}
		else
		{
			$this->_data['is_doc'] = false;
		}
		//---------------
		$this->_data['user_profile_name'] = $users->first_name." ".$users->last_name;
		$date = $users->birthday;
		$days = explode("-", $date);
		/**
		 * **************************************************************************************
		 * ************************* WARNING ****************************************************
		 * **************************************************************************************
		 * REVISAR ESTA FUNCION MKTIME PORQUE ESTA PRODUCIENDO UN ERROR, COLOCO LA ARROBA PARA QUE NO HAYA ERROR LA VISIBILIDAD.
		*/
		$days = @mktime(0,0,0,$days[1],$days[2],$days[0]);
		$age = (int)((time()-$days)/31556926 );
		if($users->sex=="m")
		{
			$sex = "Masculino";
		}
		else
		{
			$sex = "Femenino";
		}
		//------- rutina
		$users_routines = new users_routines();
		$users_routines->select('routines_id')->where('users_id', $users->id)->limit(1)->order_by('id desc')->get();
		$routines = new routines();
		$routines->select('description')->where('id', $users_routines->routines_id)->get();
		$this->_data['user_profile_routine_description']= $routines->description;
		//------- articulos
		$posts = new posts();
		$posts->where("users_id", $users->id)->limit(5)->order_by('id desc')->get();
		$posts_result_count = $posts->result_count();
		if($posts_result_count==0)
		{
			$html_posts = "No has ingresado ning&uacute;n art&iacute;culo a&uacute;n...<a href='#cont8'>Ingresa uno</a>";
		}
		else
		{
			$html_posts = "";
			foreach ($post as $key)
			{
				$html_posts .="<li>".$key->name."<li>";
			}
		}
		$this->_data['user_profile_posts'] = $html_posts;
		$this->_data['user_profile_age'] = $age;
		$this->_data['user_profile_sex'] = $sex;
		$this->_data['user_profile_created_on'] = date("Y-m-d", $users->created_on);
		$this->_data['user_profile_height'] = $users->height;
		$this->_data['user_profile_weight'] = $users->weight;
		//--------- Actividad reciente
		$stream = new users_activity();
		$stream->where('users_id', $users->id)->limit(3)->order_by("id desc")->get();
		$stream_result_count = $stream->result_count();
		if($stream_result_count==0)
		{
			$html_stream = "No ha habido actividad reciente...";
		}
		else
		{
			$html_stream = "";
			foreach ($stream as $key)
			{
				$html_stream .="<li>".$key->description."<li>";
			}
		}
		$this->_data['user_profile_stream'] = $html_stream;
		//--------- Logros
		if($users->pre_weight)
		{
			$this->_data['stream_weight_change'] = true ;
			$this->_data['stream_pre_weight'] = $users->pre_weight;
			$this->_data['stream_weight'] = $users->weight ;
		}
		else
		{
			$this->_data['stream_weight_change'] = false ;
		}
		$this->_data["user_first_name"] = $users->first_name;

		//------- Entrenadores
		$html_coaches="<ul class='lista'>";
		$coaches = new coaches();
		$coaches->limit(10)->order_by('rand()')->get();
		if($coaches->result_count() == 0)
		{
			$html_coaches ="<br>No hay esntrenadores disponibles actualmente";
		}
		else
		{
			foreach ($coaches as $key)
			{
				$imagen = new imagen();
				$imagen->where('id', $key->imagen_id)->get();
				//die("path = ".substr($imagen->path, 2));
				$html_coaches .= "<span><li><img src='".base_url().substr($imagen->path, 2)."' width='90' height='90'>".$key->name." ( ".$key->email." )</li></span>";
			}
			$html_coaches .="</ul>";
		}
		$this->_data["modals_coaches"] = $html_coaches;
		//-------------------

		$this->_data['profile_status_weight'] = $this->profile_status_weight($users);
		$this->_data["profile_picture_name"] = $image->name;
		$this->_data["comments_notification_count"] = " ";
		//====================== Mi pefil ======================

		//===============  MIS EVENTOS ====================
		$calendar = new calendar();
		$calendar->order_by("rand()");
		$calendar->limit(4);
		$this->_data['my_calendar_events'] = $calendar->get();
		//===============  MIS EVENTOS ====================

		//===============  RECETAS ====================
		$recipes = new recipes();
		$recipes->order_by("rand()");
		$recipes->limit(4);
		$this->_data['recipes'] = $recipes->get();
		//===============  RECETAS ====================

		return $this->build();
	}

	/**
	 *
	 * Paginador para directorio medico, se hace regla de 3 para dividir entre 4
	 *
	 * @author Julian Molina - julian.molina@imagina.co
	 * @version 1.0 - 22 feb 2014
	 * @copyright Imaginamos S,A
	*/
	public function paginador_numero_items(){

		#Asignacion de variables
		$paginador = array();
		$usersJoin = array();
		$contador = 1;
		$ini = 0;
		$fin = 0;
		$ids = "";
		$counts = "";
		$innerJoin = "";
		$where = "";

		#Instancias
		$users = new Users();

		if(!empty($_POST["eps"])){
			$innerJoin .= " INNER JOIN cms_users_eps ue ON ue.users_id = u.id ";
			$where .= " and ue.eps_id = {$_POST["eps"]} ";
		}

		if (!empty($_POST["prepaid"])) {
			$innerJoin .= " INNER JOIN cms_users_prepaid_health uph ON uph.users_id = u.id ";
			$where .= " and uph.prepaid_health_id = {$_POST['prepaid']} "; 
		}
			
		if (!empty($_POST["specialties"])){
			$innerJoin .= " INNER JOIN cms_users_specialties us ON us.users_id = u.id ";
			$where .= " and us.specialties_id = {$_POST['specialties']} "; 
		}

		if  (!empty($_POST["cities"])){
			$where .= " and u.cities_id = {$_POST['cities']} "; 
		}

		$sql = "SELECT 
					  count(*) as 'count'
				FROM 
					cms_users u 
				LEFT JOIN 
					cms_imagen i
				ON
					u.imagen_id = i.id 
				INNER JOIN
					cms_users_doc_profile up
				ON
					up.users_id = u.id 

				$innerJoin

				where 
					status = 1 
					$where";

		$counts = $users->query($sql)->count;

		$sql2 = "SELECT 
					  u.id as 'id'
					FROM 
						cms_users u 
					LEFT JOIN 
						cms_imagen i
					ON
						u.imagen_id = i.id 
					INNER JOIN
						cms_users_doc_profile up
					ON
						up.users_id = u.id 

					$innerJoin
					where 
						status = 1 
						$where";

		$obj = $users->query($sql2);	
		
		foreach ($obj as $value) 
			$usersMerge[] = $value->id;

		do{

			$fin += 4;
			$ini = $fin - 4;
			$coma = ",";

			for($j = $ini; $j < $fin && $ini <= $counts; $j++){
				if(isset($usersMerge[$j])){
					if(isset($usersMerge[$j])){
						if($j+1 >= $fin){						
							$coma = "";
						}
						if(!isset($usersMerge[$j+1]))
							$coma = "";

						$ids .= $usersMerge[$j].$coma;
					}
				}
			}
			
			$paginador[$contador]["ids"] = $ids;
			$contador++;
			$ids = "";

		}while($fin < $counts);

		echo json_encode($paginador);

	}

	/**
	 * Metodo de insercion de medicamentos, se reciben datos de formulario por metodo post
	 * y se encapsulan en el objeto medical_formulate, se inserta con el metodo del ORM save.
	 *
	 * @author Julian Molina - julian.molina@imagina.co
	 * @version 1.0 - 22 feb 2014
	 * @copyright Imaginamos S,A
	 * @return type ArrayJson
	 */
	public function consultarMedicos(){

		$users = new users();

		if(!empty($_POST["ids"])){
			$ids = $_POST["ids"];
			$in = "  and u.id IN($ids)"; 
		}else{
			$in = "limit 0,4";
		}
		$usersFinal = array();

		$sql = "SELECT 
					   up.id,
					   u.first_name,
					   up.profession, 
					   u.last_name, 
					   i.path, 
					   i.name 
				FROM 
					cms_users u 
				left join 
					cms_imagen i
				on 
					u.imagen_id = i.id 
				inner join
					cms_users_doc_profile up
				on 
					up.users_id = u.id
				WHERE 
					u.status = 1 
					$in	
				";

		//echo $sql;

		$usersObject = $users->query($sql);

		$i = 0;
		foreach($usersObject as $value){

			$usersFinal[$i]["id_medico"] = $value->id;
			$usersFinal[$i]["name"] = $value->first_name." ".$value->last_name;
			$usersFinal[$i]["path"] = $value->path;
			$usersFinal[$i]["name_path"] = $value->name;
			$usersFinal[$i]["url"] = $_SERVER["DOCUMENT_ROOT"];
			$usersFinal[$i]["prof"] = $value->profession;

			$i++;
		}

		echo json_encode($usersFinal);
	}

	/**
	 * Metodo de insercion de medicamentos, se reciben datos de formulario por metodo post
	 * y se encapsulan en el objeto medical_formulate, se inserta con el metodo del ORM save.
	 *
	 * @author Julian Molina - julian.molina@imagina.co
	 * @version 1.0 - 22 feb 2014
	 * @copyright Imaginamos S,A
	 * @return type ArrayJson
	 */
	public function consultarMedicosDialog(){

		#Instancias
		$users = new users();

		#Asignación de variables.
		$id_medico = $_POST["medicos"];
		$objUsers = array();
		$arrayJson = array();
		$sql = "SELECT 
					   up.id as 'id_medico',
					   u.phone,
					   u.email,
					   u.first_name,
					   up.profession, 
					   up.specialty,
					   up.profesional_register,
					   up.graduation_year,
					   up.medical_register,
					   up.school,
					   u.last_name, 
					   i.path, 
					   i.name 
				FROM 
					cms_users u 
				left join 
					cms_imagen i
				on 
					u.imagen_id = i.id 
				inner join
					cms_users_doc_profile up
				on 
					up.users_id = u.id
				WHERE 
					u.status = 1
					and up.id = $id_medico				
		";

		#Proceso
		$objUsers = $users->query($sql);
		$i = 0;
		foreach($objUsers as $value){

			$arrayJson[$i]["id_medico"] = $value->id_medico;
			$arrayJson[$i]["phone"] = $value->phone;
			$arrayJson[$i]["email"] = $value->email;
			$arrayJson[$i]["profession"] = $value->profession;
			$arrayJson[$i]["speciality"] = is_null($value->speciality)? "" : $value->speciality;
			$arrayJson[$i]["professional"] = $value->professional_reg;
			$arrayJson[$i]["graduation_year"] = $value->graduation_year;
			$arrayJson[$i]["medical_register"] = $value->medical_register;
			$arrayJson[$i]["name"] = $value->first_name." ".$value->last_name;
			$arrayJson[$i]["path"] = $value->path;
			$arrayJson[$i]["name_path"] = $value->name;
			$arrayJson[$i]["url"] = $_SERVER["DOCUMENT_ROOT"];
			$arrayJson[$i]["prof"] = $value->profession;

			$i++;
		}

		echo json_encode($arrayJson);
	}

	/**
	 * Envía emails del usuario al médico seleccionado
	 *
	 * @author Julian Molina - julian.molina@imagina.co
	 * @version 1.0 - 23 feb 2014
	 * @copyright Imaginamos S,A
	 * @return type ArrayJson
	 */
	public function enviarEmailMedico(){

		$objPost = (object)$_POST;

		/**
		 * Se hace el cambio en la librería AUTH para que envíe full_name por medio de la sessio, el archivo está localizado en:
		 * path: app/models/ion_auth_model.php Línea -> 757 método -> login($param, $param)
		 *  
		 * Se encuentra esta variable de session con las siguientes asignaciones y se añade la siguiente:
		 * -> 'full_name' => $user->first_name." ".$user->last_name
		 *
		 * 'identity' => $user->{$this->identity_column},
         * 'username' => $user->username,
         * 'email' => $user->email,
         * 'user_id' => $user->id, //everyone likes to overwrite id so we'll use user_id
         * 'old_last_login' => $user->last_login
		 */

		#Datos email
		$objPost->email = trim($objPost->email);
		$objPost->subject = "Contáctos locatel ";
		$objPost->from = "cms@imaginamos.com";
		$objPost->from_subject = 'Contáctos Locatel';

		#Datos Pagina
		$objPost->title_page = "Mensaje Locatel";
		$objPost->title_message = "Estimado ".$objPost->nombre;
		$objPost->usuario = $objPost->nombre;
		$objPost->message = "El usuario: {$this->session->userdata("full_name")} con el Email: {$this->session->userdata("email")}
							 ha querido contactar contigo y ha enviado el siguiente 
							 mensaje: 
							 <br><i>{$objPost->mensaje}</i><br>";
		$objPost->footer = "Cordialmente Locatel.sa";
		$objPost->image = cdn_imaginamos('images/bg/bg_mail_new_user.jpg');
		$objPost->html = $this->htmlContactoMedicos($objPost);;

		echo $this->sendEmail($objPost);
	}

	/**
	 * Metodo de insercion de medicamentos, se reciben datos de formulario por metodo post
	 * y se encapsulan en el objeto medical_formulate, se inserta con el metodo del ORM save.
	 *
	 * @author Julian Molina - julian.molina@imagina.co
	 * @version 1.0 - 23 feb 2014
	 * @copyright Imaginamos S,A
	 * @return type ArrayJson
	 */
	private function sendEmail($datos) {

		#Array 
        $this->load->library('email');
        //Sirve pero ahorita no sé como colocarle parámetros.
        //$html = $this->load->view('contactoMedicoEmail', 'cms@imaginamos.com', TRUE);

        $config = array (
                  'mailtype' => 'html',
                  'charset'  => 'utf-8',
                  'priority' => '1'
        );

        $this->email->initialize($config);
        $this->email->from($datos->from, $datos->from_subject);
        $this->email->to($datos->email);
        $this->email->subject($datos->subject." ". SITENAME);
        $this->email->message($datos->html);

        return $this->email->send();
    }

    /**
	 *
	 *
	 */
	public function htmlContactoMedicos($datos){

		return "<html>
					    <head>
					        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
					        <title><strong>{$datos->title_page}</strong></title>
					    </head>
					    <body>
					        <table width='632' border='0' align='center' background='{$datos->image}'>
					            <tr>
					                <td height='522'>
					                    <table width='55%' border='0' align='center'>
					                        <tr>
					                            <td>
					                                <p>
					                                	{$datos->title_message}
					                                	<br><br>
					                                    {$datos->message}				                            		
					                                    <br><br>
					                                    {$datos->footer}
					                                    <br><br>
					                                </p>
					                            </td>
					                        </tr>
					                    </table>
					                </td>
					            </tr>
					        </table>
					    </body>
					</html>";
	}

	/**
	 * Metodo de insercion de medicamentos, se reciben datos de formulario por metodo post
	 * y se encapsulan en el objeto medical_formulate, se inserta con el metodo del ORM save.
	 *
	 * @author Julian Molina - julian.molina@imagina.co
	 * @version 1.0 - 12 feb 2014
	 * @copyright Imaginamos S,A
	 * @return type ArrayJson
	 */
	public function registro_formula_medica(){

		$users = $this->ion_auth->user()->row();
		$medical_formulate = new medical_formulate();
		$medical_formulate->users_id = $users->id;
		$medical_formulate->medicine = $_POST["fm_n_m"];
		$medical_formulate->description = "";
		$medical_formulate->mail_notificaction = null;
		$medical_formulate->unity_type_id = $_POST["fm_t_u"];
		$medical_formulate->no_units_id = $_POST["fm_n_u"];
		$medical_formulate->periodicity_id = $_POST["fm_p"];

		if($medical_formulate->save())
			echo json_encode(array("respuesta" => "Insercion correcta"));
		else
			echo json_encode(array("respuesta" => "Fallo al insertar datos"));
	}

	/**
	 * Busca los medicamentos insertados, busca en las tablas relacionadas tales como unity_type,
	 * no_units y periodicity para retornar un arreglo con toda la informacion.
	 *
	 * @author Julian Molina - julian.molina@imagina.co
	 * @version 1.0 - 12 feb 2014
	 * @copyright Imaginamos S,A
	 * @return type ArrayJson
	 */
	public function consulta_formula_medica(){

		$medical_formulate = new medical_formulate();
		$unity_type = new unity_type();
		$no_units = new no_units();
		$periodicity = new periodicity();

		$user = $this->ion_auth->user()->row();

		if($user->id){

			$row = $medical_formulate->where("users_id", $user->id)->get();

			$i = 0;
			$array = array();

			foreach ($row as $value){
				$ru = $unity_type->where("id", $value->unity_type_id)->get();
				$rno = $no_units->where("id", $value->no_units_id)->get();
				$rp = $periodicity->where("id", $value->periodicity_id)->get();
					
				$array[$i]["id"] = $value->id;
				$array[$i]["m"] = $value->medicine;
				$array[$i]["u"] = $ru->name;
				$array[$i]["n"] = $rno->name;
				$array[$i]["p"] = $rp->name;
				$i++;
			}
			echo json_encode($array);
		}

	}

	/**
	 * Metodo para eliminar medicamentos insertados
	 *
	 * @author Julian Molina - julian.molina@imagina.co
	 * @version 1.0 - 12 feb 2014
	 * @copyright Imaginamos S,A
	 * @return type ArrayJson
	 */
	public function eliminar_formula_medica(){
		$medical_formulate = new medical_formulate();
		echo $medical_formulate->del_by_id($_POST["id"]);
	}
	
	/**
	 * 
	 * @param unknown $filters
	 * @param unknown $limit
	 * @return string
	 */
	public function medical_directory($filters, $limit)
	{
		$c = 0;
		$flag=0;
		$users = array();
		$rows = $this->medical_directory_rows($filters, $limit);
		
		foreach ($rows->result() as $users_doc_profile)
		{
			$this->load->model('users');
			if($filters['cities_id'])
			{
				$this->users->where('cities_id', $filters['cities_id']);
			}
			$this->users->where('id', $users_doc_profile->users_id)->get();
			foreach ($this->users as $user)
			{
				// --------------   EPS QUE ATIENDE EL MEDICO ---------------------
				$users_eps = new users_eps();
				$users_eps->select('eps_id');
				//------- Filtros
				if($filters['eps_options'])
				{
					$users_eps->where('eps_id', $filters['eps_options']);
				}
				$users_eps->where('users_id', $user->id)->get();
				$count = $users_eps->result_count();
				//------ Marcando al usuario si no tiene eps relacionadas al registro
				if($count==0)
				{
					$flag = 1;
				}
				else
				{
					$c2 = 0;
					foreach ($users_eps as $key)
					{
						$eps = new eps();
						$eps->select('name');
						$eps->where('id', $key->eps_id)->get();
						$users[$c]['eps'][$c2]=$eps->name;
						$c2++;
					}
				}
					
				// -------------- / EPS QUE ATIENDE EL MÃ‰DICO ---------------------

				// --------------   ESPECIALIDADES DEL MÃ‰DICO   ---------------------
				$users_specialties = new users_specialties();
				$users_specialties->select('specialties_id');
				if($filters['specialties_options'])
				{
					$users_specialties->where('specialties_id', $filters['specialties_options']);
				}
				$users_specialties->where('users_id', $user->id)->get();
				$count = $users_specialties->result_count();
				//------ Marcando al usuario si no tiene eps relacionadas al registro
				if($count==0)
				{
					$flag = 1;
				}
				else
				{
					$c2 = 0;
					foreach ($users_specialties as $key)
					{
						$specialties = new specialties();
						$specialties->select('name');
						$specialties->where('id', $key->specialties_id)->get();
						$users[$c]['specialties'][$c2]=$specialties->name;
						$c2++;
					}
				}
				// -------------- / ESPECIALIDADES DEL MÃ‰DICO   ---------------------
				if($flag == 0)
				{
					$users[$c]['full_name'] = $user->first_name." ".$user->last_name;
					$users[$c]['profession'] = $users_doc_profile->profession;
					$users[$c]['phone'] = $user->phone;
					$users[$c]['address'] = $users_doc_profile->address;
					$users[$c]['specialty'] = $users_doc_profile->specialty;
					//.......... Profile picture
					$image = new imagen();
					$image->where('id', $user->imagen_id)->get();
					$users[$c]['profile_picture_name'] = $image->name;
					//die("kkkkkkkkkkkkkk");
					$c++;
				}
			}
		}
		// ----------------   PAGINACIÃ“N  --------------------
		$rows = $this->medical_directory_rows($filters, false);
		$num_rows = $rows->num_rows();
		$navigation_pages = ceil($num_rows/4);
		$c=2;
		$navigation = " ";
		$cx4 = 0;
		while ($c <= $navigation_pages)
		{
			$cx4 = $c*4-4;
			$navigation .= "<li><a onclick=\"return directory_pagination('".$cx4.", 4')\">".$c."</a></li>";
			$c++;
		}
		// ---------------- / PAGINACIÃ“N  --------------------

		$data['users'] = $users;
		$data['navigation'] = $navigation;
		return $data;
	}

	public function medical_directory_ajax()
	{
		$filters = $_POST['data'];
		if(isset($_POST['data']['limit']))
		{
			$limit=$_POST['data']['limit'];
		}
		else
		{
			$limit = false;
		}
		$results = $this->medical_directory($filters, $limit);
		if(empty($results['users'][0]['eps']) || empty($results['users'][0]['specialties']))
		{
			die("<div class='fondo-receta'>No hay resultados que coincidan con sus criterios de bÃºsqueda</div>");
		}

		$rows2 = $this->medical_directory_rows($filters, false);
		$num_rows = $rows2->num_rows();
		$navigation_pages = ceil($num_rows/4);
		$c=1;
		$navigation = " ";
		$cx4 = 0;
		while ($c <= $navigation_pages)
		{
			$cx4 = $c*4-4;
			$navigation .= "<li><a onclick=\"return directory_pagination('".$cx4.", 4')\">".$c."</a></li>";
			$c++;
		}
		echo "
				<ul class='paginacion'>
				".$navigation."</ul>";
		foreach ($results['users'] as $key => $value)
			echo $i++;
		{
			?>
<a href="#directorio-modal-<?php echo $key?>" class="foto-medico inline llama_modal">
	<img src="<?php echo base_url(); ?>uploads/profile_pictures/<?php echo $value['profile_picture_name'];?>">
	<div class="over-perfil-medico">
		<p>
			<strong><?php echo $value['full_name']?> </strong>
		</p>
		<p>
			<?php echo $value['profession']?>
		</p>
	</div>
	<div class="info-perfil-medico">
	</div> 
</a>
<div class="directorio-modal clearfix"
	id="directorio-modal-<?php echo $key?>">
	<h2>
		<?php echo $value['full_name']?>
	</h2>
	<div class="e10"></div>
	<div class="col-left50 pull-left inline">
		<img
			src="<?php echo base_url(); ?>uploads/profile_pictures/<?php echo $value['profile_picture_name'];?>">
		<div class="e20"></div>
		<p class="phone">
			<?php echo $value['phone']?>
		</p>
	</div>
	<div class="col-left50 pull-left inline">
		<h2>InformaciÃ³n adicional.</h2>
		<ul class="lista">
			<li>ProfesiÃ³n: <?php echo $value['profession']?>
			</li>
			<li>Especialidad: <?php echo $value['specialty']?>
			</li>
			<li>DirecciÃ³n: <?php echo $value['address']?>
			</li>
		</ul>
		<h2>EPS que atiendo.</h2>
		<ul class="lista">
			<?php foreach ($value['eps'] as $key2 => $value2){
				echo "<li>".$value2."</li>";
			}?>
		</ul>
		<h2>Otras especialidades.</h2>
		<ul class="lista">
			<?php foreach ($value['specialties'] as $key3 => $value3){
				echo "<li>".$value3."</li>";
			}?>
		</ul>
	</div>
</div>
<?php 
		}
	}

	public function medical_directory_rows($filters, $limit)
	{
		$where ="";
		if($filters)
		{
			$where = "AND cms_users_eps.eps_id = ".$filters['eps_options']." AND cms_users_specialties.specialties_id = ".$filters['specialties_options']." AND cms_users.cities_id=".$filters['cities_id'];
		}

		$sql="
				SELECT
				cms_users.id as users_id,
				cms_users.first_name,
				cms_users.last_name,
				cms_users.phone,
				cms_users_doc_profile.profession,
				cms_users_doc_profile.address,
				cms_users_doc_profile.specialty
				FROM
				cms_users, cms_users_doc_profile, cms_users_eps, cms_users_specialties

				WHERE
				cms_users.id = cms_users_doc_profile.users_id AND
				cms_users.id = cms_users_eps.users_id AND
				cms_users.id = cms_users_specialties.users_id
				".$where."
				GROUP BY cms_users.id
				";
		if($limit)
		{
			$sql .= "LIMIT ".$limit;
		}

		//die($sql);
		$query = $this->db->query($sql);
		return $query;
	}

	public function get_tip()
	{
		$tip = new tips();
		$tip->order_by("rand()");
		$tip->limit(1);
		$tip->get();
		if(isset($_GET['ajax']))
		{
			die($tip->name);
		}
		return $tip->name;
	}

	public function reported_activity_weeks()
	{
		$user = $this->ion_auth->user()->row();
		$users_routines = new users_routines();
		$users_routines->select('routines_id')->where("users_id", $user->id)->order_by("id desc")->limit(1)->get();
		$stages = new stages();
		$stages->select("id, name")->where("routines_id", $users_routines->routines_id)->get();
		//die("count: ".$stages->result_count());
		$options = "";
		foreach ($stages as $key)
		{
			$options .= "<optgroup label='".$key->name."'>";
			$weeks = new weeks();
			$weeks->select("id, name")->where("stages_id", $key->id)->get();
			foreach ($weeks as $key2)
			{
				$reported_activity = new reported_activity();
				$reported_activity->where(array("users_id"=>$user->id, "weeks_id"=>$key2->id))->get();
				//die("count: ".$reported_activity->result_count()."<br>Userid: ".$user->id."<br>weekid: ".$key2->id);
				if(!$reported_activity->result_count()>=1)
				{
					$options .= "<option value='".$key2->id."'>".$key2->name."</option>";
				}
			}
			$options .="</optgroup>";
		}
		return $options;
	}


	public function profile_status_weight($user)
	{
		
		/**
		 * Modificación en base a que produce un error cuando $img es == 0
		 */
		$imc = 0;
		$avgHeight = $user->height/100;
		$avgTotal = $avgHeight *  $avgHeight;
		if($avgTotal != 0)
			$imc = $user->weight / $avgTotal;
			
		//Antigua funcion
		//$imc = $user->weight/((($user->height)/100)*(($user->height)/100));

		if($imc<15.99)
		{
			return "Infrapeso";
		}
		elseif ($imc<18.49)
		{
			return "Delgadez";
		}
		elseif($imc<24.99)
		{
			return "Normal";
		}
		elseif($imc<29.99)
		{
			return "Sobrepeso";
		}
		else
		{
			return $imc;
		}
			
	}
}
?>