
  
<div class="fondos-left" style="background: #F58507;">
  <div class="mask-left">  
    <h1><span>Mi</span> formula médica</h1>
    <div class="e10"></div>
    <div class="fondo-blanco">
      <p>
        Ingresa los medicamentos que consumes o utilizas actualmente y visualizalos en la parte
        lateral derecha
      </p>
    </div>
    <div class="e20"></div>
    <div class="receta-descripcion">
      <div class="fondo-receta">
        <h2 class="ribon-verde">Ingresa tu fórmula</h2>
        <form class="formulario">
            <div class="e10"></div>
            <input type="text" placeholder="Nombre del medicamento" id="fm_nombre_medicamento">
            <select class="select_dd w100" id="fm_tipo_unidad">
              <option value="">Tipo de unidad</option>
              <?php foreach($formula_medica_tipo_unidad as $value):?>
                <option value="<?php echo $value->id;?>"><?php echo $value->name;?></option>
              <?php endforeach;?>
            </select>
            <select class="select_dd w100" id="fm_numero_unidades" >
              <option value="">Número de unidades</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
              <option value="7">7</option>
              <option value="8">8</option>
              <option value="9">9</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
              <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
            </select>
            <select class="select_dd w100" id="fm_periodicidad">
              <option value="">Periodicidad</option>
              <?php foreach($formula_medica_periodicidad as $value):?>
                <option value="<?php echo $value->id;?>"><?php echo $value->name;?></option>
              <?php endforeach;?>
            </select>
            <button class="pull-right boton" type="button" id="fm_guardar">Guardar</button>
            <div class="e10"></div>
        </form>
        <div class="clear"></div>
      </div>
    </div>
  </div>
</div>




