<div class="fondos-right" style="background: url(assets/img/fondos/fondo-1.png)">
	<div class="mask-right clearfix">
		<div class="clear"></div>
		<div class="cont_fotosmedico" id="cont_fotosmedico">
			<div id="cargando" style="display:none; text-alig:center">
				<img src="img/loading.gif">
			</div>
			<div class="alert alert-error" id="alerts" style="display:none;">
			    <button type="button" class="close" data-dismiss="alert">&times;</button>
			   	<strong>Atención!</strong> No has ingresado ningún campo.
		    </div>
			<ul class="paginacion" id="items_paginador">				
			</ul>
			<div id="divMedicos">	

			</div>
			<div id="contDialogMedicos" style="display:none">
			</div>
			<div id="inputHiddens">
			</div>
		</div>
	</div>
</div>
