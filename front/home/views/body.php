<section class="cont_slider">
  <ul class="slider">
      <li class="activo_slider2" style="background: url(assets/img/fondo-2.png)">
        <div class="cont_texto clearfix">
          <p class="tit_slider">Lorem Ipsum Dolor Sit Ame Dolor Sit Amet</p>
          <div class="clear"></div>
          <p class="subtit_slider">Dolor Sit Amet Lorem Ipsum Dolor Sit Ame Dolor Sit Amet Lorem</p>
        </div>
        <div class="contimagenslider clearfix">
          <img src="<?php echo base_url(); ?>assets/img/slider1.png">
        </div>
      </li> 
      <li style="background: url(assets/img/fondo-3.png)">
        <div class="cont_texto clearfix">
          <p class="tit_slider">Lorem Ipsum Dolor Sit Ame Dolor Sit Amet</p>
          <div class="clear"></div>
          <p class="subtit_slider">Dolor Sit Amet Lorem Ipsum Dolor Sit Ame Dolor Sit Amet Lorem</p>
        </div>
        <div class="contimagenslider clearfix">
          <img src="<?php echo base_url(); ?>assets/img/slider1.png">
        </div>
      </li> 
      <li style="background: url(assets/img/fondo-4.png)">
        <div class="cont_texto clearfix">
          <p class="tit_slider">Lorem Ipsum Dolor Sit Ame Dolor Sit Amet</p>
          <div class="clear"></div>
          <p class="subtit_slider">Dolor Sit Amet Lorem Ipsum Dolor Sit Ame Dolor Sit Amet Lorem</p>
        </div>
        <div class="contimagenslider clearfix">
          <img src="<?php echo base_url(); ?>assets/img/slider1.png">
        </div>
      </li> 
      <li style="background: url(assets/img/fondo-5.png)">
        <div class="cont_texto clearfix">
          <p class="tit_slider">Lorem Ipsum Dolor Sit Ame Dolor Sit Amet</p>
          <div class="clear"></div>
          <p class="subtit_slider">Dolor Sit Amet Lorem Ipsum Dolor Sit Ame Dolor Sit Amet Lorem</p>
        </div>
        <div class="contimagenslider clearfix">
          <img src="<?php echo base_url(); ?>assets/img/slider1.png">
        </div>
      </li> 
      <li style="background: url(assets/img/fondo-1.png)">
        <div class="cont_texto clearfix">
          <p class="tit_slider">Lorem Ipsum Dolor Sit Ame Dolor Sit Amet</p>
          <div class="clear"></div>
          <p class="subtit_slider">Dolor Sit Amet Lorem Ipsum Dolor Sit Ame Dolor Sit Amet Lorem</p>
        </div>
        <div class="contimagenslider clearfix">
          <img src="<?php echo base_url(); ?>assets/img/slider1.png">
        </div>
      </li> 
  </ul>
</section>
<div class="main" id="menu_home">
  <a href="<?php echo base_url(); ?>" class="arrow_abajo1"></a>
  <a href="<?php echo base_url(); ?>" class="arrow_abajo2"></a>
  <nav>
    <ul class="menu-home">
      <li><a href="<?php echo base_url(); ?>contenidos#cont1"><span>Mi </span>Lorem Ipsum Dolor</a></li>
      <li><a href="<?php echo base_url(); ?>contenidos#cont2"><span>Mi </span>contador de bienestar</a></li>
      <li><a href="<?php echo base_url(); ?>contenidos#cont3"><span>Mi </span>directorio médico</a></li>
      <li><a href="<?php echo base_url(); ?>contenidos#cont4"><span>Mi </span>plan Vive Bien</a></li>
      <li><a href="<?php echo base_url(); ?>contenidos#cont5"><span>Mis </span>tips y recetas</a></li>
      <li><a href="<?php echo base_url(); ?>contenidos#cont6"><span>Mis</span> artículos de interés</a></li>
      <li><a href="<?php echo base_url(); ?>contenidos#cont7"><span>Mi </span>formula médica</a></li>
      <li><a href="<?php echo base_url(); ?>contenidos#cont8"><span>Mi </span>calendario Vive Bien</a></li>
      <li><a href="<?php echo base_url(); ?>contenidos#cont9"><span>Mi </span>ingreso artículos</a></li>
    </ul>
  </nav>
</div>