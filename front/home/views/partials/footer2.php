
  <!-- jquery
================================================== -->
<script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>assets/js/lib/jquery-1.8.3.min.js"></script> 
  <!-- UI
================================================== -->
<script src="<?php echo base_url(); ?>assets/js/lib/ui/jquery-ui-1.7.custom.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/lib/ui/ui.dialog.js"></script>

<!-- Jquery y jquery UI 

<script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery-ui.js"></script>
<link href="<?php echo base_url(); ?>assets/js/lib/jquery_css/jquery-ui.css" rel="stylesheet" type="text/css" />

 Jquery validity -->

<!-- M�dulos -->
<script src="<?php echo base_url(); ?>assets/js/lib/Modulos/formula_medica.js"></script>
<script src="<?php echo base_url(); ?>assets/js/lib/Modulos/ingreso_articulos.js"></script>
<script src="<?php echo base_url(); ?>assets/js/lib/Modulos/mi_plan_vive_bien.js"></script>
<script src="<?php echo base_url(); ?>assets/js/lib/Modulos/perfil_medico.js"></script>
<script src="<?php echo base_url(); ?>assets/js/lib/Modulos/modales.js"></script>
<script src="<?php echo base_url(); ?>assets/js/lib/Modulos/directorio_medico.js"></script>


<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>

<!-- Fin m�dulos -->

<!-- Columnas
  ================================================== -->
<script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>assets/js/lib/columnas/js/columnas.js"></script> <!--goofy  columnas-->
<script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>assets/js/lib/columnas/js/xylem.js"></script>  <!--alexander mueve columnas-->
<script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>assets/js/lib/columnas/js/velvetFoundation.js"></script>  <!--velvet mueve columnas-->

 <!-- Estilizar
================================================== -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/lib/dd/jquery.dd.js"></script>

 <!-- Scroll
================================================== -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/lib/jscrollpane/js/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/lib/scroll/start_stop_scroll.js"></script>

 <!-- File
================================================== -->


<!-- Slider
================================================== -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/lib/bxslider/js/jquery.bxslider.js"></script>

<!-- Bootstrap
  ================================================== -->
<script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap-fileupload.js"></script> 

  <!-- Fancybox
================================================== -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/lib/source/jquery.fancybox.js"></script>

  <!-- Add Button helper (this is optional) -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/lib/source/helpers/jquery.fancybox-buttons.js"></script>

  <!-- Add Thumbnail helper (this is optional) -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/lib/source/helpers/jquery.fancybox-thumbs.js"></script>

  <!-- Media (this is optional) -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/lib/source/helpers/jquery.fancybox-media.js"></script>


<!-- Acciones
================================================== -->
<script src="<?php echo base_url(); ?>assets/js/actions.js"></script>

<!-- Llamados
================================================== -->
<script src="<?php echo base_url(); ?>assets/js/llamados.js"></script>

</body>
</html>

