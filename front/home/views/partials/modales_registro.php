<div class="ingreso-modal clearfix" id="ingreso-modal">
  <div class="e20"></div>
  <h1 class="center cverde">Accede a tu cuenta</h1>
  <div class="e20"></div>
  <form class="centrado" action="site/login" method="post">
    <div class="input-prepend">
      <span class="add-on"><i class="icon-user"></i></span>
      <input type="text" placeholder="email" name="email" id="email_register">
    </div>
    <div class="e10"></div>
    <div class="input-prepend">
      <span class="add-on"><i class="icon-user"></i></span>
      <input type="password" placeholder="Contraseña" name="password" id="password_register">
    </div>
    <div class="e10"></div>
    <?php
    if(isset($_GET['deny']))
      {
        $msg = "Datos incorrectos";
        if(isset($_GET['inactive']))
          {
            $msg = "Su cuenta ha sido desactivada por un administrador.";
          } 
        echo "<div style='border: solid 1px #ff0000'>".$msg."</div>
        <script>
        window.onload = function()
          {
            $('#link_ingresar').trigger('click');
            document.getElementById('email_register').value='".$_GET['mail']."';
          }
        </script>
        ";
      }
    ?>
    <div class="e10"></div>
    <a class="olvido llama_modal" href="#olvido-modal">Olvidáste tu contraseña</a>
    <div class="e5"></div>
    <a class="olvido llama_modal" href="#registro-modal">Registrarse</a>
    <div class="e20 "></div>
    <a class="facebook pull-left"></a>
    <button class="pull-right boton " type="submit" onclick="location.href = 'internas'">Ingresar</button>
  </form>
</div>

<div class="registro-modal clearfix" id="registro-modal">
  <div class="e20"></div>
  <h1 class="center cverde">Regístrate</h1>
  <div class="e20"></div>
  <div class="row-fluid">
  <div class="colum-left"><h2>Usuario Vive Bien</h2>
    <div class="e10"></div>
  <p class="parrafo">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
  consequat. Duis aute irure dolor...</p>
  <div class="e20 "></div>
  <a class="boton left100" href="<?php echo base_url(); ?>registrese">Registar</a>
  </div>
  <div class="separador-vertical"></div>
  <div class="colum-right"><h2>Medico Vive Bien</h2>
    <div class="e10"></div>
  <p class="parrafo">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
  consequat. Duis aute irure dolor...</p>
  <div class="e20 "></div>
  <a class="boton left100" href="<?php echo base_url(); ?>registrese?doc">Registar</a>
  </div>
  </div>
</div>

<div class="olvido-modal clearfix" id="olvido-modal">
  <div class="e20"></div>
  <h1 class="center">Olvidaste tus datos</h1>
  <div class="e10"></div>
  <form class="centrado">
  <p class="parrafo">Ingresa tu e-mail y recibiras tus datos de acceso lo mas pronto posible</p>
  <div class="e20"></div>
  <div class="input-prepend">
    <span class="add-on"><i class="icon-envelope"></i></span>
    <input type="text" placeholder="E-mail" name="recover_email" id="recover_email">  
  </div>
  <div class="e20"></div>
  <div id="user_recover_des"></div>
  <button class="pull-right boton" type="submit" onclick="return user_recover(this.form, 'site/recover', '#user_recover_des')">Enviar</button>
  </form>
</div>

<div class="actualizar-modal clearfix" id="actualizar-modal"> 
  <h2>Actualizar información</h2>
  <div class="e10"></div>
  <p>
    <img src="http://theriteconcept.com/wp-content/uploads/2013/12/comingsoon.png">
  </p>
</div>
