
    
<div class="fondos-left" style="background: #6F388B;">
    <div class="mask-left clearfix">;
        <h1><span>Mis</span> tips y recetas</h1>
        <div class="e10"></div>

        <div class="scroll-pane scroll"> 
        <?php
        foreach ($recipes as $key)
            {
                $img = new imagen();
                $img->where('id', $key->imagen_id)->get();
        ?>
                <a class="receta-descripcion" onclick="load_recipe(<?php echo $key->id?>)">
                    <div class="fondo-receta">
                        <h2 class="ribon-verde"><?php echo $key->name?></h2>
                        <div class="clear"></div>
                        <img src="<?php echo base_url(); ?><?php echo substr($img->path, 2);?>">
                        <p><?php echo $key->description;?></p>
                        <div class="clear"></div>
                    </div>
                </a> 

        <?php
            }
        ?>
        </div>

    </div>
    </div>




