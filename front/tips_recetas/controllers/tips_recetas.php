<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.com
     * @company Imaginamos S.A.S | Todos los derechos reservados
     * @date 1-050004
     */

                        

class Tips_recetas extends Front_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		return $this->build();
	}

	public function load_recipe()
		{
			$recipes = new recipes();
			$recipes->where('id', $_GET['id'])->get();
			$img = new imagen();
			$img->where('id', $recipes->imagen_id)->get();
			//==== TIPS
			$tip = new tips();
			$tip->order_by("rand()");
			$tip->limit(1);
			$tip->get();
			?>

			<div class="mask-right clearfix">
		    <div class="compartir">
		            <div class="fb-like" data-href="http://developers.facebook.com/docs/reference/plugins/like" data-send="true" data-layout="button_count" data-width="300" data-show-faces="false" data-font="segoe ui"></div>
		            <div id="fb-root"></div>
		            <script>
		            (function(d, s, id) {
		                 var js, fjs = d.getElementsByTagName(s)[0];
		                if (d.getElementById(id)) return;
		                js = d.createElement(s); js.id = id;
		                js.src = "//connect.facebook.net/es_ES/all.js#xfbml=1";
		                fjs.parentNode.insertBefore(js, fjs);
		                }(document, 'script', 'facebook-jssdk')
		            );
		            </script>
		    </div>
		    <div class="clear"></div>
		    
		    
		    <div class="col-left50 pull-left clearfix">
		      <h2 class="ribon-slider"><?php echo $recipes->name?></h2>
		      <ul class="slider-recetas">
		        <li>
		          <figure><img src="<?php echo base_url(); ?><?php echo substr($img->path, 1)?>"></figure>
		        </li>
		      </ul>
		    </div>

		    <div class="col-left50 pull-right clearfix">

		      <div class="scroll-ingredientes scroll">
		        <article class="fondo-scroll">
		          <h3>Preparación</h3>
		          <p class="fecha"><strong>Fecha de publicación:</strong> <?php echo $recipes->date?></p>
		          <p> 
		          	<?php echo $recipes->description;?>
		          </p>
		         
		        </article>
		      </div>

		    </div>

		    <div class="clear"></div>

		    <div class="tip">
		      <h2 class="ribon-amarillo">Tip</h2>
		      <div class="e5"></div>
		      <p>
		       <?php echo $tip->name?>
		      </p>
		    </div>

		  </div>

			<?php

		}

}
?>