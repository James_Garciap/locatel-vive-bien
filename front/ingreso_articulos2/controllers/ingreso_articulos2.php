<?php

/**
 * @author Elbert Tous
 * @email elbert.tous@imaginamos.com
 * @company Imaginamos S.A.S | Todos los derechos reservados
 * @date 1-050004
 */
class Ingreso_articulos2 extends Front_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		return $this->build();
	}

	/**
	 * Metodo de ingreso de articulos, por el perfil de m�dico, recibe $_POST del formulario y inserta los datos en 
	 * la tabla post.
	 * 
	 * @author Julian Molina - julian.molina@imagina.co
	 * @version 1.0, 14 feb 2014
	 * @copyright Imaginamos S,A
	 * 
	*/
	public function ingresarArticulos(){
		
		$post = new posts();
		$user = $this->ion_auth->user()->row();
		
		$post->name = $_POST["t"];
		$post->description = $_POST["a"];
		$post->date = date("Y/m/d");
		$post->users_id = $user->id;
		$post->imagen_id = '';
		
		if($post->save())
			echo true;
		else
			echo false;
	}
	
	public function consultarArticulos(){
		
		$posts = new posts();
		
		$users = $this->ion_auth->user()->row();
		
		$posts->where("users_id", $users->id)->limit(5)->order_by('id desc')->get();
		$posts_result_count = $posts->result_count();
		if($posts_result_count==0)
		{
			$html_posts = "No has ingresado ning&uacute;n art&iacute;culo a&uacute;n...<a href='#cont8'>Ingresa uno</a>";
		}
		else
		{
			$html_posts = "";
			foreach ($posts as $key)
			{
				$html_posts .="<li><a href='#cont8'>".$key->name."</a></li>";
			}
		}
		echo $html_posts;
	}

}
?>