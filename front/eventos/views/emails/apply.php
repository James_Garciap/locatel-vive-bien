<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        <table width="632" border="0" align="center" background="<?php echo cdn_imaginamos('images/bg/bg_mail_new_user.jpg') ?>">
            <tr>
                <td height="522">
                    <table width="55%" border="0" align="center">
                        <tr>
                            <td>
                                <p style="margin-top:20px;">
                                    El usuario  <strong><?php echo $users_name ?></strong> Ha solicitado asistir a un evento.
                                    <br>
                                    Datos del evento:<br><br>
                                    <strong>Nombre:</strong><br><pre style="margin:0; padding:0;"><?php echo $calendar_name ?></pre><br>
                                    <strong>Fecha:</strong><br><pre style="margin:0; padding:0;"><?php echo $calendar_date ?></pre><br>
                                     Datos del Usuario:<br><br>
                                    <strong>Nombre:</strong><br><pre style="margin:0; padding:0;"><?php echo $users_name ?></pre><br>
                                    <strong>Email:</strong><br><pre style="margin:0; padding:0;"><?php echo $users_email ?></pre><br>
                                    <strong>Teléfono:</strong><br><pre style="margin:0; padding:0;"><?php echo $users_phone ?></pre>
                                </p>
                                <p>
                                    <br><br>
                                    Para información adicional,<br>Staff <a href="http://imaginamos.com" target="_blank">imaginamos.com</a>
                                    <br><br>
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>