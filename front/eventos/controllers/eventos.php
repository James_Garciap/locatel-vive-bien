<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.com
     * @company Imaginamos S.A.S | Todos los derechos reservados
     * @date 1-050004
     */

                        

class Eventos extends Front_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		return $this->build();
	}

	public function load_calendar_event()
		{
			$calendar = new calendar();
			$calendar->where('id', $_GET['id'])->get();
			$img = new imagen();
			$img->where('id', $calendar->imagen_id)->get();
			?>

				<div class="mask-right" >
					<div class="compartir">
				      <div class="fb-like" data-href="http://developers.facebook.com/docs/reference/plugins/like" data-send="true" data-layout="button_count" data-width="300" data-show-faces="false" data-font="segoe ui"></div>
				      <div id="fb-root"></div>
				        <script>
				        (function(d, s, id) {
				          var js, fjs = d.getElementsByTagName(s)[0];
				          if (d.getElementById(id)) return;
				          js = d.createElement(s); js.id = id;
				          js.src = "//connect.facebook.net/es_ES/all.js#xfbml=1";
				          fjs.parentNode.insertBefore(js, fjs);
				         }(document, 'script', 'facebook-jssdk'));
				        </script>
				    </div>
				    <div class="clear"></div>

				    <div class="col-left50 pull-left clearfix">
				      <h2 class="ribon-slider"><?php echo $calendar->name?></h2>
				      <ul class="slider-recetas">
				        <li>
				          <figure><img src="<?php echo base_url(); ?><?php echo substr($img->path, 1)?>"></figure>
				        </li>
				      </ul>
				    </div>

				    <div class="col-left50 pull-right clearfix">

				      <div class="scroll-ingredientes scroll">
				        <article class="fondo-scroll">
				          <h3>Información del evento</h3>
				          <p class="fecha"><strong>Fecha de publicación:</strong><?php echo $calendar->date?></p>
				          
				          <p>
				            <?php echo $calendar->description?>
				          </p>
				        </article>
				      </div>

				    </div>

				    <div class="clear"></div>

				      <a class="participar boton pull-right" onclick="apply_event(<?php echo $_GET['id']?>)">Participar</a>
				     </div>
				     <div class='fondo-receta' id="calendar_apply_des" style="display:none"></div>
			<?php
		}


public function apply()
	{
		$users = $this->ion_auth->user()->row();
		$user = new users();
		$user->where('id', $users->id)->get();
		$calendar = new calendar();
		$calendar->where('id', $_GET['id'])->get();
		//email
		$this->load->library('email');
	    $this->email->clear();
	    $this->email->protocol = 'smtp';
	    $this->_data = array(
	        'users_name' => $user->first_name." ".$user->last_name,
	        'users_email' => $user->email,
	        'users_phone' => $user->phone,
	        'calendar_name' => $calendar->name,
	        'calendar_date' => $calendar->date
	    );
	    $html = $this->view('emails/apply');
	    $this->email->from('imaginamos.prueba@gmail.com', 'CMS Imaginamos');
	    $this->email->to('juan.cantor@imagina.co');
	    $this->email->subject('Solicitude para evento:  ' . $calendar->name);
	    $this->email->message($html);
	    //echo "<pre>";print_r($this->email);echo "</pre>";
	    if($this->email->send())
	        {
	            echo "Su slicitud se ha enviado, un asesor se pondrá en contacto con usted en poco tiempo. ";
	        }
	    else
	        {
	            echo "<pre>";print_r($this->email);echo "</pre>";
	            echo "No se ha podido completar la solicitud, intentelo de nuevo más atarde";
	        }


	}
	
}
?>