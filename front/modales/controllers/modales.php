<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.com
     * @company Imaginamos S.A.S | Todos los derechos reservados
     * @date 1-050004
     */

                        

class Modales extends Front_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		return $this->build();
	}
	
	/**
	 * Metodo de insercion de reporte de actividades
	 *
	 * @author Julian Molina - julian.molina@imagina.co
	 * @version 1.0 - 17 feb 2014
	 * @copyright Imaginamos S,A
	 * @return type Boolean
	 */
	public function registrarRutina(){

		try {
			$routines = new reported_activity();
			$users = $this->ion_auth->user()->row();
			
			$routines->users_id = $users->id;
			$routines->weight = (int)$_POST["p"];
			$routines->time = 1;
			$routines->description = "...";
			$routines->activity_minutes_id = $_POST["m"];
			$routines->weeks_id  = 1;
			$routines->routines_id = $_POST["e"];

			if($routines->save())
				echo true;
			else
				echo false;
			
		} catch (Exception $e) {
			echo $e->getMessage();
		}
		
	}
	
	
	/**
	 * Metodo de insercion de informe de peso
	 *
	 * @author Julian Molina - julian.molina@imagina.co
	 * @version 1.0 - 17 feb 2014
	 * @copyright Imaginamos S,A
	 * @return type Boolean
	 */
	public function consultarPeso(){
	
		try {

			$todo = array();
			$aux = array();
			$arrayUsers = array();
			$arrayActivity = array();
			$arrayWeigth = array();
			
			$session = $this->ion_auth->user()->row();
			
			$users = new users();
			$arrayUsers = $users->where("id", $session->id)->get();
			
			$reported_activity = new reported_activity();
			$arrayActivity = $reported_activity->where("users_id", $session->id)->get();
			
			$amount =  $this->dias_transcurridos(date('Y-m-d', $arrayUsers->created_on), date("Y-m-d"));
			
			for($i = 1; $i<=count($arrayActivity); $i++)
				$aux[] = "Rutinas $i";
			
			$i = 0;
			foreach ($arrayActivity as $row){
					$arrayWeigth[] = (int)$row->weight;
				$i++;
			}
			 
			$todo["dias"] = $aux; 
			$todo["peso"] = $arrayWeigth;
			$todo["nombre"] = "{$arrayUsers->first_name} {$arrayUsers->last_name}";
	
			echo json_encode($todo);
				
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	
	}

	public function dias_transcurridos($fecha_i, $fecha_f)
	{
		$dias	= (strtotime($fecha_i)-strtotime($fecha_f))/86400;
		$dias 	= abs($dias); $dias = floor($dias);
		return $dias;
	}

	public function reported_activity()
	{
		//echo "<pre>";print_r($_POST);echo"</pre>";die();
        $user = $this->ion_auth->user()->row();
		$reported_activity = new reported_activity();
		$reported_activity->users_id = $user->id;
		$reported_activity->weight = $_POST['data']['reported_activity_weight'];
		$reported_activity->activity_minutes_id = 13;
		$reported_activity->weeks_id = $_POST['data']['reported_activity'];
		//echo "<pre>";print_r($reported_activity);echo"</pre>";die();
		if($reported_activity->save())
			{
                //----- Actualizando peso del usuario
                $users = new users();
                $users->where('id', $user->id)->get();
                $users->pre_weight = $users->weight;
                $users->weight = $reported_activity->weight;
                $users->full_name="0";
                //---- Guardando stream
                $stream = new users_activity();
                $stream->users_id = $user->id;
                $stream->action = "update";
                $stream->module = "users";
                $stream->description = $users->first_name." ha registrado un cambio de peso.";
                $stream->date=date("Y-m-d");
                if($users->save())
                	{
                		die("Actividad reportada exitosamente");		
                	}
                else
                	{
                		foreach ($users->error as $error) 
	                		{
	                			echo $error."<br>";
	                		}
                	}    

				
			}
		foreach ($reported_activity->error->all as $error)
                    {
                        echo $error."<br>";
                    }

		die("No ha sido posible realizar el reporte, intentalo más tarde.");
		//echo "<pre>";print_r($_POST);echo"</pre>";die();
	}

}
?>