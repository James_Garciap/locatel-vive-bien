<div class="contactar-modal clearfix" id="contactar-modal">
	<hgroup>
		<h2>Contactar entrenador</h2>
		<div class="e10"></div>
		<!--<h3 class="camarillo">Lorem Ipsum Dolor</h3>-->
	</hgroup>
	<div class="e10 clear"></div>

	<div class="col-left50  inline">
		<h3 class="camarillo">Entrenadores disponibles</h3>
		<p>
			<?php echo $modals_coaches;?>
		</p>
	</div>
</div>

<div class="reportar-modal clearfix" id="reportar-modal">
	<h2>Reporta tu Actividad</h2>
	<div class="e20"></div>

	<div class="alert alert-success" id="alertOk" style="display: none;">
		<strong>Inserci&oacute;n correcta</strong> El art&iacute;culo se
		insert&oacute; correctamente.
	</div>
	<div class="alert alert-danger" id="alertBad" style="display: none;">
		<strong>Inserci&oacute;n fallida</strong> Hubo un fallo al insertar.
	</div>

	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
		eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
		minim veniam, quis nostrud exercitation ullamco laboris</p>
	<form class="formulario">
		<div class="e10"></div>
		<input type="text" placeholder="Peso actual" id="peso_actual">

		<div class="col-left50 pull-left clearfix">
			<select class="select_dd w100" id="ejercicio_realizado">
				<option value="">Ejercicio realizado</option>
				<?php foreach ($rutina_ejercicios as $value):?>
				<option value="<?php echo $value->id;?>">
					<?php echo $value->name; ?>
				</option>
				<?php endforeach;?>
			</select>
		</div>
		<div class="col-left50 pull-right clearfix">
			<select class="select_dd w100" id="minutos">
				<option value="">Minutos de actividad</option>
				<?php foreach ($rutina_minutos as $value):?>
				<option value="<?php echo $value->id;?>">
					<?php echo $value->name; ?>
				</option>
				<?php endforeach;?>
			</select>
		</div>
		<button class="pull-right boton" type="button" id="reportar_rutina">Reportar</button>
	</form>
</div>

<div class="conoce-modal" id="conoce-modal">
	<h2>Conoce tu Avance</h2>
	<div class="e20"></div>
	<figure>
		<div id="contenidoChart"
			style="min-width: 500px; height: 300px; margin: 0 auto"></div>
	</figure>
</div>

<div id="div_mapa_sitio" style="width: 400px; display:none; height: 600;">
	<ul class="nav nav-list">
		<li class="nav-header">Mapa de navegación</li>
		<li><a href="#cont1">Home</a></li>
		<li><a href="#cont2">Library</a></li>
		<li><a href="#cont3">Home</a></li>
		<li><a href="#cont4">Library</a></li>
		<li><a href="#cont5">Home</a></li>
		<li><a href="#cont6">Library</a></li>
		<li><a href="#cont7">Cont8</a></li>
		<li><a href="#cont8">Library</a></li>
	</ul>
</div>

<div id="div_politicas" style="width: 800px; display:none; height: 400;">
	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
	 minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit 
	 in voluptate velit esse cillum dolore eu 
	fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim 
	ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
	reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
	 sunt in culpa qui officia deserunt mollit anim id est laborum.
</div>

<div id="div_terminos_condiciones" style="width: 800px; display:none; height: 400;">
	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
	 minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit 
	 in voluptate velit esse cillum dolore eu 
	fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim 
	ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
	reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
	 sunt in culpa qui officia deserunt mollit anim id est laborum.
</div>
