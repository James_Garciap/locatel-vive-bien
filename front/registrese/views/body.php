<style>
@media screen and (max-width:980px) {
	body {
		overflow: auto;
	}
}
</style>

<a href="<?php echo base_url(); ?>contenidos" class="cerrar_registrese">
	<img src="<?php echo base_url(); ?>assets/img/iconos/fle_regresar.png"
	alt="">
</a>
<div class="cont_registrese clearfix">
	<div id="tabs">
		<ul>
			<li><a href="#paso-1">1</a></li>
			<li><a href="#paso-2">2</a></li>
			<li><a href="#paso-3">3</a></li>
		</ul>
		<div id="paso-1" class="ui-tabs-panel">
			<div class="colm-left">
				<h2>Instrucciones</h2>
				<div class="e10"></div>
				<h3>Paso 1</h3>
				<div class="e20"></div>
				<p class="parrafo">Lorem ipsum dolor sit amet, consectetur
					adipisicing elit, sed do eiusmod tempor incididunt ut labore et
					dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
					exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate
					velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
					occaecat cupidatat non proident, sunt in culpa qui officia deserunt
					mollit anim id est laborum.</p>
				<div class="e10"></div>
				<p class="parrafo">Lorem ipsum dolor sit amet, consectetur
					adipisicing elit, sed do eiusmod tempor incididunt ut labore et
					dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
					exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat.</p>


				<label>Fotografía</label>
				<div>
					<img id="imagePreview" src="#" alt="your image" width="100"
						height="100" />
					<div class="clear"></div>
					<input type='file' id="imgInp" name="imagen" accept="image/*" /> <span
						class="f_help">Las imagenes se redimencionaran: 200 * 200px</span>
				</div>


			</div>
			<form class="registrarse" method="post" onsubmit="return false">
				<div class="colm-right">

					<input type="hidden" value="" name="ext_image_data"
						id="ext_image_data" /> <input type="hidden" value=""
						name="image_data" id="image_data" /> <label>*Nombres</label> <input
						class="registrarse-input" type="text" id="first_name"
						name="first_name" placeholder="Nombres" required value="">
					<div class="e10"></div>
					<label>*Apellidos</label> <input class="registrarse-input"
						type="text" id="last_name" name="last_name"
						placeholder="Apellidos" required value="">
					<div class="e10"></div>
					<label>*Sexo</label> <select name='sex' id="sex" placeholder="Sexo"
						required>
						<option value="m">Masculino</option>
						<option value="f">Femenino</option>
					</select>

					<div class="e10"></div>
					<label>*Fecha de Nacimiento</label> <input
						class="registrarse-input date" type="date" id="birthday"
						name="birthday" placeholder="Fecha de Nacimiento" required
						value="">
					<div class="e10"></div>
					<label>*Correo Electrónico</label> <input class="registrarse-input"
						type="email" id="email" name="email"
						placeholder="Correo Electrónico" required style="height: 30px"
						value="">
					<div class="e10"></div>
					<label>*Teléfono</label> <input class="registrarse-input"
						type="number" id="phone" name="phone" placeholder="Teléfono"
						required style="height: 30px" value="">
					<div class="e10"></div>
					<label>*Ciudad de residencia</label> <select id="cities_id"
						name="cities_id">
						<?php
						foreach ($cities_id_options as $key)
						{
							echo "<option value='".$key->id."'>".$key->name."</option>";
						}
						?>
					</select>

					<div class="e10"></div>

				</div>
		
		</div>
		
		<div id="paso-2" class="ui-tabs-panel ui-tabs-hide">
			<div class="colm-left">
				<h2>Instrucciones</h2>
				<div class="e5"></div>
				<h3>Paso 2</h3>
				<div class="e20"></div>
				<p class="parrafo">Lorem ipsum dolor sit amet, consectetur
					adipisicing elit, sed do eiusmod tempor incididunt ut labore et
					dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
					exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate
					velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
					occaecat cupidatat non proident, sunt in culpa qui officia deserunt
					mollit anim id est laborum.</p>
				<div class="e10"></div>

			</div>

			<div class="colm-right">

				<label>*Ingresa tu estatura (cm)</label> <input
					class="registrarse-input" type="number" name="height" id="height"
					placeholder="Estatura" required style="height: 30px" value="">
				<div class="e10"></div>
				<label>*Ingresa tu Peso (Kgs)</label> <input
					class="registrarse-input" type="number" name="weight" id="weight"
					placeholder="Peso" required style="height: 30px" value="">
				<div class="e10"></div>
				<label>*Nivel de actividad física</label>
				<div class="clear"></div>
				<select name="activity_level_id" id="activity_level_id"
					placeholder="nivel de actividad física">
					<?php 
					foreach ($activity_level_id_options as $key )
					{
			 		echo "<option value='".$key->id."'>".$key->name."</option>";
			 	}
			 	?>
				</select>
				<div class="e10"></div>
				<label>Hobbies / pasatiempos</label>
				<div class="clear"></div>
				<!--		<input class="registrarse-input" type="text" name="hobbies" placeholder="Pasatiempos">    	-->
				<?php
				foreach ($hobbies_options as $key)
				{
					echo "<div class='cont_radiocheck'><p>".$key->name."</p><input class='registrarse-input' type='checkbox' name='hobbie_".$key->id."' value='".$key->id."'></div>";
				}
				?>

				<?php
				if(!isset($_GET['doc']))
				{
					?>

				<div class="e10"></div>
				<!--
				<label>*Foto de perfil</label>
				<input type="file" name="profile_picture_upload" id="profile_picture_upload" accept="image/*">
				<br>
				-->
				<label>*Captcha</label>
				<div>
					<?php
					echo $captcha;
					?>
				</div>
				Ingrese los caráteres que ve en la imagen: <input type="text"
					name="captcha" id="captcha">
				<button class="mover next-tab" type="submit"
					onClick="return users_signup(this.form, 'site/register', '#user_register_des');"
					disabled="disabled" style="background: #cccccc;"
					id="submit_register_doc">Registrar</button>
				<div class="e10"></div>
				<div>
					<input type="checkbox" onclick="terms();"
						name="terms_and_conditions" id="terms_and_conditions"> He leído y
					estoy de acuerdo con los <a href="#terms">términos y condiciones</a>
				</div>
				<div class="e10"></div>
				<div id="user_register_des" style="display: none;">Se ha enviado un
					mensaje de verificación con tu nueva contraseña al correo
					electrónico proporcionado</div>
			</div>
		</div>
		</br> </br> </br> </br> </br> </br> </br> </br>
		<?php
				}
				else
				{

					?>

		<!-- ==============================          DATOS ADICIONALES PARA MEDICOS     ============================== -->
	</div>
</div>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
<div id="paso-3" class="ui-tabs-panel ui-tabs-hide">
	<div class="colm-left">
		<h2>Instrucciones</h2>
		<div class="e5"></div>
		<h3>Paso 3</h3>
		<div class="e20"></div>
		<p class="parrafo">Lorem ipsum dolor sit amet, consectetur adipisicing
			elit, sed do eiusmod tempor incididunt ut labore et dolore magna
			aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
			laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
			dolor in reprehenderit in voluptate velit esse cillum dolore eu
			fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est
			laborum.</p>
		<div class="e10"></div>
		<p class="parrafo">Lorem ipsum dolor sit amet, consectetur adipisicing
			elit, sed do eiusmod tempor incididunt ut labore et dolore magna
			aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
			laboris nisi ut aliquip ex ea commodo consequat.</p>
	</div>

	<div class="colm-right">


		<label>Profesión</label> <input class="registrarse-input" type="text"
			placeholder="" name="profession" id="profession" value="">
		<div class="e10"></div>
		<label>Facultad de medicina donde se graduó</label> <input
			class="registrarse-input" type="text" placeholder="" name="school"
			id="school" value="">
		<div class="e10"></div>
		<label>Registro profesional</label> <input class="registrarse-input"
			type="text" placeholder="" name="profesional_register"
			id="profesional_register" value="">
		<div class="e10"></div>
		<label>Año de graduación</label> <select name="graduation_year"
			id="graduation_year" class="registrarse-input">
			<?php
			$c = 1940;
			$a = date("Y");
			while($c<=$a)
			{
				echo "<option value='".$c."'>".$c."</option>";
				$c++;
			}

			?>
		</select>
		<div class="e10"></div>
		<label>Especialidad</label> <input class="registrarse-input"
			type="text" placeholder="" name="specialty" id="specialty" value="">
		<div class="e10"></div>
		<label>áreas de especialidad</label>
		<?php
		foreach ($specialties_options as $key)
		{
			echo "<div class='cont_radiocheck'><p>".$key->name."</p><input class='registrarse-input' type='checkbox' name='specialty_".$key->id."' value='".$key->id."'></div>";
		}
		?>
		<div class="e10"></div>
		<label>Facultad del título</label> <input class="registrarse-input"
			type="text" placeholder="" name="specialty_school"
			id="specialty_school" value="">
		<div class="e10"></div>
		<label>Registro médico</label> <input class="registrarse-input"
			type="text" placeholder="" name="medical_register"
			id="medical_register" value="">
		<div class="e10"></div>
		<label>Asociación a la que pertenece</label> <input
			class="registrarse-input" type="text" placeholder="" name="society"
			id="society" value="">
		<div class="e10"></div>
		<label>Lugar donde trabaja</label> <span>
			<div id="register_work_company">
				<input class="registrarse-input" type="text" placeholder=""
					name="work_company1" id="work_company1" value=""> <input
					class="registrarse-input" type="text" placeholder=""
					name="work_company2" id="work_company2" style="display: none"
					value=""> <input class="registrarse-input" type="text"
					placeholder="" name="work_company3" id="work_company3"
					style="display: none" value=""> <input class="registrarse-input"
					type="text" placeholder="" name="work_company4" id="work_company4"
					style="display: none" value=""> <input type="hidden"
					id="work_company_flag" name="work_company_flag" value="2">
			</div> <img src="img/iconos/plus.png" style="cursor: pointer"
			onclick="add_work_company();">
		</span>
		<div class="e10"></div>
		<label>Teléfono de Contacto</label> <input class="registrarse-input"
			type="text" placeholder="" name="phone" id="phone" value="">
		<div class="e10"></div>
		<label>Dirección de Contacto</label> <input class="registrarse-input"
			type="text" placeholder="" name="address" id="address" value="">
		<div class="e10"></div>
		<label>Horarios de Atención</label>
		<?php
		foreach ($opening_hours_options as $key)
		{
			echo "<div class='cont_radiocheck'><p>".$key->name."</p><input class='registrarse-input' type='checkbox' name='opening_hour_".$key->id."' value='".$key->id."'></div>";
		}
		?>
		<div class="e10"></div>
		<label>Seguros que atiende</label>
		<?php
		foreach ($eps_options as $key)
		{
			echo "<div class='cont_radiocheck'><p>".$key->name."</p><input class='registrarse-input' type='checkbox' name='eps_".$key->id."' value='".$key->id."'></div>";
		}
		?>
		<div class="e10"></div>
		<label>Costo Promedio de Consulta</label> <input
			class="registrarse-input" type="text" placeholder=""
			name="average_consulting_cost" id="average_consulting_cost" value="">
		<button class="mover next-tab" type="submit"
			onClick="return users_signup(this.form, 'site/register', '#user_register_des')"
			id="submit_register_doc" disabled="disabled"
			style="background: #cccccc;">Registar</button>
		<input type="hidden" name="is_doc" id="is_doc" value="true">
		<div class="e10"></div>
		<label>*Captcha</label>

		<div>
			<?php
			echo $captcha;
			?>
		</div>
		Ingrese los caráteres que ve en la imagen: <input type="text"
			name="captcha" id="captcha">

		<div class="e10"></div>
		<div>
			<input type="checkbox" onclick="terms();" name="terms_and_conditions"
				id="terms_and_conditions"> He leído y estoy de acuerdo con los <a
				href="#terms">términos y condiciones</a>
		</div>
		<div class="e10"></div>

		<div id="user_register_des" style="display: none;">Se ha enviado un
			mensaje de verificación con tu nueva contraseña al correo electrónico
			proporcionado</div>

	</div>
</div>

</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
<!-- ==============================         /DATOS ADICIONALES PARA MEDICOS     ============================== -->
<?php 
				}
				?>



</form>

</div>
</div>


<?php //include("footer"); ?>
<?php //include("footer2"); ?>


