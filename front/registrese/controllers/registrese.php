<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.com
     * @company Imaginamos S.A.S | Todos los derechos reservados
     * @date 1-050004
     */

                        

class Registrese extends Front_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		//echo "<pre>";print_r($this);echo "</pre>";die("--");

		$activity_level = new activity_level();
		$this->_data['activity_level_id_options'] = $activity_level->get();
		$hobbies = new hobbies();
		$this->_data['hobbies_options'] = $hobbies->get();
		$specialties = new specialties();
		$this->_data['specialties_options'] = $specialties->get();
		$opening_hours = new opening_hours();
		$this->_data['opening_hours_options'] = $opening_hours->get();
		$eps = new eps();
		$this->_data['eps_options'] = $eps->get();
		$cities = new cities();
		$this->_data['cities_id_options'] = $cities->get();
		//echo "<pre>";print_r($this->_data['activity_level_id_options']);echo "</pre>";die("--");		

		//======================= < Caotcha > =====================//
		$this->load->helper('captcha');
		$vals = array(
		    'img_path'	 => 'assets/img/captcha/',
		    'img_url'	 => base_url().'assets/img/captcha/',
		    );

		$cap = create_captcha($vals);

		if($cap){
			$data = array(
					'captcha_time'	=> $cap['time'],
					'ip_address'	=> $this->input->ip_address(),
					'word'	 => $cap['word'],
			);
			
			$query = $this->db->insert_string('cms_captcha', $data);
			$this->db->query($query);
			
			$this->_data['captcha'] = $cap['image'];
		}
	
		//echo 'Submit the word you see below:';
		//echo $cap['image'];
		//echo '<input type="text" name="captcha" value="" />';
		//======================= </ Caotcha > =====================//

		return $this->build();
	}

}
?>