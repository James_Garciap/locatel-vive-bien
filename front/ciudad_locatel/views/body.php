
<div class="fondos-left fondos_fixed" style="background: url(assets/img/fondos/fondo-2.png);">
  <div class="mask-left clearfix">
    <div class="cont_circleft cont_circ clearfix">
      
      <div class="circulo amarillo inline">
        <h1>Ciudad Locatel</h1>
        <p><?php echo $static_texts['ciudadlocatel']?></p>

      </div>
      <div class="clear"></div>
      <div class="botones-ciudad"> 
        <a href="<?php echo $static_texts['ciudadlocatelizquierda']?>" class="boton pull-left">Visitar</a> 
        <a href="<?php echo $static_texts['ciudadlocatelderecha']?>" class="boton pull-right">Visitar</a> 
      </div>

    </div>
  </div>
</div>
