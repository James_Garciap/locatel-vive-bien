﻿<?php        
class Actualizar_perfil extends Front_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		//-----------------------      LISTAS    ------------------------------
		$activity_level = new activity_level();
		$this->_data['activity_level_id_options'] = $activity_level->get();
		$hobbies = new hobbies();
		$this->_data['hobbies_options'] = $hobbies->get();
		$specialties = new specialties();
		$this->_data['specialties_options'] = $specialties->get();
		$opening_hours = new opening_hours();
		$this->_data['opening_hours_options'] = $opening_hours->get();
		$eps = new eps();
		$this->_data['eps_options'] = $eps->get();
        $cities = new cities();
        $this->_data['cities_id_options'] = $cities->get();
		//----------------------------------------------------------------------

		//---------------------------------------------------------------------------------------------
		//-------------------------- INFORMACIÓN DEL USUARIO ------------------------------------------

		$this->load->model("users");
		$user = $this->ion_auth->user()->row();
	    $this->users->get_by_id($user->id);
	    //echo "<pre>";print_r($this->users->fields);echo "</pre>";die("-");
	    foreach ($this->users->fields as $key => $value)
		    {
		    	$this->_data[$value] = $this->users->$value;
		    }


		$this->load->model("users_doc_profile");
		$this->users_doc_profile->select('*');
	    $this->users_doc_profile->where('users_id', $user->id);
	    $this->users_doc_profile->get();
	    if($this->users_doc_profile->id)
	    	{
	    		$this->_data['is_doc'] = true;
	    	}
	    else
	    	{
	    		$this->_data['is_doc'] = false;
	    	}
	    foreach ($this->users_doc_profile->fields as $key => $value)
		    {
		    	$this->_data[$value] = $this->users_doc_profile->$value;
		    }
	   // echo "<pre>";print_r($this->_data);echo "</pre>";die("-");

	    //------------- INFORMACIÓN DE CHECKBOX PARA EL USUARIO -------------
	    $users_hobbies = new users_hobbies();
	    $users_hobbies->select('hobbies_id');
	    $users_hobbies->where('users_id', $user->id);
	    $this->_data['hobbies'] = $users_hobbies->get();

		$users_specialties = new users_specialties();
	    $users_specialties->select('specialties_id');
	    $users_specialties->where('users_id', $user->id);
	    $this->_data['specialties'] = $users_specialties->get();

	    $users_opening_hours = new users_opening_hours();
	    $users_opening_hours->select('opening_hours_id');
	    $users_opening_hours->where('users_id', $user->id);
	    $this->_data['opening_hours'] = $users_opening_hours->get();

	    $users_eps = new users_eps();
	    $users_eps->select('eps_id');
	    $users_eps->where('users_id', $user->id);
	    $this->_data['eps'] = $users_eps->get();
            
            $users_imagen = new imagen();
            $users_imagen->select('path,name');
            //$users_imagen->where('id', $user->imagen_id); // original
            $users_imagen->where('id', $this->users->imagen_id);   // Modificado pro Juan Cantor
            $this->_data['imagen'] = $users_imagen->get();
            $ruta = dirname(__FILE__);
            
	    //------------- INFORMACIÓN DE CHECKBOX PARA EL USUARIO -------------

	    //----------------------------------------------------------------------------------------------


		return $this->build();
	}



 

     public function update() {

//echo "<pre>";print_r($_POST);echo "</pre>";die("--");

        $this->load->model("users", '', TRUE);
        $user = $this->ion_auth->user()->row();
	    $this->users->get_by_id($user->id);
         //------------------------ Guardando stream 
        $stream = new users_activity();
        $stream->users_id = $this->users->id;
        $stream->action = "update";
        $stream->module = "users";
        $stream->description = $this->users->first_name." ha cambiado su informaicón de perfil.";
        $stream->date=date("Y-m-d");
        $stream->save();
        //----------------------------------------
        $_POST = $_POST['data'];

            foreach ($this->users->fields as $key => $value) 
                {
                    //echo $c;
                    foreach ($_POST as $key2 => $value2)
                        {
                            if($key2 == $value)
                                {
                                    if(!empty($value2))
                                    	{
                                    		$this->users->$value = $value2;
                                    	}
                                }
                        }
                    //$c++;
                }
                //die("--");
            // ============= </ Sincronizando el post con el objeto de usuario > ============

            if(!empty($_POST['new_password']))
            	{
            		
                    //------------------------ Guardando stream 
                    $stream = new users_activity();
                    $stream->users_id = $this->users->id;
                    $stream->action = "update";
                    $stream->module = "users";
                    $stream->description = $this->users->first_name." ha cambiado su contraseña.";
                    $stream->date=date("Y-m-d");
                    $stream->save();
                    //----------------------------------------
                    $this->users->password = $this->ACL->hash_password($_POST['new_password'], $this->users->salt);
            	}
            $this->users->full_name=$this->users->first_name." ".$this->users->last_name;
            //echo "pos = <pre>";print_r($this->users);echo "</pre>";die("--");
            $var = $this->users->save();//true;
            //echo "pos = <pre>";print_r($this->users->id);echo "</pre>";die("--");
             

            if ($var)
                {
                    
                    //================ < Recolectando y almacenando Hobbies > ==================
                    $hobbies_users = new users_hobbies();
                    $hobbies_users->where('users_id', $this->users->id)->get();
                    $hobbies_users->delete_all();
                    foreach ($_POST as $key => $value)
                        {
                            $hobbie_array = explode("hobbie_", $key);
                            if($value == 'true')
                                {
                                    if(isset($hobbie_array[1]))    
                                        {	
                                        	$hobbies_users = new users_hobbies();
                                            $hobbies_users->users_id = $this->users->id;
                                            $hobbies_users->hobbies_id = $hobbie_array[1];
                                            if(!$hobbies_users->save())
                                                {
                                                    foreach ($hobbies_users->error->all as $error)
                                                        {
                                                            echo $error."<br>";
                                                        }
                                                }
                                        }
                                    
                                }
                        }
                    //================ </ Recolectando y almacenando Hobbies > ==================




                    //================ < Guardando imagen de perfil  > ==================

                    if(!empty($_POST["image_data"])){
                            $imageData=$_POST["image_data"];
                                $filteredData=str_replace('data:image/png;base64,', '', $imageData);
                                $filteredData=str_replace('data:image/jpeg;base64,', '', $imageData);
                                $filteredData = str_replace(' ', '+', $filteredData);
                                $unencodedData=base64_decode($filteredData);
                                $ext = (!empty($_POST['ext_image_data'])) ? $_POST['ext_image_data'] : '.png';
                                $fileUp = md5($this->users->id).$ext;
                                $ruta = dirname(__FILE__);
                                $ruta = str_replace('front/actualizar_perfil/controllers', '', $ruta);
                                //$ruta = str_replace('\\', '/', $ruta);
                                $success = file_put_contents( $ruta . '/uploads/profile_pictures/' . $fileUp, $unencodedData);
                                if($success){
                                    //inicio de redimensión de imagen
                                    $resize['image_library'] = 'GD2';
                                    $resize['source_image']	= $ruta . '/uploads/profile_pictures/' . $fileUp;
                                    $resize['maintain_ratio'] = TRUE;
                                    $resize['width']	 = 200;
                                    $resize['height']	= 200;
                                    $resize['new_image'] = $ruta . '/uploads/profile_pictures/' . $fileUp;
                                    $this->load->library('image_lib', $resize); 
                                    $this->image_lib->resize();
//                                    if ( ! $this->image_lib->resize())
//                                    {
//                                    $data = array(
//                                    'ok' => false,
//                                    'resize' => $this->image_lib->display_errors()
//                                    );    
//                                    }else{
//                                    $data = array(
//                                    'ok' => true,
//                                    'resize' => true,
//                                    'data' => $this->upload->data()
//                                    );
//                                    }
                                    //fin redimensión de imagen
                                    
                                    $imageModel = new imagen();
                                    $imageModel->path = './uploads/profile_pictures/';
                                    $imageModel->name = $fileUp;
                                    if($imageModel->save()){
                                       
                                        $this->users->imagen_id = $imageModel->id;
                                         //-- Añadido por Juan Cantor
                                        //die("--");
                                        $stream = new users_activity();
                                        $stream->users_id = $this->users->id;
                                        $stream->action = "update";
                                        $stream->module = "users";
                                        $stream->description = $this->users->first_name." ha cambiado su foto de perfil.";
                                        $stream->date=date("Y-m-d");
                                        $stream->save();
                                        //-------------------------
                                        if(!$this->users->save()){
                                            foreach ($this->users->error->all as $error)
                                                        {
                                                            echo $error."<br>";
                                                        }
                                        }

                                    }else{
                                        
                                        foreach ($imageModel->error->all as $error)
                                                        {
                                                            echo $error."<br>";
                                                        }
                                    }
                                }else{
                                    echo 'Error al construir imagen';
                                }
                        }

                    //================ </ Guardando imagen de perfil  > ==================



                    // ==================   <  GUARDANDO DATOS PARA CASO DE USUARIO MÉDICO   >  ============================ //
                    if(isset($_POST['is_doc']))
                        {
                            $this->load->model("users_doc_profile", '', TRUE);
                            $this->users_doc_profile->select('*');
						    $this->users_doc_profile->where('users_id', $user->id);
						    $this->users_doc_profile->get();

                             foreach ($this->users_doc_profile->fields as $key => $value) 
                                {
                                    foreach ($_POST as $key2 => $value2)
                                        {
                                            if($key2 == $value)
                                                {
                                                    if(!empty($value2))
				                                    	{
				                                    		$this->users_doc_profile->$value = $value2;
				                                    	}
                                                }
                                        }
                                }
                                $this->users_doc_profile->save();
                                // =========================   GUARDANDO CAMPOS RELACIONADOS =======================================//

                                        // ======================  ÁREAS DE ESPECIALIDAD =================================
                                		$users_specialties = new users_specialties();
					                    $users_specialties->where('users_id', $this->users->id)->get();
					                    $users_specialties->delete_all();

                                        foreach ($_POST as $key => $value)
                                            {
                                                $specialty_array = explode("specialty_", $key);
                                                if($value == 'true')
                                                    {
                                                        if(isset($specialty_array[1]))    
                                                            {
                                                                $users_specialties = new users_specialties();
                                                                $users_specialties->users_id = $this->users->id;
                                                                $users_specialties->specialties_id = $specialty_array[1];
                                                                if(!$users_specialties->save())
                                                                    {
                                                                        foreach ($users_specialties->error->all as $error)
                                                                            {
                                                                                echo $error."<br>";
                                                                            }
                                                                    }
                                                            }
                                                            
                                                        
                                                    }
                                            }
                                        // ====================== / ÁREAS DE ESPECIALIDAD =================================



                                        // ======================  HORARIOS DE ATENCIÓN =================================
                                        
                                        $users_opening_hours = new users_opening_hours();
			                            $users_opening_hours->where('users_id', $this->users->id)->get();
			                            $users_opening_hours->delete_all();

                                        foreach ($_POST as $key => $value)
                                            {
                                                $opening_hours_array = explode("opening_hour_", $key);
                                                if($value == 'true')
                                                    {
                                                        if(isset($opening_hours_array[1]))    
                                                            {
                                                                $users_opening_hours = new users_opening_hours();
                                                                $users_opening_hours->users_id = $this->users->id;
                                                                $users_opening_hours->opening_hours_id = $opening_hours_array[1];
                                                                if(!$users_opening_hours->save())
                                                                    {
                                                                        foreach ($users_opening_hours->error->all as $error)
                                                                            {
                                                                                echo $error."<br>";
                                                                            }
                                                                    }
                                                            }
                                                        
                                                    }
                                            }
                                        // ====================== / HORARIOS DE ATENCIÓN =================================



                                        // ======================  EPS  =================================

                                        $users_eps = new users_eps();
			                            $users_eps->where('users_id', $this->users->id)->get();
			                            $users_eps->delete_all();    

                                        foreach ($_POST as $key => $value)
                                            {
                                            	

                                                $eps_array = explode("eps_", $key);
                                                if($value == 'true')
                                                    {
                                                        if(isset($eps_array[1]))    
                                                            {
                                                                $users_eps = new users_eps();
                                                                $users_eps->users_id = $this->users->id;
                                                                $users_eps->eps_id = $eps_array[1];
                                                                if(!$users_eps->save())
                                                                    {
                                                                        foreach ($users_eps->error->all as $error)
                                                                            {
                                                                                echo $error."<br>";
                                                                            }
                                                                    }
                                                            }
                                                        
                                                    }
                                            }
                                        // ====================== / EPS =================================


                                // =========================  /GUARDANDO CAMPOS RELACIONADOS =======================================//

                            
                        }

                    // ==================   </ GUARDANDO DATOS PARA CASO DE USUARIO MÉDICO   >  ============================ //

                    die("Datos actualizados correctamente");

                }
            else
                {
                    foreach ($this->users->error->all as $error)
                    {
                        echo $error."<br>";
                    }
                }

                //echo "<pre>";print_r($this->users);echo "</pre>";die("--");
             

    

    
    

    

   // echo "<pre>";print_r($this->users);echo "</pre>";die("--");
    }

    // ################# </ Guardando datos del usuario (mediante AJAX desde el front de la aplicación) > ######################


}
?>