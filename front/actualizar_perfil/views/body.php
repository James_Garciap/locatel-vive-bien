
<style>
	
@media screen and (max-width:980px){
  
  	body {
    	overflow: auto;
  	}
 }

</style>

	<a href="<?php echo base_url(); ?>contenidos" class="cerrar_registrese">
		<img src="<?php echo base_url(); ?>assets/img/iconos/fle_regresar.png" alt="">
	</a>
	<div class="cont_registrese clearfix">
		<div id="tabs">
			<ul>
				<li><a href="#paso-1">1</a></li>
				<li><a href="#paso-2">2</a></li>
			</ul>
			<div id="paso-1" class="ui-tabs-panel">
				<div class="colm-left">
					<form class="registrarse"  method="post" onsubmit="return false" >
						<label>*Nombres</label>
						<?php echo $first_name;?>
					<div class="e10"></div>
						<label>*Apellidos</label>
						<?php echo $last_name?>
					<div class="e10"></div>
					<div class="e10"></div>
					<label>*Fecha de Nacimiento</label> <input
						class="registrarse-input date" type="date" id="birthday"
						name="birthday" placeholder="Fecha de Nacimiento" required
						value="<?php echo $birthday?>">
					<div class="e10"></div>
					<label>*Sexo</label> <select name='sex' id="sex" placeholder="Sexo"
						required>
						<?php
						if($sex == "m")
							{
								$m_selected = "selected";
								$f_selected = "";
							}
						else
							{
								$m_selected = "";
								$f_selected = "selected";
							}
						?>
						<option value="m" <?php echo $m_selected?>>Masculino</option>
						<option value="f" <?php echo $f_selected?>>Femenino</option>
					</select>
					<label>*Correo Electrónico</label>
						<?php echo $email?>
					<div class="e10"></div>
						<label>*Teléfono</label>
						<input class="registrarse-input" type="number" id ="phone" name="phone" placeholder="Teléfono" required style="height:30px" value="<?php echo $phone?>">
					<div class="e10"></div>
					<label>*Ciudad de residencia</label>
						<select id="cities_id" name="cities_id">
							<?php
							foreach ($cities_id_options as $key)
								{
									$selected = " ";
							 		if($cities_id == $key->id)
							 			{
							 				$selected = "selected";
							 			}
									echo "<option value='".$key->id."' ".$selected.">".$key->name."</option>";
								}
							?>
						</select>
					<div class="e10"></div>
						<label>Cambiar contraseña</label>
						<input class="registrarse-input" type="password" id ="new_password" name="new_password" placeholder="Nueva contraseña">
                                            <label>Fotografía</label>
                                                    <div>
                                                        <img id="imageAnt" src="<?php echo base_url() . $imagen->path . $imagen->name; ?>" alt="your image" width="100" height="100" />
                                                        <img id="imagePreview" src="#" alt="your image" width="100" height="100" />
                                                        <div class="clear" ></div>
                                                        <input type='file' id="imgInp" name="imagen" accept="image/*" />

                                                        <span class="f_help">Las imagenes se redimencionaran: 200 * 200px</span>
                                                    </div>
                                                <input type="hidden" value="" name="ext_image_data" id="ext_image_data" />
                                                <input type="hidden" value="" name="image_data" id="image_data" />
                                     </div>

				<div class="colm-right">

					<label>*Ingresa tu estatura (cm)</label>
						<input class="registrarse-input" type="number" name="height" id="height"  placeholder="Estatura" required style="height:30px" value="<?php echo $height?>">
					<div class="e10"></div>
						<label>*Ingresa tu Peso (Kgs)</label>
						<input class="registrarse-input" type="number" name="weight" id="weight" placeholder="Peso" required style="height:30px" value="<?php echo $weight?>">
					<div class="e10"></div>
					<label>*Nivel de actividad física</label>
					<div class="clear"></div>
							<select name="activity_level_id" id="activity_level_id" placeholder="placeholder='nivel de actividad física">
							<?php 
							foreach ($activity_level_id_options as $key ) 
								{
							 		$checked = " ";
							 		if($activity_level_id == $key->id)
							 			{
							 				$checked = "selected";
							 			}
							 		echo "<option value='".$key->id."' ' ".$checked.">".$key->name."</option>";
							 	} 
							?>
							</select>
					<div class="e10"></div>
					<label>Hobbies / pasatiempos</label>
					<div class="clear"></div>
						<!--		<input class="registrarse-input" type="text" name="hobbies" placeholder="Pasatiempos">    	-->
						<?php
						foreach ($hobbies_options as $key)
							{
								$checked = " ";
								foreach ($hobbies as $key2 ) 
									{
										if($key2->hobbies_id == $key->id)
											{
												$checked = "checked";
											}
									}
								echo "<div class='cont_radiocheck'><p>".$key->name."</p><input class='registrarse-input' type='checkbox' name='hobbie_".$key->id."' value='".$key->id."' ".$checked."></div>";
							}
						?>

						<?php
						if(!$is_doc)
							{
								?>
		
						<button class="mover next-tab" type="submit" onClick="return users_update(this.form, 'actualizar_perfil/update', '#user_update_des');" >Actualizar</button>
							<div class="e10"></div>
						<div id="user_update_des" style="display: none;">Datos actualizados correctamente</div>
				</div>
			</div>
			</br></br></br></br></br></br></br></br>
						<?php
							}
						else
							{
						
						?>
		
		<!-- ==============================          DATOS ADICIONALES PARA MEDICOS     ============================== -->
				</div>
			</div>

			<div id="paso-2" class="ui-tabs-panel ui-tabs-hide">
				<div class="colm-left">
					
					<label>Profesión</label>
						<input class="registrarse-input" type="text" placeholder="" name="profession" id="profession" value="<?php echo $profession?>">
					<div class="e10"></div>
					<label>Facultad de medicina donde se graduó</label>
						<?php echo $school?>
					<div class="e10"></div>
					<label>Registro profesional</label>
						<?php echo $profesional_register?>
					<div class="e10"></div>
					<label>Año de graduación</label>
					<select name="graduation_year" id="graduation_year" class="registrarse-input">
						<?php
							$c = 1940;
							$a = date("Y");
							$checked =" ";
							while($c<=$a)
								{
									if($c == $graduation_year)
										{
											$checked = "checked";
										}
									echo "<option value='".$c."' ".$checked.">".$c."</option>";
									$c++;
								}
								
						?>
					</select>
					<div class="e10"></div>
					<label>Especialidad</label>
						<input class="registrarse-input" type="text" placeholder="" name="specialty" id="specialty" value="<?php echo $specialty?>">
					<div class="e10"></div>
					<label>áreas de especialidad</label>
						<?php
							foreach ($specialties_options as $key)
								{
									$checked = " ";
									foreach ($specialties as $key2) 
										{
											if($key2->specialties_id == $key->id)
												{
													$checked = "checked";
												}
										}
									echo "<div class='cont_radiocheck'><p>".$key->name."</p><input class='registrarse-input' type='checkbox' name='specialty_".$key->id."' value='".$key->id."' ".$checked."></div>";
								}
						?>

				</div>

				<div class="colm-right">

					
					<div class="e10"></div>
					<label>Facultad del título</label>
						<input class="registrarse-input" type="text" placeholder="" name="specialty_school" id="specialty_school" value="<?php echo $specialty_school?>">
						<div class="e10"></div>
					<label>Registro médico</label>
						<?php echo $medical_register?>
						<div class="e10"></div>
					<label>Asociación a la que pertenece</label>
						<input class="registrarse-input" type="text" placeholder="" name="society" id="society" value="<?php echo $society?>">
						<div class="e10"></div>
					<label>Lugar donde trabaja</label>
						<span>
							<div id="register_work_company">
								<input class="registrarse-input" type="text" placeholder="" name="work_company1" id="work_company1" value="<?php echo $work_company1?>">
								<input class="registrarse-input" type="text" placeholder="" name="work_company2" id="work_company2" value="<?php echo $work_company2?>">
								<input class="registrarse-input" type="text" placeholder="" name="work_company3" id="work_company3" value="<?php echo $work_company3?>">
								<input class="registrarse-input" type="text" placeholder="" name="work_company4" id="work_company4" value="<?php echo $work_company4?>">
								<input type="hidden" id="work_company_flag" name="work_company_flag" value="2">
							</div>
							<img src="img/iconos/plus.png" style="cursor:pointer" onclick="add_work_company();">
						</span>
						<div class="e10"></div>
					<label>Teléfono de Contacto</label>
						<input class="registrarse-input" type="text" placeholder="" name="phone" id="phone" value="<?php echo $phone?>">
						<div class="e10"></div>
					<label>Dirección de Contacto</label>
						<input class="registrarse-input" type="text" placeholder="" name="address" id="address" value="<?php echo $address?>">
						<div class="e10"></div>
					<label>Horarios de Atención</label>
						<?php
							foreach ($opening_hours_options as $key)
								{
									$checked = " ";
									foreach ($opening_hours as $key2) 
										{
											if($key2->opening_hours_id == $key->id)
												{
													$checked = "checked";
												}
										}
									echo "<div class='cont_radiocheck'><p>".$key->name."</p><input class='registrarse-input' type='checkbox' name='opening_hour_".$key->id."' value='".$key->id."' ".$checked."></div>";
								}
						?>
						<div class="e10"></div>
					<label>Seguros que atiende</label>
						<?php
							foreach ($eps_options as $key)
								{
									$checked = " ";
									foreach ($eps as $key2) 
										{
											if($key2->eps_id == $key->id)
												{
													$checked = "checked";
												}
										}
									echo "<div class='cont_radiocheck'><p>".$key->name."</p><input class='registrarse-input' type='checkbox' name='eps_".$key->id."' value='".$key->id."' ".$checked."></div>";
								}
						?>
						<div class="e10"></div>
					<label>Costo Promedio de Consulta</label>
						<input class="registrarse-input" type="text" placeholder="" name="average_consulting_cost" id="average_consulting_cost" value="<?php echo $average_consulting_cost?>">
					<button class="mover next-tab" type="submit" onClick="return users_update(this.form, 'actualizar_perfil/update', '#user_update_des')" id="submit_register_doc">Actualizar</button>
					<input type="hidden" name="is_doc" id="is_doc" value="true">
				<div id="user_update_des" style="display: none;">Datos actualizados correctamente</div>

				</div>
			</div>
								
								</br></br></br></br></br></br></br></br>
		<!-- ==============================         /DATOS ADICIONALES PARA MEDICOS     ============================== -->
		<?php 
	}	
?>


	
				</form>

		</div>
	</div>


<?php //include("footer"); ?>
<?php //include("footer2"); ?>


