<?php
class Site extends Front_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		return $this->build();
	}


    // ################# < Verificando si existe el correo electrónico ingresado para el registro de usuario > ######################

    public function email_exist($email)
    {
        $this->load->model("users");
        $this->users->select('id, email');
        $this->users->where('email', $email);
        $this->users->get();
        if($this->users->id)
            {
                return true;
            }
        else
            {
                return false;
            }  
    }


    // ################# </ Verificando si existe el correo electrónico ingresado para el registro de usuario > ######################




    // ################# < Guardando datos del usuario (mediante AJAX desde el front de la aplicación) > ######################

     public function register() {

//echo "<pre>";print_r($_POST['data']['sex']);echo "</pre>";die("--");

        $this->load->model("users", '', TRUE);
        $this->load->model("imagen", '', TRUE);
        $_POST = $_POST['data'];



//========================================<  Captcha >==========================================//
        // First, delete old captchas
$expiration = time()-7200; // Two hour limit
$this->db->query("DELETE FROM cms_captcha WHERE captcha_time < ".$expiration);  

// Then see if a captcha exists:
$sql = "SELECT COUNT(*) AS count FROM cms_captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
$binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
$query = $this->db->query($sql, $binds);
$row = $query->row();
if ($row->count == 0)
    {
        die("El texto ingresado no coincide con el de la imagen");
    }
//========================================< / Captcha ==========================================//
//$ruta = dirname(__FILE__);
//$ruta = str_replace('front/site/controllers', '', $ruta);
//$ruta = str_replace('\\', '/', $ruta);
//die("ruta: ".$ruta);

 if(empty($_POST["image_data"]))
    {
        die("Debe adjuntar una imagen");
    }

    if($this->email_exist($_POST['email']))
        {
            die("Ya existe una cuenta asociada a estre correo electrónico.");
        }
    else
        {
            
            // ============= < Sincronizando el post con el objeto de usuario > ============
            foreach ($this->users->fields as $key => $value) 
                {
                    //echo $c;
                    foreach ($_POST as $key2 => $value2)
                        {
                            if($key2 == $value)
                                {
                                    $this->users->$value = $value2;
                                }
                        }
                }
            // ============= </ Sincronizando el post con el objeto de usuario > ============

            $this->users->full_name = '0';
            $this->users->username = str_replace(" ","",microtime());
            $password = substr( md5(microtime()), 1, 8);
            $this->users->salt = substr( md5(microtime()), 1, 10);
            $this->users->active = "1";
            $this->users->created_on = time();
            $this->users->password = $this->ACL->hash_password($password, $this->users->salt);
            $this->users->status = 1;
            $var = $this->users->save();             
            if ($var)
                {

                    //================ < Asignando una rutina > ==================
                    $routines = new routines();
                    $routines->select('id')->limit(1)->order_by('rand()')->get();
                    $users_routines = new users_routines();
                    $users_routines->routines_id = $routines->id;
                    $users_routines->users_id = $this->users->id;
                    //================ < Asignando una rutina > ==================


                    //================ < Recolectando y almacenando Hobbies > ==================
                    foreach ($_POST as $key => $value)
                        {
                            $hobbie_array = explode("hobbie_", $key);
                            if($value == 'true')
                                {
                                    if(isset($hobbie_array[1]))    
                                        {
                                            $hobbies_users = new users_hobbies();
                                            $hobbies_users->users_id = $this->users->id;
                                            $hobbies_users->hobbies_id = $hobbie_array[1];
                                            if(!$hobbies_users->save())
                                                {
                                                    foreach ($hobbies_users->error->all as $error)
                                                        {
                                                            echo $error."<br>";
                                                        }
                                                }
                                        }
                                    
                                }
                        }
                    //================ </ Recolectando y almacenando Hobbies > ==================


                    //================ < Guardando imagen de perfil  > ==================
                        
                        if(!empty($_POST["image_data"])){
                            $imageData=$_POST["image_data"];
                                $filteredData=str_replace('data:image/png;base64,', '', $imageData);
                                $filteredData=str_replace('data:image/jpeg;base64,', '', $imageData);
                                $filteredData = str_replace(' ', '+', $filteredData);
                                $unencodedData=base64_decode($filteredData);
                                $ext = (!empty($_POST['ext_image_data'])) ? $_POST['ext_image_data'] : '.png';
                                $fileUp = md5($this->users->id).$ext;
                                $ruta = dirname(__FILE__);
                                $ruta = str_replace('front/site/controllers', '', $ruta);
                                //$ruta = str_replace('\\', '/', $ruta);
                                $success = file_put_contents( $ruta . '/uploads/profile_pictures/' . $fileUp, $unencodedData);
                                if($success){
                                    //inicio de redimensión de imagen
                                    $resize['image_library'] = 'GD2';
                                    $resize['source_image']	= $ruta . '/uploads/profile_pictures/' . $fileUp;
                                    $resize['maintain_ratio'] = TRUE;
                                    $resize['width']	 = 200;
                                    $resize['height']	= 200;
                                    $resize['new_image'] = $ruta . '/uploads/profile_pictures/' . $fileUp;
                                    $this->load->library('image_lib', $resize); 
                                    $this->image_lib->resize();
//                                    if ( ! $this->image_lib->resize())
//                                    {
//                                    $data = array(
//                                    'ok' => false,
//                                    'resize' => $this->image_lib->display_errors()
//                                    );    
//                                    }else{
//                                    $data = array(
//                                    'ok' => true,
//                                    'resize' => true,
//                                    'data' => $this->upload->data()
//                                    );
//                                    }
                                    //fin redimensión de imagen
                                    
                                    $imageModel = new imagen();
                                    $imageModel->path = './uploads/profile_pictures/';
                                    $imageModel->name = $fileUp;
                                    if($imageModel->save()){
                                        $this->users->imagen_id = $imageModel->id;
                                        if(!$this->users->save()){
                                            foreach ($this->users->error->all as $error)
                                                        {
                                                            echo $error."<br>";
                                                        }
                                        }
                                    }else{
                                        foreach ($imageModel->error->all as $error)
                                                        {
                                                            echo $error."<br>";
                                                        }
                                    }
                                }else{
                                    echo 'Error al construir imagen';
                                }
                        }else{
                            die('Debe adjuntar una imagen');
                        }
                    //================ </ Guardando imagen de perfil  > ==================

                    // ==================   <  GUARDANDO DATOS PARA CASO DE USUARIO MÉDICO   >  ============================ //
                    if(isset($_POST['is_doc']))
                        {
                             $this->load->model("users_doc_profile", '', TRUE);
                             foreach ($this->users_doc_profile->fields as $key => $value) 
                                {
                                    foreach ($_POST as $key2 => $value2)
                                        {
                                            if($key2 == $value)
                                                {
                                                    $this->users_doc_profile->$value = $value2;
                                                }
                                        }
                                }
                                $this->users_doc_profile->users_id=$this->users->id;;
                                $this->users_doc_profile->save();
                                // =========================   GUARDANDO CAMPOS RELACIONADOS =======================================//

                                        // ======================  ÁREAS DE ESPECIALIDAD =================================
                                        foreach ($_POST as $key => $value)
                                            {
                                                $specialty_array = explode("specialty_", $key);
                                                if($value == 'true')
                                                    {
                                                        if(isset($specialty_array[1]))    
                                                            {
                                                                $users_specialties = new users_specialties();
                                                                $users_specialties->users_id = $this->users->id;
                                                                $users_specialties->specialties_id = $specialty_array[1];
                                                                if(!$users_specialties->save())
                                                                    {
                                                                        foreach ($users_specialties->error->all as $error)
                                                                            {
                                                                                echo $error."<br>";
                                                                            }
                                                                    }
                                                            }
                                                            
                                                        
                                                    }
                                            }
                                        // ====================== / ÁREAS DE ESPECIALIDAD =================================



                                        // ======================  HORARIOS DE ATENCIÓN =================================
                                        foreach ($_POST as $key => $value)
                                            {
                                                $opening_hours_array = explode("opening_hour_", $key);
                                                if($value == 'true')
                                                    {
                                                        if(isset($opening_hours_array[1]))    
                                                            {
                                                                $users_opening_hours = new users_opening_hours();
                                                                $users_opening_hours->users_id = $this->users->id;
                                                                $users_opening_hours->opening_hours_id = $opening_hours_array[1];
                                                                if(!$users_opening_hours->save())
                                                                    {
                                                                        foreach ($users_opening_hours->error->all as $error)
                                                                            {
                                                                                echo $error."<br>";
                                                                            }
                                                                    }
                                                            }
                                                        
                                                    }
                                            }
                                        // ====================== / HORARIOS DE ATENCIÓN =================================



                                        // ======================  EPS  =================================
                                        foreach ($_POST as $key => $value)
                                            {
                                                $eps_array = explode("eps_", $key);
                                                if($value == 'true')
                                                    {
                                                        if(isset($eps_array[1]))    
                                                            {
                                                                $users_eps = new users_eps();
                                                                $users_eps->users_id = $this->users->id;
                                                                $users_eps->eps_id = $eps_array[1];
                                                                if(!$users_eps->save())
                                                                    {
                                                                        foreach ($users_eps->error->all as $error)
                                                                            {
                                                                                echo $error."<br>";
                                                                            }
                                                                    }
                                                            }
                                                        
                                                    }
                                            }
                                        // ====================== / EPS =================================


                                // =========================  /GUARDANDO CAMPOS RELACIONADOS =======================================//

                            
                        }

                    // ==================   </ GUARDANDO DATOS PARA CASO DE USUARIO MÉDICO   >  ============================ //

                    // ==================   <   ASIGNANDO LA DIETA CORRESPONDIENTE  >  ============================ //
                    //echo "<pre>";print_r($this->users);die("</pre>");
                    $date = $this->users->birthday;  
                    $days = explode("-", $date);
                    $days = mktime(0,0,0,$days[1],$days[2],$days[0]);
                    $age = (int)((time()-$days)/31556926 );
                    if($this->users->sex=='m')
                        {
                            $imb = 66+(13.75 * $this->users->weight) + (5.08 * $this->users->height) - (6.78 * $age);
                        }
                    else
                        {
                           $imb = 665 + (9.56 * $this->users->weight) + (1.85* $this->users->height) - (4.68 * $age);
                        }
                    if ($imb<1500)
                        {
                            $cat = 1200;
                        }
                    elseif ($imb<1800) 
                        {
                            $cat = 1500;
                        }
                    else
                        {
                            $cat = 1800;
                        }
                    $diets_categories = new  diets_categories();
                    $diets_categories->select('id');
                    $diets_categories->where('description', $cat)->get();
                    $diets = new diets();
                    $diets->select('id');
                    $diets->where('diets_categories_id', $diets_categories->id);
                    $diets->order_by('rand()');
                    $diets->get();
                    $users_diets = new users_diets();
                    $users_diets->users_id = $this->users->id;
                    $users_diets->diets_id = $diets->id;
                    $users_diets->save();
                    // ==================   < / ASIGNANDO LA DIETA CORRESPONDIENTE  >  ============================ //


                    // ==================   <  VERIFICANDO CORREO ELECTRÓNICO CON LA CONTRASEÑA GENERADA >  ============================ //
                    $this->load->library('email');
                    $this->email->clear();
                    $this->email->protocol = 'smtp';
                    $this->_data = array(
                        'username' => $this->users->first_name,
                        'email' => $this->users->email,
                        'password' => $password
                    );
                    $html = $this->view('emails/nuevo_usuario');
                    $this->email->from('imaginamos.prueba@gmail.com', 'CMS Imaginamos');
                    $this->email->to($this->users->email);
                    $this->email->subject('Bienvenido ' . $this->users->first_name);
                    $this->email->message($html);
                    //echo "<pre>";print_r($this->email);echo "</pre>";
                    if($this->email->send())
                        {
                            echo "Se ha enviado un mensaje de verificación a ".$this->users->email;
                        }
                    else
                        {
                            echo "<pre>";print_r($this->email);echo "</pre>";
                            echo "Su cuenta ha sido creada pero no se ha podido enviar el mensaje de verificación a ".$this->users->email."<br>Por favor pongase en contacto el departamento de atención al cliente.";
                        }

                    // ==================   </ VERIFICANDO CORREO ELECTRÓNICO CON LA CONTRASEÑA GENERADA >  ============================ //

                }
            else
                {
                    foreach ($this->users->error->all as $error)
                    {
                        echo $error."<br>";
                    }
                }

                //echo "<pre>";print_r($this->users);echo "</pre>";die("--");
        }     

    

    
    

    

   // echo "<pre>";print_r($this->users);echo "</pre>";die("--");
    }

    // ################# </ Guardando datos del usuario (mediante AJAX desde el front de la aplicación) > ######################




    // ################# < Log in > ######################
    public function login()
        {
            //echo "<pre>";print_r($_POST);echo "</pre>";die("--");
            $user = new users();
            $user->where('email', $_POST['email'])->get();
            if($user->status == 0)
                {
                    //die("status 0");
                    header("location: ".base_url()."?deny&inactive=1&mail");
                    die();
                }
                
            //--------------------------
            $remember = true;
            $ok = $this->ion_auth->login($_POST['email'], $_POST['password'], $remember);
            if ($ok) 
                {
                    $user->full_name="0";
                    $user->last_login = time();
                    $user->save();
                    header("location: ".base_url()."contenidos");
                } 
                else 
                {
                    //die("Datos incorrectos :(");
                    header("location: ".base_url()."?deny&mail=".$_POST['email']);
                }
        }
    // ################# < /Log in > ######################



    // ################# < Log out > ######################
    public function logout()
        {
            $this->ion_auth->logout();
            header("location: ".base_url());
        }
    // ################# < /Log out > ######################





    // ################# < Log out > ######################
    public function recover()
        {
            //die("recuperand contraseña---");
            $_POST['email'] = $_POST['data']['recover_email'];             
            $password = substr( md5(microtime()), 1, 8);
            $salt = substr( md5(microtime()), 1, 10);
            $new_password =  $this->ACL->hash_password($password, $salt);
            //--------------------------------------------------

            if($this->email_exist($_POST['email']))
                {
                    $this->load->library('email');
                    $this->email->clear();
                    $this->email->protocol = 'smtp';
                    $this->_data = array(
                        'password' => $password,
                        'email' => $_POST['email'],
                    );
                    $html = $this->view('emails/recover');
                    $this->email->from('imaginamos.prueba@gmail.com', 'CMS Imaginamos');
                    $this->email->to($_POST['email']);
                    $this->email->subject('Recuperar contraseña.');
                    $this->email->message($html);
                    //echo "<pre>";print_r($this->email);echo "</pre>";

                    if($this->email->send())
                        {
                            $this->load->model("users");
                            $this->users->select('*');
                            $this->users->where('email', $_POST['email']);
                            $this->users->get_by_id($this->users->id);
                            $this->users->salt = $salt;
                            $this->users->password = $new_password;
                            $this->users->full_name = $this->users->first_name. "" . $this->users->last_name;
                            //echo "<pre>";print_r($this->users);echo "</pre>";
                           if($this->users->save())
                                {
                                    die("Se ha enviado un mensaje con la nueva contraseña a ".$_POST['email']);
                                }
                            else
                                {
                                    foreach ($this->users->error->all as $error)
                                            {
                                                echo $error."<br>";
                                            }

                                }
                        }
                    else
                        {
                            //echo "<pre>";print_r($this->email);echo "</pre>";
                            die("No se ha podido enviar la nueva contraseña a ".$_POST['email']."<br>Por favor pongase en contacto el departamento de atención al cliente.");
                        }
                }
            else
                {
                    die("La cuenta de correo ingresada no existe en nuestra base de datos");
                }
        }
    // ################# < /Log out > ######################


}
?>