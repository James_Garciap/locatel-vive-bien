<section>
  <article>
    <div class="fondos-left" style="background: #EE4E11;">
      
      <!-- <div class="mask-left clearfix"> <a href="actualizar_perfil" class="actualizar pull-right llama_modal">Actualizar información</a> -->
      <div class="mask-left clearfix"> <a href="actualizar_perfil" class="actualizar pull-right llama_modal">Actualizar información</a>
      
        <h1>Mi perfil</h1>
        <div class="e10"></div>
        <div class="col-left50 pull-left inline">
          <div class="miperfil-img pull_left inline">
            <figure><img src="<?php echo base_url(); ?>uploads/profile_pictures/<?php echo $profile_picture_name?>"></figure>
            <?php echo $comments_notification_count?> </div>
          <div class="e20"></div>
          <div class="fondo-articulos pull_left">
            <h2 class="ribon-verde">Mi Información</h2>
            <div class="e10"></div>
            <p><strong>Nombre: </strong><?php echo $user_profile_name?></p>
            <p><strong>Edad: </strong><?php echo $user_profile_age?></p>
            <p><strong>Sexo: </strong><?php echo $user_profile_sex?></p>
            <p><strong>Altura: </strong><?php echo $user_profile_height?></p>
            <p><strong>Peso: </strong><?php echo $user_profile_weight?></p>
            <div class="e10 clear"></div>
          </div>
        </div>
        <div class="col-left50 pull-right inline">
          <div class="fondo-blanco">
            <h2 class="ribon-negro ">Mi estado: <?php echo $profile_status_weight?></h2>
            <div class="e5 clear"></div>
            <p class="text_plan">Plan a largo plazo.</p>
            <p class="text_plan"><strong>Fecha de inicio:</strong> <?php echo $user_profile_created_on;?></p>
            <p class="text_plan"><strong>Estado:</strong> Activo </p>
          </div>
        </div>
      </div>
    </div>
  </article>
</section>
