/**
 * 
 *
 */
$(function (){
	
	/**
	 * 
	 */
	var ingresarArticulo = $("#registrar_articulo");
	ingresarArticulo.click(function (){
		
		var titulo = $("#titulo_articulo");
		var articulo = $("#cuerpo_articulo");
		var file = $("#archivo_adjunto");
		
		/**
		 * 
		 */
		$.ajax({
            type: "POST",
            url: 'ingreso_articulos2/ingresarArticulos',
            data: {
               t:titulo.val(),
               a:articulo.val(),
               f:file.val()
            },
            success: function(respuesta) {
            	
        		var alerts = $("#alerts");
        		
        		var ok = '<div class="alert alert-success" id="alertOk"'+
							'style="display: block;">'+
							'<strong>Inserci&oacute;n correcta</strong> El art&iacute;culo se'+
							'insert&oacute; correctamente.'+
						'</div>';
        		
				var err = '<div class="alert alert-danger" id="alertBad"'+
						'style="display: block;">'+
						'<strong>Inserci&oacute;n fallida</strong> Hubo un fallo al'+
						'insertar.'+
					'</div>';
            	
            	if(respuesta)
            		alerts.html(ok);
            	else
            		alerts.html(err);
            	
            	$.ajax({
                    type: "POST",
                    url: 'ingreso_articulos2/consultarArticulos',
                    success: function(json) {
                    	
                        $("#listaArticulos").html(json);
                    },
                    beforeSend: function() {

                    }
                });//here
            	
            },
            beforeSend: function() {

            }
        });
		
	});
});
