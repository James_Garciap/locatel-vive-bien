/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function() {
    /**
     * Ingreso y validación de formulas médicas
     * @author Julian Molina
     * @version 1.0 
     */
    var fmGuardar = $("#fm_guardar");
    fmGuardar.click(function() {

        var fm_n_m = $("#fm_nombre_medicamento");
        var fm_t_u = $("#fm_tipo_unidad");
        var fm_n_u = $("#fm_numero_unidades");
        var fm_p = $("#fm_periodicidad");

        if (fm_n_m.val() != '' && fm_t_u.val() != '' && fm_n_u.val() != '' && fm_p.val() != '') {
            $.ajax({
                type: "POST",
                url: 'contenidos/registro_formula_medica',
                data: {
                    fm_n_m: fm_n_m.val(),
                    fm_t_u: fm_t_u.val(),
                    fm_n_u: fm_n_u.val(),
                    fm_p: fm_p.val()
                },
                dataType: 'json',
                success: function(json) {

                    alert(json.respuesta);
                    
                    consultar_medicamentos()
                },
                beforeSend: function() {

                }
            });

            /**
             * 
             */
        } else {
            alert("Los campos no fueron registrados correctamente");
        }
    });

    $("#fm_consulta_medicamentos").html("contenido");

    consultar_medicamentos();
});

function consultar_medicamentos() {

    $.ajax({
        type: "POST",
        url: 'contenidos/consulta_formula_medica',
        dataType: 'json',
        success: function(json) {

            var body = "";
            var contenido = "";

            $.each(json, function(i, item) {
                contenido += '<div class="medicamentos">' +
                        '<p class="text"><strong>Medicamentos</strong>: ' +
                        item.m +
                        '</p>' +
                        '<div class="medicamentos-opciones">' +
                        '<a id="fm_ver' + i + '"><i class=" icon-eye-open"></i> Ver</a>' +
                        '|' +
                        '<a id="fm_del' + i + '"><i class=" icon-remove-circle"></i> Eliminar</a>' +
                        '</div>' +
                        '<div class="clear"></div>' +
                        "</div>";

                $("#fm_ver" + i).die("click");
                $("#fm_ver" + i).live("click", function() {
                    var table = "<table style='margin:0 auto; padding-left:15px;' class='table'>" +
                            "<tr>" +
                            "<td><strong>Medicamento</strong></td>" +
                            "<td>" + item.m + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td><strong>Tipo de unidad</strong></td>" +
                            "<td>" + item.u + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td><strong>Número de unidades</strong></td>" +
                            "<td>" + item.n + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td><strong>Periodicidad</strong></td>" +
                            "<td>" + item.p + "</td>" +
                            "</tr>" +
                            "</table>";
                    $("#fm_div_ver").html(table);
                    $("#fm_div_ver").dialog({
                        modal: true,
                        buttons: {
                            Ok: function() {
                                $(this).dialog("close");
                            }
                        }
                    });
                });

                /**
                 * Eliminación
                 */
                $("#fm_del" + i).die("click");
                $("#fm_del" + i).live("click", function() {

                    var r = confirm("Está seguro que quiere eliminar el registro?");

                    if (r) {
                        $.ajax({
                            type: "POST",
                            url: 'contenidos/eliminar_formula_medica',
                            data: {
                                id: item.id
                            },
                            success: function(json) {
                                if (json)
                                    alert("Se eliminó correctamente");
                                else
                                    alert("Fallo al eliminar");
                                
                                consultar_medicamentos();
                            }
                        });
                    }
                });
            });//Fin each

            $("#fm_consulta_medicamentos").html(contenido);
        },
        beforeSend: function() {

        }
    });//here
}

