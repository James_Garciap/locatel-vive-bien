/**
 * Se cambia Librería a $ NoConfilct para evitar incompatibilidad con otras librerías.
 * @author Julian Molina <jualien@misena.edu.co>
 * @version 1.0 
 */
$(function (){

	ver_medicos("0, 4", 123);
	paginador(123);
	
	/**
	 * Evento click de directorio medico
	 */
	var directorio_medico = $("#filtrar_directorio_medico");
	directorio_medico.click(function (){
		paginador(12345);
	});
});

/**
 *
 */
function paginador(desk){

	var eps = $("#eps_options");
	var prepaid = $("#prepagada");
	var cities = $("#cities_id");
	var specialities = $("#specialties_options");
	var array = new Array();

	if(desk != 123){
		array = {
				eps:eps.val(),
				prepaid: prepaid.val(),
				cities:cities.val(),
				specialities:specialities.val()
				};
	}else{
		array = {};
	}

	$.ajax({
			type:"POST",
			url: 'contenidos/paginador_numero_items',
			data:array,
			dataType:'json',
			success: function (respuesta){

				$("#buscandoBtn").val("Buscar");

				var items = $("#items_paginador");
				items.html("");
				var listas = "";

				$.each(respuesta, function (i, item){
					$("#listas"+i).die("click");
				});
					
				$.each(respuesta, function (i, item){
					listas += "<li id='listas"+i+"'><a class='active'>"+i+"</a></li>";

					$("#listas"+i).live("click", function (){
						ver_medicos(item.ids, "other");
					});
				});

				items.html(listas);

			},
			beforeSend: function (){
				$("#buscandoBtn").val("Buscando...");
			},
			error:function (){

			}
	});
	
}

function ver_medicos(ids, dinamic){

		var div_medicos = $("#divMedicos");
		var html = "";
		var tableDoc = "";

		if(dinamic == 123){
			dinamic = {};
		}else{
			dinamic = {
				ids: ids
			}
		}

		$.ajax({
			type:"POST",
			url: 'contenidos/consultarMedicos',
			data:dinamic,
			dataType:'json',
			success: function (json){

				div_medicos.html("");

				/**
				 * Se recorren los médicos por los filtros de búsqueda.
				 * @version 23 - 02 - 2014
				 */
				$.each(json, function (i, item){

						/**
						 * Se generan las ventanas en la vista derecha de directorio médico
						 * @version 23 - 02 - 2014
						 */
						html += '<a id="divFoto'+i+'" class="foto-medico inline">'+
									'<img src="'+item.path+item.name_path+'">'+
									'<div class="over-perfil-medico">'+
										'<p>'+
											'<strong>'+item.name+'</strong>'+
										'</p>'+
										'<p>'+
											item.prof+
										'</p>'+
									'</div>'+
								'</a>';

						/**
						 * Evento click a div de medico, se despliega Dialog $.
						 * @version 23 - 02 - 2014
						 */
						$("#divFoto"+i).die("click");
						$("#divFoto"+i).live("click", function (){

							$.ajax({
								type:"POST",
								url: 'contenidos/consultarMedicosDialog',
								data:{
									medicos: item.id_medico
								},
								dataType:'json',
								success: function (json){

									var dialogHtml = "";
									$("#contDialogMedicos").empty();

									$.each(json, function (i, item){

										dialogHtml = "<table class='table'>"+
														"<thead>"+
															"<tr>"+
																"<td>"+
																	"<strong>Médico: </strong>"+
																"</td>"+
																"<td>"+
																	item.name+
																"</td>"+
															"</tr>"+						
														"</thead>"+
														"<tbody>"+
															"<tr>"+
																"<td >"+
																	"<strong>Profesión</strong>"+
																"</td>"+
																"<td>"+
																	item.profession+
																"</td>"+
															"</tr>"+
															"<tr>"+
																"<td >"+
																	"<strong>Especialidad</strong>"+
																"</td>"+
																"<td >"+
																	item.speciality+
																"</td>"+
															"</tr>"+
															"<tr>"+
																"<td >"+
																	"<strong>Email</strong>"+
																"</td>"+
																"<td >"+
																	item.email+
																"</td>"+
															"</tr>"+
															"<tr>"+
																"<td >"+
																	"<strong>Teléfono</strong>"+
																"</td>"+
																"<td >"+
																	item.phone+
																"</td>"+
															"</tr>"+
															"<tr>"+
																"<td >"+
																	"<strong>Año de graduación</strong>"+
																"</td>"+
																"<td >"+
																	item.graduation_year+
																"</td>"+
															"</tr>"+
															"<tr>"+
																"<td >"+
																	"<strong>Registro médico</strong>"+
																"</td>"+
																"<td >"+
																	item.medical_register+
																"</td>"+
															"</tr>"+
															"<tr>"+
																"<td >"+
																	"<strong>Contácto</strong>"+
																"</td>"+
																"<td><textarea id='mensajeMedico"+i+"'></textarea></td>"+
															"</tr>"+
															"<tr>"+
																"<td colspan='2'>"+
																	"<input id='enviarEmail"+i+"' class='btn' type='button' value='Enviar email' />"+
																"</td>"+
															"</tr>"+
														"</tbody>"+
													 "</table>";

											$("#mensajeMedico"+i).remove();
											$("#contDialogMedicos").html("<div id='divDialogMedicos'>"+dialogHtml+"</div>");
											/**
											 * Evento para el envío de email hacía el médico
											 * 
											 */
											$("#enviarEmail"+i).die("click");
											$("#enviarEmail"+i).live("click",function (){

												var r = confirm("Está seguro que desea enviar el mensaje?");

												if (r == true)
												{
													$.ajax({
														type:"POST",
														url: 'contenidos/enviarEmailMedico',
														data:{
															medicos: item.id_medico,
															email: item.email,
															nombre: item.name,
															mensaje: $("#mensajeMedico"+i).val()
														},
														success: function (rta){

															$("#enviarEmail"+i).attr("enable","enable").val("Enviar");

															if(rta){
																alert("Enviado¡¡¡");
															}else{
																alert("Hubo un fallo al enviar el correo, intenta otra vez.");
															}
														},
														beforeSend: function (){
															$("#enviarEmail"+i).attr("disable","disable").val("Enviando email...");
														}
													});
												}

											});

									});//Fin each médicos
									
									$("#divDialogMedicos").dialog({								
												resizable: true,
												width:500,
												height:'auto',
												modal: true,
												buttons: {
													"cerrar": function() {
														$(this).dialog("close");
													}
												},
												Cancel: function() {
													$(this).dialog("close");
												}
									});
								},
								beforeSend:function (){

								},
								error: function (e){
										console.log("el error_ ->"+e);
								}
							});
						});
				});

				div_medicos.html(html);
			},
			beforeSend: function (){

			},
			error: function (){

			}
		});
}
