/**
 * 
 */
$(function (){
	
	var alertError = $("#alertBad");
	var alertOk = $("#alertOk");
	
	reportePeso();
	
	/**
	 * 
	 */
	var reportar_actividad = $("#reportar_rutina");
	reportar_actividad.click(function (){
		
		var peso = $("#peso_actual");
		var minutos = $("#minutos");
		var ejercicios = $("#ejercicio_realizado");
		
		$.ajax({
            type: "POST",
            url: 'modales/registrarRutina',
            data: {
               p:peso.val(),
               m:minutos.val(),
               e:ejercicios.val()
            },
            success: function(respuesta) {
            	
            	console.log("entra 1");
            	
            	if(respuesta)
            		alertOk.show();
            	else
            		alertError.show();
            	
            	
            	reportePeso();
            	console.log("entra 2");
            	
            },
            beforeSend: function() {
            	
            }
        });
	});
	
	
});

function reportePeso(){
	
    $.ajax({
        type: "POST",
        url: 'modales/consultarPeso',
        dataType:'json',
        data: {
        	
        },
        success: function(respuesta) {
        	
        	/**
        	 * 
        	 */
        	var si = -20;
        	
            $('#contenidoChart').highcharts({
                title: {
                    text: 'Avance físico',
                    x: si
                },
                subtitle: {
                    text: 'Locatel.sa',
                    x: -40
                },
                xAxis: {
                    categories: respuesta.dias
                },
                yAxis: {
                    title: {
                        text: 'Peso'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: 'Kg'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [{
                    name: respuesta.nombre,
                    data: respuesta.peso
                }]
            });
        },
        beforeSend: function() {
        	
        },
        error: function (){
        	console.log("Error al consultar reporte");
        }
    });
}