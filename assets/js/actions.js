
$(document).ready(function() {

    /*Menu home*/

    $('ul.menu-home li:first').before($('ul.menu-home li:last'));


    $('.arrow_abajo1').click(function(event) {
        event.preventDefault();
        var item_width = $('ul.menu-home li').outerWidth();
        var left_indent = parseInt($('ul.menu-home').css('left')) - item_width;
        $('ul.menu-home:not(:animated)').animate({'left': left_indent}, 500, function() {
            $('ul.menu-home li:last').after($('ul.menu-home li:first'));
            $('ul.menu-home').css({'left': -item_width});
        });
    });


    $('.arrow_abajo2').click(function(event) {
        event.preventDefault();
        var item_width = $('ul.menu-home li').outerWidth();
        var left_indent = parseInt($('ul.menu-home').css('left')) + item_width;
        $('ul.menu-home:not(:animated)').animate({'left': left_indent}, 500, function() {
            $('ul.menu-home li:first').before($('ul.menu-home li:last'));
            $('ul.menu-home').css({'left': -item_width});
        });
    });




    /*Mover menu home con scroll*/

    var mousewheelevt = (/Firefox/i.test(navigator.userAgent)) ? "DOMMouseScroll" : "mousewheel" //FF doesn't recognize mousewheel as of FF3.x
    $('body').bind(mousewheelevt, function(e) {

        var evt = window.event || e //equalize event object
        evt = evt.originalEvent ? evt.originalEvent : evt; //convert to originalEvent if possible
        var delta = evt.detail ? evt.detail * (-40) : evt.wheelDelta //check for detail first, because it is used by Opera and FF

        if (delta > 0) {

            $(".arrow_abajo1").click();

        }
        else {

            $(".arrow_abajo2").click();
        }
    });


    /*loader*/

    $(window).bind("load", function() {
        jQuery("#loader").fadeOut("slow");
    });

    /*footer*/

    $('.slide-up').toggle(
            function() {
                $(".slide-up").attr('class', 'slide-down');
                $("footer").animate({height: '115'}, "600");
            },
            function() {
                $(".slide-down").attr('class', 'slide-up');
                $("footer").animate({height: '65'}, "1200");
            }
    );


    $(".miplan").click(function(event) {
        event.preventDefault();
        $(".dieta_izq").css("left", "0");
        $(".dieta_der").css("right", "0");
    });

    $(".rutina").click(function(event) {
        event.preventDefault();
        $(".rutina_izq").css("left", "0");
        $(".rutina_der").css("right", "0");
    });

    $("a.cerrar_emerlados").click(function(event) {
        event.preventDefault();
        $(".dieta_izq").css("left", "-55%");
        $(".dieta_der").css("right", "-55%");
        $(".rutina_izq").css("left", "-55%");
        $(".rutina_der").css("right", "-55%");
    });

    $("#responsive-menu-button").click(function(event) {
        event.preventDefault();
        $('.bgcom').css({'opacity': '1', 'display': 'block'});
        $("nav.nav_responsivo").css("left", "0");
    });

    $(".bgcom, .cerrar_menumod, nav.nav_responsivointernas ul li a").click(function(event) {
        event.preventDefault();
        $(".bgcom").css({'opacity': '0', 'display': 'none'});
        $("nav.nav_responsivo").css("left", "-55%");
    });

    $("#responsive-menu-buttonperfil").click(function(event) {
        event.preventDefault();
        $('.bgcom').css({'opacity': '1', 'display': 'block'});
        $(".footer_responsivo").css({'right': '0', 'opacity': '1'});
    });

    $(".bgcom, .cerrar_footermod").click(function(event) {
        event.preventDefault();
        $(".bgcom").css({'opacity': '0', 'display': 'none'});
        $(".footer_responsivo").css("right", "-55%");
    });

    /*navegacion*/


    $(window).resize(function() {

        var cont1 = $('#cont1');
        var cont1_offset = cont1.offset();
        var cont2 = $('#cont2');
        var cont2_offset = cont2.offset();
        var cont3 = $('#cont3');
        var cont3_offset = cont3.offset();
        var cont4 = $('#cont4');
        var cont4_offset = cont4.offset();
        var cont5 = $('#cont5');
        var cont5_offset = cont5.offset();
        var cont6 = $('#cont6');
        var cont6_offset = cont6.offset();
        var cont7 = $('#cont7');
        var cont7_offset = cont7.offset();
        var cont8 = $('#cont8');
        var cont8_offset = cont8.offset();
        var cont9 = $('#cont9');
        var cont9_offset = cont9.offset();

        $(window).on('scroll', function() {

            if ($('#container').length) {

                if ($(window).scrollTop() >= cont1_offset.top - 100) {
                    $("ul.lista_navint li a").removeClass('activo_nav');
                    $("ul.lista_navint li a:eq(0)").addClass('activo_nav');
                    $("nav.nav_responsivo ul li a").removeClass('activo_navresp');
                    $("nav.nav_responsivo ul li a:eq(0)").addClass('activo_navresp');
                }
                if ($(window).scrollTop() >= cont2_offset.top - 100) {
                    var conteo = 2;
                    $("ul.lista_navint li a").removeClass('activo_nav');
                    $("ul.lista_navint li a:eq(1)").addClass('activo_nav');
                    $("nav.nav_responsivo ul li a").removeClass('activo_navresp');
                    $("nav.nav_responsivo ul li a:eq(1)").addClass('activo_navresp');
                }
                if ($(window).scrollTop() >= cont3_offset.top - 100) {
                    $("ul.lista_navint li a").removeClass('activo_nav');
                    $("ul.lista_navint li a:eq(2)").addClass('activo_nav');
                    $("nav.nav_responsivo ul li a").removeClass('activo_navresp');
                    $("nav.nav_responsivo ul li a:eq(2)").addClass('activo_navresp');
                    //console.log("funciona")
                }
                if ($(window).scrollTop() >= cont4_offset.top - 100) {
                    $("ul.lista_navint li a").removeClass('activo_nav');
                    $("ul.lista_navint li a:eq(3)").addClass('activo_nav');
                    $("nav.nav_responsivo ul li a").removeClass('activo_navresp');
                    $("nav.nav_responsivo ul li a:eq(3)").addClass('activo_navresp');
                    ////console.log("funciona")
                }
                if ($(window).scrollTop() >= cont5_offset.top - 100) {
                    $("ul.lista_navint li a").removeClass('activo_nav');
                    $("ul.lista_navint li a:eq(4)").addClass('activo_nav');
                    $("nav.nav_responsivo ul li a").removeClass('activo_navresp');
                    $("nav.nav_responsivo ul li a:eq(4)").addClass('activo_navresp');
                    //console.log("funciona")
                }
                if ($(window).scrollTop() >= cont6_offset.top - 100) {
                    $("ul.lista_navint li a").removeClass('activo_nav');
                    $("ul.lista_navint li a:eq(5)").addClass('activo_nav');
                    $("nav.nav_responsivo ul li a").removeClass('activo_navresp');
                    $("nav.nav_responsivo ul li a:eq(5)").addClass('activo_navresp');
                    //console.log("funciona")
                }
                if ($(window).scrollTop() >= cont7_offset.top - 100) {
                    $("ul.lista_navint li a").removeClass('activo_nav');
                    $("ul.lista_navint li a:eq(6)").addClass('activo_nav');
                    $("nav.nav_responsivo ul li a").removeClass('activo_navresp');
                    $("nav.nav_responsivo ul li a:eq(6)").addClass('activo_navresp');
                    //console.log("funciona")
                }
                if ($(window).scrollTop() >= cont8_offset.top - 100) {
                    $("ul.lista_navint li a").removeClass('activo_nav');
                    $("ul.lista_navint li a:eq(7)").addClass('activo_nav');
                    $("nav.nav_responsivo ul li a").removeClass('activo_navresp');
                    $("nav.nav_responsivo ul li a:eq(7)").addClass('activo_navresp');
                    //console.log("funciona")
                }
                if ($(window).scrollTop() >= cont9_offset.top - 100) {
                    $("ul.lista_navint li a").removeClass('activo_nav');
                    $("ul.lista_navint li a:eq(8)").addClass('activo_nav');
                    $("nav.nav_responsivo ul li a").removeClass('activo_navresp');
                    $("nav.nav_responsivo ul li a:eq(8)").addClass('activo_navresp');
                    //console.log("funciona")
                }
            }

        });

    });

// Ejecutamos la función
    $(window).resize();


    /*scroll*/

    $('ul.lista_navint li a, nav.nav_responsivointernas ul li a, a.escribir_articulo').click(function(event) {
        event.preventDefault();
        enlace = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(enlace).offset().top
        }, 1000);
    });

    /*centrar botones inicio*/

    $(window).resize(function() {

        $('.navint').css({
            position: 'fixed',
            top: ($(window).height() - $('.navint').outerHeight()) / 2,
        });


        $(".mask-left").each(function() {
            var alto_papa = $(this).parent(".fondos-left").height();
            var alto_this = $(this).height();
            //console.log(alto_papa);
            //console.log(alto_this);

            if (alto_papa > alto_this + 50) {

                $(this).css({
                    position: 'absolute',
                    top: (alto_papa - $(this).outerHeight()) / 2,
                });

            } else {
                $(this).css({
                    position: 'relative',
                    marginTop: 60,
                    marginBottom: 60
                });
            }


        });

        $(".mask-right").each(function() {
            var alto_papa = $(this).parent(".fondos-right").height();
            var alto_this = $(this).height();
            //console.log(alto_papa);
            //console.log(alto_this);

            if (alto_papa > alto_this + 50) {

                $(this).css({
                    position: 'absolute',
                    top: (alto_papa - $(this).outerHeight()) / 2,
                });

            } else {
                $(this).css({
                    position: 'relative',
                    marginTop: 60,
                    marginBottom: 60
                });
            }


        });

    });

    // Ejecutamos la función
    $(window).resize();
});



// Funciones añadidas por Juan Cantor

function make_json(frm)
{
    c = 0;
    var data = '{';
    while (c < frm.length)
    {
        switch (frm[c].type)
        {
            case 'text':
                data += '"' + frm[c].name + '": "' + frm[c].value + '", ';
                break;
            case 'email':
                data += '"' + frm[c].name + '": "' + frm[c].value + '", ';
                break;
            case 'date':
                data += '"' + frm[c].name + '": "' + frm[c].value + '", ';
                break;
            case 'select-one':
                data += '"' + frm[c].name + '": "' + frm[c].value + '", ';
                break;
            case 'password':
                data += '"' + frm[c].name + '": "' + frm[c].value + '", ';
                break;
            case 'number':
                data += '"' + frm[c].name + '": "' + frm[c].value + '", ';
                break;
            case 'textarea':
                data += '"' + frm[c].name + '": "' + frm[c].value + '", ';
                break;
            case 'radio':
                data += '"' + frm[c].name + '": "' + frm[c].value + '", ';
                break;
            case 'checkbox':
                data += '"' + frm[c].name + '": "' + frm[c].checked + '", ';
                break;
            case 'hidden':
                data += '"' + frm[c].name + '": "' + frm[c].value + '", ';
                break;
        }
        c++;
    }
    data = data.substring(0, data.length - 2);
    data += '}';
    data = JSON.parse(data);
    return data;
}




/*
 function loadFileAjax(idfile, url)
 {
 var inputFileImage = document.getElementById(idfile);
 var file = inputFileImage.files[0];
 //alert(file);
 var data = new FormData();
 data.append('profile_picture_upload', file);
 
 $.ajax({
 url:url,
 type:'POST',
 contentType:false,
 data:data,
 processData:false,
 cache:false
 });
 }
 
 */


function validate_fields(frm, url, destination)
{

    var c = 0;
    var radios = new Array();
    //alert("hi");
    while (c < frm.length)
    {
        if (frm[c].required == true && (frm[c].value == "" || frm[c].value == "null"))
        {
            frm[c].style.border = "1px solid rgb(231, 107, 83)";
            alert("El campo " + frm[c].placeholder + " es obligatorio");
            return false;
        }
        switch (frm[c].type)
        {
            case 'email':
                var reg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                if (!reg.test(frm[c].value))
                {
                    frm[c].style.border = "1px solid rgb(231, 107, 83)";
                    return false;
                }
                break;
            case 'number':
                if (!/^([0-9])*$/.test(frm[c].value))
                {
                    frm[c].value = "";
                    frm[c].style.border = "1px solid rgb(231, 107, 83)";
                    return false;
                }
                break;
            case 'radio':
                var found = jQuery.inArray(frm[c].name, radios);
                if (found >= 0)
                {
                }
                else
                {
                    // Element was not found, add it.
                    radios.push(frm[c].name);
                }


                break;
        }
        c++;
    }
    var co = 0;
    for (co = 0; co < radios.length; co++)
    {
        var val_radio = validate_radio(radios[co]);
        if (val_radio == false)
        {
            return false;
        }
    }

}


function validate_radio(name)
{
    //alert('validando radios.. '+name);
    var a = document.getElementsByName(name);
    var ok = 0;
    var label = a[0].placeholder;
    var length = a.length;
    var i = 0;
    for (i = 0; i < a.length; i++)
    {
        //alert(name+" = "+a[i].value+" -> "+a[i].placeholder);
        if (a[i].checked)
        {
            ok = 1;
        }
    }
    if (ok == 0)
    {
        alert('Seleccione una opción para el campo ' + label);
        return false;
    }

}



function alter_form(frm, alter, des)
{
    var c = 0;
    if (alter == 'disable')
    {
        while (c < frm.length)
        {
            switch (frm[c].type)
            {
                case 'text':
                    frm[c].disabled = true;
                    frm[c].style.backgroundColor = 'inherit';
                    break;
                case 'email':
                    frm[c].disabled = true;
                    frm[c].style.backgroundColor = 'inherit';
                    break;
                case 'date':
                    frm[c].disabled = true;
                    frm[c].style.backgroundColor = 'inherit';
                    break;
                case 'select-one':
                    frm[c].disabled = true;
                    frm[c].style.backgroundColor = 'inherit';
                    break;
                case 'password':
                    frm[c].disabled = true;
                    frm[c].style.backgroundColor = 'inherit';
                    break;
                case 'number':
                    frm[c].disabled = true;
                    frm[c].style.backgroundColor = 'inherit';
                    break;
                case 'textarea':
                    frm[c].disabled = true;
                    frm[c].style.backgroundColor = 'inherit';
                    break;
                case 'radio':
                    frm[c].disabled = true;
                    frm[c].style.backgroundColor = 'inherit';
                    break;
                case 'checkbox':
                    frm[c].disabled = true;
                    frm[c].style.backgroundColor = 'inherit';
                    break;
            }

            c++;
        }
    }
    else
    {
        while (c < frm.length)
        {
            switch (frm[c].type)
            {
                case 'text':
                    frm[c].disabled = false;
                    frm[c].style.backgroundColor = '#f0f0f0';
                    break;
                case 'email':
                    frm[c].disabled = false;
                    frm[c].style.backgroundColor = '#f0f0f0';
                    break;
                case 'date':
                    frm[c].disabled = false;
                    frm[c].style.backgroundColor = '#f0f0f0';
                    break;
                case 'select-one':
                    frm[c].disabled = false;
                    frm[c].style.backgroundColor = '#f0f0f0';
                    break;
                case 'password':
                    frm[c].disabled = false;
                    frm[c].style.backgroundColor = '#f0f0f0';
                    break;
                case 'number':
                    frm[c].disabled = false;
                    frm[c].style.backgroundColor = '#f0f0f0';
                    break;
                case 'textarea':
                    frm[c].disabled = false;
                    frm[c].style.backgroundColor = '#f0f0f0';
                    break;
                case 'radio':
                    frm[c].disabled = true;
                    frm[c].style.backgroundColor = 'inherit';
                    break;
                case 'checkbox':
                    frm[c].disabled = true;
                    frm[c].style.backgroundColor = 'inherit';
                    break;
            }
            c++;
        }
    }
}
function custom_save(frm, loadurl, destination)
{
    //alert("c_save");
    var data = make_json(frm);
    //alter_form(frm, 'disable', destination);

    //========= < cargando fotro de perfil > ==================//
    //loadFileAjax('profile_picture_upload', loadurl+"?photoajax");
    //========= </ cargando fotro de perfil > ==================//

    $(destination).load(loadurl, {data: data}, function()
    {
        $(destination).fadeIn(200);
    });

    return false;
}
// ======================= FUNCIONES EXCLUSIVAS PARA EL REGISTRO DE USUARIOS =======================================
function add_work_company()
{
    var c = document.getElementById('work_company_flag');
    if (c.value <= 4)
    {
        document.getElementById('work_company' + c.value).style.display = "block";
        c.value = parseInt(c.value) + 1;
    }
    else
    {
        alert("Solo pueden añadirse como máximo 4 lugares de trabajo :P");
    }
}

function terms()
{
    if (document.getElementById('terms_and_conditions').checked = true)
    {
        document.getElementById('submit_register_doc').disabled = false;
        document.getElementById('submit_register_doc').style.background = '#ffa300';
    }
    else
    {
        document.getElementById('submit_register_doc').disabled = true;
        document.getElementById('submit_register_doc').style.background = '#cccccc';
    }
}


//-------------- GUARDAR USUARIOS -------------------------//
function users_signup(frm, url, destination)
{
    //alert("user signup");
    var validate = validate_fields(frm, url, destination);
    if (validate == false)
    {
        return false;
    }
    //--------------
    var custom = custom_save(frm, url, destination);
    var enable = document.getElementById('captcha').disabled = false;
}

//-------------- GUARDAR USUARIOS -------------------------//


//-------------- RECUPERAR CONTRASEÑA -------------------------//
function user_recover(frm, url, destination)
{
    //alert("user signup");
    var validate = validate_fields(frm, url, destination);
    if (validate == false)
    {
        return false;
    }
    var custom = custom_save(frm, url, destination);
    return false;
    //var enable = document.getElementById('captcha').disabled = false;
}

//-------------- / RECUPERAR CONTRASEÑA -------------------------//





// ======================= / FUNCIONES EXCLUSIVAS PARA EL REGISTRO DE USUARIOS =======================================





// =======================   FUNCIONES PARA ZONAS PROTEGIDAS =======================================

function users_update(frm, url, destination)
{
    //alert("user signup");
    var validate = validate_fields(frm, url, destination);
    if (validate == false)
    {
        return false;
    }
    var custom = custom_save(frm, url, destination);
    //var enable = document.getElementById('captcha').disabled = false;
}


// ======================= / FUNCIONES PARA ZONAS PROTEGIDAS =======================================

//------- Directorio méidco
function directory(frm, limit)
{
    document.getElementById('limit').value = limit;
    var custom = custom_save(frm, 'contenidos/medical_directory_ajax', '#cont_fotosmedico');
    return false;
}
function directory_pagination(limit)
{
    var frm = document.getElementById('directory_search');
    document.getElementById('limit').value = limit;
    var custom = custom_save(frm, 'contenidos/medical_directory_ajax', '#cont_fotosmedico');
    return false;
}

//------- / Direcptrio médico


// --------- Tips
function new_tip()
{

    $("#mi_dieta_tip").load('contenidos/get_tip?ajax', function()
    {
    });
}
//--------- / Tips
function load_calendar_event(id)
  {
    $("#calendar_event_ajax").fadeOut(50);
    $("#calendar_event_ajax").load('eventos/load_calendar_event?id='+id, function()
        {
          $("#calendar_event_ajax").fadeIn(400);
        });
  }


function apply_event(id)
{
    $("#calendar_apply_des").load('eventos/apply?id=' + id, function() {
        $("#calendar_apply_des").fadeIn(200);
        });
}

function load_recipe(id)
        {
            $("#recipe_ajax").fadeOut(50);
            $("#recipe_ajax").load('tips_recetas/load_recipe?id=' + id, function()
            {
                $("#recipe_ajax").fadeIn(400);
            });

        }

function reported_activity_save(frm)
    {
        var validate = validate_fields(frm);
        if (validate == false)
        {
            return false;
        }
        var custom = custom_save(frm, 'modales/reported_activity', '#des_reported_activity');
        alter_form(frm, 'disable');
        return false;
    }

//================= / FUNCIONES EXTRA / =====================

    $(document).ready(function() {
        $('#imagePreview').hide();
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#imagePreview').fadeIn();
                $('#imagePreview').attr('src', e.target.result);
                $('#image_data').val(e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function() {
        readURL(this);
        var extension = (this.value.substring(this.value.lastIndexOf("."))).toLowerCase();
        $('#ext_image_data').val(extension);
    });