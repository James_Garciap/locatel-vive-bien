$(document).ready(function() {

  $(function() {
    var $tabs = $('#tabs').tabs();
    $(".ui-tabs-panel").each(function(i){
        var totalSize = $(".ui-tabs-panel").size() - 1;

        if (i != totalSize) {
            next = i + 2;
            $(this).append("<a class='next-tab mover' rel='" + next + "'>Siguiente</a>");
        }
        if (i != 0) {
            prev = i;
            $(this).append("<a class='prev-tab mover' rel='" + prev + "'>Anterior</a>");
        }
      });

    $('.next-tab, .prev-tab').click(function() {
      $tabs.tabs('select', $(this).attr("rel"));
      return false;
    });
  });

  //SELECT_DD

  $(".select_dd").msDropDown().data("dd");


  //SCROLLPANE

  $('.scroll').jScrollPane({autoReinitialise: true, verticalDragMinHeight: 60,
      verticalDragMaxHeight: 60});

   //SLIDER

   $('.slider-recetas').bxSlider({
    mode: 'fade',
    auto: 'true',
    controls: 'true',
    pager: 'true'

  });

  $('.slider-tusarticulos').bxSlider({
    minSlides: 3,
    maxSlides: 3,
    slideWidth: 180,
    slideMargin: 10
  });

$('.slider').bxSlider({
  pause: 7000,
  auto: 'false',
  speed: 1500,
  mode: 'fade',
  minSlides: 1,
  maxSlides: 1,
  pager: false,
});

  //MODALES

  $(".llama_modal").fancybox({
    'autoScale' : true,
    'transitionIn' : 'none',
    'transitionOut' : 'none'
  });

   $(".modal_terminos").fancybox({
   'autoScale' : true,
    'transitionIn' : 'none',
    'transitionOut' : 'none'
  });

  $(".cargatusarticulos").fancybox({
    'width' : 600,
    'height' : 200,
    fitToView : false,
    autoSize : false,
    'transitionIn' : 'none',
    'transitionOut' : 'none',
    'type' : 'iframe',
    'padding' : 20
  });


});


