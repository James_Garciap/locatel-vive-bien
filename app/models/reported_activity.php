<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.com
     * @company Imaginamos.com | todos los derechos reservados
     */

                        

class reported_activity extends  DataMapper {

    /**
     * @var int Max length is 10.
     */
    public $id;

    /**
     * @var int Max length is 10.  unsigned.
     */
    public $users_id;

    /**
     * @var varchar Max length is 5.
     */
    public $weight;

    /**
     * @var varchar Max length is 8.
     */
    public $time;

    /**
     * @var text
     */
    public $description;

    /**
     * @var tinyint Max length is 2.
     */
    public $activity_minutes_id;

    /**
     * @var int Max length is 10.
     */
    public $weeks_id;

    /**
     * @var int Max length is 11.
     */
    public $routines_id;

    public $table = 'reported_activity';

    public $model = 'reported_activity';
    public $primarykey = 'id';
    public $_fields = array('id','users_id','weight','time','description','activity_minutes_id','weeks_id','routines_id');

    public $has_one =  array(
                'users' => array(
                  'class' => 'users',
                  'other_field' => 'reported_activity',
                  'join_other_as' => 'users',
                  'join_self_as' => 'reported_activity',
                  'join_table' => 'cms_users',
                ),

                'activity_minutes' => array(
                  'class' => 'activity_minutes',
                  'other_field' => 'reported_activity',
                  'join_other_as' => 'activity_minutes',
                  'join_self_as' => 'reported_activity',
                  'join_table' => 'cms_activity_minutes',
                ),

                'weeks' => array(
                  'class' => 'weeks',
                  'other_field' => 'reported_activity',
                  'join_other_as' => 'weeks',
                  'join_self_as' => 'reported_activity',
                  'join_table' => 'cms_weeks',
                ),

                'routines' => array(
                  'class' => 'routines',
                  'other_field' => 'reported_activity',
                  'join_other_as' => 'routines',
                  'join_self_as' => 'reported_activity',
                  'join_table' => 'cms_routines',
                )
            );



    public $has_many = array();



    public function __construct($id = NULL) {
         parent::__construct($id);
    }


    public function get_data($id = '', $campo = 'name') {
        $obj = new $this->model();
        $arrList = array();
        if (empty($id)) {
             $obj->get_iterated();
              foreach ($obj as $value) {
                 $arrList = array('id' => $value->id,'name' => $value->{$campo});
              }
              return $arrList;
        } else {
              return $obj->get_by_id($id);
        }
    }


    public function get_users_list($campo="name",$where=array()) {
         $model = new users();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_users($join_retale="") {
         $model = new users();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_reported_activity_id($this->id);
         }else{
         	return $model->get_by_reported_activity_id($this->id);
         }
    }


    public function get_activity_minutes_list($campo="name",$where=array()) {
         $model = new activity_minutes();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_activity_minutes($join_retale="") {
         $model = new activity_minutes();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_reported_activity_id($this->id);
         }else{
         	return $model->get_by_reported_activity_id($this->id);
         }
    }


    public function get_weeks_list($campo="name",$where=array()) {
         $model = new weeks();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_weeks($join_retale="") {
         $model = new weeks();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_reported_activity_id($this->id);
         }else{
         	return $model->get_by_reported_activity_id($this->id);
         }
    }


    public function get_routines_list($campo="name",$where=array()) {
         $model = new routines();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_routines($join_retale="") {
         $model = new routines();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_reported_activity_id($this->id);
         }else{
         	return $model->get_by_reported_activity_id($this->id);
         }
    }


    public function selected_id($related_id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->where_related($related, 'id', $related_id)->get();
        if ($obj->exists()) {
        	return $obj->id;
        } else {
        	return 0;
        }
    }


    public function selected_multiple_id($id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->join_related($related)->get_by_id($id);
        $array = array();
        if ($obj->exists()) {
        	foreach ($obj as $value) {
        		$array[] = $value->modelo_id;
        	}
        }
        return $array;
    }


    public function get_rule($campo, $rule){
         if(array_key_exists($rule, $this->validation[$campo]['rules']))
            return $this->validation[$campo]['rules'][$rule];
         else
            return false;
    }


    public function is_rule($campo, $rule){
         if(in_array($rule, $this->validation[$campo]['rules']))
            return true;
         else
            return false;
    }


    public function to_array_first_row() {
     $model = clone $this;
     $model->get_by_id(1);
     $datos = array();
      foreach ($this->fields as $key) {
           if($key != 'id')
             $datos[$key] = $model->{$key};
      }
      return $datos;
    }


    public $default_order_by = array('id' => 'desc');


    public function post_model_init($from_cache = FALSE){}


    public function _encrypt($field)
    {
          if (!empty($this->{$field}))
          {
              if (empty($this->salt))
              {
                  $this->salt = md5(uniqid(rand(), true));
              }
             $this->{$field} = sha1($this->salt . $this->{$field});
          }
    }


    public $validation =  array(
                'id' => array(
                  'rules' => array( 'max_length' => 10 ),
                  'label' => 'ID',
                ),

                'users_id' => array(
                  'rules' => array( 'min_length' => 0, 'max_length' => 10, 'required' ),
                  'label' => 'USERS',
                ),

                'weight' => array(
                  'rules' => array( 'max_length' => 5 ),
                  'label' => 'WEIGHT',
                ),

                'time' => array(
                  'rules' => array( 'max_length' => 8 ),
                  'label' => 'TIME',
                ),

                'activity_minutes_id' => array(
                  'rules' => array( 'max_length' => 2, 'required' ),
                  'label' => 'ACTIVITYMINUTES',
                ),

                'weeks_id' => array(
                  'rules' => array( 'max_length' => 10, 'required' ),
                  'label' => 'WEEKS',
                ),

                'routines_id' => array(
                  'rules' => array( 'max_length' => 11 ),
                  'label' => 'ROUTINES',
                )
            );


    public $coments =  array(
                'users_id' => 'combox|view|label#Usuario#',
                'weight' => 'input|view|label#Peso#',
                'time' => 'input|view|label#Tiempo#',
                'description' => 'input|view|label#Descripción#',
                'activity_minutes_id' => 'combox|view|label#Minutos de actividad#',
                'weeks_id' => 'combox|view|label#Semana#',
);

}