<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.com
     * @company Imaginamos.com | todos los derechos reservados
     */

                        

class prepaid_health extends  DataMapper {

    /**
     * @var int Max length is 11.
     */
    public $id;

    /**
     * @var varchar Max length is 45.
     */
    public $name;

    /**
     * @var int Max length is 11.
     */
    public $logic_state;

    /**
     * @var date
     */
    public $date_insert;

    public $table = 'prepaid_health';

    public $model = 'prepaid_health';
    public $primarykey = 'id';
    public $_fields = array('id','name','logic_state','date_insert');

    public $has_one = array();
    public $has_many =  array(
                'users' => array(
                  'class' => 'users',
                  'other_field' => 'prepaid_health',
                  'join_other_as' => 'users',
                  'join_self_as' => 'prepaid_health',
                  'join_table' => 'cms_users_prepaid_health',
                ),

                'users_prepaid_health' => array(
                  'class' => 'users_prepaid_health',
                  'other_field' => 'prepaid_health',
                  'join_other_as' => 'users_prepaid_health',
                  'join_self_as' => 'prepaid_health',
                  'join_table' => 'cms_users_prepaid_health',
                )
            );



    public function __construct($id = NULL) {
         parent::__construct($id);
    }


    public function get_data($id = '', $campo = 'name') {
        $obj = new $this->model();
        $arrList = array();
        if (empty($id)) {
             $obj->get_iterated();
              foreach ($obj as $value) {
                 $arrList = array('id' => $value->id,'name' => $value->{$campo});
              }
              return $arrList;
        } else {
              return $obj->get_by_id($id);
        }
    }


    public function selected_id($related_id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->where_related($related, 'id', $related_id)->get();
        if ($obj->exists()) {
        	return $obj->id;
        } else {
        	return 0;
        }
    }


    public function selected_multiple_id($id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->join_related($related)->get_by_id($id);
        $array = array();
        if ($obj->exists()) {
        	foreach ($obj as $value) {
        		$array[] = $value->modelo_id;
        	}
        }
        return $array;
    }


    public function get_rule($campo, $rule){
         if(array_key_exists($rule, $this->validation[$campo]['rules']))
            return $this->validation[$campo]['rules'][$rule];
         else
            return false;
    }


    public function is_rule($campo, $rule){
         if(in_array($rule, $this->validation[$campo]['rules']))
            return true;
         else
            return false;
    }


    public function to_array_first_row() {
     $model = clone $this;
     $model->get_by_id(1);
     $datos = array();
      foreach ($this->fields as $key) {
           if($key != 'id')
             $datos[$key] = $model->{$key};
      }
      return $datos;
    }


    public $default_order_by = array('id' => 'desc');


    public function post_model_init($from_cache = FALSE){}


    public function _encrypt($field)
    {
          if (!empty($this->{$field}))
          {
              if (empty($this->salt))
              {
                  $this->salt = md5(uniqid(rand(), true));
              }
             $this->{$field} = sha1($this->salt . $this->{$field});
          }
    }


    public $validation =  array(
                'id' => array(
                  'rules' => array( 'max_length' => 11 ),
                  'label' => 'ID',
                ),

                'name' => array(
                  'rules' => array( 'max_length' => 45, 'required' ),
                  'label' => 'NAME',
                ),

                'logic_state' => array(
                  'rules' => array( 'max_length' => 11 ),
                  'label' => 'LOGICSTATE',
                ),

                'date_insert' => array(
                  'rules' => array( 'valid_date' ),
                  'label' => 'DATEINSERT',
                )
            );


    public $coments =  array(
                'name' => 'input|view|label#Nombre de la entidad#',
);

}