<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.com
     * @company Imaginamos.com | todos los derechos reservados
     */

                        

class users extends  DataMapper {

    /**
     * @var int Max length is 10.  unsigned.
     */
    public $id;

    /**
     * @var varbinary Max length is 16.
     */
    public $ip_address;

    /**
     * @var varchar Max length is 100.
     */
    public $username;

    /**
     * @var varchar Max length is 40.
     */
    public $password;

    /**
     * @var varchar Max length is 40.
     */
    public $salt;

    /**
     * @var varchar Max length is 100.
     */
    public $email;

    /**
     * @var varchar Max length is 40.
     */
    public $activation_code;

    /**
     * @var varchar Max length is 40.
     */
    public $forgotten_password_code;

    /**
     * @var int Max length is 11.  unsigned.
     */
    public $forgotten_password_time;

    /**
     * @var varchar Max length is 40.
     */
    public $remember_code;

    /**
     * @var int Max length is 11.  unsigned.
     */
    public $created_on;

    /**
     * @var int Max length is 11.  unsigned.
     */
    public $last_login;

    /**
     * @var tinyint Max length is 1.  unsigned.
     */
    public $active;

    /**
     * @var varchar Max length is 50.
     */
    public $first_name;

    /**
     * @var varchar Max length is 50.
     */
    public $last_name;

    /**
     * @var varchar Max length is 100.
     */
    public $company;

    /**
     * @var varchar Max length is 20.
     */
    public $phone;

    /**
     * @var varchar Max length is 6.
     */
    public $sex;

    /**
     * @var varchar Max length is 10.
     */
    public $birthday;

    /**
     * @var tinyint Max length is 1.
     */
    public $doctor;

    /**
     * @var int Max length is 3.
     */
    public $height;

    /**
     * @var int Max length is 3.
     */
    public $weight;

     /**
     * @var int Max length is 3.
     */
    public $pre_weight;

    /**
     * @var tinyint Max length is 2.
     */
    public $activity_level_id;

    /**
     * @var tinyint Max length is 2.
     */
    public $cities_id;

    /**
     * @var int Max length is 11.
     */
    public $imagen_id;

      /**
     * @var int Max length is 1.
     */
    public $status;

    public $table = 'users';

    public $model = 'users';
    public $primarykey = 'id';
    public $_fields = array('id','ip_address','username','password','salt','email','activation_code','forgotten_password_code','forgotten_password_time','remember_code','created_on','last_login','active','first_name','last_name','company','phone','sex','birthday','doctor','height','weight','pre_weight' ,'activity_level_id', 'cities_id', 'imagen_id', 'status');
    //public $_fields = array('id','ip_address','username','password','salt','email','activation_code','forgotten_password_code','forgotten_password_time','remember_code','created_on','last_login','active','first_name','last_name','company','phone','sex','birthday','doctor','height','weight','activity_level_id','cities_id','imagen_id');

    public $has_one =  array(
                'activity_level' => array(
                  'class' => 'activity_level',
                  'other_field' => 'users',
                  'join_other_as' => 'activity_level',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_activity_level',
                ),

                'cities' => array(
                  'class' => 'cities',
                  'other_field' => 'users',
                  'join_other_as' => 'cities',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_cities',
                ),

                'imagen' => array(
                  'class' => 'imagen',
                  'other_field' => 'users',
                  'join_other_as' => 'imagen',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_imagen',
                )
            );



    public $has_many =  array(
                'calendar' => array(
                  'class' => 'calendar',
                  'other_field' => 'users',
                  'join_other_as' => 'calendar',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_calendar',
                ),

                'components' => array(
                  'class' => 'components',
                  'other_field' => 'users',
                  'join_other_as' => 'components',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_components',
                ),

                'medical_formulate' => array(
                  'class' => 'medical_formulate',
                  'other_field' => 'users',
                  'join_other_as' => 'medical_formulate',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_medical_formulate',
                ),

                'posts' => array(
                  'class' => 'posts',
                  'other_field' => 'users',
                  'join_other_as' => 'posts',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_posts',
                ),

                'recipes' => array(
                  'class' => 'recipes',
                  'other_field' => 'users',
                  'join_other_as' => 'recipes',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_recipes',
                ),

                'reported_activity' => array(
                  'class' => 'reported_activity',
                  'other_field' => 'users',
                  'join_other_as' => 'reported_activity',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_reported_activity',
                ),

                'users_activity' => array(
                  'class' => 'users_activity',
                  'other_field' => 'users',
                  'join_other_as' => 'users_activity',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_users_activity',
                ),

                'diets' => array(
                  'class' => 'users',
                  'other_field' => 'users',
                  'join_other_as' => 'diets',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_users_diets',
                ),

                'users_diets' => array(
                  'class' => 'users_diets',
                  'other_field' => 'users',
                  'join_other_as' => 'users_diets',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_users_diets',
                ),

                'users_doc_profile' => array(
                  'class' => 'users_doc_profile',
                  'other_field' => 'users',
                  'join_other_as' => 'users_doc_profile',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_users_doc_profile',
                ),

                'eps' => array(
                  'class' => 'users',
                  'other_field' => 'users',
                  'join_other_as' => 'eps',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_users_eps',
                ),

                'users_eps' => array(
                  'class' => 'users_eps',
                  'other_field' => 'users',
                  'join_other_as' => 'users_eps',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_users_eps',
                ),

                'groups' => array(
                  'class' => 'users',
                  'other_field' => 'users',
                  'join_other_as' => 'groups',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_users_groups',
                ),

                'hobbies' => array(
                  'class' => 'users',
                  'other_field' => 'users',
                  'join_other_as' => 'hobbies',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_users_hobbies',
                ),

                'users_hobbies' => array(
                  'class' => 'users_hobbies',
                  'other_field' => 'users',
                  'join_other_as' => 'users_hobbies',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_users_hobbies',
                ),

                'opening_hours' => array(
                  'class' => 'users',
                  'other_field' => 'users',
                  'join_other_as' => 'opening_hours',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_users_opening_hours',
                ),

                'users_opening_hours' => array(
                  'class' => 'users_opening_hours',
                  'other_field' => 'users',
                  'join_other_as' => 'users_opening_hours',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_users_opening_hours',
                ),

                'routines' => array(
                  'class' => 'users',
                  'other_field' => 'users',
                  'join_other_as' => 'routines',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_users_routines',
                ),

                'users_routines' => array(
                  'class' => 'users_routines',
                  'other_field' => 'users',
                  'join_other_as' => 'users_routines',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_users_routines',
                ),

                'specialties' => array(
                  'class' => 'users',
                  'other_field' => 'users',
                  'join_other_as' => 'specialties',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_users_specialties',
                ),

                'users_specialties' => array(
                  'class' => 'users_specialties',
                  'other_field' => 'users',
                  'join_other_as' => 'users_specialties',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_users_specialties',
                ),
                
                'static_texts' => array(
                  'class' => 'static_texts',
                  'other_field' => 'users',
                  'join_other_as' => 'static_texts',
                  'join_self_as' => 'users',
                  'join_table' => 'cms_static_texts',
                ),
            );



    public function __construct($id = NULL) {
         parent::__construct($id);
    }


    public function get_data($id = '', $campo = 'name') {
        $obj = new $this->model();
        $arrList = array();
        if (empty($id)) {
             $obj->get_iterated();
              foreach ($obj as $value) {
                 $arrList = array('id' => $value->id,'name' => $value->{$campo});
              }
              return $arrList;
        } else {
              return $obj->get_by_id($id);
        }
    }


    public function get_activity_level_list($campo="name",$where=array()) {
         $model = new activity_level();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_activity_level($join_retale="") {
         $model = new activity_level();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_users_id($this->id);
         }else{
         	return $model->get_by_users_id($this->id);
         }
    }


    public function get_cities_list($campo="name",$where=array()) {
         $model = new cities();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_cities($join_retale="") {
         $model = new cities();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_users_id($this->id);
         }else{
         	return $model->get_by_users_id($this->id);
         }
    }


    public function get_imagen_list($campo="name",$where=array()) {
         $model = new imagen();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_imagen($join_retale="") {
         $model = new imagen();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_users_id($this->id);
         }else{
         	return $model->get_by_users_id($this->id);
         }
    }


    public function selected_id($related_id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->where_related($related, 'id', $related_id)->get();
        if ($obj->exists()) {
        	return $obj->id;
        } else {
        	return 0;
        }
    }


    public function selected_multiple_id($id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->join_related($related)->get_by_id($id);
        $array = array();
        if ($obj->exists()) {
        	foreach ($obj as $value) {
        		$array[] = $value->modelo_id;
        	}
        }
        return $array;
    }


    public function get_rule($campo, $rule){
         if(array_key_exists($rule, $this->validation[$campo]['rules']))
            return $this->validation[$campo]['rules'][$rule];
         else
            return false;
    }


    public function is_rule($campo, $rule){
         if(in_array($rule, $this->validation[$campo]['rules']))
            return true;
         else
            return false;
    }


    public function to_array_first_row() {
     $model = clone $this;
     $model->get_by_id(1);
     $datos = array();
      foreach ($this->fields as $key) {
           if($key != 'id')
             $datos[$key] = $model->{$key};
      }
      return $datos;
    }


    public $default_order_by = array('id' => 'desc');


    public function post_model_init($from_cache = FALSE){}


    public function _encrypt($field)
    {
          if (!empty($this->{$field}))
          {
              if (empty($this->salt))
              {
                  $this->salt = md5(uniqid(rand(), true));
              }
             $this->{$field} = sha1($this->salt . $this->{$field});
          }
    }


    public $validation =  array(
                'id' => array(
                  'rules' => array( 'min_length' => 0, 'max_length' => 10 ),
                  'label' => 'ID',
                ),

                'ip_address' => array(
                  'rules' => array( 'required' ),
                  'label' => 'IPADDRESS',
                ),

                'username' => array(
                  'rules' => array( 'max_length' => 100, 'required' ),
                  'label' => 'USERNAME',
                ),

                'password' => array(
                  'rules' => array( 'max_length' => 40, 'required' ),
                  'label' => 'PASSWORD',
                ),

                'salt' => array(
                  'rules' => array( 'max_length' => 40 ),
                  'label' => 'SALT',
                ),

                'email' => array(
                  'rules' => array( 'max_length' => 100, 'required' ),
                  'label' => 'EMAIL',
                ),

                'activation_code' => array(
                  'rules' => array( 'max_length' => 40 ),
                  'label' => 'ACTIVATIONCODE',
                ),

                'forgotten_password_code' => array(
                  'rules' => array( 'max_length' => 40 ),
                  'label' => 'FORGOTTENPASSWORDCODE',
                ),

                'forgotten_password_time' => array(
                  'rules' => array( 'min_length' => 0, 'max_length' => 11 ),
                  'label' => 'FORGOTTENPASSWORDTIME',
                ),

                'remember_code' => array(
                  'rules' => array( 'max_length' => 40 ),
                  'label' => 'REMEMBERCODE',
                ),

                'created_on' => array(
                  'rules' => array( 'min_length' => 0, 'max_length' => 11, 'required' ),
                  'label' => 'CREATEDON',
                ),

                'last_login' => array(
                  'rules' => array( 'min_length' => 0, 'max_length' => 11 ),
                  'label' => 'LASTLOGIN',
                ),

                'active' => array(
                  'rules' => array( 'min_length' => 0, 'max_length' => 1 ),
                  'label' => 'ACTIVE',
                ),

                'first_name' => array(
                  'rules' => array( 'max_length' => 50 ),
                  'label' => 'FIRSTNAME',
                ),

                'last_name' => array(
                  'rules' => array( 'max_length' => 50 ),
                  'label' => 'LASTNAME',
                ),

                'company' => array(
                  'rules' => array( 'max_length' => 100 ),
                  'label' => 'COMPANY',
                ),

                'phone' => array(
                  'rules' => array( 'max_length' => 20 ),
                  'label' => 'PHONE',
                ),

                'sex' => array(
                  'rules' => array( 'max_length' => 6 ),
                  'label' => 'SEX',
                ),

                'birthday' => array(
                  'rules' => array( 'max_length' => 10 ),
                  'label' => 'BIRTHDAY',
                ),

                'doctor' => array(
                  'rules' => array( 'max_length' => 1 ),
                  'label' => 'DOCTOR',
                ),

                'height' => array(
                  'rules' => array( 'max_length' => 3 ),
                  'label' => 'HEIGHT',
                ),

                'weight' => array(
                  'rules' => array( 'max_length' => 3 ),
                  'label' => 'WEIGHT',
                ),

                'pre_weight' => array(
                  'rules' => array( 'max_length' => 3 ),
                  'label' => 'PRE_WEIGHT',
                ),

                'activity_level_id' => array(
                  'rules' => array( 'max_length' => 2, 'required' ),
                  'label' => 'ACTIVITYLEVEL',
                ),

                'cities_id' => array(
                  'rules' => array( 'max_length' => 2, 'required' ),
                  'label' => 'CITIES',
                ),

                'imagen_id' => array(
                  'rules' => array( 'max_length' => 11 ),
                  'label' => 'IMAGEN',
                ),

                'status' => array(
                  'rules' => array( 'max_length' => 1 ),
                  'label' => 'STATUS',
                )
            );


    public $coments =  array(
                'sex' => 'inputradio|view|label#Sexo#',
                'birthday' => 'input|view|label#Fecha de nacimiento#',
                'doctor' => 'inputcheckbox|view|label#¿Es ustde médico?#',
                'height' => 'input|view|label#Altura#',
                'weight' => 'input|view|label#Peso#',
                'activity_level_id' => 'combox|view|label#Nivel de actividad física que realizas#',
                'cities_id' => 'combox|view|label#Ciudad#',
);

}