<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.com
     * @company Imaginamos.com | todos los derechos reservados
     */

                        

class workouts extends  DataMapper {

    /**
     * @var int Max length is 4.
     */
    public $id;

    /**
     * @var varchar Max length is 25.
     */
    public $name;

    /**
     * @var text
     */
    public $description;

    /**
     * @var tinyint Max length is 4.
     */
    public $status;

    /**
     * @var int Max length is 10.
     */
    public $weeks_id;

    public $table = 'workouts';

    public $model = 'workouts';
    public $primarykey = 'id';
    public $_fields = array('id','name','description','status','weeks_id');

    public $has_one =  array(
                'weeks' => array(
                  'class' => 'weeks',
                  'other_field' => 'workouts',
                  'join_other_as' => 'weeks',
                  'join_self_as' => 'workouts',
                  'join_table' => 'cms_weeks',
                )
            );



    public $has_many =  array(
                'imagen' => array(
                  'class' => 'workouts',
                  'other_field' => 'workouts',
                  'join_other_as' => 'imagen',
                  'join_self_as' => 'workouts',
                  'join_table' => 'cms_workouts_imagen',
                ),

                'workouts_imagen' => array(
                  'class' => 'workouts_imagen',
                  'other_field' => 'workouts',
                  'join_other_as' => 'workouts_imagen',
                  'join_self_as' => 'workouts',
                  'join_table' => 'cms_workouts_imagen',
                )
            );



    public function __construct($id = NULL) {
         parent::__construct($id);
    }


    public function get_data($id = '', $campo = 'name') {
        $obj = new $this->model();
        $arrList = array();
        if (empty($id)) {
             $obj->get_iterated();
              foreach ($obj as $value) {
                 $arrList = array('id' => $value->id,'name' => $value->{$campo});
              }
              return $arrList;
        } else {
              return $obj->get_by_id($id);
        }
    }


    public function get_weeks_list($campo="name",$where=array()) {
         $model = new weeks();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_weeks($join_retale="") {
         $model = new weeks();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_workouts_id($this->id);
         }else{
         	return $model->get_by_workouts_id($this->id);
         }
    }


    public function selected_id($related_id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->where_related($related, 'id', $related_id)->get();
        if ($obj->exists()) {
        	return $obj->id;
        } else {
        	return 0;
        }
    }


    public function selected_multiple_id($id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->join_related($related)->get_by_id($id);
        $array = array();
        if ($obj->exists()) {
        	foreach ($obj as $value) {
        		$array[] = $value->modelo_id;
        	}
        }
        return $array;
    }


    public function get_rule($campo, $rule){
         if(array_key_exists($rule, $this->validation[$campo]['rules']))
            return $this->validation[$campo]['rules'][$rule];
         else
            return false;
    }


    public function is_rule($campo, $rule){
         if(in_array($rule, $this->validation[$campo]['rules']))
            return true;
         else
            return false;
    }


    public function to_array_first_row() {
     $model = clone $this;
     $model->get_by_id(1);
     $datos = array();
      foreach ($this->fields as $key) {
           if($key != 'id')
             $datos[$key] = $model->{$key};
      }
      return $datos;
    }


    public $default_order_by = array('id' => 'desc');


    public function post_model_init($from_cache = FALSE){}


    public function _encrypt($field)
    {
          if (!empty($this->{$field}))
          {
              if (empty($this->salt))
              {
                  $this->salt = md5(uniqid(rand(), true));
              }
             $this->{$field} = sha1($this->salt . $this->{$field});
          }
    }


    public $validation =  array(
                'id' => array(
                  'rules' => array( 'max_length' => 4 ),
                  'label' => 'ID',
                ),

                'name' => array(
                  'rules' => array( 'max_length' => 25 ),
                  'label' => 'NAME',
                ),

                'status' => array(
                  'rules' => array( 'max_length' => 4 ),
                  'label' => 'STATUS',
                ),

                'weeks_id' => array(
                  'rules' => array( 'max_length' => 10, 'required' ),
                  'label' => 'WEEKS',
                )
            );


    public $coments =  array(
                'name' => 'input|view|label#Título#',
                'description' => 'input|view|label#Descripción#',
                'status' => 'input|view|label#Imagen#\\n',
                'weeks_id' => 'combox|view|label#Semana#',
);

}