<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.com
     * @company Imaginamos.com | todos los derechos reservados
     */

                        

class medical_formulate extends  DataMapper {

    /**
     * @var int Max length is 4.
     */
    public $id;

    /**
     * @var int Max length is 10.  unsigned.
     */
    public $users_id;

    /**
     * @var varchar Max length is 45.
     */
    public $medicine;

    /**
     * @var text
     */
    public $description;

    /**
     * @var tinyint Max length is 1.
     */
    public $mail_notificaction;

    /**
     * @var tinyint Max length is 2.
     */
    public $unity_type_id;

    /**
     * @var tinyint Max length is 2.
     */
    public $no_units_id;

    /**
     * @var tinyint Max length is 2.
     */
    public $periodicity_id;

    public $table = 'medical_formulate';

    public $model = 'medical_formulate';
    public $primarykey = 'id';
    public $_fields = array('id','users_id','medicine','description','mail_notificaction','unity_type_id','no_units_id','periodicity_id');

    public $has_one =  array(
                'users' => array(
                  'class' => 'users',
                  'other_field' => 'medical_formulate',
                  'join_other_as' => 'users',
                  'join_self_as' => 'medical_formulate',
                  'join_table' => 'cms_users',
                ),

                'unity_type' => array(
                  'class' => 'unity_type',
                  'other_field' => 'medical_formulate',
                  'join_other_as' => 'unity_type',
                  'join_self_as' => 'medical_formulate',
                  'join_table' => 'cms_unity_type',
                ),

                'no_units' => array(
                  'class' => 'no_units',
                  'other_field' => 'medical_formulate',
                  'join_other_as' => 'no_units',
                  'join_self_as' => 'medical_formulate',
                  'join_table' => 'cms_no_units',
                ),

                'periodicity' => array(
                  'class' => 'periodicity',
                  'other_field' => 'medical_formulate',
                  'join_other_as' => 'periodicity',
                  'join_self_as' => 'medical_formulate',
                  'join_table' => 'cms_periodicity',
                )
            );



    public $has_many = array();



    public function __construct($id = NULL) {
         parent::__construct($id);
    }


    public function get_data($id = '', $campo = 'name') {
        $obj = new $this->model();
        $arrList = array();
        if (empty($id)) {
             $obj->get_iterated();
              foreach ($obj as $value) {
                 $arrList = array('id' => $value->id,'name' => $value->{$campo});
              }
              return $arrList;
        } else {
              return $obj->get_by_id($id);
        }
    }


    public function get_users_list($campo="name",$where=array()) {
         $model = new users();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_users($join_retale="") {
         $model = new users();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_medical_formulate_id($this->id);
         }else{
         	return $model->get_by_medical_formulate_id($this->id);
         }
    }


    public function get_unity_type_list($campo="name",$where=array()) {
         $model = new unity_type();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_unity_type($join_retale="") {
         $model = new unity_type();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_medical_formulate_id($this->id);
         }else{
         	return $model->get_by_medical_formulate_id($this->id);
         }
    }


    public function get_no_units_list($campo="name",$where=array()) {
         $model = new no_units();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_no_units($join_retale="") {
         $model = new no_units();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_medical_formulate_id($this->id);
         }else{
         	return $model->get_by_medical_formulate_id($this->id);
         }
    }


    public function get_periodicity_list($campo="name",$where=array()) {
         $model = new periodicity();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_periodicity($join_retale="") {
         $model = new periodicity();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_medical_formulate_id($this->id);
         }else{
         	return $model->get_by_medical_formulate_id($this->id);
         }
    }


    public function selected_id($related_id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->where_related($related, 'id', $related_id)->get();
        if ($obj->exists()) {
        	return $obj->id;
        } else {
        	return 0;
        }
    }


    public function selected_multiple_id($id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->join_related($related)->get_by_id($id);
        $array = array();
        if ($obj->exists()) {
        	foreach ($obj as $value) {
        		$array[] = $value->modelo_id;
        	}
        }
        return $array;
    }


    public function get_rule($campo, $rule){
         if(array_key_exists($rule, $this->validation[$campo]['rules']))
            return $this->validation[$campo]['rules'][$rule];
         else
            return false;
    }


    public function is_rule($campo, $rule){
         if(in_array($rule, $this->validation[$campo]['rules']))
            return true;
         else
            return false;
    }


    public function to_array_first_row() {
     $model = clone $this;
     $model->get_by_id(1);
     $datos = array();
      foreach ($this->fields as $key) {
           if($key != 'id')
             $datos[$key] = $model->{$key};
      }
      return $datos;
    }


    public $default_order_by = array('id' => 'desc');


    public function post_model_init($from_cache = FALSE){}


    public function _encrypt($field)
    {
          if (!empty($this->{$field}))
          {
              if (empty($this->salt))
              {
                  $this->salt = md5(uniqid(rand(), true));
              }
             $this->{$field} = sha1($this->salt . $this->{$field});
          }
    }


    public $validation =  array(
                'id' => array(
                  'rules' => array( 'max_length' => 4 ),
                  'label' => 'ID',
                ),

                'users_id' => array(
                  'rules' => array( 'min_length' => 0, 'max_length' => 10, 'required' ),
                  'label' => 'USERS',
                ),

                'medicine' => array(
                  'rules' => array( 'max_length' => 45 ),
                  'label' => 'MEDICINE',
                ),

                'mail_notificaction' => array(
                  'rules' => array( 'max_length' => 1 ),
                  'label' => 'MAILNOTIFICACTION',
                ),

                'unity_type_id' => array(
                  'rules' => array( 'max_length' => 2, 'required' ),
                  'label' => 'UNITYTYPE',
                ),

                'no_units_id' => array(
                  'rules' => array( 'max_length' => 2, 'required' ),
                  'label' => 'NOUNITS',
                ),

                'periodicity_id' => array(
                  'rules' => array( 'max_length' => 2, 'required' ),
                  'label' => 'PERIODICITY',
                )
            );


    public $coments =  array(
                'medicine' => 'input|view|label#Medicina#',
                'description' => 'input|view|label#Datos importantes#',
                'mail_notificaction' => 'input|view|label#Recibir recordatorio en el correo electrónico#',
                'unity_type_id' => 'combox|view|label#Tipo de unidad#',
                'no_units_id' => 'combox|view|label#N° de unidades#',
                'periodicity_id' => 'combox|view|label#Periodicidad de la fórmula#',
);

}