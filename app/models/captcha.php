<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.com
     * @company Imaginamos.com | todos los derechos reservados
     */

                        

class captcha extends  DataMapper {

    /**
     * @var bigint Max length is 13.  unsigned.
     */
    public $captcha_id;

    /**
     * @var int Max length is 10.  unsigned.
     */
    public $captcha_time;

    /**
     * @var varchar Max length is 16.
     */
    public $ip_address;

    /**
     * @var varchar Max length is 20.
     */
    public $word;

    public $table = 'captcha';

    public $model = 'captcha';
    public $primarykey = 'captcha_id';
    public $_fields = array('captcha_id','captcha_time','ip_address','word');

    public $has_one =  array(
                'captcha_basic' => array(
                  'class' => 'captcha',
                  'other_field' => 'captcha_recurrences',
                  'join_table' => 'cms_captcha',
                  'join_self_as' => 'captcha',
                  'join_other_as' => 'captcha',
                )
            );



    public $has_many =  array(
                'captcha_recurrences' => array(
                  'class' => 'captcha',
                  'other_field' => 'captcha_basic',
                  'join_table' => 'cms_captcha',
                  'join_self_as' => 'captcha',
                  'join_other_as' => 'captcha',
                )
            );



    public function __construct($id = NULL) {
         parent::__construct($id);
    }


    public function get_data($id = '', $campo = 'name') {
        $obj = new $this->model();
        $arrList = array();
        if (empty($id)) {
             $obj->get_iterated();
              foreach ($obj as $value) {
                 $arrList = array('id' => $value->id,'name' => $value->{$campo});
              }
              return $arrList;
        } else {
              return $obj->get_by_id($id);
        }
    }


    public function get_captcha_list($campo="name",$where=array()) {
         $model = new captcha();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_captcha($join_retale="") {
         $model = new captcha();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_captcha_id($this->id);
         }else{
         	return $model->get_by_captcha_id($this->id);
         }
    }


    public function selected_id($related_id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->where_related($related, 'id', $related_id)->get();
        if ($obj->exists()) {
        	return $obj->id;
        } else {
        	return 0;
        }
    }


    public function selected_multiple_id($id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->join_related($related)->get_by_id($id);
        $array = array();
        if ($obj->exists()) {
        	foreach ($obj as $value) {
        		$array[] = $value->modelo_id;
        	}
        }
        return $array;
    }


    public function get_rule($campo, $rule){
         if(array_key_exists($rule, $this->validation[$campo]['rules']))
            return $this->validation[$campo]['rules'][$rule];
         else
            return false;
    }


    public function is_rule($campo, $rule){
         if(in_array($rule, $this->validation[$campo]['rules']))
            return true;
         else
            return false;
    }


    public function to_array_first_row() {
     $model = clone $this;
     $model->get_by_id(1);
     $datos = array();
      foreach ($this->fields as $key) {
           if($key != 'id')
             $datos[$key] = $model->{$key};
      }
      return $datos;
    }


    public $default_order_by = array('id' => 'desc');


    public function post_model_init($from_cache = FALSE){}


    public function _encrypt($field)
    {
          if (!empty($this->{$field}))
          {
              if (empty($this->salt))
              {
                  $this->salt = md5(uniqid(rand(), true));
              }
             $this->{$field} = sha1($this->salt . $this->{$field});
          }
    }


    public $validation =  array(
                'captcha_id' => array(
                  'rules' => array( 'min_length' => 0, 'max_length' => 13 ),
                  'label' => 'CAPTCHA',
                ),

                'captcha_time' => array(
                  'rules' => array( 'min_length' => 0, 'max_length' => 10, 'required' ),
                  'label' => 'CAPTCHATIME',
                ),

                'ip_address' => array(
                  'rules' => array( 'max_length' => 16, 'required' ),
                  'label' => 'IPADDRESS',
                ),

                'word' => array(
                  'rules' => array( 'max_length' => 20, 'required' ),
                  'label' => 'WORD',
                )
            );


    public $coments =  array(
);

}