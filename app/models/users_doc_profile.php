<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.com
     * @company Imaginamos.com | todos los derechos reservados
     */

                        

class users_doc_profile extends  DataMapper {

    /**
     * @var int Max length is 10.
     */
    public $id;

    /**
     * @var int Max length is 10.  unsigned.
     */
    public $users_id;

    /**
     * @var varchar Max length is 25.
     */
    public $profession;

    /**
     * @var varchar Max length is 45.
     */
    public $school;

    /**
     * @var varchar Max length is 45.
     */
    public $profesional_register;

    /**
     * @var int Max length is 4.
     */
    public $graduation_year;

    /**
     * @var varchar Max length is 54.
     */
    public $specialty;

    /**
     * @var varchar Max length is 45.
     */
    public $specialty_school;

    /**
     * @var varchar Max length is 45.
     */
    public $medical_register;

    /**
     * @var varchar Max length is 45.
     */
    public $society;

    /**
     * @var varchar Max length is 45.
     */
    public $work_company1;

    /**
     * @var varchar Max length is 45.
     */
    public $work_company2;

    /**
     * @var varchar Max length is 45.
     */
    public $work_company3;

    /**
     * @var varchar Max length is 45.
     */
    public $work_company4;

    /**
     * @var varchar Max length is 45.
     */
    public $address;

    /**
     * @var varchar Max length is 45.
     */
    public $average_consulting_cost;

    public $table = 'users_doc_profile';

    public $model = 'users_doc_profile';
    public $primarykey = 'id';
    public $_fields = array('id','users_id','profession','school','profesional_register','graduation_year','specialty','specialty_school','medical_register','society','work_company1','work_company2','work_company3','work_company4','address','average_consulting_cost');

    public $has_one =  array(
                'users' => array(
                  'class' => 'users',
                  'other_field' => 'users_doc_profile',
                  'join_other_as' => 'users',
                  'join_self_as' => 'users_doc_profile',
                  'join_table' => 'cms_users',
                )
            );



    public $has_many = array();



    public function __construct($id = NULL) {
         parent::__construct($id);
    }


    public function get_data($id = '', $campo = 'name') {
        $obj = new $this->model();
        $arrList = array();
        if (empty($id)) {
             $obj->get_iterated();
              foreach ($obj as $value) {
                 $arrList = array('id' => $value->id,'name' => $value->{$campo});
              }
              return $arrList;
        } else {
              return $obj->get_by_id($id);
        }
    }


    public function get_users_list($campo="name",$where=array()) {
         $model = new users();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_users($join_retale="") {
         $model = new users();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_users_doc_profile_id($this->id);
         }else{
         	return $model->get_by_users_doc_profile_id($this->id);
         }
    }


    public function selected_id($related_id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->where_related($related, 'id', $related_id)->get();
        if ($obj->exists()) {
        	return $obj->id;
        } else {
        	return 0;
        }
    }


    public function selected_multiple_id($id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->join_related($related)->get_by_id($id);
        $array = array();
        if ($obj->exists()) {
        	foreach ($obj as $value) {
        		$array[] = $value->modelo_id;
        	}
        }
        return $array;
    }


    public function get_rule($campo, $rule){
         if(array_key_exists($rule, $this->validation[$campo]['rules']))
            return $this->validation[$campo]['rules'][$rule];
         else
            return false;
    }


    public function is_rule($campo, $rule){
         if(in_array($rule, $this->validation[$campo]['rules']))
            return true;
         else
            return false;
    }


    public function to_array_first_row() {
     $model = clone $this;
     $model->get_by_id(1);
     $datos = array();
      foreach ($this->fields as $key) {
           if($key != 'id')
             $datos[$key] = $model->{$key};
      }
      return $datos;
    }


    public $default_order_by = array('id' => 'desc');


    public function post_model_init($from_cache = FALSE){}


    public function _encrypt($field)
    {
          if (!empty($this->{$field}))
          {
              if (empty($this->salt))
              {
                  $this->salt = md5(uniqid(rand(), true));
              }
             $this->{$field} = sha1($this->salt . $this->{$field});
          }
    }


    public $validation =  array(
                'id' => array(
                  'rules' => array( 'max_length' => 10 ),
                  'label' => 'ID',
                ),

                'users_id' => array(
                  'rules' => array( 'min_length' => 0, 'max_length' => 10, 'required' ),
                  'label' => 'USERS',
                ),

                'profession' => array(
                  'rules' => array( 'max_length' => 25 ),
                  'label' => 'PROFESSION',
                ),

                'school' => array(
                  'rules' => array( 'max_length' => 45 ),
                  'label' => 'SCHOOL',
                ),

                'profesional_register' => array(
                  'rules' => array( 'max_length' => 45 ),
                  'label' => 'PROFESIONALREGISTER',
                ),

                'graduation_year' => array(
                  'rules' => array( 'max_length' => 4, 'required' ),
                  'label' => 'GRADUATIONYEAR',
                ),

                'specialty' => array(
                  'rules' => array( 'max_length' => 54, 'required' ),
                  'label' => 'SPECIALTY',
                ),

                'specialty_school' => array(
                  'rules' => array( 'max_length' => 45 ),
                  'label' => 'SPECIALTYSCHOOL',
                ),

                'medical_register' => array(
                  'rules' => array( 'max_length' => 45 ),
                  'label' => 'MEDICALREGISTER',
                ),

                'society' => array(
                  'rules' => array( 'max_length' => 45 ),
                  'label' => 'SOCIETY',
                ),

                'work_company1' => array(
                  'rules' => array( 'max_length' => 45 ),
                  'label' => 'WORKCOMPANY1',
                ),

                'work_company2' => array(
                  'rules' => array( 'max_length' => 45 ),
                  'label' => 'WORKCOMPANY2',
                ),

                'work_company3' => array(
                  'rules' => array( 'max_length' => 45 ),
                  'label' => 'WORKCOMPANY3',
                ),

                'work_company4' => array(
                  'rules' => array( 'max_length' => 45 ),
                  'label' => 'WORKCOMPANY4',
                ),

                'address' => array(
                  'rules' => array( 'max_length' => 45 ),
                  'label' => 'ADDRESS',
                ),

                'average_consulting_cost' => array(
                  'rules' => array( 'max_length' => 45 ),
                  'label' => 'AVERAGECONSULTINGCOST',
                )
            );


    public $coments =  array(
                'profession' => 'input|view|label#Profesión#',
                'school' => 'input|view|label#Universidad#',
                'profesional_register' => 'input|view|label#Tarjeta profesional#',
                'graduation_year' => 'input|view|label#Año de graduación#',
                'specialty' => 'input|view|label#Especialidad#',
                'specialty_school' => 'input|view|label#Universidad (especialización)#',
                'medical_register' => 'input|view|label#Registro médico#',
                'society' => 'input|view|label#Sociedad a la que pertenece#',
                'work_company1' => 'input|view|label#Empresa donde trabaja 1#',
                'work_company2' => 'input|view|label#Empresa donde trabaja 2#',
                'work_company3' => 'input|view|label#Empresa donde trabaja 3#',
                'work_company4' => 'input|view|label#Empresa donde trabaja 4#',
                'address' => 'input|view|label#Dirección de contacto#',
                'average_consulting_cost' => 'input|view|label#Costo promedio de consulta#',
);

}