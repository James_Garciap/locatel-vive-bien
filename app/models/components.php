<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.com
     * @company Imaginamos.com | todos los derechos reservados
     */

                        

class components extends  DataMapper {

    /**
     * @var int Max length is 10.
     */
    public $id;

    /**
     * @var tinyint Max length is 2.
     */
    public $component_category_id;

    /**
     * @var varchar Max length is 50.
     */
    public $name;

    /**
     * @var text
     */
    public $description;

    /**
     * @var int Max length is 10.
     */
    public $parent;

    /**
     * @var int Max length is 10.  unsigned.
     */
    public $users_id;

    public $table = 'components';

    public $model = 'components';
    public $primarykey = 'id';
    public $_fields = array('id','component_category_id','name','description','parent','users_id');

    public $has_one =  array(
                'component_category' => array(
                  'class' => 'component_category',
                  'other_field' => 'components',
                  'join_other_as' => 'component_category',
                  'join_self_as' => 'components',
                  'join_table' => 'cms_component_category',
                ),

                'users' => array(
                  'class' => 'users',
                  'other_field' => 'components',
                  'join_other_as' => 'users',
                  'join_self_as' => 'components',
                  'join_table' => 'cms_users',
                )
            );



    public $has_many =  array(
                'component_fields' => array(
                  'class' => 'component_fields',
                  'other_field' => 'components',
                  'join_other_as' => 'component_fields',
                  'join_self_as' => 'components',
                  'join_table' => 'cms_component_fields',
                )
            );



    public function __construct($id = NULL) {
         parent::__construct($id);
    }


    public function get_data($id = '', $campo = 'name') {
        $obj = new $this->model();
        $arrList = array();
        if (empty($id)) {
             $obj->get_iterated();
              foreach ($obj as $value) {
                 $arrList = array('id' => $value->id,'name' => $value->{$campo});
              }
              return $arrList;
        } else {
              return $obj->get_by_id($id);
        }
    }


    public function get_component_category_list($campo="name",$where=array()) {
         $model = new component_category();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_component_category($join_retale="") {
         $model = new component_category();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_components_id($this->id);
         }else{
         	return $model->get_by_components_id($this->id);
         }
    }


    public function get_users_list($campo="name",$where=array()) {
         $model = new users();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_users($join_retale="") {
         $model = new users();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_components_id($this->id);
         }else{
         	return $model->get_by_components_id($this->id);
         }
    }


    public function selected_id($related_id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->where_related($related, 'id', $related_id)->get();
        if ($obj->exists()) {
        	return $obj->id;
        } else {
        	return 0;
        }
    }


    public function selected_multiple_id($id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->join_related($related)->get_by_id($id);
        $array = array();
        if ($obj->exists()) {
        	foreach ($obj as $value) {
        		$array[] = $value->modelo_id;
        	}
        }
        return $array;
    }


    public function get_rule($campo, $rule){
         if(array_key_exists($rule, $this->validation[$campo]['rules']))
            return $this->validation[$campo]['rules'][$rule];
         else
            return false;
    }


    public function is_rule($campo, $rule){
         if(in_array($rule, $this->validation[$campo]['rules']))
            return true;
         else
            return false;
    }


    public function to_array_first_row() {
     $model = clone $this;
     $model->get_by_id(1);
     $datos = array();
      foreach ($this->fields as $key) {
           if($key != 'id')
             $datos[$key] = $model->{$key};
      }
      return $datos;
    }


    public $default_order_by = array('id' => 'desc');


    public function post_model_init($from_cache = FALSE){}


    public function _encrypt($field)
    {
          if (!empty($this->{$field}))
          {
              if (empty($this->salt))
              {
                  $this->salt = md5(uniqid(rand(), true));
              }
             $this->{$field} = sha1($this->salt . $this->{$field});
          }
    }


    public $validation =  array(
                'id' => array(
                  'rules' => array( 'max_length' => 10 ),
                  'label' => 'ID',
                ),

                'component_category_id' => array(
                  'rules' => array( 'max_length' => 2, 'required' ),
                  'label' => 'COMPONENTCATEGORY',
                ),

                'name' => array(
                  'rules' => array( 'max_length' => 50 ),
                  'label' => 'NAME',
                ),

                'parent' => array(
                  'rules' => array( 'max_length' => 10 ),
                  'label' => 'PARENT',
                ),

                'users_id' => array(
                  'rules' => array( 'min_length' => 0, 'max_length' => 10, 'required' ),
                  'label' => 'USERS',
                )
            );


    public $coments =  array(
                'component_category_id' => 'combox|view|label#Tipo de componente#',
                'name' => 'input|view|label#Nombre#',
                'description' => 'input|view|label#Descripción#',
                'parent' => 'input|view|label#Relacionado..#',
);

}