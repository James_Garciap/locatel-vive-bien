<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.com
     * @company Imaginamos.com | todos los derechos reservados
     */

                        

class weeks extends  DataMapper {

    /**
     * @var int Max length is 10.
     */
    public $id;

    /**
     * @var varchar Max length is 25.
     */
    public $name;

    /**
     * @var text
     */
    public $description;

    /**
     * @var int Max length is 10.
     */
    public $stages_id;

    /**
     * @var int Max length is 10.
     */
    public $phases_id;

    public $table = 'weeks';

    public $model = 'weeks';
    public $primarykey = 'id';
    public $_fields = array('id','name','description','stages_id','phases_id');

    public $has_one =  array(
                'stages' => array(
                  'class' => 'stages',
                  'other_field' => 'weeks',
                  'join_other_as' => 'stages',
                  'join_self_as' => 'weeks',
                  'join_table' => 'cms_stages',
                ),

                'phases' => array(
                  'class' => 'phases',
                  'other_field' => 'weeks',
                  'join_other_as' => 'phases',
                  'join_self_as' => 'weeks',
                  'join_table' => 'cms_phases',
                )
            );



    public $has_many =  array(
                'reported_activity' => array(
                  'class' => 'reported_activity',
                  'other_field' => 'weeks',
                  'join_other_as' => 'reported_activity',
                  'join_self_as' => 'weeks',
                  'join_table' => 'cms_reported_activity',
                ),

                'workouts' => array(
                  'class' => 'workouts',
                  'other_field' => 'weeks',
                  'join_other_as' => 'workouts',
                  'join_self_as' => 'weeks',
                  'join_table' => 'cms_workouts',
                )
            );



    public function __construct($id = NULL) {
         parent::__construct($id);
    }


    public function get_data($id = '', $campo = 'name') {
        $obj = new $this->model();
        $arrList = array();
        if (empty($id)) {
             $obj->get_iterated();
              foreach ($obj as $value) {
                 $arrList = array('id' => $value->id,'name' => $value->{$campo});
              }
              return $arrList;
        } else {
              return $obj->get_by_id($id);
        }
    }


    public function get_stages_list($campo="name",$where=array()) {
         $model = new stages();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_stages($join_retale="") {
         $model = new stages();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_weeks_id($this->id);
         }else{
         	return $model->get_by_weeks_id($this->id);
         }
    }


    public function get_phases_list($campo="name",$where=array()) {
         $model = new phases();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_phases($join_retale="") {
         $model = new phases();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_weeks_id($this->id);
         }else{
         	return $model->get_by_weeks_id($this->id);
         }
    }


    public function selected_id($related_id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->where_related($related, 'id', $related_id)->get();
        if ($obj->exists()) {
        	return $obj->id;
        } else {
        	return 0;
        }
    }


    public function selected_multiple_id($id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->join_related($related)->get_by_id($id);
        $array = array();
        if ($obj->exists()) {
        	foreach ($obj as $value) {
        		$array[] = $value->modelo_id;
        	}
        }
        return $array;
    }


    public function get_rule($campo, $rule){
         if(array_key_exists($rule, $this->validation[$campo]['rules']))
            return $this->validation[$campo]['rules'][$rule];
         else
            return false;
    }


    public function is_rule($campo, $rule){
         if(in_array($rule, $this->validation[$campo]['rules']))
            return true;
         else
            return false;
    }


    public function to_array_first_row() {
     $model = clone $this;
     $model->get_by_id(1);
     $datos = array();
      foreach ($this->fields as $key) {
           if($key != 'id')
             $datos[$key] = $model->{$key};
      }
      return $datos;
    }


    public $default_order_by = array('id' => 'desc');


    public function post_model_init($from_cache = FALSE){}


    public function _encrypt($field)
    {
          if (!empty($this->{$field}))
          {
              if (empty($this->salt))
              {
                  $this->salt = md5(uniqid(rand(), true));
              }
             $this->{$field} = sha1($this->salt . $this->{$field});
          }
    }


    public $validation =  array(
                'id' => array(
                  'rules' => array( 'max_length' => 10 ),
                  'label' => 'ID',
                ),

                'name' => array(
                  'rules' => array( 'max_length' => 25 ),
                  'label' => 'NAME',
                ),

                'stages_id' => array(
                  'rules' => array( 'max_length' => 10, 'required' ),
                  'label' => 'STAGES',
                ),

                'phases_id' => array(
                  'rules' => array( 'max_length' => 10, 'required' ),
                  'label' => 'PHASES',
                )
            );


    public $coments =  array(
                'name' => 'input|view|label#Nombre#',
                'stages_id' => 'combox|view|label#Etapa#',
                'phases_id' => 'combox|view|label#Tipo de fase#',
);

}