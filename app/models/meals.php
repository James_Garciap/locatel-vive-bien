<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.com
     * @company Imaginamos.com | todos los derechos reservados
     */

                        

class meals extends  DataMapper {

    /**
     * @var tinyint Max length is 3.
     */
    public $id;

    /**
     * @var int Max length is 4.
     */
    public $diets_id;

    /**
     * @var text
     */
    public $description;

    /**
     * @var tinyint Max length is 2.
     */
    public $meals_type_id;

    public $table = 'meals';

    public $model = 'meals';
    public $primarykey = 'id';
    public $_fields = array('id','diets_id','description','meals_type_id');

    public $has_one =  array(
                'diets' => array(
                  'class' => 'diets',
                  'other_field' => 'meals',
                  'join_other_as' => 'diets',
                  'join_self_as' => 'meals',
                  'join_table' => 'cms_diets',
                ),

                'meals_type' => array(
                  'class' => 'meals_type',
                  'other_field' => 'meals',
                  'join_other_as' => 'meals_type',
                  'join_self_as' => 'meals',
                  'join_table' => 'cms_meals_type',
                )
            );



    public $has_many = array();



    public function __construct($id = NULL) {
         parent::__construct($id);
    }


    public function get_data($id = '', $campo = 'name') {
        $obj = new $this->model();
        $arrList = array();
        if (empty($id)) {
             $obj->get_iterated();
              foreach ($obj as $value) {
                 $arrList = array('id' => $value->id,'name' => $value->{$campo});
              }
              return $arrList;
        } else {
              return $obj->get_by_id($id);
        }
    }


    public function get_diets_list($campo="name",$where=array()) {
         $model = new diets();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_diets($join_retale="") {
         $model = new diets();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_meals_id($this->id);
         }else{
         	return $model->get_by_meals_id($this->id);
         }
    }


    public function get_meals_type_list($campo="name",$where=array()) {
         $model = new meals_type();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_meals_type($join_retale="") {
         $model = new meals_type();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_meals_id($this->id);
         }else{
         	return $model->get_by_meals_id($this->id);
         }
    }


    public function selected_id($related_id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->where_related($related, 'id', $related_id)->get();
        if ($obj->exists()) {
        	return $obj->id;
        } else {
        	return 0;
        }
    }


    public function selected_multiple_id($id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->join_related($related)->get_by_id($id);
        $array = array();
        if ($obj->exists()) {
        	foreach ($obj as $value) {
        		$array[] = $value->modelo_id;
        	}
        }
        return $array;
    }


    public function get_rule($campo, $rule){
         if(array_key_exists($rule, $this->validation[$campo]['rules']))
            return $this->validation[$campo]['rules'][$rule];
         else
            return false;
    }


    public function is_rule($campo, $rule){
         if(in_array($rule, $this->validation[$campo]['rules']))
            return true;
         else
            return false;
    }


    public function to_array_first_row() {
     $model = clone $this;
     $model->get_by_id(1);
     $datos = array();
      foreach ($this->fields as $key) {
           if($key != 'id')
             $datos[$key] = $model->{$key};
      }
      return $datos;
    }


    public $default_order_by = array('id' => 'desc');


    public function post_model_init($from_cache = FALSE){}


    public function _encrypt($field)
    {
          if (!empty($this->{$field}))
          {
              if (empty($this->salt))
              {
                  $this->salt = md5(uniqid(rand(), true));
              }
             $this->{$field} = sha1($this->salt . $this->{$field});
          }
    }


    public $validation =  array(
                'id' => array(
                  'rules' => array( 'max_length' => 3 ),
                  'label' => 'ID',
                ),

                'diets_id' => array(
                  'rules' => array( 'max_length' => 4, 'required' ),
                  'label' => 'DIETS',
                ),

                'meals_type_id' => array(
                  'rules' => array( 'max_length' => 2, 'required' ),
                  'label' => 'MEALSTYPE',
                )
            );


    public $coments =  array(
                'diets_id' => 'combox|view|label#Dieta N|#',
                'description' => 'input|view|label#Descripción#',
                'meals_type_id' => 'combox|view|label#Tipo de comida#',
);

}