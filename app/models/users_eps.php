<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.com
     * @company Imaginamos.com | todos los derechos reservados
     */

                        

class users_eps extends  DataMapper {

    /**
     * @var int Max length is 10.
     */
    public $id;

    /**
     * @var int Max length is 10.  unsigned.
     */
    public $users_id;

    /**
     * @var tinyint Max length is 2.
     */
    public $eps_id;

    public $table = 'users_eps';

    public $model = 'users_eps';
    public $primarykey = 'id';
    public $_fields = array('id','users_id','eps_id');

    public $has_one =  array(
                'users' => array(
                  'class' => 'users',
                  'other_field' => 'users_eps',
                  'join_other_as' => 'users',
                  'join_self_as' => 'users_eps',
                  'join_table' => 'cms_users',
                ),

                'eps' => array(
                  'class' => 'eps',
                  'other_field' => 'users_eps',
                  'join_other_as' => 'eps',
                  'join_self_as' => 'users_eps',
                  'join_table' => 'cms_eps',
                )
            );



    public $has_many = array();



    public function __construct($id = NULL) {
         parent::__construct($id);
    }


    public function get_data($id = '', $campo = 'name') {
        $obj = new $this->model();
        $arrList = array();
        if (empty($id)) {
             $obj->get_iterated();
              foreach ($obj as $value) {
                 $arrList = array('id' => $value->id,'name' => $value->{$campo});
              }
              return $arrList;
        } else {
              return $obj->get_by_id($id);
        }
    }


    public function get_users_list($campo="name",$where=array()) {
         $model = new users();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_users($join_retale="") {
         $model = new users();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_users_eps_id($this->id);
         }else{
         	return $model->get_by_users_eps_id($this->id);
         }
    }


    public function get_eps_list($campo="name",$where=array()) {
         $model = new eps();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_eps($join_retale="") {
         $model = new eps();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_users_eps_id($this->id);
         }else{
         	return $model->get_by_users_eps_id($this->id);
         }
    }


    public function selected_id($related_id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->where_related($related, 'id', $related_id)->get();
        if ($obj->exists()) {
        	return $obj->id;
        } else {
        	return 0;
        }
    }


    public function selected_multiple_id($id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->join_related($related)->get_by_id($id);
        $array = array();
        if ($obj->exists()) {
        	foreach ($obj as $value) {
        		$array[] = $value->modelo_id;
        	}
        }
        return $array;
    }


    public function get_rule($campo, $rule){
         if(array_key_exists($rule, $this->validation[$campo]['rules']))
            return $this->validation[$campo]['rules'][$rule];
         else
            return false;
    }


    public function is_rule($campo, $rule){
         if(in_array($rule, $this->validation[$campo]['rules']))
            return true;
         else
            return false;
    }


    public function to_array_first_row() {
     $model = clone $this;
     $model->get_by_id(1);
     $datos = array();
      foreach ($this->fields as $key) {
           if($key != 'id')
             $datos[$key] = $model->{$key};
      }
      return $datos;
    }


    public $default_order_by = array('id' => 'desc');


    public function post_model_init($from_cache = FALSE){}


    public function _encrypt($field)
    {
          if (!empty($this->{$field}))
          {
              if (empty($this->salt))
              {
                  $this->salt = md5(uniqid(rand(), true));
              }
             $this->{$field} = sha1($this->salt . $this->{$field});
          }
    }


    public $validation =  array(
                'id' => array(
                  'rules' => array( 'max_length' => 10 ),
                  'label' => 'ID',
                ),

                'users_id' => array(
                  'rules' => array( 'min_length' => 0, 'max_length' => 10, 'required' ),
                  'label' => 'USERS',
                ),

                'eps_id' => array(
                  'rules' => array( 'max_length' => 2, 'required' ),
                  'label' => 'EPS',
                )
            );


    public $coments =  array(
                'users_id' => 'combox|view|label#Usuario#',
                'eps_id' => 'combox|view|label#EPS#',
);

}