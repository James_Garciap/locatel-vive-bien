<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.com
     * @company Imaginamos.com | todos los derechos reservados
     */

                        

class component_fields extends  DataMapper {

    /**
     * @var int Max length is 10.
     */
    public $id;

    /**
     * @var int Max length is 10.
     */
    public $components_id;

    /**
     * @var text
     */
    public $value;

    /**
     * @var varchar Max length is 45.
     */
    public $field;

    /**
     * @var varchar Max length is 20.
     */
    public $type;

    /**
     * @var tinyint Max length is 2.
     */
    public $field_type_id;

    public $table = 'component_fields';

    public $model = 'component_fields';
    public $primarykey = 'id';
    public $_fields = array('id','components_id','value','field','type','field_type_id');

    public $has_one =  array(
                'components' => array(
                  'class' => 'components',
                  'other_field' => 'component_fields',
                  'join_other_as' => 'components',
                  'join_self_as' => 'component_fields',
                  'join_table' => 'cms_components',
                ),

                'field_type' => array(
                  'class' => 'field_type',
                  'other_field' => 'component_fields',
                  'join_other_as' => 'field_type',
                  'join_self_as' => 'component_fields',
                  'join_table' => 'cms_field_type',
                )
            );



    public $has_many = array();



    public function __construct($id = NULL) {
         parent::__construct($id);
    }


    public function get_data($id = '', $campo = 'name') {
        $obj = new $this->model();
        $arrList = array();
        if (empty($id)) {
             $obj->get_iterated();
              foreach ($obj as $value) {
                 $arrList = array('id' => $value->id,'name' => $value->{$campo});
              }
              return $arrList;
        } else {
              return $obj->get_by_id($id);
        }
    }


    public function get_components_list($campo="name",$where=array()) {
         $model = new components();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_components($join_retale="") {
         $model = new components();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_component_fields_id($this->id);
         }else{
         	return $model->get_by_component_fields_id($this->id);
         }
    }


    public function get_field_type_list($campo="name",$where=array()) {
         $model = new field_type();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_field_type($join_retale="") {
         $model = new field_type();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_component_fields_id($this->id);
         }else{
         	return $model->get_by_component_fields_id($this->id);
         }
    }


    public function selected_id($related_id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->where_related($related, 'id', $related_id)->get();
        if ($obj->exists()) {
        	return $obj->id;
        } else {
        	return 0;
        }
    }


    public function selected_multiple_id($id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->join_related($related)->get_by_id($id);
        $array = array();
        if ($obj->exists()) {
        	foreach ($obj as $value) {
        		$array[] = $value->modelo_id;
        	}
        }
        return $array;
    }


    public function get_rule($campo, $rule){
         if(array_key_exists($rule, $this->validation[$campo]['rules']))
            return $this->validation[$campo]['rules'][$rule];
         else
            return false;
    }


    public function is_rule($campo, $rule){
         if(in_array($rule, $this->validation[$campo]['rules']))
            return true;
         else
            return false;
    }


    public function to_array_first_row() {
     $model = clone $this;
     $model->get_by_id(1);
     $datos = array();
      foreach ($this->fields as $key) {
           if($key != 'id')
             $datos[$key] = $model->{$key};
      }
      return $datos;
    }


    public $default_order_by = array('id' => 'desc');


    public function post_model_init($from_cache = FALSE){}


    public function _encrypt($field)
    {
          if (!empty($this->{$field}))
          {
              if (empty($this->salt))
              {
                  $this->salt = md5(uniqid(rand(), true));
              }
             $this->{$field} = sha1($this->salt . $this->{$field});
          }
    }


    public $validation =  array(
                'id' => array(
                  'rules' => array( 'max_length' => 10 ),
                  'label' => 'ID',
                ),

                'components_id' => array(
                  'rules' => array( 'max_length' => 10, 'required' ),
                  'label' => 'COMPONENTS',
                ),

                'field' => array(
                  'rules' => array( 'max_length' => 45 ),
                  'label' => 'FIELD',
                ),

                'type' => array(
                  'rules' => array( 'max_length' => 20 ),
                  'label' => 'TYPE',
                ),

                'field_type_id' => array(
                  'rules' => array( 'max_length' => 2, 'required' ),
                  'label' => 'FIELDTYPE',
                )
            );


    public $coments =  array(
                'components_id' => 'combox|view|label#Componente#',
                'value' => 'input|view|label#Valor#',
                'field' => 'input|view|label#Campo#',
                'field_type_id' => 'combox|view|label#Tipo de campo#',
);

}