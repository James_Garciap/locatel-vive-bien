<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.com
     * @company Imaginamos.com | todos los derechos reservados
     */

                        

class routines extends  DataMapper {

    /**
     * @var int Max length is 4.
     */
    public $id;

    /**
     * @var int Max length is 4.
     */
    public $routines_categories_id;

    /**
     * @var varchar Max length is 25.
     */
    public $name;

    /**
     * @var text
     */
    public $description;

    public $table = 'routines';

    public $model = 'routines';
    public $primarykey = 'id';
    public $_fields = array('id','routines_categories_id','name','description');

    public $has_one =  array(
                'routines_categories' => array(
                  'class' => 'routines_categories',
                  'other_field' => 'routines',
                  'join_other_as' => 'routines_categories',
                  'join_self_as' => 'routines',
                  'join_table' => 'cms_routines_categories',
                )
            );



    public $has_many =  array(
      /*
                'reported_activity' => array(
                  'class' => 'reported_activity',
                  'other_field' => 'routines',
                  'join_other_as' => 'reported_activity',
                  'join_self_as' => 'routines',
                  'join_table' => 'cms_reported_activity',
                ),
      */

                'stages' => array(
                  'class' => 'stages',
                  'other_field' => 'routines',
                  'join_other_as' => 'stages',
                  'join_self_as' => 'routines',
                  'join_table' => 'cms_stages',
                ),
      /*
                'users' => array(
                  'class' => 'users',
                  'other_field' => 'routines',
                  'join_other_as' => 'users',
                  'join_self_as' => 'routines',
                  'join_table' => 'cms_users_routines',
                ),
      */
    
                'users_routines' => array(
                  'class' => 'users_routines',
                  'other_field' => 'routines',
                  'join_other_as' => 'users_routines',
                  'join_self_as' => 'routines',
                  'join_table' => 'cms_users_routines',
                )
            );



    public function __construct($id = NULL) {
         parent::__construct($id);
    }


    public function get_data($id = '', $campo = 'name') {
        $obj = new $this->model();
        $arrList = array();
        if (empty($id)) {
             $obj->get_iterated();
              foreach ($obj as $value) {
                 $arrList = array('id' => $value->id,'name' => $value->{$campo});
              }
              return $arrList;
        } else {
              return $obj->get_by_id($id);
        }
    }


    public function get_routines_categories_list($campo="name",$where=array()) {
         $model = new routines_categories();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_routines_categories($join_retale="") {
         $model = new routines_categories();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_routines_id($this->id);
         }else{
         	return $model->get_by_routines_id($this->id);
         }
    }


    public function selected_id($related_id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->where_related($related, 'id', $related_id)->get();
        if ($obj->exists()) {
        	return $obj->id;
        } else {
        	return 0;
        }
    }


    public function selected_multiple_id($id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->join_related($related)->get_by_id($id);
        $array = array();
        if ($obj->exists()) {
        	foreach ($obj as $value) {
        		$array[] = $value->modelo_id;
        	}
        }
        return $array;
    }


    public function get_rule($campo, $rule){
         if(array_key_exists($rule, $this->validation[$campo]['rules']))
            return $this->validation[$campo]['rules'][$rule];
         else
            return false;
    }


    public function is_rule($campo, $rule){
         if(in_array($rule, $this->validation[$campo]['rules']))
            return true;
         else
            return false;
    }


    public function to_array_first_row() {
     $model = clone $this;
     $model->get_by_id(1);
     $datos = array();
      foreach ($this->fields as $key) {
           if($key != 'id')
             $datos[$key] = $model->{$key};
      }
      return $datos;
    }


    public $default_order_by = array('id' => 'desc');


    public function post_model_init($from_cache = FALSE){}


    public function _encrypt($field)
    {
          if (!empty($this->{$field}))
          {
              if (empty($this->salt))
              {
                  $this->salt = md5(uniqid(rand(), true));
              }
             $this->{$field} = sha1($this->salt . $this->{$field});
          }
    }


    public $validation =  array(
                'id' => array(
                  'rules' => array( 'max_length' => 4 ),
                  'label' => 'ID',
                ),

                'routines_categories_id' => array(
                  'rules' => array( 'max_length' => 4, 'required' ),
                  'label' => 'ROUTINESCATEGORIES',
                ),

                'name' => array(
                  'rules' => array( 'max_length' => 25 ),
                  'label' => 'NAME',
                )
            );


    public $coments =  array(
                'routines_categories_id' => 'combox|view|label#Categoríar#',
                'name' => 'input|view|label#Nombre#',
                'description' => 'input|view|label#Descripción#',
);

}