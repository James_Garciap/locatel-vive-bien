<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.com
     * @company Imaginamos.com | todos los derechos reservados
     */

                        

class frontmenu_items extends  DataMapper {

    /**
     * @var int Max length is 4.
     */
    public $id;

    /**
     * @var int Max length is 4.
     */
    public $frontmenu_id;

    /**
     * @var varchar Max length is 25.
     */
    public $label;

    /**
     * @var varchar Max length is 250.
     */
    public $value;

    /**
     * @var int Max length is 4.
     */
    public $parent;

    public $table = 'frontmenu_items';

    public $model = 'frontmenu_items';
    public $primarykey = 'id';
    public $_fields = array('id','frontmenu_id','label','value','parent');

    public $has_one =  array(
                'frontmenu' => array(
                  'class' => 'frontmenu',
                  'other_field' => 'frontmenu_items',
                  'join_other_as' => 'frontmenu',
                  'join_self_as' => 'frontmenu_items',
                  'join_table' => 'cms_frontmenu',
                )
            );



    public $has_many = array();



    public function __construct($id = NULL) {
         parent::__construct($id);
    }


    public function get_data($id = '', $campo = 'name') {
        $obj = new $this->model();
        $arrList = array();
        if (empty($id)) {
             $obj->get_iterated();
              foreach ($obj as $value) {
                 $arrList = array('id' => $value->id,'name' => $value->{$campo});
              }
              return $arrList;
        } else {
              return $obj->get_by_id($id);
        }
    }


    public function get_frontmenu_list($campo="name",$where=array()) {
         $model = new frontmenu();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_frontmenu($join_retale="") {
         $model = new frontmenu();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_frontmenu_items_id($this->id);
         }else{
         	return $model->get_by_frontmenu_items_id($this->id);
         }
    }


    public function selected_id($related_id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->where_related($related, 'id', $related_id)->get();
        if ($obj->exists()) {
        	return $obj->id;
        } else {
        	return 0;
        }
    }


    public function selected_multiple_id($id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->join_related($related)->get_by_id($id);
        $array = array();
        if ($obj->exists()) {
        	foreach ($obj as $value) {
        		$array[] = $value->modelo_id;
        	}
        }
        return $array;
    }


    public function get_rule($campo, $rule){
         if(array_key_exists($rule, $this->validation[$campo]['rules']))
            return $this->validation[$campo]['rules'][$rule];
         else
            return false;
    }


    public function is_rule($campo, $rule){
         if(in_array($rule, $this->validation[$campo]['rules']))
            return true;
         else
            return false;
    }


    public function to_array_first_row() {
     $model = clone $this;
     $model->get_by_id(1);
     $datos = array();
      foreach ($this->fields as $key) {
           if($key != 'id')
             $datos[$key] = $model->{$key};
      }
      return $datos;
    }


    public $default_order_by = array('id' => 'desc');


    public function post_model_init($from_cache = FALSE){}


    public function _encrypt($field)
    {
          if (!empty($this->{$field}))
          {
              if (empty($this->salt))
              {
                  $this->salt = md5(uniqid(rand(), true));
              }
             $this->{$field} = sha1($this->salt . $this->{$field});
          }
    }


    public $validation =  array(
                'id' => array(
                  'rules' => array( 'max_length' => 4 ),
                  'label' => 'ID',
                ),

                'frontmenu_id' => array(
                  'rules' => array( 'max_length' => 4, 'required' ),
                  'label' => 'FRONTMENU',
                ),

                'label' => array(
                  'rules' => array( 'max_length' => 25 ),
                  'label' => 'LABEL',
                ),

                'value' => array(
                  'rules' => array( 'max_length' => 250 ),
                  'label' => 'VALUE',
                ),

                'parent' => array(
                  'rules' => array( 'max_length' => 4 ),
                  'label' => 'PARENT',
                )
            );


    public $coments =  array(
                'frontmenu_id' => 'combox|view|label#Menú superiorr#',
                'label' => 'input|view|label#Etiqueta#',
                'value' => 'input|view|label#Valor (link)#',
                'parent' => 'input|view|label#Parent#',
);

}