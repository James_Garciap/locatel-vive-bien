<?php

    /**
     * @autor Elbert Tous
     * @email elbert.tous@imaginamos.com
     * @company Imaginamos.com | todos los derechos reservados
     */

                        

class users_prepaid_health extends  DataMapper {

    /**
     * @var int Max length is 11.
     */
    public $id;

    /**
     * @var int Max length is 11.
     */
    public $users_id;

    /**
     * @var int Max length is 11.
     */
    public $prepaid_health_id;

    /**
     * @var int Max length is 11.
     */
    public $logic_state;

    /**
     * @var date
     */
    public $date_insert;

    public $table = 'users_prepaid_health';

    public $model = 'users_prepaid_health';
    public $primarykey = 'id';
    public $_fields = array('id','users_id','prepaid_health_id','logic_state','date_insert');

    public $has_one =  array(
                
      /*
                'users_prepaid_health_basic' => array(
                  'class' => 'users_prepaid_health',
                  'other_field' => 'users_prepaid_health_recurrences',
                  'join_table' => 'cms_users_prepaid_health',
                  'join_self_as' => 'users_prepaid_health',
                  'join_other_as' => 'users_prepaid_health',
                ),
      */
                'users' => array(
                  'class' => 'users',
                  'other_field' => 'users_prepaid_health',
                  'join_other_as' => 'users',
                  'join_self_as' => 'users_prepaid_health',
                  'join_table' => 'cms_users',
                ),

                'prepaid_health' => array(
                  'class' => 'prepaid_health',
                  'other_field' => 'users_prepaid_health',
                  'join_other_as' => 'prepaid_health',
                  'join_self_as' => 'users_prepaid_health',
                  'join_table' => 'cms_prepaid_health',
                )
            );



    public $has_many =  array(
      /*
                'users_prepaid_health_recurrences' => array(
                  'class' => 'users_prepaid_health',
                  'other_field' => 'users_prepaid_health_basic',
                  'join_table' => 'cms_users_prepaid_health',
                  'join_self_as' => 'users_prepaid_health',
                  'join_other_as' => 'users_prepaid_health',
                )
      */
            );



    public function __construct($id = NULL) {
         parent::__construct($id);
    }


    public function get_data($id = '', $campo = 'name') {
        $obj = new $this->model();
        $arrList = array();
        if (empty($id)) {
             $obj->get_iterated();
              foreach ($obj as $value) {
                 $arrList = array('id' => $value->id,'name' => $value->{$campo});
              }
              return $arrList;
        } else {
              return $obj->get_by_id($id);
        }
    }


    public function get_users_prepaid_health_list($campo="name",$where=array()) {
         $model = new users_prepaid_health();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_users_prepaid_health($join_retale="") {
         $model = new users_prepaid_health();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_users_prepaid_health_id($this->id);
         }else{
         	return $model->get_by_users_prepaid_health_id($this->id);
         }
    }


    public function get_users_list($campo="name",$where=array()) {
         $model = new users();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_users($join_retale="") {
         $model = new users();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_users_prepaid_health_id($this->id);
         }else{
         	return $model->get_by_users_prepaid_health_id($this->id);
         }
    }


    public function get_prepaid_health_list($campo="name",$where=array()) {
         $model = new prepaid_health();
         $model->where($where)->get();
         $arrList = array();
         foreach ($model as $k) {
         	$arrList [] = array(
         		'id' => $k->id,
         		'name' => $k->{$campo},
         	);
         }
         return $arrList;
    }


    public function get_prepaid_health($join_retale="") {
         $model = new prepaid_health();
         if($join_retale!=""){
         	return $model->join_related($join_retale)->get_by_users_prepaid_health_id($this->id);
         }else{
         	return $model->get_by_users_prepaid_health_id($this->id);
         }
    }


    public function selected_id($related_id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->where_related($related, 'id', $related_id)->get();
        if ($obj->exists()) {
        	return $obj->id;
        } else {
        	return 0;
        }
    }


    public function selected_multiple_id($id = '', $related = 'modelo') {
        $obj = new $this->model();
        $obj->join_related($related)->get_by_id($id);
        $array = array();
        if ($obj->exists()) {
        	foreach ($obj as $value) {
        		$array[] = $value->modelo_id;
        	}
        }
        return $array;
    }


    public function get_rule($campo, $rule){
         if(array_key_exists($rule, $this->validation[$campo]['rules']))
            return $this->validation[$campo]['rules'][$rule];
         else
            return false;
    }


    public function is_rule($campo, $rule){
         if(in_array($rule, $this->validation[$campo]['rules']))
            return true;
         else
            return false;
    }


    public function to_array_first_row() {
     $model = clone $this;
     $model->get_by_id(1);
     $datos = array();
      foreach ($this->fields as $key) {
           if($key != 'id')
             $datos[$key] = $model->{$key};
      }
      return $datos;
    }


    public $default_order_by = array('id' => 'desc');


    public function post_model_init($from_cache = FALSE){}


    public function _encrypt($field)
    {
          if (!empty($this->{$field}))
          {
              if (empty($this->salt))
              {
                  $this->salt = md5(uniqid(rand(), true));
              }
             $this->{$field} = sha1($this->salt . $this->{$field});
          }
    }


    public $validation =  array(
                'users_prepaid_health_id' => array(
                  'rules' => array( 'max_length' => 11 ),
                  'label' => 'USERSPREPAIDHEALTH',
                ),

                'users_id' => array(
                  'rules' => array( 'max_length' => 11, 'required' ),
                  'label' => 'USERS',
                ),

                'prepaid_health_id' => array(
                  'rules' => array( 'max_length' => 11, 'required' ),
                  'label' => 'PREPAIDHEALTH',
                ),

                'logic_state' => array(
                  'rules' => array( 'max_length' => 11 ),
                  'label' => 'LOGICSTATE',
                ),

                'date_insert' => array(
                  'rules' => array( 'valid_date' ),
                  'label' => 'DATEINSERT',
                )
            );


    public $coments =  array(
);

}