DROP TABLE IF EXISTS `cms_static_texts`;
CREATE TABLE IF NOT EXISTS `cms_static_texts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT 'input|view|label#Nombre del texto#',
  `description` text NOT NULL COMMENT 'input|view|label#Detalle (a mostrar)#',
  `date` text NOT NULL,
  `users_id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;