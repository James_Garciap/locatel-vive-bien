DROP TABLE IF EXISTS `cms_slider`;
CREATE TABLE IF NOT EXISTS `cms_slider` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `text1` varchar(200) NOT NULL COMMENT 'input|view|label#Texto 1#',
  `text2` varchar(200) NOT NULL COMMENT 'input|view|label#Texto 2#',
  `imagen_id` int(11) NOT NULL COMMENT 'input|view|label#Imagen#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;