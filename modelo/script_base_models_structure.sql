SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_api_oauth`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vivebien`.`cms_api_oauth` ;

CREATE TABLE IF NOT EXISTS `vivebien`.`cms_api_oauth` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `provider` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `strategy` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `api_key` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `api_secret` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `scope` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `active` TINYINT(4) NOT NULL DEFAULT '0',
  `active_oauth` TINYINT(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `vivebien`.`cms_contacto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vivebien`.`cms_contacto` ;

CREATE TABLE IF NOT EXISTS `vivebien`.`cms_contacto` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `direccion` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `telefono` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `fax` VARCHAR(18) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `celular` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `email` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `ciudad` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `cordenada_x` DOUBLE NOT NULL DEFAULT '0',
  `cordenada_y` DOUBLE NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `vivebien`.`cms_departamento`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vivebien`.`cms_departamento` ;

CREATE TABLE IF NOT EXISTS `vivebien`.`cms_departamento` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 34;


-- -----------------------------------------------------
-- Table `vivebien`.`cms_groups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vivebien`.`cms_groups` ;

CREATE TABLE IF NOT EXISTS `vivebien`.`cms_groups` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `description` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `vivebien`.`cms_permissions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vivebien`.`cms_permissions` ;

CREATE TABLE IF NOT EXISTS `vivebien`.`cms_permissions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `var` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `type` ENUM('module','function','component') CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `vivebien`.`cms_groups_permissions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vivebien`.`cms_groups_permissions` ;

CREATE TABLE IF NOT EXISTS `vivebien`.`cms_groups_permissions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `group_id` INT(10) UNSIGNED NOT NULL,
  `permission_id` INT(11) NOT NULL,
  `view` TINYINT(1) NULL DEFAULT '0',
  `create` TINYINT(1) NULL DEFAULT '0',
  `update` TINYINT(1) NULL DEFAULT '0',
  `delete` TINYINT(1) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_cms_users_permissions_cms_permissions1_idx` (`permission_id` ASC),
  INDEX `fk_cms_groups_permissions_cms_groups1_idx` (`group_id` ASC),
  CONSTRAINT `fk_cms_groups_permissions_cms_groups1`
    FOREIGN KEY (`group_id`)
    REFERENCES `vivebien`.`cms_groups` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_groups_permissions_cms_permissions1`
    FOREIGN KEY (`permission_id`)
    REFERENCES `vivebien`.`cms_permissions` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `vivebien`.`cms_imagen`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vivebien`.`cms_imagen` ;

CREATE TABLE IF NOT EXISTS `vivebien`.`cms_imagen` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'inputhidden|none',
  `path` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `name` VARCHAR(70) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 70
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `vivebien`.`cms_login_attempts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vivebien`.`cms_login_attempts` ;

CREATE TABLE IF NOT EXISTS `vivebien`.`cms_login_attempts` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` VARBINARY(16) NOT NULL,
  `login` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `time` INT(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `vivebien`.`cms_menu`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vivebien`.`cms_menu` ;

CREATE TABLE IF NOT EXISTS `vivebien`.`cms_menu` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `parent` INT(11) NULL DEFAULT NULL,
  `name` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `name_short` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `url` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `content` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `image` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `nombre_UNIQUE` (`name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `vivebien`.`cms_municipios`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vivebien`.`cms_municipio` ;

CREATE TABLE IF NOT EXISTS `vivebien`.`cms_municipio` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `departamento_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_cms_municipio_cms_departamento1` (`departamento_id` ASC),
  CONSTRAINT `fk_cms_municipio_cms_departamento1`
    FOREIGN KEY (`departamento_id`)
    REFERENCES `vivebien`.`cms_departamento` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1121;


-- -----------------------------------------------------
-- Table `vivebien`.`cms_oauth_config`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vivebien`.`cms_oauth_config` ;

CREATE TABLE IF NOT EXISTS `vivebien`.`cms_oauth_config` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `uri` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `var` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `vivebien`.`cms_redes_sociales`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vivebien`.`cms_redes_sociales` ;

CREATE TABLE IF NOT EXISTS `vivebien`.`cms_redes_sociales` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `red_social` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `link_red` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `fecha_creacion` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `vivebien`.`cms_sessions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vivebien`.`cms_sessions` ;

CREATE TABLE IF NOT EXISTS `vivebien`.`cms_sessions` (
  `session_id` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL DEFAULT '0',
  `ip_address` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL DEFAULT '0',
  `user_agent` VARCHAR(120) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `last_activity` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  PRIMARY KEY (`session_id`),
  INDEX `last_activity_idx` (`last_activity` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `vivebien`.`cms_users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vivebien`.`cms_users` ;

CREATE TABLE IF NOT EXISTS `vivebien`.`cms_users` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` VARBINARY(16) NOT NULL,
  `username` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `password` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `salt` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `email` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `activation_code` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `forgotten_password_code` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `forgotten_password_time` INT(11) UNSIGNED NULL DEFAULT NULL,
  `remember_code` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `created_on` INT(11) UNSIGNED NOT NULL,
  `last_login` INT(11) UNSIGNED NULL DEFAULT NULL,
  `active` TINYINT(1) UNSIGNED NULL DEFAULT NULL,
  `first_name` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `last_name` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `company` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `phone` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `vivebien`.`cms_users_groups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vivebien`.`cms_users_groups` ;

CREATE TABLE IF NOT EXISTS `vivebien`.`cms_users_groups` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(10) UNSIGNED NOT NULL,
  `group_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `user_users_groups` (`user_id` ASC),
  INDEX `group_users_groups` (`group_id` ASC),
  CONSTRAINT `group_users_groups`
    FOREIGN KEY (`group_id`)
    REFERENCES `vivebien`.`cms_groups` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `user_users_groups`
    FOREIGN KEY (`user_id`)
    REFERENCES `vivebien`.`cms_users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
