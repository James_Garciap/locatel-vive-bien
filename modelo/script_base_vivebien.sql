SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- CREATE SCHEMA IF NOT EXISTS `vivebien` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
-- USE `vivebien` ;

-- -----------------------------------------------------
-- Table `vivebien`.`cms_activity_level`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_activity_level` (
  `id` TINYINT(2) AUTO_INCREMENT NOT NULL ,
  `name` VARCHAR(75) NULL COMMENT 'input|view|label#Descripción#',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Tabla para administrar la información de la lista \"¿Qué nive /* comment truncated */ /*l de actividad físíca realizas?"  en el registro de usuario*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_routines_categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_routines_categories` (
  `id` INT(4) AUTO_INCREMENT NOT NULL ,
  `name` VARCHAR(25) NULL COMMENT 'input|view|label#Nombre#',
  `description` TEXT NULL COMMENT 'input|view|label#Description#',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Tabla para adminsitrar las categorías de rutinas disponibles /* comment truncated */ /* */';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_routines`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_routines` (
  `id` INT(4) AUTO_INCREMENT NOT NULL ,
  `routines_categories_id` INT(4) NOT NULL COMMENT 'combox|view|label#Categoríar#',
  `name` VARCHAR(25) NULL COMMENT 'input|view|label#Nombre#',
  `description` TEXT NULL COMMENT 'input|view|label#Descripción#',
  PRIMARY KEY (`id`),
  INDEX `fk_routines_routines_categories1_idx` (`routines_categories_id` ASC),
  CONSTRAINT `fk_cms_routines_routines_cms_categories1`
    FOREIGN KEY (`routines_categories_id`)
    REFERENCES `vivebien`.`cms_routines_categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Taba para administrar las rutinas de ejercicios que manejará /* comment truncated */ /* la aplicación*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_users`
-- -----------------------------------------------------
ALTER TABLE  `vivebien`.`cms_users` ADD COLUMN (
  -- `id` INT(10) AUTO_INCREMENT NOT NULL ,
  -- `name` VARCHAR(50) NULL COMMENT 'input|view|label#Nombres y apellidos#',
  `sex` VARCHAR(6) NULL COMMENT 'inputradio|view|label#Sexo#',
  `birthday` VARCHAR(10) NULL COMMENT 'input|view|label#Fecha de nacimiento#',
  -- `email` VARCHAR(76) NULL COMMENT 'input|view|label#Correo electrónico#',
  `doctor` TINYINT(1) NULL COMMENT 'inputcheckbox|view|label#¿Es ustde médico?#',
  -- `password` VARCHAR(76) NULL COMMENT 'inputpassword|view|label#Contraseña#',
  `height` INT(3) NULL COMMENT 'input|view|label#Altura#',
  `weight` INT(3) NULL COMMENT 'input|view|label#Peso#',
  `activity_level_id` TINYINT(2) NOT NULL COMMENT 'combox|view|label#Nivel de actividad física que realizas#',
  `routines_id` INT(10) NOT NULL,
  -- PRIMARY KEY (`id`),
  INDEX `fk_cms_users_activity_level1_idx` (`activity_level_id` ASC),
  INDEX `fk_cms_users_routines1_idx` (`routines_id` ASC),
  CONSTRAINT `fk_cms_users_cms_activity_level1`
    FOREIGN KEY (`activity_level_id`)
    REFERENCES `vivebien`.`cms_activity_level` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_users_cms_routines1`
    FOREIGN KEY (`routines_id`)
    REFERENCES `vivebien`.`cms_routines` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
-- ENGINE = InnoDB
-- COMMENT = 'Tabla propia del CMS. En este diagrama solo se simula aunque /* comment truncated */ /* hay que sincronizarla con la original al momento de la integración de los esquemas.*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_users_doc_profile`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_users_doc_profile` (
  `id` INT(10) AUTO_INCREMENT NOT NULL ,
  `users_id` INT(10) unsigned  NOT NULL,
  `profession` VARCHAR(25) NULL COMMENT 'input|view|label#Profesión#',
  `school` VARCHAR(45) NULL COMMENT 'input|view|label#Universidad#',
  `profesional_register` VARCHAR(45) NULL COMMENT 'input|view|label#Tarjeta profesional#',
  `specialty_school` VARCHAR(45) NULL COMMENT 'input|view|label#Universidad (especialización)#',
  `medical_register` VARCHAR(45) NULL COMMENT 'input|view|label#Registro médico#',
  `society` VARCHAR(45) NULL COMMENT 'input|view|label#Sociedad a la que pertenece#',
  `work_company` VARCHAR(45) NULL COMMENT 'input|view|label#Empresa donde trabaja#',
  `address` VARCHAR(45) NULL COMMENT 'input|view|label#Dirección de contacto#',
  `average_consulting_cost` VARCHAR(45) NULL COMMENT 'input|view|label#Costo promedio de consulta#',
  PRIMARY KEY (`id`),
  INDEX `fk_users_doc_profile_users_idx` (`users_id` ASC),
  CONSTRAINT `fk_cms_users_doc_profile_cms_users`
    FOREIGN KEY (`users_id`)
    REFERENCES `vivebien`.`cms_users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabla para almacenar información extra de usuario (en el cas /* comment truncated */ /*o de los usuarios médicos)*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_unity_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_unity_type` (
  `id` TINYINT(2) AUTO_INCREMENT NOT NULL ,
  `name` VARCHAR(25) NULL COMMENT 'input|view|label#Tipo de unidad#',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Tabla para administrar la información de la lista \"Tipo de u /* comment truncated */ /*nidad" del módulo de fórmula médica*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_no_units`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_no_units` (
  `id` TINYINT(2) AUTO_INCREMENT NOT NULL ,
  `name` VARCHAR(4) NULL COMMENT 'input|view|label#Cantidad#',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Tabla para administrar la información de la lista \"Número de /* comment truncated */ /* unidades" del módulo de fórmula médica*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_periodicity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_periodicity` (
  `id` TINYINT(2) AUTO_INCREMENT NOT NULL ,
  `name` VARCHAR(20) NULL COMMENT 'input|view|label#Periodicidad#',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Tabla para administrar la información de la lista \"Periodici /* comment truncated */ /*dad" del módulo de fórmula médica*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_medical_formulate`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_medical_formulate` (
  `id` INT(4) AUTO_INCREMENT NOT NULL ,
  `users_id` INT(10) unsigned  NOT NULL,
  `medicine` VARCHAR(45) NULL COMMENT 'input|view|label#Medicina#',
  `description` TEXT NULL COMMENT 'input|view|label#Datos importantes#',
  `mail_notificaction` TINYINT(1) NULL COMMENT 'input|view|label#Recibir recordatorio en el correo electrónico#',
  `unity_type_id` TINYINT(2) NOT NULL COMMENT 'combox|view|label#Tipo de unidad#',
  `no_units_id` TINYINT(2) NOT NULL COMMENT 'combox|view|label#N° de unidades#',
  `periodicity_id` TINYINT(2) NOT NULL COMMENT 'combox|view|label#Periodicidad de la fórmula#',
  PRIMARY KEY (`id`),
  INDEX `fk_medical_formulate_cms_users1_idx` (`users_id` ASC),
  INDEX `fk_medical_formulate_unity_type1_idx` (`unity_type_id` ASC),
  INDEX `fk_medical_formulate_no_units1_idx` (`no_units_id` ASC),
  INDEX `fk_medical_formulate_periodicity1_idx` (`periodicity_id` ASC),
  CONSTRAINT `fk_cms_medical_formulate_cms_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `vivebien`.`cms_users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_medical_formulate_cms_unity_type1`
    FOREIGN KEY (`unity_type_id`)
    REFERENCES `vivebien`.`cms_unity_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_medical_formulate_cms_no_units1`
    FOREIGN KEY (`no_units_id`)
    REFERENCES `vivebien`.`cms_no_units` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_medical_formulate_cms_periodicity1`
    FOREIGN KEY (`periodicity_id`)
    REFERENCES `vivebien`.`cms_periodicity` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabla para el módulo de Fórmula médica (administrar informac /* comment truncated */ /*ión de formulación médica del usuario)*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_users_activity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_users_activity` (
  `id` INT(10) AUTO_INCREMENT NOT NULL ,
  `users_id` INT(10) unsigned  NOT NULL,
  `action` VARCHAR(80) NULL COMMENT 'input|view|label#Acción#',
  `module` VARCHAR(25) NULL COMMENT 'input|view|label#Módulo#',
  `description` TEXT NULL COMMENT 'input|view|label#Descripción#',
  `related_id` INT(10) NULL COMMENT 'input|view|label#Relacionado#',
  `date` VARCHAR(19) NULL COMMENT 'input|view|label#Fecha#',
  PRIMARY KEY (`id`),
  INDEX `fk_users_activity_cms_users1_idx` (`users_id` ASC),
  CONSTRAINT `fk_cms_users_activity_cms_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `vivebien`.`cms_users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabla para recolectar información de la intereacción del usu /* comment truncated */ /*ario con el sistema para la sección "Actividad reciente" */';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_activity_minutes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_activity_minutes` (
  `id` TINYINT(2) AUTO_INCREMENT NOT NULL ,
  `name` VARCHAR(5) NULL COMMENT 'input|view|label#Minutos#',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Tabla para administrar la lista \"Minutos de actividad\" en el /* comment truncated */ /* formulario " Reporta tu actividad
- Actualmente en desuso, por definir si el campo apunta a esta lista o se deja abierto*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_stages`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_stages` (
  `id` INT(10) AUTO_INCREMENT NOT NULL ,
  `routines_id` INT(4) NOT NULL COMMENT 'combox|view|label#Rutinar#',
  `name` VARCHAR(25) NULL COMMENT 'input|view|label#Nombre#',
  `type` VARCHAR(25) NULL COMMENT 'input|view|label#Tipo#',
  `description` TEXT NULL COMMENT 'input|view|label#Descripción#',
  PRIMARY KEY (`id`),
  INDEX `fk_workouts_routines1_idx` (`routines_id` ASC),
  CONSTRAINT `fk_cms_workouts_cms_routines1`
    FOREIGN KEY (`routines_id`)
    REFERENCES `vivebien`.`cms_routines` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tablas para administrar las etapas de una rutina de ejericio';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_phases`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_phases` (
  `id` INT(4) AUTO_INCREMENT NOT NULL ,
  `name` VARCHAR(30) NULL COMMENT 'input|view|label#Nombre#',
  `description` TEXT NULL COMMENT 'input|view|label#Descripción#',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Tabla para administrar los tipos de  fases de rutinas dispob /* comment truncated */ /*ibles*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_weeks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_weeks` (
  `id` INT(10) AUTO_INCREMENT NOT NULL ,
  `name` VARCHAR(25) NULL COMMENT 'input|view|label#Nombre#',
  `description` TEXT NULL,
  `stages_id` INT(10) NOT NULL COMMENT 'combox|view|label#Etapa#',
  `phases_id` INT(10) NOT NULL COMMENT 'combox|view|label#Tipo de fase#',
  PRIMARY KEY (`id`),
  INDEX `fk_weeks_stages1_idx` (`stages_id` ASC),
  INDEX `fk_weeks_phases1_idx` (`phases_id` ASC),
  CONSTRAINT `fk_cms_weeks_cms_stages1`
    FOREIGN KEY (`stages_id`)
    REFERENCES `vivebien`.`cms_stages` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_weeks_cms_phases1`
    FOREIGN KEY (`phases_id`)
    REFERENCES `vivebien`.`cms_phases` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabla para dministrar las semanas de una fase, de una rutina /* comment truncated */ /* de ejercicios*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_reported_activity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_reported_activity` (
  `id` INT(10) AUTO_INCREMENT NOT NULL ,
  `users_id` INT(10) unsigned  NOT NULL COMMENT 'combox|view|label#Usuario#',
  `weight` VARCHAR(5) NULL COMMENT 'input|view|label#Peso#',
  `time` VARCHAR(8) NULL COMMENT 'input|view|label#Tiempo#',
  `description` TEXT NULL COMMENT 'input|view|label#Descripción#',
  `actitity_minutes_id` TINYINT(2) NOT NULL COMMENT 'combox|view|label#Minutos de actividad#',
  `weeks_id` INT(10) NOT NULL COMMENT 'combox|view|label#Semana#',
  PRIMARY KEY (`id`),
  INDEX `fk_reported_activity_cms_users1_idx` (`users_id` ASC),
  INDEX `fk_reported_activity_actitity_minutes1_idx` (`actitity_minutes_id` ASC),
  INDEX `fk_reported_activity_weeks1_idx` (`weeks_id` ASC),
  CONSTRAINT `fk_cms_reported_activity_cms_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `vivebien`.`cms_users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_reported_activity_cms_activity_minutes1`
    FOREIGN KEY (`actitity_minutes_id`)
    REFERENCES `vivebien`.`cms_activity_minutes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_reported_activity_cms_weeks1`
    FOREIGN KEY (`weeks_id`)
    REFERENCES `vivebien`.`cms_weeks` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabla apra administrar la información del módulo \"Reporta tu /* comment truncated */ /* actividad"*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_diets_categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_diets_categories` (
  `id` TINYINT(2) AUTO_INCREMENT NOT NULL ,
  `name` VARCHAR(45) NULL COMMENT 'input|view|label#Nombre#',
  `description` TEXT NULL COMMENT 'input|view|label#Descripción#',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Tabla apra administrar las categorías a las que puede perten /* comment truncated */ /*cer una dieta, dependiendo de los resultados de las fórmulas suministradas*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_diets`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_diets` (
  `id` INT(4) AUTO_INCREMENT NOT NULL ,
  `diets_categories_id` TINYINT(2) NOT NULL COMMENT 'combox|view|label#Categorías#',
  `name` VARCHAR(45) NULL COMMENT 'input|view|label#Nombre#',
  `description` TEXT NULL COMMENT 'input|view|label#Descripción#',
  PRIMARY KEY (`id`),
  INDEX `fk_diets_diets_categories1_idx` (`diets_categories_id` ASC),
  CONSTRAINT `fk_cms_diets_cms_diets_categories1`
    FOREIGN KEY (`diets_categories_id`)
    REFERENCES `vivebien`.`cms_diets_categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabla para dministrar las dietas que se crearn';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_meals_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_meals_type` (
  `id` TINYINT(2) AUTO_INCREMENT NOT NULL ,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivebien`.`cms_meals`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_meals` (
  `id` TINYINT(3) AUTO_INCREMENT NOT NULL ,
  `diets_id` INT(4) NOT NULL COMMENT 'combox|view|label#Dieta N|#',
  `description` TEXT NULL COMMENT 'input|view|label#Descripción#',
  `meals_type_id` TINYINT(2) NOT NULL COMMENT 'combox|view|label#Tipo de comida#',
  PRIMARY KEY (`id`),
  INDEX `fk_meals_diets1_idx` (`diets_id` ASC),
  INDEX `fk_cms_meals_cms_meals_type1_idx` (`meals_type_id` ASC),
  CONSTRAINT `fk_cms_meals_cms_diets1`
    FOREIGN KEY (`diets_id`)
    REFERENCES `vivebien`.`cms_diets` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_meals_cms_meals_type1`
    FOREIGN KEY (`meals_type_id`)
    REFERENCES `vivebien`.`cms_meals_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabla para administrar las comidas pretenecientes a una diet /* comment truncated */ /*a (desaynos, almuerzos, cena, etc..)*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_component_category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_component_category` (
  `id` TINYINT(2) AUTO_INCREMENT NOT NULL ,
  `name` VARCHAR(45) NULL COMMENT 'input|view|label#Nombre#',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Tabla para administrar las categorías en las que se pueden c /* comment truncated */ /*lasificar los componentes*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_components`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_components` (
  `id` INT(10) AUTO_INCREMENT NOT NULL ,
  `component_category_id` TINYINT(2) NOT NULL COMMENT 'combox|view|label#Tipo de componente#',
  `name` VARCHAR(50) NULL COMMENT 'input|view|label#Nombre#',
  `description` TEXT NULL COMMENT 'input|view|label#Descripción#',
  `parent` INT(10) NULL COMMENT 'input|view|label#Relacionado..#',
  `users_id` INT(10) unsigned  NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_components_component_category1_idx` (`component_category_id` ASC),
  INDEX `fk_cms_components_cms_users1_idx` (`users_id` ASC),
  CONSTRAINT `fk_cms_components_cms_component_category1`
    FOREIGN KEY (`component_category_id`)
    REFERENCES `vivebien`.`cms_component_category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_components_cms_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `vivebien`.`cms_users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabla para dministrar los componentes del FrontEnd del sitio /* comment truncated */ /* (banners, footers, etc..)*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_field_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_field_type` (
  `id` TINYINT(2) AUTO_INCREMENT NOT NULL ,
  `name` VARCHAR(50) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Tabla de especialización para los tipos de campos en los com /* comment truncated */ /*ponentes*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_component_fields`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_component_fields` (
  `id` INT(10) AUTO_INCREMENT NOT NULL ,
  `components_id` INT(10) NOT NULL,
  `value` TEXT NULL COMMENT 'input|view|label#Valor#',
  `field` VARCHAR(45) NULL,
  `type` VARCHAR(20) NULL COMMENT 'input|view|label#Tipo de campo#',
  `field_type_id` TINYINT(2) NOT NULL COMMENT 'combox|view|label#Tipo de campo#',
  PRIMARY KEY (`id`),
  INDEX `fk_component_fields_components1_idx` (`components_id` ASC),
  INDEX `fk_cms_component_fields_cms_field_type1_idx` (`field_type_id` ASC),
  CONSTRAINT `fk_cms_component_fields_cms_components1`
    FOREIGN KEY (`components_id`)
    REFERENCES `vivebien`.`cms_components` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_component_fields_cms_field_type1`
    FOREIGN KEY (`field_type_id`)
    REFERENCES `vivebien`.`cms_field_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabla para almacenar los campos correspondientes a cada comp /* comment truncated */ /*onente*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_frontmenu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_frontmenu` (
  `id` INT AUTO_INCREMENT NOT NULL ,
  `name` VARCHAR(25) NULL COMMENT 'input|view|label#Nombre#',
  `description` TEXT NULL COMMENT 'input|view|label#Descripción#',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Tabla que contiene los menús que se usarán en la aplicación  /* comment truncated */ /*(la información de los items se almacena en otra tabla (menu_items) )
*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_frontmenu_items`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_frontmenu_items` (
  `id` INT(4) AUTO_INCREMENT NOT NULL ,
  `menu_id` INT(4) NOT NULL,
  `label` VARCHAR(25) NULL COMMENT 'input|view|label#Etiqueta#',
  `value` VARCHAR(250) NULL COMMENT 'input|view|label#Valor (link)#',
  `parent` INT(4) NULL COMMENT 'combox|view|label#Item superior#',
  PRIMARY KEY (`id`),
  INDEX `fk_frontmenu_items_menu1_idx` (`menu_id` ASC),
  CONSTRAINT `fk_cms_frontmenu_items_cms_menu1`
    FOREIGN KEY (`menu_id`)
    REFERENCES `vivebien`.`cms_frontmenu` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabla donde se alamcenan todos los items de menús, si un ite /* comment truncated */ /*m es un child y tiene un parent, este es buscado en la misma tabla*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_hobbies`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_hobbies` (
  `id` TINYINT(2) AUTO_INCREMENT NOT NULL ,
  `name` VARCHAR(30) NULL COMMENT 'combox|view|label#Nombre#',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Tabla para almacenar los hobbies que puede tener el usuario  /* comment truncated */ /*a momento del registro*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_users_hobbies`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_users_hobbies` (
  `id` INT(10) AUTO_INCREMENT NOT NULL ,
  `users_id` INT(10) unsigned  NOT NULL COMMENT 'combox|view|label#Usuario#',
  `hobbies_id1` TINYINT(2) NOT NULL COMMENT 'combox|view|label#Pasatiempos#',
  PRIMARY KEY (`id`),
  INDEX `fk_users_hobbies_cms_users1_idx` (`users_id` ASC),
  INDEX `fk_users_hobbies_hobbies1_idx` (`hobbies_id1` ASC),
  CONSTRAINT `fk_cms_users_hobbies_cms_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `vivebien`.`cms_users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_users_hobbies_cms_hobbies1`
    FOREIGN KEY (`hobbies_id1`)
    REFERENCES `vivebien`.`cms_hobbies` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabla intermedia entre usuarios y los hobbies o pasatiempos';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_opening_hours`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_opening_hours` (
  `id` INT(10) AUTO_INCREMENT NOT NULL ,
  `name` VARCHAR(8) NULL COMMENT 'input|view|label#Hora#',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Tabla para administrar la información de las opciones de \"Ho /* comment truncated */ /*rarios de atención" en el formulario de registro de usuarios*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_users_opening_hours`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_users_opening_hours` (
  `id` INT AUTO_INCREMENT NOT NULL ,
  `opening_hours_id` INT(10) NOT NULL COMMENT 'combox|view|label#Horario de atención#',
  `users_id` INT(10) unsigned NOT NULL COMMENT 'input|view|label#Usuario#',
  PRIMARY KEY (`id`),
  INDEX `fk_users_opening_hours_opening_hours1_idx` (`opening_hours_id` ASC),
  INDEX `fk_users_opening_hours_cms_users1_idx` (`users_id` ASC),
  CONSTRAINT `fk_cms_users_opening_hours_cms_opening_hours1`
    FOREIGN KEY (`opening_hours_id`)
    REFERENCES `vivebien`.`cms_opening_hours` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_users_opening_hours_cms_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `vivebien`.`cms_users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabla intermedia entre usuarios y horarios (para casos de us /* comment truncated */ /*uario médico (En que horarios atiende))*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_eps`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_eps` (
  `id` TINYINT(2) AUTO_INCREMENT NOT NULL ,
  `name` VARCHAR(25) NULL COMMENT 'input|view|label#Nombre#',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Tabla para administrar las listas de EPS en el registro de u /* comment truncated */ /*suarios básicos y de usuarios médicos*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_users_eps`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_users_eps` (
  `id` INT(10) AUTO_INCREMENT NOT NULL ,
  `users_id` INT(10) unsigned  NOT NULL COMMENT 'combox|view|label#Usuario#',
  `eps_id` TINYINT(2) NOT NULL COMMENT 'combox|view|label#EPS#',
  PRIMARY KEY (`id`),
  INDEX `fk_users_eps_cms_users1_idx` (`users_id` ASC),
  INDEX `fk_users_eps_eps1_idx` (`eps_id` ASC),
  CONSTRAINT `fk_users_eps_cms_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `vivebien`.`cms_users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_eps_eps1`
    FOREIGN KEY (`eps_id`)
    REFERENCES `vivebien`.`cms_eps` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabla intermedia entre usuarios y EPS (muchos a muchos en ca /* comment truncated */ /*so de usuario médico (que EPS attiende))*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_workouts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_workouts` (
  `id` INT(4) AUTO_INCREMENT NOT NULL ,
  `name` VARCHAR(25) NULL,
  `description` TEXT NULL,
  `imagen_id` INT(11) NULL,
  `weeks_id` INT(10) NOT NULL COMMENT 'combox|view|label#Semana#',
  PRIMARY KEY (`id`),
  INDEX `fk_workouts_weeks1_idx` (`weeks_id` ASC),
  CONSTRAINT `fk_cms_workouts_cms_weeks1`
    FOREIGN KEY (`weeks_id`)
    REFERENCES `vivebien`.`cms_weeks` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabla para administrar las sesiones de ejercicios de las rut /* comment truncated */ /*inas */';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_users_diets`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_users_diets` (
  `id` INT(10) AUTO_INCREMENT NOT NULL ,
  `users_id` INT(10) unsigned  NOT NULL,
  `diets_id` INT(4) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_cms_users_diets_cms_users1_idx` (`users_id` ASC),
  INDEX `fk_cms_users_diets_cms_diets1_idx` (`diets_id` ASC),
  CONSTRAINT `fk_cms_users_diets_cms_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `vivebien`.`cms_users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_users_diets_cms_diets1`
    FOREIGN KEY (`diets_id`)
    REFERENCES `vivebien`.`cms_diets` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'tabla intermedia para romper relación de muchos a muchos ent /* comment truncated */ /*re dietas y usuarios*/';


-- -----------------------------------------------------
-- Table `vivebien`.`cms_specialties`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_specialties` (
  `id` INT(10) AUTO_INCREMENT NOT NULL ,
  `name` VARCHAR(45) NULL COMMENT 'input|view|label#Áreas#',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Tabla para administrar las áreas de especialidad disponibles /* comment truncated */ /* para el registro de médicos*/';



-- -----------------------------------------------------
-- Table `vivebien`.`cms_users_routines`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `vivebien`.`cms_users_routines` (
  `id` INT AUTO_INCREMENT NOT NULL ,
  `routines_id` INT(4) NOT NULL COMMENT 'combox|view|label#Rutina#',
  `users_id` INT(10) unsigned NOT NULL COMMENT 'input|view|label#Usuario#',
  PRIMARY KEY (`id`),
  INDEX `fk_users_routines_routines1_idx` (`routines_id` ASC),
  INDEX `fk_users_routines_cms_users1_idx` (`users_id` ASC),
  CONSTRAINT `fk_cms_users_routines_cms_routines1`
    FOREIGN KEY (`routines_id`)
    REFERENCES `vivebien`.`cms_routines` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_users_routines_cms_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `vivebien`.`cms_users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabla intermedia entre usuarios y rutinas';



-- -----------------------------------------------------
-- Table `vivebien`.`cms_users_specialties`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivebien`.`cms_users_specialties` (
  `id` INT(10) AUTO_INCREMENT NOT NULL ,
  `cms_specialties_id` INT(10) NOT NULL,
  `users_id` INT(10) unsigned  NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_cms_users_specialties_cms_specialties1_idx` (`cms_specialties_id` ASC),
  INDEX `fk_cms_users_specialties_cms_users1_idx` (`users_id` ASC),
  CONSTRAINT `fk_cms_users_specialties_cms_specialties1`
    FOREIGN KEY (`cms_specialties_id`)
    REFERENCES `vivebien`.`cms_specialties` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_users_specialties_cms_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `vivebien`.`cms_users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabla intermedia para romper la relación muchos a muchos ent /* comment truncated */ /*re áreas de especialidad y usuarios (médicos)*/';


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
