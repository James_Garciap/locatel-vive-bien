-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 18, 2014 at 12:09 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vivebien`
--

-- --------------------------------------------------------

--
-- Table structure for table `cms_activity_level`
--

CREATE TABLE IF NOT EXISTS `cms_activity_level` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(75) DEFAULT NULL COMMENT 'input|view|label#Descripción#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar la información de la lista "¿Qué nive /* comment truncated */ /*l de actividad físíca realizas?"  en el registro de usuario*/' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `cms_activity_level`
--

INSERT INTO `cms_activity_level` (`id`, `name`) VALUES
(2, 'Sedentaria (No realizas actividad física) '),
(3, 'Ligera (Realizas actividad física de 1 a 3 días por semana)'),
(4, 'Moderada (Realizas actividad física de 3 a 5 días por semana)'),
(5, 'Intensa (Realizas actividad física de 5 a 7 días por semana)');

-- --------------------------------------------------------

--
-- Table structure for table `cms_activity_minutes`
--

CREATE TABLE IF NOT EXISTS `cms_activity_minutes` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(5) DEFAULT NULL COMMENT 'input|view|label#Minutos#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar la lista "Minutos de actividad" en el /* comment truncated */ /* formulario " Reporta tu actividad\n- Actualmente en desuso, por definir si el campo apunta a esta lista o se deja abierto*/' AUTO_INCREMENT=14 ;

--
-- Dumping data for table `cms_activity_minutes`
--

INSERT INTO `cms_activity_minutes` (`id`, `name`) VALUES
(1, '5'),
(2, '10'),
(3, '15'),
(4, '20'),
(5, '30'),
(6, '45'),
(7, '60'),
(8, '90'),
(11, '120'),
(12, '150'),
(13, '0');

-- --------------------------------------------------------

--
-- Table structure for table `cms_api_oauth`
--

CREATE TABLE IF NOT EXISTS `cms_api_oauth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `strategy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `api_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `api_secret` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `scope` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `active_oauth` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `cms_api_oauth`
--

INSERT INTO `cms_api_oauth` (`id`, `name`, `provider`, `strategy`, `api_key`, `api_secret`, `scope`, `active`, `active_oauth`) VALUES
(1, 'Facebook', 'facebook', 'oauth2', '1416132521932785', '335e4e5e18413ce88244a2bd0be4f226', 'offline_access,email,publish_stream,manage_pages', 1, 1),
(2, 'Twitter', 'twitter', 'oauth1', 'oaUPjhYWiIETQBNrvt3XfQ', 'kPsTns4nNWAhcnNyP6TtWP02lEgiRQRevfDdHF5Qw4U', '', 1, 1),
(3, 'Google', 'google', 'oauth2', '1073082606021.apps.googleusercontent.com', 'dZgio_ypJkpmQEUBjwK1FYGQ', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_calendar`
--

CREATE TABLE IF NOT EXISTS `cms_calendar` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT 'input|view|label#Nombre del evento#',
  `description` text NOT NULL COMMENT 'input|view|label#Descripción#',
  `date` date NOT NULL COMMENT 'input|view|label#Fecha#',
  `users_id` int(10) NOT NULL,
  `imagen_id` int(11) DEFAULT NULL COMMENT 'input|view|label#Imagen#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `cms_calendar`
--

INSERT INTO `cms_calendar` (`id`, `name`, `description`, `date`, `users_id`, `imagen_id`) VALUES
(1, 'Integración ', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur', '2014-02-12', 5, 67),
(2, 'Cena aburrida', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur', '2014-02-12', 5, 68),
(3, 'Charla con una pantalla enorme sobre un tema cool', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur', '2014-02-12', 5, 69);

-- --------------------------------------------------------

--
-- Table structure for table `cms_captcha`
--

CREATE TABLE IF NOT EXISTS `cms_captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `captcha_time` int(10) unsigned NOT NULL,
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=236 ;

--
-- Dumping data for table `cms_captcha`
--

INSERT INTO `cms_captcha` (`captcha_id`, `captcha_time`, `ip_address`, `word`) VALUES
(231, 1392153413, '127.0.0.1', 'nVx1xgPc'),
(232, 1392219201, '127.0.0.1', 'sI9Tdwea'),
(233, 1392679732, '127.0.0.1', '42dc6Ju0'),
(234, 1392679841, '127.0.0.1', 'ngMkGemh'),
(235, 1392679858, '127.0.0.1', 'UUWRabS7');

-- --------------------------------------------------------

--
-- Table structure for table `cms_cities`
--

CREATE TABLE IF NOT EXISTS `cms_cities` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(75) NOT NULL COMMENT 'input|view|label#Nombre#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `cms_cities`
--

INSERT INTO `cms_cities` (`id`, `name`) VALUES
(1, 'Bogotá'),
(2, 'Medellín'),
(3, 'Cali'),
(4, 'Barranquilla');

-- --------------------------------------------------------

--
-- Table structure for table `cms_coaches`
--

CREATE TABLE IF NOT EXISTS `cms_coaches` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT 'input|view|label#Nombre#',
  `email` varchar(150) NOT NULL COMMENT 'input|view|label#Email#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `cms_coaches`
--

INSERT INTO `cms_coaches` (`id`, `name`, `email`) VALUES
(3, 'Juan Cantor', 'juan.cantor@imagina.co'),
(4, 'Alvaro Martínez', 'alvaro.martinez@vivebien.com');

-- --------------------------------------------------------

--
-- Table structure for table `cms_components`
--

CREATE TABLE IF NOT EXISTS `cms_components` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `component_category_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#Tipo de componente#',
  `name` varchar(50) DEFAULT NULL COMMENT 'input|view|label#Nombre#',
  `description` text COMMENT 'input|view|label#Descripción#',
  `parent` int(10) DEFAULT NULL COMMENT 'input|view|label#Relacionado..#',
  `users_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_components_component_category1_idx` (`component_category_id`),
  KEY `fk_cms_components_cms_users1_idx` (`users_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla para dministrar los componentes del FrontEnd del sitio /* comment truncated */ /* (banners, footers, etc..)*/' AUTO_INCREMENT=10 ;

--
-- Dumping data for table `cms_components`
--

INSERT INTO `cms_components` (`id`, `component_category_id`, `name`, `description`, `parent`, `users_id`) VALUES
(2, 4, 'Artículo de prueba', 'Artículo de prueba para el módulo de componentes', 0, 5),
(3, 7, 'Tip3', '--', NULL, 5),
(4, 7, 'tip2', NULL, NULL, 5),
(5, 7, 'tip1', NULL, NULL, 5),
(6, 8, 'Enveto #1', 'Descripción del evento empor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, 5),
(7, 8, 'Evento #2', 'Descripción del evento empor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, 5),
(8, 8, 'Evento #3', 'Descripción del evento empor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, 5),
(9, 8, 'Evento #4', 'Descripción del evento empor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, 5);

-- --------------------------------------------------------

--
-- Table structure for table `cms_component_category`
--

CREATE TABLE IF NOT EXISTS `cms_component_category` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Nombre#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar las categorías en las que se pueden c /* comment truncated */ /*lasificar los componentes*/' AUTO_INCREMENT=9 ;

--
-- Dumping data for table `cms_component_category`
--

INSERT INTO `cms_component_category` (`id`, `name`) VALUES
(1, 'Banner de fondo'),
(2, 'Menú horizontal'),
(3, 'Footer'),
(4, 'Artículos'),
(5, 'Comentarios'),
(6, 'Textos estáticos'),
(7, 'Tips y recetas'),
(8, 'Eventos de calendario');

-- --------------------------------------------------------

--
-- Table structure for table `cms_component_fields`
--

CREATE TABLE IF NOT EXISTS `cms_component_fields` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `components_id` int(10) NOT NULL COMMENT 'combox|view|label#Componente#',
  `value` text COMMENT 'input|view|label#Valor#',
  `field` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Campo#',
  `type` varchar(20) DEFAULT NULL,
  `field_type_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#Tipo de campo#',
  PRIMARY KEY (`id`),
  KEY `fk_component_fields_components1_idx` (`components_id`),
  KEY `fk_cms_component_fields_cms_field_type1_idx` (`field_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla para almacenar los campos correspondientes a cada comp /* comment truncated */ /*onente*/' AUTO_INCREMENT=7 ;

--
-- Dumping data for table `cms_component_fields`
--

INSERT INTO `cms_component_fields` (`id`, `components_id`, `value`, `field`, `type`, `field_type_id`) VALUES
(1, 2, 'Locatel lanza el programa ViveBien', 'titulo', NULL, 1),
(2, 2, 'orem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'body', NULL, 1),
(3, 2, '<img alt="CMS Imaginamos" width="60" src="http://localhost/locatel/vivebien/assets/img/logo_header.png">', 'image', NULL, 2),
(4, 5, 'Ejercitar yoga beneficia a muchos problemas digestivos.', 'body', NULL, 4),
(5, 4, 'Ejercitar yoga beneficia a muchos problemas digestivos', 'body', NULL, 4),
(6, 3, 'Las manzanas, y no la cafeína, son más eficientes para despertarte en la mañana. ', 'body', NULL, 4);

-- --------------------------------------------------------

--
-- Table structure for table `cms_contacto`
--

CREATE TABLE IF NOT EXISTS `cms_contacto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `direccion` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `celular` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ciudad` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cordenada_x` double NOT NULL DEFAULT '0',
  `cordenada_y` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cms_contacto`
--

INSERT INTO `cms_contacto` (`id`, `direccion`, `telefono`, `fax`, `celular`, `email`, `ciudad`, `cordenada_x`, `cordenada_y`) VALUES
(1, 'Calle 90 Cra. 19a of. 301 Edificio 90', '+ 57 555 555 5555', '+ 57 555 555 5555', '+ 57 (1) 555 5555', 'cms@imaginamos.com', 'Bogotá - Colombia', 4.678391, -74.053883);

-- --------------------------------------------------------

--
-- Table structure for table `cms_departamento`
--

CREATE TABLE IF NOT EXISTS `cms_departamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `cms_departamento`
--

INSERT INTO `cms_departamento` (`id`, `nombre`) VALUES
(1, 'AMAZONAS'),
(2, 'ANTIOQUIA'),
(3, 'ARAUCA'),
(4, 'ATLÁNTICO'),
(5, 'BOLÍVAR'),
(6, 'BOYACÁ'),
(7, 'CALDAS'),
(8, 'CAQUETÁ'),
(9, 'CASANARE'),
(10, 'CAUCA'),
(11, 'CESAR'),
(12, 'CHOCÓ'),
(13, 'CÓRDOBA'),
(14, 'CUNDINAMARCA'),
(15, 'GUAINÍA'),
(16, 'GUAVIARE'),
(17, 'HUILA'),
(18, 'LA GUAJIRA'),
(19, 'MAGDALENA'),
(20, 'META'),
(21, 'NARIÑO'),
(22, 'NORTE DE SANTANDER'),
(23, 'PUTUMAYO'),
(24, 'QUINDÍO'),
(25, 'RISARALDA'),
(26, 'SAN ANDRÉS Y ROVIDENCIA'),
(27, 'SANTANDER'),
(28, 'SUCRE'),
(29, 'TOLIMA'),
(30, 'VALLE DEL CAUCA'),
(31, 'VAUPÉS'),
(32, 'VICHADA');

-- --------------------------------------------------------

--
-- Table structure for table `cms_diets`
--

CREATE TABLE IF NOT EXISTS `cms_diets` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `diets_categories_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#Categorías#',
  `name` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Nombre#',
  `description` text COMMENT 'input|view|label#Descripción#',
  PRIMARY KEY (`id`),
  KEY `fk_diets_diets_categories1_idx` (`diets_categories_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla para dministrar las dietas que se crearn' AUTO_INCREMENT=5 ;

--
-- Dumping data for table `cms_diets`
--

INSERT INTO `cms_diets` (`id`, `diets_categories_id`, `name`, `description`) VALUES
(2, 2, 'A-1200-001', '-	Tomar el 3% del peso corporal en Agua Pura al día. -	Si llega a dar hambre, comer una porción de Proteína. -	El snack de media tarde se puede cambiar a media mañana o dividir en 2 – Ej. A media mañana la Fruta y a media tarde la porción de Proteína'),
(3, 3, 'A-1500-001', '-	Tomar el 3% del peso corporal en Agua Pura al día. -	Si llega a dar hambre, comer una porción de Proteína. -	El snack de media tarde se puede cambiar a media mañana o dividir en 2 – Ej. A media mañana la Fruta y a media tarde la porción de Proteína'),
(4, 3, 'A-1500-001', '-	Tomar el 3% del peso corporal en Agua Pura al día. -	Si llega a dar hambre, comer una porción de Proteína. -	El snack de media tarde se puede cambiar a media mañana o dividir en 2 – Ej. A media mañana la Fruta y a media tarde la porción de Proteína');

-- --------------------------------------------------------

--
-- Table structure for table `cms_diets_categories`
--

CREATE TABLE IF NOT EXISTS `cms_diets_categories` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Nombre#',
  `description` text COMMENT 'input|view|label#Descripción#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla apra administrar las categorías a las que puede perten /* comment truncated */ /*cer una dieta, dependiendo de los resultados de las fórmulas suministradas*/' AUTO_INCREMENT=5 ;

--
-- Dumping data for table `cms_diets_categories`
--

INSERT INTO `cms_diets_categories` (`id`, `name`, `description`) VALUES
(1, 'Categoría 1', '0'),
(2, 'Plan ''A'' - 1200 calorías', '1200'),
(3, 'Plan ''B'' - 1500 calorías', '1500'),
(4, 'Plan ''C'' - 1800 calorías', '1800');

-- --------------------------------------------------------

--
-- Table structure for table `cms_eps`
--

CREATE TABLE IF NOT EXISTS `cms_eps` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Nombre#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar las listas de EPS en el registro de u /* comment truncated */ /*suarios básicos y de usuarios médicos*/' AUTO_INCREMENT=7 ;

--
-- Dumping data for table `cms_eps`
--

INSERT INTO `cms_eps` (`id`, `name`) VALUES
(1, 'Sanitas'),
(2, 'Cafesalud'),
(3, 'Coomeva'),
(4, 'Famisanar'),
(5, 'Saludcoop'),
(6, 'Particular');

-- --------------------------------------------------------

--
-- Table structure for table `cms_field_type`
--

CREATE TABLE IF NOT EXISTS `cms_field_type` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT 'input|view|label#Tipo de campo#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla de especialización para los tipos de campos en los com /* comment truncated */ /*ponentes*/' AUTO_INCREMENT=5 ;

--
-- Dumping data for table `cms_field_type`
--

INSERT INTO `cms_field_type` (`id`, `name`) VALUES
(1, 'Texto'),
(2, 'Imagen'),
(3, 'Fecha'),
(4, 'Número');

-- --------------------------------------------------------

--
-- Table structure for table `cms_frontmenu`
--

CREATE TABLE IF NOT EXISTS `cms_frontmenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Nombre#',
  `description` text COMMENT 'input|view|label#Descripción#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla que contiene los menús que se usarán en la aplicación  /* comment truncated */ /*(la información de los items se almacena en otra tabla (menu_items) )\n*/' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cms_frontmenu`
--

INSERT INTO `cms_frontmenu` (`id`, `name`, `description`) VALUES
(1, 'Menú principal (home)', 'Menú inicial horizontal (home)');

-- --------------------------------------------------------

--
-- Table structure for table `cms_frontmenu_items`
--

CREATE TABLE IF NOT EXISTS `cms_frontmenu_items` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `frontmenu_id` int(4) NOT NULL COMMENT 'combox|view|label#Menú superiorr#',
  `label` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Etiqueta#',
  `value` varchar(250) DEFAULT NULL COMMENT 'input|view|label#Valor (link)#',
  `parent` int(4) DEFAULT NULL COMMENT 'input|view|label#Parent#',
  PRIMARY KEY (`id`),
  KEY `fk_frontmenu_items_menu1_idx` (`frontmenu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla donde se alamcenan todos los items de menús, si un ite /* comment truncated */ /*m es un child y tiene un parent, este es buscado en la misma tabla*/' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cms_frontmenu_items`
--

INSERT INTO `cms_frontmenu_items` (`id`, `frontmenu_id`, `label`, `value`, `parent`) VALUES
(1, 1, 'Mi calendario vivebien', 'contenidos.php#cont7', NULL),
(2, 1, 'Mi directorio médico', 'contenidos.php#cont3', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_groups`
--

CREATE TABLE IF NOT EXISTS `cms_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `cms_groups`
--

INSERT INTO `cms_groups` (`id`, `name`, `description`) VALUES
(1, 'superadmin', 'Super Administrador'),
(2, 'admin', 'Administrador'),
(3, 'usuarios', 'Usuarios'),
(4, 'cliente', 'Cliente'),
(5, 'proveedor', 'Proveedor');

-- --------------------------------------------------------

--
-- Table structure for table `cms_groups_permissions`
--

CREATE TABLE IF NOT EXISTS `cms_groups_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL,
  `permission_id` int(11) NOT NULL,
  `view` tinyint(1) DEFAULT '0',
  `create` tinyint(1) DEFAULT '0',
  `update` tinyint(1) DEFAULT '0',
  `delete` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_cms_users_permissions_cms_permissions1_idx` (`permission_id`),
  KEY `fk_cms_groups_permissions_cms_groups1_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `cms_groups_permissions`
--

INSERT INTO `cms_groups_permissions` (`id`, `group_id`, `permission_id`, `view`, `create`, `update`, `delete`) VALUES
(5, 1, 1, 1, 1, 1, 1),
(6, 1, 2, 1, 1, 1, 1),
(7, 2, 1, 1, 0, 0, 0),
(8, 2, 2, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cms_hobbies`
--

CREATE TABLE IF NOT EXISTS `cms_hobbies` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL COMMENT 'input|view|label#Pasatiempo#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla para almacenar los hobbies que puede tener el usuario  /* comment truncated */ /*a momento del registro*/' AUTO_INCREMENT=9 ;

--
-- Dumping data for table `cms_hobbies`
--

INSERT INTO `cms_hobbies` (`id`, `name`) VALUES
(1, 'Lectura'),
(2, 'Golf'),
(3, 'Fútbol'),
(4, 'Cine/Teatro'),
(5, 'Cocina'),
(6, 'Tecnología'),
(7, 'Música'),
(8, 'Otro.');

-- --------------------------------------------------------

--
-- Table structure for table `cms_imagen`
--

CREATE TABLE IF NOT EXISTS `cms_imagen` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'inputhidden|none',
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=70 ;

--
-- Dumping data for table `cms_imagen`
--

INSERT INTO `cms_imagen` (`id`, `path`, `name`) VALUES
(1, './uploads/34d0e54eb0ba84ed0ee36f2a0ad3c147.jpg', NULL),
(2, './uploads/f71843dcb924bd1fe6595776d122a372.gif', NULL),
(3, './uploads/profile_pictures/', '3c59dc048e8850243be8079a5c74d079.jpg'),
(4, './uploads/profile_pictures/', '3c59dc048e8850243be8079a5c74d079.jpg'),
(5, './uploads/profile_pictures/', '33e75ff09dd601bbe69f351039152189.jpg'),
(6, './uploads/profile_pictures/', '6ea9ab1baa0efb9e19094440c317e21b.jpg'),
(7, './uploads/profile_pictures/', '34173cb38f07f89ddbebc2ac9128303f.jpg'),
(8, './uploads/profile_pictures/', 'c16a5320fa475530d9583c34fd356ef5.jpg'),
(9, './uploads/profile_pictures/', '182be0c5cdcd5072bb1864cdee4d3d6e.jpg'),
(10, './uploads/profile_pictures/', '182be0c5cdcd5072bb1864cdee4d3d6e.jpg'),
(11, './uploads/profile_pictures/', '1c383cd30b7c298ab50293adfecb7b18.jpg'),
(12, './uploads/profile_pictures/', 'a5bfc9e07964f8dddeb95fc584cd965d.jpg'),
(13, './uploads/profile_pictures/', 'a5771bce93e200c36f7cd9dfd0e5deaa.jpg'),
(14, './uploads/profile_pictures/', 'd67d8ab4f4c10bf22aa353e27879133c.jpg'),
(15, './uploads/profile_pictures/', 'd645920e395fedad7bbbed0eca3fe2e0.jpg'),
(16, './uploads/profile_pictures/', 'a1d0c6e83f027327d8461063f4ac58a6.jpg'),
(17, './uploads/profile_pictures/', '17e62166fc8586dfa4d1bc0e1742c08b.jpg'),
(18, './uploads/profile_pictures/', 'f7177163c833dff4b38fc8d2872f1ec6.jpg'),
(19, './uploads/profile_pictures/', '6c8349cc7260ae62e3b1396831a8398f.jpg'),
(20, './uploads/profile_pictures/', 'd9d4f495e875a2e075a1a4a6e1b9770f.jpg'),
(21, './uploads/profile_pictures/', '67c6a1e7ce56d3d6fa748ab6d9af3fd7.jpg'),
(22, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(23, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(24, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(25, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(26, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(27, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(28, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(29, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(30, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(31, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(32, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(33, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(34, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(35, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(36, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(37, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(38, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(39, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(40, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(41, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(42, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(43, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(44, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(45, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(46, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(47, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(48, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(49, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(50, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(51, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(52, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(53, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(54, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(55, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(56, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(57, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(58, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(59, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(60, './uploads/profile_pictures/', 'd41d8cd98f00b204e9800998ecf8427e.jpg'),
(61, './uploads/profile_pictures/', '3c59dc048e8850243be8079a5c74d079.jpg'),
(62, './uploads/d867f6a2d899b569daa6c38af2443442.jpg', NULL),
(63, './uploads/76ee376ebe9289aebade7eee74f0e813.jpg', NULL),
(64, './uploads/63ef5b704a3d586e8e2114f8fe0832ee.jpg', NULL),
(65, './uploads/e36451e588ab6d1af452fb938e3b8ade.jpg', NULL),
(66, './uploads/2eaa4b8c4f3b63cacfc7ed3784a374b4.jpg', NULL),
(67, './uploads/0b3eefe8a64522cb9c182073bf249717.png', NULL),
(68, './uploads/73c20425f71cb67657a333d6a43a6db6.jpg', NULL),
(69, './uploads/00965f13d891c2f21ff8e73e3dc469e3.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_login_attempts`
--

CREATE TABLE IF NOT EXISTS `cms_login_attempts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cms_meals`
--

CREATE TABLE IF NOT EXISTS `cms_meals` (
  `id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `diets_id` int(4) NOT NULL COMMENT 'combox|view|label#Dieta N|#',
  `description` text COMMENT 'input|view|label#Descripción#',
  `meals_type_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#Tipo de comida#',
  PRIMARY KEY (`id`),
  KEY `fk_meals_diets1_idx` (`diets_id`),
  KEY `fk_cms_meals_cms_meals_type1_idx` (`meals_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar las comidas pretenecientes a una diet /* comment truncated */ /*a (desaynos, almuerzos, cena, etc..)*/' AUTO_INCREMENT=5 ;

--
-- Dumping data for table `cms_meals`
--

INSERT INTO `cms_meals` (`id`, `diets_id`, `description`, `meals_type_id`) VALUES
(1, 3, '1 PORCION DE PROTEÍNA+ 2 PORCIONES DE VEGETALES + 1 PORCION DE ENSALADA + 1 GRANO + 1 FRUTA Ejemplo:  • 3 oz. de pechuga de pollo o pavo, pescado, atún o carne con un puño de alguna harina (papas al horno, puré de auyama). - ADEMÁS 2 tazas de ensalada verde  - ADEMÁS 1 taza de vegetales picados  - OPCIONAL 1 taza de Sopa de tomate  - ADEMÁS 1 naranja grande', 1),
(2, 3, '1 PORCION DE PROTEÍNA + 2 VERDURAS o 1 HARINA + 1 PORCION DE ENSALADA + 1 FRUTA Ejemplo:  - 1 pechuga de pollo/ pescado/ asada o a la plancha. - 2 tazas de vegetales de hoja verde con vinagre sazonado. - 2 tazas de verduras al vapor.  - 1 naranja grande.', 3),
(3, 3, '1 PORCION DE PROTEINA + 1 FRUTA (Baja en azúcar y calorías)', 4),
(4, 3, '1 PORCION DE PROTEÍNA + 1 PORCION DE VEGETALES + ENSALADA + 1 FRUTA Ejemplo:  • Pescado a la parrilla con salsa teriyaki.  -  1 taza de espinaca al vapor + 1 taza de zanahorias al vapor o  una ensalada verde mixta, con queso campesino.  - 1 taza de melón dulce o papaya.', 5);

-- --------------------------------------------------------

--
-- Table structure for table `cms_meals_type`
--

CREATE TABLE IF NOT EXISTS `cms_meals_type` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Tipo de comida#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `cms_meals_type`
--

INSERT INTO `cms_meals_type` (`id`, `name`) VALUES
(1, 'Desayuno'),
(2, 'Snack (Media mañana)'),
(3, 'Almuerzo'),
(4, 'Snack (Media tarde)'),
(5, 'Cena');

-- --------------------------------------------------------

--
-- Table structure for table `cms_medical_formulate`
--

CREATE TABLE IF NOT EXISTS `cms_medical_formulate` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `users_id` int(10) unsigned NOT NULL,
  `medicine` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Medicina#',
  `description` text COMMENT 'input|view|label#Datos importantes#',
  `mail_notificaction` tinyint(1) DEFAULT NULL COMMENT 'input|view|label#Recibir recordatorio en el correo electrónico#',
  `unity_type_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#Tipo de unidad#',
  `no_units_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#N° de unidades#',
  `periodicity_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#Periodicidad de la fórmula#',
  PRIMARY KEY (`id`),
  KEY `fk_medical_formulate_cms_users1_idx` (`users_id`),
  KEY `fk_medical_formulate_unity_type1_idx` (`unity_type_id`),
  KEY `fk_medical_formulate_no_units1_idx` (`no_units_id`),
  KEY `fk_medical_formulate_periodicity1_idx` (`periodicity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla para el módulo de Fórmula médica (administrar informac /* comment truncated */ /*ión de formulación médica del usuario)*/' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cms_medical_formulate`
--

INSERT INTO `cms_medical_formulate` (`id`, `users_id`, `medicine`, `description`, `mail_notificaction`, `unity_type_id`, `no_units_id`, `periodicity_id`) VALUES
(1, 5, 'Ibuprofeno', 'DESC', NULL, 2, 2, 1),
(2, 5, 'Amoxicilina', 'Categoría de prueba', 1, 3, 5, 4);

-- --------------------------------------------------------

--
-- Table structure for table `cms_menu`
--

CREATE TABLE IF NOT EXISTS `cms_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_short` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `image` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cms_municipio`
--

CREATE TABLE IF NOT EXISTS `cms_municipio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `departamento_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_municipio_cms_departamento1` (`departamento_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1113 ;

--
-- Dumping data for table `cms_municipio`
--

INSERT INTO `cms_municipio` (`id`, `nombre`, `departamento_id`) VALUES
(1, 'EL ENCANTO', 1),
(2, 'LA CHORRERA', 1),
(3, 'LA PEDRERA', 1),
(4, 'LA VICTORIA', 1),
(5, 'LETICIA', 1),
(6, 'MIRITI', 1),
(7, 'PUERTO ALEGRIA', 1),
(8, 'PUERTO ARICA', 1),
(9, 'PUERTO NARIÑO', 1),
(10, 'PUERTO SANTANDER', 1),
(11, 'TURAPACA', 1),
(12, 'ABEJORRAL', 2),
(13, 'ABRIAQUI', 2),
(14, 'ALEJANDRIA', 2),
(15, 'AMAGA', 2),
(16, 'AMALFI', 2),
(17, 'ANDES', 2),
(18, 'ANGELOPOLIS', 2),
(19, 'ANGOSTURA', 2),
(20, 'ANORI', 2),
(21, 'ANTIOQUIA', 2),
(22, 'ANZA', 2),
(23, 'APARTADO', 2),
(24, 'ARBOLETES', 2),
(25, 'ARGELIA', 2),
(26, 'ARMENIA', 2),
(27, 'BARBOSA', 2),
(28, 'BELLO', 2),
(29, 'BELMIRA', 2),
(30, 'BETANIA', 2),
(31, 'BETULIA', 2),
(32, 'BOLIVAR', 2),
(33, 'BRICEÑO', 2),
(34, 'BURITICA', 2),
(35, 'CACERES', 2),
(36, 'CAICEDO', 2),
(37, 'CALDAS', 2),
(38, 'CAMPAMENTO', 2),
(39, 'CANASGORDAS', 2),
(40, 'CARACOLI', 2),
(41, 'CARAMANTA', 2),
(42, 'CAREPA', 2),
(43, 'CARMEN DE VIBORAL', 2),
(44, 'CAROLINA DEL PRINCIPE', 2),
(45, 'CAUCASIA', 2),
(46, 'CHIGORODO', 2),
(47, 'CISNEROS', 2),
(48, 'COCORNA', 2),
(49, 'CONCEPCION', 2),
(50, 'CONCORDIA', 2),
(51, 'COPACABANA', 2),
(52, 'DABEIBA', 2),
(53, 'DONMATIAS', 2),
(54, 'EBEJICO', 2),
(55, 'EL BAGRE', 2),
(56, 'EL PENOL', 2),
(57, 'EL RETIRO', 2),
(58, 'ENTRERRIOS', 2),
(59, 'ENVIGADO', 2),
(60, 'FREDONIA', 2),
(61, 'FRONTINO', 2),
(62, 'GIRALDO', 2),
(63, 'GIRARDOTA', 2),
(64, 'GOMEZ PLATA', 2),
(65, 'GRANADA', 2),
(66, 'GUADALUPE', 2),
(67, 'GUARNE', 2),
(68, 'GUATAQUE', 2),
(69, 'HELICONIA', 2),
(70, 'HISPANIA', 2),
(71, 'ITAGUI', 2),
(72, 'ITUANGO', 2),
(73, 'JARDIN', 2),
(74, 'JERICO', 2),
(75, 'LA CEJA', 2),
(76, 'LA ESTRELLA', 2),
(77, 'LA PINTADA', 2),
(78, 'LA UNION', 2),
(79, 'LIBORINA', 2),
(80, 'MACEO', 2),
(81, 'MARINILLA', 2),
(82, 'MEDELLIN', 2),
(83, 'MONTEBELLO', 2),
(84, 'MURINDO', 2),
(85, 'MUTATA', 2),
(86, 'NARINO', 2),
(87, 'NECHI', 2),
(88, 'NECOCLI', 2),
(89, 'OLAYA', 2),
(90, 'PEQUE', 2),
(91, 'PUEBLORRICO', 2),
(92, 'PUERTO BERRIO', 2),
(93, 'PUERTO NARE', 2),
(94, 'PUERTO TRIUNFO', 2),
(95, 'REMEDIOS', 2),
(96, 'RIONEGRO', 2),
(97, 'SABANALARGA', 2),
(98, 'SABANETA', 2),
(99, 'SALGAR', 2),
(100, 'SAN ANDRES DE CUERQUIA', 2),
(101, 'SAN CARLOS', 2),
(102, 'SAN FRANCISCO', 2),
(103, 'SAN JERONIMO', 2),
(104, 'SAN JOSE DE LA MONTAÑA', 2),
(105, 'SAN JUAN DE URABA', 2),
(106, 'SAN LUIS', 2),
(107, 'SAN PEDRO DE LOS MILAGROS', 2),
(108, 'SAN PEDRO DE URABA', 2),
(109, 'SAN RAFAEL', 2),
(110, 'SAN ROQUE', 2),
(111, 'SAN VICENTE', 2),
(112, 'SANTA BARBARA', 2),
(113, 'SANTA ROSA DE OSOS', 2),
(114, 'SANTO DOMINGO', 2),
(115, 'SANTUARIO', 2),
(116, 'SEGOVIA', 2),
(117, 'SONSON', 2),
(118, 'SOPETRAN', 2),
(119, 'TAMESIS', 2),
(120, 'TARAZA', 2),
(121, 'TARSO', 2),
(122, 'TITIRIBI', 2),
(123, 'TOLEDO', 2),
(124, 'TURBO', 2),
(125, 'URAMITA', 2),
(126, 'URRAO', 2),
(127, 'VALDIVIA', 2),
(128, 'VALPARAISO', 2),
(129, 'VEGACHI', 2),
(130, 'VENECIA', 2),
(131, 'VIGIA DEL FUERTE', 2),
(132, 'YALI', 2),
(133, 'YARUMAL', 2),
(134, 'YOLOMBO', 2),
(135, 'YONDO', 2),
(136, 'ZARAGOZA', 2),
(137, 'ARAUCA', 3),
(138, 'ARAUQUITA', 3),
(139, 'CRAVO NORTE', 3),
(140, 'FORTUL', 3),
(141, 'PUERTO RONDON', 3),
(142, 'SARAVENA', 3),
(143, 'TAME', 3),
(144, 'BARANOA', 4),
(145, 'BARRANQUILLA', 4),
(146, 'CAMPO DE LA CRUZ', 4),
(147, 'CANDELARIA', 4),
(148, 'GALAPA', 4),
(149, 'JUAN DE ACOSTA', 4),
(150, 'LURUACO', 4),
(151, 'MALAMBO', 4),
(152, 'MANATI', 4),
(153, 'PALMAR DE VARELA', 4),
(154, 'PIOJO', 4),
(155, 'POLO NUEVO', 4),
(156, 'PONEDERA', 4),
(157, 'PUERTO COLOMBIA', 4),
(158, 'REPELON', 4),
(159, 'SABANAGRANDE', 4),
(160, 'SABANALARGA', 4),
(161, 'SANTA LUCIA', 4),
(162, 'SANTO TOMAS', 4),
(163, 'SOLEDAD', 4),
(164, 'SUAN', 4),
(165, 'TUBARA', 4),
(166, 'USIACURI', 4),
(167, 'ACHI', 5),
(168, 'ALTOS DEL ROSARIO', 5),
(169, 'ARENAL', 5),
(170, 'ARJONA', 5),
(171, 'ARROYOHONDO', 5),
(172, 'BARRANCO DE LOBA', 5),
(173, 'BRAZUELO DE PAPAYAL', 5),
(174, 'CALAMAR', 5),
(175, 'CANTAGALLO', 5),
(176, 'CARTAGENA DE INDIAS', 5),
(177, 'CICUCO', 5),
(178, 'CLEMENCIA', 5),
(179, 'CORDOBA', 5),
(180, 'EL CARMEN DE BOLIVAR', 5),
(181, 'EL GUAMO', 5),
(182, 'EL PENION', 5),
(183, 'HATILLO DE LOBA', 5),
(184, 'MAGANGUE', 5),
(185, 'MAHATES', 5),
(186, 'MARGARITA', 5),
(187, 'MARIA LA BAJA', 5),
(188, 'MONTECRISTO', 5),
(189, 'MORALES', 5),
(190, 'MORALES', 5),
(191, 'NOROSI', 5),
(192, 'PINILLOS', 5),
(193, 'REGIDOR', 5),
(194, 'RIO VIEJO', 5),
(195, 'SAN CRISTOBAL', 5),
(196, 'SAN ESTANISLAO', 5),
(197, 'SAN FERNANDO', 5),
(198, 'SAN JACINTO', 5),
(199, 'SAN JACINTO DEL CAUCA', 5),
(200, 'SAN JUAN DE NEPOMUCENO', 5),
(201, 'SAN MARTIN DE LOBA', 5),
(202, 'SAN PABLO', 5),
(203, 'SAN PABLO NORTE', 5),
(204, 'SANTA CATALINA', 5),
(205, 'SANTA CRUZ DE MOMPOX', 5),
(206, 'SANTA ROSA', 5),
(207, 'SANTA ROSA DEL SUR', 5),
(208, 'SIMITI', 5),
(209, 'SOPLAVIENTO', 5),
(210, 'TALAIGUA NUEVO', 5),
(211, 'TUQUISIO', 5),
(212, 'TURBACO', 5),
(213, 'TURBANA', 5),
(214, 'VILLANUEVA', 5),
(215, 'ZAMBRANO', 5),
(216, 'AQUITANIA', 6),
(217, 'ARCABUCO', 6),
(218, 'BELÉN', 6),
(219, 'BERBEO', 6),
(220, 'BETÉITIVA', 6),
(221, 'BOAVITA', 6),
(222, 'BOYACÁ', 6),
(223, 'BRICEÑO', 6),
(224, 'BUENAVISTA', 6),
(225, 'BUSBANZÁ', 6),
(226, 'CALDAS', 6),
(227, 'CAMPO HERMOSO', 6),
(228, 'CERINZA', 6),
(229, 'CHINAVITA', 6),
(230, 'CHIQUINQUIRÁ', 6),
(231, 'CHÍQUIZA', 6),
(232, 'CHISCAS', 6),
(233, 'CHITA', 6),
(234, 'CHITARAQUE', 6),
(235, 'CHIVATÁ', 6),
(236, 'CIÉNEGA', 6),
(237, 'CÓMBITA', 6),
(238, 'COPER', 6),
(239, 'CORRALES', 6),
(240, 'COVARACHÍA', 6),
(241, 'CUBARA', 6),
(242, 'CUCAITA', 6),
(243, 'CUITIVA', 6),
(244, 'DUITAMA', 6),
(245, 'EL COCUY', 6),
(246, 'EL ESPINO', 6),
(247, 'FIRAVITOBA', 6),
(248, 'FLORESTA', 6),
(249, 'GACHANTIVÁ', 6),
(250, 'GÁMEZA', 6),
(251, 'GARAGOA', 6),
(252, 'GUACAMAYAS', 6),
(253, 'GÜICÁN', 6),
(254, 'IZA', 6),
(255, 'JENESANO', 6),
(256, 'JERICÓ', 6),
(257, 'LA UVITA', 6),
(258, 'LA VICTORIA', 6),
(259, 'LABRANZA GRANDE', 6),
(260, 'MACANAL', 6),
(261, 'MARIPÍ', 6),
(262, 'MIRAFLORES', 6),
(263, 'MONGUA', 6),
(264, 'MONGUÍ', 6),
(265, 'MONIQUIRÁ', 6),
(266, 'MOTAVITA', 6),
(267, 'MUZO', 6),
(268, 'NOBSA', 6),
(269, 'NUEVO COLÓN', 6),
(270, 'OICATÁ', 6),
(271, 'OTANCHE', 6),
(272, 'PACHAVITA', 6),
(273, 'PÁEZ', 6),
(274, 'PAIPA', 6),
(275, 'PAJARITO', 6),
(276, 'PANQUEBA', 6),
(277, 'PAUNA', 6),
(278, 'PAYA', 6),
(279, 'PAZ DE RÍO', 6),
(280, 'PESCA', 6),
(281, 'PISBA', 6),
(282, 'PUERTO BOYACA', 6),
(283, 'QUÍPAMA', 6),
(284, 'RAMIRIQUÍ', 6),
(285, 'RÁQUIRA', 6),
(286, 'RONDÓN', 6),
(287, 'SABOYÁ', 6),
(288, 'SÁCHICA', 6),
(289, 'SAMACÁ', 6),
(290, 'SAN EDUARDO', 6),
(291, 'SAN JOSÉ DE PARE', 6),
(292, 'SAN LUÍS DE GACENO', 6),
(293, 'SAN MATEO', 6),
(294, 'SAN MIGUEL DE SEMA', 6),
(295, 'SAN PABLO DE BORBUR', 6),
(296, 'SANTA MARÍA', 6),
(297, 'SANTA ROSA DE VITERBO', 6),
(298, 'SANTA SOFÍA', 6),
(299, 'SANTANA', 6),
(300, 'SATIVANORTE', 6),
(301, 'SATIVASUR', 6),
(302, 'SIACHOQUE', 6),
(303, 'SOATÁ', 6),
(304, 'SOCHA', 6),
(305, 'SOCOTÁ', 6),
(306, 'SOGAMOSO', 6),
(307, 'SORA', 6),
(308, 'SORACÁ', 6),
(309, 'SOTAQUIRÁ', 6),
(310, 'SUSACÓN', 6),
(311, 'SUTARMACHÁN', 6),
(312, 'TASCO', 6),
(313, 'TIBANÁ', 6),
(314, 'TIBASOSA', 6),
(315, 'TINJACÁ', 6),
(316, 'TIPACOQUE', 6),
(317, 'TOCA', 6),
(318, 'TOGÜÍ', 6),
(319, 'TÓPAGA', 6),
(320, 'TOTA', 6),
(321, 'TUNJA', 6),
(322, 'TUNUNGUÁ', 6),
(323, 'TURMEQUÉ', 6),
(324, 'TUTA', 6),
(325, 'TUTAZÁ', 6),
(326, 'UMBITA', 6),
(327, 'VENTA QUEMADA', 6),
(328, 'VILLA DE LEYVA', 6),
(329, 'VIRACACHÁ', 6),
(330, 'ZETAQUIRA', 6),
(331, 'AGUADAS', 7),
(332, 'ANSERMA', 7),
(333, 'ARANZAZU', 7),
(334, 'BELALCAZAR', 7),
(335, 'CHINCHINÁ', 7),
(336, 'FILADELFIA', 7),
(337, 'LA DORADA', 7),
(338, 'LA MERCED', 7),
(339, 'MANIZALES', 7),
(340, 'MANZANARES', 7),
(341, 'MARMATO', 7),
(342, 'MARQUETALIA', 7),
(343, 'MARULANDA', 7),
(344, 'NEIRA', 7),
(345, 'NORCASIA', 7),
(346, 'PACORA', 7),
(347, 'PALESTINA', 7),
(348, 'PENSILVANIA', 7),
(349, 'RIOSUCIO', 7),
(350, 'RISARALDA', 7),
(351, 'SALAMINA', 7),
(352, 'SAMANA', 7),
(353, 'SAN JOSE', 7),
(354, 'SUPÍA', 7),
(355, 'VICTORIA', 7),
(356, 'VILLAMARÍA', 7),
(357, 'VITERBO', 7),
(358, 'ALBANIA', 8),
(359, 'BELÉN ANDAQUIES', 8),
(360, 'CARTAGENA DEL CHAIRA', 8),
(361, 'CURILLO', 8),
(362, 'EL DONCELLO', 8),
(363, 'EL PAUJIL', 8),
(364, 'FLORENCIA', 8),
(365, 'LA MONTAÑITA', 8),
(366, 'MILÁN', 8),
(367, 'MORELIA', 8),
(368, 'PUERTO RICO', 8),
(369, 'SAN  VICENTE DEL CAGUAN', 8),
(370, 'SAN JOSÉ DE FRAGUA', 8),
(371, 'SOLANO', 8),
(372, 'SOLITA', 8),
(373, 'VALPARAÍSO', 8),
(374, 'AGUAZUL', 9),
(375, 'CHAMEZA', 9),
(376, 'HATO COROZAL', 9),
(377, 'LA SALINA', 9),
(378, 'MANÍ', 9),
(379, 'MONTERREY', 9),
(380, 'NUNCHIA', 9),
(381, 'OROCUE', 9),
(382, 'PAZ DE ARIPORO', 9),
(383, 'PORE', 9),
(384, 'RECETOR', 9),
(385, 'SABANA LARGA', 9),
(386, 'SACAMA', 9),
(387, 'SAN LUIS DE PALENQUE', 9),
(388, 'TAMARA', 9),
(389, 'TAURAMENA', 9),
(390, 'TRINIDAD', 9),
(391, 'VILLANUEVA', 9),
(392, 'YOPAL', 9),
(393, 'ALMAGUER', 10),
(394, 'ARGELIA', 10),
(395, 'BALBOA', 10),
(396, 'BOLÍVAR', 10),
(397, 'BUENOS AIRES', 10),
(398, 'CAJIBIO', 10),
(399, 'CALDONO', 10),
(400, 'CALOTO', 10),
(401, 'CORINTO', 10),
(402, 'EL TAMBO', 10),
(403, 'FLORENCIA', 10),
(404, 'GUAPI', 10),
(405, 'INZA', 10),
(406, 'JAMBALÓ', 10),
(407, 'LA SIERRA', 10),
(408, 'LA VEGA', 10),
(409, 'LÓPEZ', 10),
(410, 'MERCADERES', 10),
(411, 'MIRANDA', 10),
(412, 'MORALES', 10),
(413, 'PADILLA', 10),
(414, 'PÁEZ', 10),
(415, 'PATIA (EL BORDO)', 10),
(416, 'PIAMONTE', 10),
(417, 'PIENDAMO', 10),
(418, 'POPAYÁN', 10),
(419, 'PUERTO TEJADA', 10),
(420, 'PURACE', 10),
(421, 'ROSAS', 10),
(422, 'SAN SEBASTIÁN', 10),
(423, 'SANTA ROSA', 10),
(424, 'SANTANDER DE QUILICHAO', 10),
(425, 'SILVIA', 10),
(426, 'SOTARA', 10),
(427, 'SUÁREZ', 10),
(428, 'SUCRE', 10),
(429, 'TIMBÍO', 10),
(430, 'TIMBIQUÍ', 10),
(431, 'TORIBIO', 10),
(432, 'TOTORO', 10),
(433, 'VILLA RICA', 10),
(434, 'AGUACHICA', 11),
(435, 'AGUSTÍN CODAZZI', 11),
(436, 'ASTREA', 11),
(437, 'BECERRIL', 11),
(438, 'BOSCONIA', 11),
(439, 'CHIMICHAGUA', 11),
(440, 'CHIRIGUANÁ', 11),
(441, 'CURUMANÍ', 11),
(442, 'EL COPEY', 11),
(443, 'EL PASO', 11),
(444, 'GAMARRA', 11),
(445, 'GONZÁLEZ', 11),
(446, 'LA GLORIA', 11),
(447, 'LA JAGUA IBIRICO', 11),
(448, 'MANAURE BALCÓN DEL CESAR', 11),
(449, 'PAILITAS', 11),
(450, 'PELAYA', 11),
(451, 'PUEBLO BELLO', 11),
(452, 'RÍO DE ORO', 11),
(453, 'ROBLES (LA PAZ)', 11),
(454, 'SAN ALBERTO', 11),
(455, 'SAN DIEGO', 11),
(456, 'SAN MARTÍN', 11),
(457, 'TAMALAMEQUE', 11),
(458, 'VALLEDUPAR', 11),
(459, 'ACANDI', 12),
(460, 'ALTO BAUDO (PIE DE PATO)', 12),
(461, 'ATRATO', 12),
(462, 'BAGADO', 12),
(463, 'BAHIA SOLANO (MUTIS)', 12),
(464, 'BAJO BAUDO (PIZARRO)', 12),
(465, 'BOJAYA (BELLAVISTA)', 12),
(466, 'CANTON DE SAN PABLO', 12),
(467, 'CARMEN DEL DARIEN', 12),
(468, 'CERTEGUI', 12),
(469, 'CONDOTO', 12),
(470, 'EL CARMEN', 12),
(471, 'ISTMINA', 12),
(472, 'JURADO', 12),
(473, 'LITORAL DEL SAN JUAN', 12),
(474, 'LLORO', 12),
(475, 'MEDIO ATRATO', 12),
(476, 'MEDIO BAUDO (BOCA DE PEPE)', 12),
(477, 'MEDIO SAN JUAN', 12),
(478, 'NOVITA', 12),
(479, 'NUQUI', 12),
(480, 'QUIBDO', 12),
(481, 'RIO IRO', 12),
(482, 'RIO QUITO', 12),
(483, 'RIOSUCIO', 12),
(484, 'SAN JOSE DEL PALMAR', 12),
(485, 'SIPI', 12),
(486, 'TADO', 12),
(487, 'UNGUIA', 12),
(488, 'UNIÓN PANAMERICANA', 12),
(489, 'AYAPEL', 13),
(490, 'BUENAVISTA', 13),
(491, 'CANALETE', 13),
(492, 'CERETÉ', 13),
(493, 'CHIMA', 13),
(494, 'CHINÚ', 13),
(495, 'CIENAGA DE ORO', 13),
(496, 'COTORRA', 13),
(497, 'LA APARTADA', 13),
(498, 'LORICA', 13),
(499, 'LOS CÓRDOBAS', 13),
(500, 'MOMIL', 13),
(501, 'MONTELÍBANO', 13),
(502, 'MONTERÍA', 13),
(503, 'MOÑITOS', 13),
(504, 'PLANETA RICA', 13),
(505, 'PUEBLO NUEVO', 13),
(506, 'PUERTO ESCONDIDO', 13),
(507, 'PUERTO LIBERTADOR', 13),
(508, 'PURÍSIMA', 13),
(509, 'SAHAGÚN', 13),
(510, 'SAN ANDRÉS SOTAVENTO', 13),
(511, 'SAN ANTERO', 13),
(512, 'SAN BERNARDO VIENTO', 13),
(513, 'SAN CARLOS', 13),
(514, 'SAN PELAYO', 13),
(515, 'TIERRALTA', 13),
(516, 'VALENCIA', 13),
(517, 'AGUA DE DIOS', 14),
(518, 'ALBAN', 14),
(519, 'ANAPOIMA', 14),
(520, 'ANOLAIMA', 14),
(521, 'ARBELAEZ', 14),
(522, 'BELTRÁN', 14),
(523, 'BITUIMA', 14),
(524, 'BOGOTÁ DC', 14),
(525, 'BOJACÁ', 14),
(526, 'CABRERA', 14),
(527, 'CACHIPAY', 14),
(528, 'CAJICÁ', 14),
(529, 'CAPARRAPÍ', 14),
(530, 'CAQUEZA', 14),
(531, 'CARMEN DE CARUPA', 14),
(532, 'CHAGUANÍ', 14),
(533, 'CHIA', 14),
(534, 'CHIPAQUE', 14),
(535, 'CHOACHÍ', 14),
(536, 'CHOCONTÁ', 14),
(537, 'COGUA', 14),
(538, 'COTA', 14),
(539, 'CUCUNUBÁ', 14),
(540, 'EL COLEGIO', 14),
(541, 'EL PEÑÓN', 14),
(542, 'EL ROSAL1', 14),
(543, 'FACATATIVA', 14),
(544, 'FÓMEQUE', 14),
(545, 'FOSCA', 14),
(546, 'FUNZA', 14),
(547, 'FÚQUENE', 14),
(548, 'FUSAGASUGA', 14),
(549, 'GACHALÁ', 14),
(550, 'GACHANCIPÁ', 14),
(551, 'GACHETA', 14),
(552, 'GAMA', 14),
(553, 'GIRARDOT', 14),
(554, 'GRANADA2', 14),
(555, 'GUACHETÁ', 14),
(556, 'GUADUAS', 14),
(557, 'GUASCA', 14),
(558, 'GUATAQUÍ', 14),
(559, 'GUATAVITA', 14),
(560, 'GUAYABAL DE SIQUIMA', 14),
(561, 'GUAYABETAL', 14),
(562, 'GUTIÉRREZ', 14),
(563, 'JERUSALÉN', 14),
(564, 'JUNÍN', 14),
(565, 'LA CALERA', 14),
(566, 'LA MESA', 14),
(567, 'LA PALMA', 14),
(568, 'LA PEÑA', 14),
(569, 'LA VEGA', 14),
(570, 'LENGUAZAQUE', 14),
(571, 'MACHETÁ', 14),
(572, 'MADRID', 14),
(573, 'MANTA', 14),
(574, 'MEDINA', 14),
(575, 'MOSQUERA', 14),
(576, 'NARIÑO', 14),
(577, 'NEMOCÓN', 14),
(578, 'NILO', 14),
(579, 'NIMAIMA', 14),
(580, 'NOCAIMA', 14),
(581, 'OSPINA PÉREZ', 14),
(582, 'PACHO', 14),
(583, 'PAIME', 14),
(584, 'PANDI', 14),
(585, 'PARATEBUENO', 14),
(586, 'PASCA', 14),
(587, 'PUERTO SALGAR', 14),
(588, 'PULÍ', 14),
(589, 'QUEBRADANEGRA', 14),
(590, 'QUETAME', 14),
(591, 'QUIPILE', 14),
(592, 'RAFAEL REYES', 14),
(593, 'RICAURTE', 14),
(594, 'SAN  ANTONIO DEL  TEQUENDAMA', 14),
(595, 'SAN BERNARDO', 14),
(596, 'SAN CAYETANO', 14),
(597, 'SAN FRANCISCO', 14),
(598, 'SAN JUAN DE RIOSECO', 14),
(599, 'SASAIMA', 14),
(600, 'SESQUILÉ', 14),
(601, 'SIBATÉ', 14),
(602, 'SILVANIA', 14),
(603, 'SIMIJACA', 14),
(604, 'SOACHA', 14),
(605, 'SOPO', 14),
(606, 'SUBACHOQUE', 14),
(607, 'SUESCA', 14),
(608, 'SUPATÁ', 14),
(609, 'SUSA', 14),
(610, 'SUTATAUSA', 14),
(611, 'TABIO', 14),
(612, 'TAUSA', 14),
(613, 'TENA', 14),
(614, 'TENJO', 14),
(615, 'TIBACUY', 14),
(616, 'TIBIRITA', 14),
(617, 'TOCAIMA', 14),
(618, 'TOCANCIPÁ', 14),
(619, 'TOPAIPÍ', 14),
(620, 'UBALÁ', 14),
(621, 'UBAQUE', 14),
(622, 'UBATÉ', 14),
(623, 'UNE', 14),
(624, 'UTICA', 14),
(625, 'VERGARA', 14),
(626, 'VIANI', 14),
(627, 'VILLA GOMEZ', 14),
(628, 'VILLA PINZÓN', 14),
(629, 'VILLETA', 14),
(630, 'VIOTA', 14),
(631, 'YACOPÍ', 14),
(632, 'ZIPACÓN', 14),
(633, 'ZIPAQUIRÁ', 14),
(634, 'BARRANCO MINAS', 15),
(635, 'CACAHUAL', 15),
(636, 'INÍRIDA', 15),
(637, 'LA GUADALUPE', 15),
(638, 'MAPIRIPANA', 15),
(639, 'MORICHAL', 15),
(640, 'PANA PANA', 15),
(641, 'PUERTO COLOMBIA', 15),
(642, 'SAN FELIPE', 15),
(643, 'CALAMAR', 16),
(644, 'EL RETORNO', 16),
(645, 'MIRAFLOREZ', 16),
(646, 'SAN JOSÉ DEL GUAVIARE', 16),
(647, 'ACEVEDO', 17),
(648, 'AGRADO', 17),
(649, 'AIPE', 17),
(650, 'ALGECIRAS', 17),
(651, 'ALTAMIRA', 17),
(652, 'BARAYA', 17),
(653, 'CAMPO ALEGRE', 17),
(654, 'COLOMBIA', 17),
(655, 'ELIAS', 17),
(656, 'GARZÓN', 17),
(657, 'GIGANTE', 17),
(658, 'GUADALUPE', 17),
(659, 'HOBO', 17),
(660, 'IQUIRA', 17),
(661, 'ISNOS', 17),
(662, 'LA ARGENTINA', 17),
(663, 'LA PLATA', 17),
(664, 'NATAGA', 17),
(665, 'NEIVA', 17),
(666, 'OPORAPA', 17),
(667, 'PAICOL', 17),
(668, 'PALERMO', 17),
(669, 'PALESTINA', 17),
(670, 'PITAL', 17),
(671, 'PITALITO', 17),
(672, 'RIVERA', 17),
(673, 'SALADO BLANCO', 17),
(674, 'SAN AGUSTÍN', 17),
(675, 'SANTA MARIA', 17),
(676, 'SUAZA', 17),
(677, 'TARQUI', 17),
(678, 'TELLO', 17),
(679, 'TERUEL', 17),
(680, 'TESALIA', 17),
(681, 'TIMANA', 17),
(682, 'VILLAVIEJA', 17),
(683, 'YAGUARA', 17),
(684, 'ALBANIA', 18),
(685, 'BARRANCAS', 18),
(686, 'DIBULLA', 18),
(687, 'DISTRACCIÓN', 18),
(688, 'EL MOLINO', 18),
(689, 'FONSECA', 18),
(690, 'HATO NUEVO', 18),
(691, 'LA JAGUA DEL PILAR', 18),
(692, 'MAICAO', 18),
(693, 'MANAURE', 18),
(694, 'RIOHACHA', 18),
(695, 'SAN JUAN DEL CESAR', 18),
(696, 'URIBIA', 18),
(697, 'URUMITA', 18),
(698, 'VILLANUEVA', 18),
(699, 'ALGARROBO', 19),
(700, 'ARACATACA', 19),
(701, 'ARIGUANI', 19),
(702, 'CERRO SAN ANTONIO', 19),
(703, 'CHIVOLO', 19),
(704, 'CIENAGA', 19),
(705, 'CONCORDIA', 19),
(706, 'EL BANCO', 19),
(707, 'EL PIÑON', 19),
(708, 'EL RETEN', 19),
(709, 'FUNDACION', 19),
(710, 'GUAMAL', 19),
(711, 'NUEVA GRANADA', 19),
(712, 'PEDRAZA', 19),
(713, 'PIJIÑO DEL CARMEN', 19),
(714, 'PIVIJAY', 19),
(715, 'PLATO', 19),
(716, 'PUEBLO VIEJO', 19),
(717, 'REMOLINO', 19),
(718, 'SABANAS DE SAN ANGEL', 19),
(719, 'SALAMINA', 19),
(720, 'SAN SEBASTIAN DE BUENAVISTA', 19),
(721, 'SAN ZENON', 19),
(722, 'SANTA ANA', 19),
(723, 'SANTA BARBARA DE PINTO', 19),
(724, 'SANTA MARTA', 19),
(725, 'SITIONUEVO', 19),
(726, 'TENERIFE', 19),
(727, 'ZAPAYAN', 19),
(728, 'ZONA BANANERA', 19),
(729, 'ACACIAS', 20),
(730, 'BARRANCA DE UPIA', 20),
(731, 'CABUYARO', 20),
(732, 'CASTILLA LA NUEVA', 20),
(733, 'CUBARRAL', 20),
(734, 'CUMARAL', 20),
(735, 'EL CALVARIO', 20),
(736, 'EL CASTILLO', 20),
(737, 'EL DORADO', 20),
(738, 'FUENTE DE ORO', 20),
(739, 'GRANADA', 20),
(740, 'GUAMAL', 20),
(741, 'LA MACARENA', 20),
(742, 'LA URIBE', 20),
(743, 'LEJANÍAS', 20),
(744, 'MAPIRIPÁN', 20),
(745, 'MESETAS', 20),
(746, 'PUERTO CONCORDIA', 20),
(747, 'PUERTO GAITÁN', 20),
(748, 'PUERTO LLERAS', 20),
(749, 'PUERTO LÓPEZ', 20),
(750, 'PUERTO RICO', 20),
(751, 'RESTREPO', 20),
(752, 'SAN  JUAN DE ARAMA', 20),
(753, 'SAN CARLOS GUAROA', 20),
(754, 'SAN JUANITO', 20),
(755, 'SAN MARTÍN', 20),
(756, 'VILLAVICENCIO', 20),
(757, 'VISTA HERMOSA', 20),
(758, 'ALBAN', 21),
(759, 'ALDAÑA', 21),
(760, 'ANCUYA', 21),
(761, 'ARBOLEDA', 21),
(762, 'BARBACOAS', 21),
(763, 'BELEN', 21),
(764, 'BUESACO', 21),
(765, 'CHACHAGUI', 21),
(766, 'COLON (GENOVA)', 21),
(767, 'CONSACA', 21),
(768, 'CONTADERO', 21),
(769, 'CORDOBA', 21),
(770, 'CUASPUD', 21),
(771, 'CUMBAL', 21),
(772, 'CUMBITARA', 21),
(773, 'EL CHARCO', 21),
(774, 'EL PEÑOL', 21),
(775, 'EL ROSARIO', 21),
(776, 'EL TABLÓN', 21),
(777, 'EL TAMBO', 21),
(778, 'FUNES', 21),
(779, 'GUACHUCAL', 21),
(780, 'GUAITARILLA', 21),
(781, 'GUALMATAN', 21),
(782, 'ILES', 21),
(783, 'IMUES', 21),
(784, 'IPIALES', 21),
(785, 'LA CRUZ', 21),
(786, 'LA FLORIDA', 21),
(787, 'LA LLANADA', 21),
(788, 'LA TOLA', 21),
(789, 'LA UNION', 21),
(790, 'LEIVA', 21),
(791, 'LINARES', 21),
(792, 'LOS ANDES', 21),
(793, 'MAGUI', 21),
(794, 'MALLAMA', 21),
(795, 'MOSQUEZA', 21),
(796, 'NARIÑO', 21),
(797, 'OLAYA HERRERA', 21),
(798, 'OSPINA', 21),
(799, 'PASTO', 21),
(800, 'PIZARRO', 21),
(801, 'POLICARPA', 21),
(802, 'POTOSI', 21),
(803, 'PROVIDENCIA', 21),
(804, 'PUERRES', 21),
(805, 'PUPIALES', 21),
(806, 'RICAURTE', 21),
(807, 'ROBERTO PAYAN', 21),
(808, 'SAMANIEGO', 21),
(809, 'SAN BERNARDO', 21),
(810, 'SAN LORENZO', 21),
(811, 'SAN PABLO', 21),
(812, 'SAN PEDRO DE CARTAGO', 21),
(813, 'SANDONA', 21),
(814, 'SANTA BARBARA', 21),
(815, 'SANTACRUZ', 21),
(816, 'SAPUYES', 21),
(817, 'TAMINANGO', 21),
(818, 'TANGUA', 21),
(819, 'TUMACO', 21),
(820, 'TUQUERRES', 21),
(821, 'YACUANQUER', 21),
(822, 'ABREGO', 22),
(823, 'ARBOLEDAS', 22),
(824, 'BOCHALEMA', 22),
(825, 'BUCARASICA', 22),
(826, 'CÁCHIRA', 22),
(827, 'CÁCOTA', 22),
(828, 'CHINÁCOTA', 22),
(829, 'CHITAGÁ', 22),
(830, 'CONVENCIÓN', 22),
(831, 'CÚCUTA', 22),
(832, 'CUCUTILLA', 22),
(833, 'DURANIA', 22),
(834, 'EL CARMEN', 22),
(835, 'EL TARRA', 22),
(836, 'EL ZULIA', 22),
(837, 'GRAMALOTE', 22),
(838, 'HACARI', 22),
(839, 'HERRÁN', 22),
(840, 'LA ESPERANZA', 22),
(841, 'LA PLAYA', 22),
(842, 'LABATECA', 22),
(843, 'LOS PATIOS', 22),
(844, 'LOURDES', 22),
(845, 'MUTISCUA', 22),
(846, 'OCAÑA', 22),
(847, 'PAMPLONA', 22),
(848, 'PAMPLONITA', 22),
(849, 'PUERTO SANTANDER', 22),
(850, 'RAGONVALIA', 22),
(851, 'SALAZAR', 22),
(852, 'SAN CALIXTO', 22),
(853, 'SAN CAYETANO', 22),
(854, 'SANTIAGO', 22),
(855, 'SARDINATA', 22),
(856, 'SILOS', 22),
(857, 'TEORAMA', 22),
(858, 'TIBÚ', 22),
(859, 'TOLEDO', 22),
(860, 'VILLA CARO', 22),
(861, 'VILLA DEL ROSARIO', 22),
(862, 'COLÓN', 23),
(863, 'MOCOA', 23),
(864, 'ORITO', 23),
(865, 'PUERTO ASÍS', 23),
(866, 'PUERTO CAYCEDO', 23),
(867, 'PUERTO GUZMÁN', 23),
(868, 'PUERTO LEGUÍZAMO', 23),
(869, 'SAN FRANCISCO', 23),
(870, 'SAN MIGUEL', 23),
(871, 'SANTIAGO', 23),
(872, 'SIBUNDOY', 23),
(873, 'VALLE DEL GUAMUEZ', 23),
(874, 'VILLAGARZÓN', 23),
(875, 'ARMENIA', 24),
(876, 'BUENAVISTA', 24),
(877, 'CALARCÁ', 24),
(878, 'CIRCASIA', 24),
(879, 'CÓRDOBA', 24),
(880, 'FILANDIA', 24),
(881, 'GÉNOVA', 24),
(882, 'LA TEBAIDA', 24),
(883, 'MONTENEGRO', 24),
(884, 'PIJAO', 24),
(885, 'QUIMBAYA', 24),
(886, 'SALENTO', 24),
(887, 'APIA', 25),
(888, 'BALBOA', 25),
(889, 'BELÉN DE UMBRÍA', 25),
(890, 'DOS QUEBRADAS', 25),
(891, 'GUATICA', 25),
(892, 'LA CELIA', 25),
(893, 'LA VIRGINIA', 25),
(894, 'MARSELLA', 25),
(895, 'MISTRATO', 25),
(896, 'PEREIRA', 25),
(897, 'PUEBLO RICO', 25),
(898, 'QUINCHÍA', 25),
(899, 'SANTA ROSA DE CABAL', 25),
(900, 'SANTUARIO', 25),
(901, 'PROVIDENCIA', 26),
(902, 'SAN ANDRES', 26),
(903, 'SANTA CATALINA', 26),
(904, 'AGUADA', 27),
(905, 'ALBANIA', 27),
(906, 'ARATOCA', 27),
(907, 'BARBOSA', 27),
(908, 'BARICHARA', 27),
(909, 'BARRANCABERMEJA', 27),
(910, 'BETULIA', 27),
(911, 'BOLÍVAR', 27),
(912, 'BUCARAMANGA', 27),
(913, 'CABRERA', 27),
(914, 'CALIFORNIA', 27),
(915, 'CAPITANEJO', 27),
(916, 'CARCASI', 27),
(917, 'CEPITA', 27),
(918, 'CERRITO', 27),
(919, 'CHARALÁ', 27),
(920, 'CHARTA', 27),
(921, 'CHIMA', 27),
(922, 'CHIPATÁ', 27),
(923, 'CIMITARRA', 27),
(924, 'CONCEPCIÓN', 27),
(925, 'CONFINES', 27),
(926, 'CONTRATACIÓN', 27),
(927, 'COROMORO', 27),
(928, 'CURITÍ', 27),
(929, 'EL CARMEN', 27),
(930, 'EL GUACAMAYO', 27),
(931, 'EL PEÑÓN', 27),
(932, 'EL PLAYÓN', 27),
(933, 'ENCINO', 27),
(934, 'ENCISO', 27),
(935, 'FLORIÁN', 27),
(936, 'FLORIDABLANCA', 27),
(937, 'GALÁN', 27),
(938, 'GAMBITA', 27),
(939, 'GIRÓN', 27),
(940, 'GUACA', 27),
(941, 'GUADALUPE', 27),
(942, 'GUAPOTA', 27),
(943, 'GUAVATÁ', 27),
(944, 'GUEPSA', 27),
(945, 'HATO', 27),
(946, 'JESÚS MARIA', 27),
(947, 'JORDÁN', 27),
(948, 'LA BELLEZA', 27),
(949, 'LA PAZ', 27),
(950, 'LANDAZURI', 27),
(951, 'LEBRIJA', 27),
(952, 'LOS SANTOS', 27),
(953, 'MACARAVITA', 27),
(954, 'MÁLAGA', 27),
(955, 'MATANZA', 27),
(956, 'MOGOTES', 27),
(957, 'MOLAGAVITA', 27),
(958, 'OCAMONTE', 27),
(959, 'OIBA', 27),
(960, 'ONZAGA', 27),
(961, 'PALMAR', 27),
(962, 'PALMAS DEL SOCORRO', 27),
(963, 'PÁRAMO', 27),
(964, 'PIEDECUESTA', 27),
(965, 'PINCHOTE', 27),
(966, 'PUENTE NACIONAL', 27),
(967, 'PUERTO PARRA', 27),
(968, 'PUERTO WILCHES', 27),
(969, 'RIONEGRO', 27),
(970, 'SABANA DE TORRES', 27),
(971, 'SAN ANDRÉS', 27),
(972, 'SAN BENITO', 27),
(973, 'SAN GIL', 27),
(974, 'SAN JOAQUÍN', 27),
(975, 'SAN JOSÉ DE MIRANDA', 27),
(976, 'SAN MIGUEL', 27),
(977, 'SAN VICENTE DE CHUCURÍ', 27),
(978, 'SANTA BÁRBARA', 27),
(979, 'SANTA HELENA', 27),
(980, 'SIMACOTA', 27),
(981, 'SOCORRO', 27),
(982, 'SUAITA', 27),
(983, 'SUCRE', 27),
(984, 'SURATA', 27),
(985, 'TONA', 27),
(986, 'VALLE SAN JOSÉ', 27),
(987, 'VÉLEZ', 27),
(988, 'VETAS', 27),
(989, 'VILLANUEVA', 27),
(990, 'ZAPATOCA', 27),
(991, 'BUENAVISTA', 28),
(992, 'CAIMITO', 28),
(993, 'CHALÁN', 28),
(994, 'COLOSO', 28),
(995, 'COROZAL', 28),
(996, 'EL ROBLE', 28),
(997, 'GALERAS', 28),
(998, 'GUARANDA', 28),
(999, 'LA UNIÓN', 28),
(1000, 'LOS PALMITOS', 28),
(1001, 'MAJAGUAL', 28),
(1002, 'MORROA', 28),
(1003, 'OVEJAS', 28),
(1004, 'PALMITO', 28),
(1005, 'SAMPUES', 28),
(1006, 'SAN BENITO ABAD', 28),
(1007, 'SAN JUAN DE BETULIA', 28),
(1008, 'SAN MARCOS', 28),
(1009, 'SAN ONOFRE', 28),
(1010, 'SAN PEDRO', 28),
(1011, 'SINCÉ', 28),
(1012, 'SINCELEJO', 28),
(1013, 'SUCRE', 28),
(1014, 'TOLÚ', 28),
(1015, 'TOLUVIEJO', 28),
(1016, 'ALPUJARRA', 29),
(1017, 'ALVARADO', 29),
(1018, 'AMBALEMA', 29),
(1019, 'ANZOATEGUI', 29),
(1020, 'ARMERO (GUAYABAL)', 29),
(1021, 'ATACO', 29),
(1022, 'CAJAMARCA', 29),
(1023, 'CARMEN DE APICALÁ', 29),
(1024, 'CASABIANCA', 29),
(1025, 'CHAPARRAL', 29),
(1026, 'COELLO', 29),
(1027, 'COYAIMA', 29),
(1028, 'CUNDAY', 29),
(1029, 'DOLORES', 29),
(1030, 'ESPINAL', 29),
(1031, 'FALÁN', 29),
(1032, 'FLANDES', 29),
(1033, 'FRESNO', 29),
(1034, 'GUAMO', 29),
(1035, 'HERVEO', 29),
(1036, 'HONDA', 29),
(1037, 'IBAGUÉ', 29),
(1038, 'ICONONZO', 29),
(1039, 'LÉRIDA', 29),
(1040, 'LÍBANO', 29),
(1041, 'MARIQUITA', 29),
(1042, 'MELGAR', 29),
(1043, 'MURILLO', 29),
(1044, 'NATAGAIMA', 29),
(1045, 'ORTEGA', 29),
(1046, 'PALOCABILDO', 29),
(1047, 'PIEDRAS PLANADAS', 29),
(1048, 'PRADO', 29),
(1049, 'PURIFICACIÓN', 29),
(1050, 'RIOBLANCO', 29),
(1051, 'RONCESVALLES', 29),
(1052, 'ROVIRA', 29),
(1053, 'SALDAÑA', 29),
(1054, 'SAN ANTONIO', 29),
(1055, 'SAN LUIS', 29),
(1056, 'SANTA ISABEL', 29),
(1057, 'SUÁREZ', 29),
(1058, 'VALLE DE SAN JUAN', 29),
(1059, 'VENADILLO', 29),
(1060, 'VILLAHERMOSA', 29),
(1061, 'VILLARRICA', 29),
(1062, 'ALCALÁ', 30),
(1063, 'ANDALUCÍA', 30),
(1064, 'ANSERMA NUEVO', 30),
(1065, 'ARGELIA', 30),
(1066, 'BOLÍVAR', 30),
(1067, 'BUENAVENTURA', 30),
(1068, 'BUGA', 30),
(1069, 'BUGALAGRANDE', 30),
(1070, 'CAICEDONIA', 30),
(1071, 'CALI', 30),
(1072, 'CALIMA (DARIEN)', 30),
(1073, 'CANDELARIA', 30),
(1074, 'CARTAGO', 30),
(1075, 'DAGUA', 30),
(1076, 'EL AGUILA', 30),
(1077, 'EL CAIRO', 30),
(1078, 'EL CERRITO', 30),
(1079, 'EL DOVIO', 30),
(1080, 'FLORIDA', 30),
(1081, 'GINEBRA GUACARI', 30),
(1082, 'JAMUNDÍ', 30),
(1083, 'LA CUMBRE', 30),
(1084, 'LA UNIÓN', 30),
(1085, 'LA VICTORIA', 30),
(1086, 'OBANDO', 30),
(1087, 'PALMIRA', 30),
(1088, 'PRADERA', 30),
(1089, 'RESTREPO', 30),
(1090, 'RIO FRÍO', 30),
(1091, 'ROLDANILLO', 30),
(1092, 'SAN PEDRO', 30),
(1093, 'SEVILLA', 30),
(1094, 'TORO', 30),
(1095, 'TRUJILLO', 30),
(1096, 'TULÚA', 30),
(1097, 'ULLOA', 30),
(1098, 'VERSALLES', 30),
(1099, 'VIJES', 30),
(1100, 'YOTOCO', 30),
(1101, 'YUMBO', 30),
(1102, 'ZARZAL', 30),
(1103, 'CARURÚ', 31),
(1104, 'MITÚ', 31),
(1105, 'PACOA', 31),
(1106, 'PAPUNAUA', 31),
(1107, 'TARAIRA', 31),
(1108, 'YAVARATÉ', 31),
(1109, 'CUMARIBO', 32),
(1110, 'LA PRIMAVERA', 32),
(1111, 'PUERTO CARREÑO', 32),
(1112, 'SANTA ROSALIA', 32);

-- --------------------------------------------------------

--
-- Table structure for table `cms_no_units`
--

CREATE TABLE IF NOT EXISTS `cms_no_units` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(4) DEFAULT NULL COMMENT 'input|view|label#Cantidad#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar la información de la lista "Número de /* comment truncated */ /* unidades" del módulo de fórmula médica*/' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `cms_no_units`
--

INSERT INTO `cms_no_units` (`id`, `name`) VALUES
(1, '1'),
(2, '2'),
(3, '3'),
(4, '4'),
(5, '5');

-- --------------------------------------------------------

--
-- Table structure for table `cms_oauth_config`
--

CREATE TABLE IF NOT EXISTS `cms_oauth_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cms_oauth_config`
--

INSERT INTO `cms_oauth_config` (`id`, `uri`, `var`) VALUES
(1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `cms_opening_hours`
--

CREATE TABLE IF NOT EXISTS `cms_opening_hours` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(8) DEFAULT NULL COMMENT 'input|view|label#Hora#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar la información de las opciones de "Ho /* comment truncated */ /*rarios de atención" en el formulario de registro de usuarios*/' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `cms_opening_hours`
--

INSERT INTO `cms_opening_hours` (`id`, `name`) VALUES
(1, '8:00'),
(2, '10:00'),
(3, '12:00');

-- --------------------------------------------------------

--
-- Table structure for table `cms_periodicity`
--

CREATE TABLE IF NOT EXISTS `cms_periodicity` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL COMMENT 'input|view|label#Periodicidad#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar la información de la lista "Periodici /* comment truncated */ /*dad" del módulo de fórmula médica*/' AUTO_INCREMENT=5 ;

--
-- Dumping data for table `cms_periodicity`
--

INSERT INTO `cms_periodicity` (`id`, `name`) VALUES
(1, 'Cada 2 horas'),
(2, 'Cada 6 horas'),
(3, 'Diariamente'),
(4, 'Semanalmente');

-- --------------------------------------------------------

--
-- Table structure for table `cms_permissions`
--

CREATE TABLE IF NOT EXISTS `cms_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` enum('module','function','component') COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cms_permissions`
--

INSERT INTO `cms_permissions` (`id`, `name`, `var`, `type`) VALUES
(1, 'Permisos', 'cms_admin_perms', 'module'),
(2, 'Oauth', 'cms_config_oauth', 'module');

-- --------------------------------------------------------

--
-- Table structure for table `cms_phases`
--

CREATE TABLE IF NOT EXISTS `cms_phases` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL COMMENT 'input|view|label#Nombre#',
  `description` text COMMENT 'input|view|label#Descripción#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar los tipos de  fases de rutinas dispob /* comment truncated */ /*ibles*/' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cms_phases`
--

INSERT INTO `cms_phases` (`id`, `name`, `description`) VALUES
(1, 'Fase de prueba', 'Fase de pruebas '),
(2, 'Fase de estabilidad', 'Fase de estabilidad');

-- --------------------------------------------------------

--
-- Table structure for table `cms_posts`
--

CREATE TABLE IF NOT EXISTS `cms_posts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT 'input|view|label#Nombre#',
  `description` text NOT NULL COMMENT 'input|view|label#Descripción#',
  `date` date NOT NULL COMMENT 'input|view|label#Fecha#',
  `users_id` int(10) NOT NULL,
  `imagen_id` int(11) DEFAULT NULL COMMENT 'input|view|label#Imagen#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cms_recipes`
--

CREATE TABLE IF NOT EXISTS `cms_recipes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT 'input|view|label#Titulo de la receta#',
  `description` text NOT NULL COMMENT 'input|view|label#Descripción#',
  `date` date DEFAULT NULL COMMENT 'input|view|label#Fecha#',
  `users_id` int(10) NOT NULL,
  `imagen_id` int(11) DEFAULT NULL COMMENT 'input|view|label#Imagen#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `cms_recipes`
--

INSERT INTO `cms_recipes` (`id`, `name`, `description`, `date`, `users_id`, `imagen_id`) VALUES
(1, 'Crema a la Reina.', 'Se pone a hervir la pechuga con el agua, la zanahoria, el nabo, la cebolla y el puerro, todo cortado en trocitos y con el caldito.  Cuando empieza a hervir se espuma. Se deja cocer hasta que esté tierna.  Se deshuesa y se pasa por la trituradora con un poco de caldo caliente.  Se reserva. En un cazo se pone la mantequilla y se rehoga la harina, se añade la leche caliente más tres cuartos de caldo de cocción, hasta que se consiga una crema clara.  Se une a la pechuga. Se mezcla bien y se le da un hervor.  Se conserva caliente al baño María hasta que se vaya a servir.  En ese momento se deslíen las yemas en un poco de leche fría.  Se une a la crema muy caliente sin dejar de remover para que quede bien ligada.', '2014-02-12', 5, 63),
(2, ' Calzone de verduras', 'En un recipiente se pone la harina colada, se deja un hueco en el centro, se echa la levadura y el agua. Se espolvorea con harina, se tapa y se deja 15 minutos en un sitio caliente.  Se echa sal, huevo, aceite y se forma una masa. Tapar y dejar reposar durante 20 minutos.  Limpiar y cortar la verdura. Se fríe todo en la sartén. Se estira tira la masa y se parte en dos partes.  Engrasar la bandeja, poner la masa encima del relleno, se cubre con la otra parte de la masa, cerrándolo de abajo a arriba. Se pone al horno durante 30 minutos hasta que se dore.', '2014-02-12', 5, 64),
(3, 'Ensalada de Tomate II', 'Esta receta es similar a la Capresa Italiana, la diferencia esta en el queso de búfala que la hace superior.  Se cortan los tomates en ruedas (sin semillas), se les agrega el queso de búfala en trozos pequeños, se le agrega el orégano y se salpimienta, se adorna con las hojas de albahaca y se le echa el aceite de oliva al gusto.', '2014-02-12', 5, 66);

-- --------------------------------------------------------

--
-- Table structure for table `cms_redes_sociales`
--

CREATE TABLE IF NOT EXISTS `cms_redes_sociales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `red_social` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_red` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `cms_redes_sociales`
--

INSERT INTO `cms_redes_sociales` (`id`, `red_social`, `link_red`, `fecha_creacion`) VALUES
(1, 'Facebook', 'https://www.facebook.com/clinicarangelpereira', '2013-10-21 16:58:04'),
(2, 'Twitter', 'http://www.twitter.com/clinicarangelpereira', '2013-10-21 16:58:07'),
(3, 'Youtube', 'http://www.youtube.com/clinicarangelpereira', '2013-10-21 16:58:11');

-- --------------------------------------------------------

--
-- Table structure for table `cms_reported_activity`
--

CREATE TABLE IF NOT EXISTS `cms_reported_activity` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `users_id` int(10) unsigned NOT NULL COMMENT 'combox|view|label#Usuario#',
  `weight` varchar(5) DEFAULT NULL COMMENT 'input|view|label#Peso#',
  `time` varchar(8) DEFAULT NULL COMMENT 'input|view|label#Tiempo#',
  `description` text COMMENT 'input|view|label#Descripción#',
  `activity_minutes_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#Minutos de actividad#',
  `weeks_id` int(10) NOT NULL COMMENT 'combox|view|label#Semana#',
  PRIMARY KEY (`id`),
  KEY `fk_reported_activity_cms_users1_idx` (`users_id`),
  KEY `fk_reported_activity_actitity_minutes1_idx` (`activity_minutes_id`),
  KEY `fk_reported_activity_weeks1_idx` (`weeks_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla apra administrar la información del módulo "Reporta tu /* comment truncated */ /* actividad"*/' AUTO_INCREMENT=19 ;

--
-- Dumping data for table `cms_reported_activity`
--

INSERT INTO `cms_reported_activity` (`id`, `users_id`, `weight`, `time`, `description`, `activity_minutes_id`, `weeks_id`) VALUES
(1, 5, '56', '566', '5656565', 12, 1),
(18, 21, '57', NULL, NULL, 13, 2);

-- --------------------------------------------------------

--
-- Table structure for table `cms_routines`
--

CREATE TABLE IF NOT EXISTS `cms_routines` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `routines_categories_id` int(4) NOT NULL COMMENT 'combox|view|label#Categoríar#',
  `name` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Nombre#',
  `description` text COMMENT 'input|view|label#Descripción#',
  PRIMARY KEY (`id`),
  KEY `fk_routines_routines_categories1_idx` (`routines_categories_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Taba para administrar las rutinas de ejercicios que manejará /* comment truncated */ /* la aplicación*/' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cms_routines`
--

INSERT INTO `cms_routines` (`id`, `routines_categories_id`, `name`, `description`) VALUES
(1, 2, 'Rutina #1', 'Rutina de prueba #1'),
(2, 2, 'Rutina de prueba #2', 'Rutina de prueba #2 descripción');

-- --------------------------------------------------------

--
-- Table structure for table `cms_routines_categories`
--

CREATE TABLE IF NOT EXISTS `cms_routines_categories` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Nombre#',
  `description` text COMMENT 'input|view|label#Description#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla para adminsitrar las categorías de rutinas disponibles /* comment truncated */ /* */' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cms_routines_categories`
--

INSERT INTO `cms_routines_categories` (`id`, `name`, `description`) VALUES
(1, 'Categoría 1', 'Categoría de prueba'),
(2, 'Categoría 2', 'Categoría de prueba #2');

-- --------------------------------------------------------

--
-- Table structure for table `cms_sessions`
--

CREATE TABLE IF NOT EXISTS `cms_sessions` (
  `session_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `user_agent` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cms_sessions`
--

INSERT INTO `cms_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('002913351690d1480fc76241d4b47a31', '127.0.0.1', '0', 1390253699, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('003f13a45077f918607249b836a27aaa', '127.0.0.1', '0', 1390252768, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('00748f6f9aafe26061fd9cdc84166071', '127.0.0.1', '0', 1390256112, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('008d8bd495037df089b7aeb47436f514', '127.0.0.1', '0', 1390252894, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('00e3447201697af41f1d5415c188bb86', '127.0.0.1', '0', 1390252254, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('00ea6e668702e319644af0fb896f3b9e', '127.0.0.1', '0', 1390256233, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('00feffe474fdb0464ab17c7a22cfc137', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0118ebed0b6d8a1c5dbfdcd8dc3fe287', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', 1391806461, 'a:8:{s:9:"user_data";s:0:"";s:8:"identity";s:18:"cms@imaginamos.com";s:8:"username";s:13:"administrator";s:5:"email";s:18:"cms@imaginamos.com";s:7:"user_id";s:1:"5";s:14:"old_last_login";s:4:"2014";s:9:"page_epss";s:1:"1";s:24:"flash:old:alert_messages";s:121:"<script>$.sticky("Datos Guardados con exito...!", {autoclose : 5000, position: "top-center", type: "st-info" });</script>";}'),
('012b67b6bf38864e1500df70cfd812f2', '127.0.0.1', '0', 1390253697, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('018780dd91ecf68d078fcf4f8d02cb04', '127.0.0.1', '0', 1390253256, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('018dda2075d515bd24088748683dc6bc', '127.0.0.1', '0', 1390249239, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('01b647493b1b596d35e19e968385153e', '127.0.0.1', '0', 1390251167, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('01c856aed9a69f67b0ea2d15e7c959fb', '127.0.0.1', '0', 1390256903, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('01cb13a09e4ac659feea11412001c1c7', '127.0.0.1', '0', 1390253980, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('01d2601971ce4a68249301ba03c59cf8', '127.0.0.1', '0', 1390249120, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('020ff26fac78ca1a4b1b47533204e1e5', '127.0.0.1', '0', 1390249238, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0224995a21033a0bfa5d1d4dfe34831c', '127.0.0.1', '0', 1390249079, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('023350dcfe060f3d9851b37d02085a05', '127.0.0.1', '0', 1390253968, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0234140efee74270a042628af112eb90', '127.0.0.1', '0', 1390253692, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('02435ff85a2cb945b3df7393c588ccd5', '127.0.0.1', '0', 1390252771, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('026cb1901ba03e7fb86c3630a2771a99', '127.0.0.1', '0', 1390252338, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0272bd2902dea5263beca42a863f3df6', '127.0.0.1', '0', 1390252219, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('02a977dc0cb048af669ef70ec0abeb60', '127.0.0.1', '0', 1390254224, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('030bccaea06b4de2d81dd42f197d3fbf', '127.0.0.1', '0', 1390252893, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('030fb5f93d526ee03dd1287d57381162', '127.0.0.1', '0', 1390252243, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('03178c7940c6b9575287f88747d04855', '127.0.0.1', '0', 1390252240, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('034fab3b2106ead31d8a636bb65ead4e', '127.0.0.1', '0', 1390256399, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0361120be4817baca2651c769572ae6a', '127.0.0.1', '0', 1390252891, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('037cb5f9220c422a6d96b0ca43fc87cc', '127.0.0.1', '0', 1390253267, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('03b540c83dfe45100e3a8032ea2bad65', '127.0.0.1', '0', 1390253967, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('03c5db0792974804424d69d5528267e2', '127.0.0.1', '0', 1390249122, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('03d9a800b4714a1c956af3df82c9d6b2', '127.0.0.1', '0', 1390256116, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('03e3e1c37eea9f672e38ac013a726a6b', '127.0.0.1', '0', 1390249237, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('03f2c9e9fa40663612affd0fe316a0c5', '127.0.0.1', '0', 1390252258, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('041ec58578394c655c519f9fa6ef55dc', '127.0.0.1', '0', 1390256898, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('04216e457a401e62a730d1f52344fc3b', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', 1392149847, 'a:8:{s:9:"user_data";s:0:"";s:8:"identity";s:18:"cms@imaginamos.com";s:8:"username";s:13:"administrator";s:5:"email";s:18:"cms@imaginamos.com";s:7:"user_id";s:1:"5";s:14:"old_last_login";s:4:"2014";s:22:"page_component_fieldss";s:1:"1";s:16:"current_user_one";b:1;}'),
('042276270266ff7755f2514d5fc2e9e4', '127.0.0.1', '0', 1390256397, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0484d094c3450dcb64e991c9901b5f11', '127.0.0.1', '0', 1390253696, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('04b85527347729e49be820be0dd595c0', '127.0.0.1', '0', 1390252343, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('04d726daf79ae32d09205c9590104ee0', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', 1392041199, 'a:8:{s:9:"user_data";s:0:"";s:8:"identity";s:18:"cms@imaginamos.com";s:8:"username";s:13:"administrator";s:5:"email";s:18:"cms@imaginamos.com";s:7:"user_id";s:1:"5";s:14:"old_last_login";s:4:"2014";s:16:"page_componentss";s:1:"1";s:22:"page_component_fieldss";s:1:"1";}'),
('04ff7fd252292247d9372520701db6ba', '127.0.0.1', '0', 1390252344, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('05230c9a620464bae17b4d9ba6d23fe0', '127.0.0.1', '0', 1390252339, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('052d15b5b789bd5ca10cd54c3ae39202', '127.0.0.1', '0', 1390253255, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('057101174ad8160392e239625eb5b648', '127.0.0.1', '0', 1390253254, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('057e2e928ce80ba73a2a7511edb0369b', '127.0.0.1', '0', 1390256004, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('059709fc8b039cf8e50ac7daf28b7d67', '127.0.0.1', '0', 1390252229, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('05a54d770eb3f64cbf9df9f07b2b9dbe', '127.0.0.1', '0', 1390252902, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('05af54767c0e5b6186dbfd559a2dd0b2', '127.0.0.1', '0', 1390256220, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('05c16fe5b8969c251a205efcba591405', '127.0.0.1', '0', 1390252776, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0612d4bc314485dd0351a73f4a8de70e', '127.0.0.1', '0', 1390253252, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('062ce0266f85d19c72482c27cf14fd6c', '127.0.0.1', '0', 1390256900, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('063248070aea9be31ea198999c6d4725', '127.0.0.1', '0', 1390253254, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('06531406d5ef8d824f9bec79782f477c', '127.0.0.1', '0', 1390253696, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0686613ef68d606810a0cd71f8c34600', '127.0.0.1', '0', 1390252901, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('06b715b0bc35addc1ca9e7f378c0529e', '127.0.0.1', '0', 1390256225, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('06c1d53417579c7b82dfb1cf230f195a', '127.0.0.1', '0', 1390256116, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('06c6f68ea974e879ea1f76e89435f03c', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390840740, 'a:7:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;s:8:"identity";s:22:"juan.cantor@imagina.co";s:8:"username";s:12:"juancantor12";s:5:"email";s:22:"juan.cantor@imagina.co";s:7:"user_id";s:2:"15";s:14:"old_last_login";s:4:"2014";}'),
('06cc6c0ec452f7dfaff9540ab7f317cf', '127.0.0.1', '0', 1390249121, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('06cd5a32cba1b73640499f382c2b52ab', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0', 1384835266, 'a:10:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:16:"page_video_lugar";s:1:"1";s:11:"page_barner";s:1:"1";s:3:"add";s:1:"0";s:6:"editar";s:1:"0";s:6:"delete";s:1:"0";s:19:"page_destacado_home";s:1:"1";}'),
('06f172373ba12d24f42def30c13e05ad', '127.0.0.1', '0', 1390256112, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0720db8eaa473eca84334530ad95f468', '127.0.0.1', '0', 1390249426, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('07336673180b6033ef7c08ce94df6029', '127.0.0.1', '0', 1390256004, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('075c0b74c140e0570508172121daa7a3', '127.0.0.1', '0', 1390252899, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('075c760efc46e8bb3fc2a4ac95e13fd1', '127.0.0.1', '0', 1390253687, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('076016f4006464186a8a308984e49d36', '127.0.0.1', '0', 1390254259, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0763fa4987e17c8a3932cad27bc915e1', '127.0.0.1', '0', 1390249236, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('078050341e050521422ca3c69c4381aa', '127.0.0.1', '0', 1390254371, ''),
('07a3ab9ce056543a79e80d366ed3deeb', '127.0.0.1', '0', 1390253704, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('07c95d5ada1cd51c89f1bc65f3788930', '127.0.0.1', '0', 1390252897, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('07e258dbf1e959b13119dfcd278b5362', '127.0.0.1', '0', 1390252351, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('08595972c0fa1f23abaa539484f6fdae', '127.0.0.1', '0', 1390255954, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('08911eb0a8be17244823bc2dc00a31fd', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390853812, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:22:"juan.cantor@imagina.co";s:2:"id";s:2:"15";s:7:"user_id";s:2:"15";s:16:"current_user_one";b:1;}'),
('089407ac902ff45337b24eec8dd5eb19', '127.0.0.1', '0', 1390253978, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('08b29d79415cb1a8b5ce848e7effddb4', '127.0.0.1', '0', 1390253257, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('08d311b21e0e39b7ca6a9aa3cba82de8', '127.0.0.1', '0', 1390256003, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0902cf12965bc233461c8c3fd582e192', '127.0.0.1', '0', 1390256117, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0922e9a4def76deb01eb8676d72e0052', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0925c2c5be5d03af65362925e1e2fa88', '127.0.0.1', '0', 1390256114, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('094e021f1e9bc923a3aacb41542442a9', '127.0.0.1', '0', 1390252253, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('09ab26e8240d0f76a7b8e62bc698eada', '127.0.0.1', '0', 1390254327, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0a60cdd617a9292ef03c3d1070673ae7', '127.0.0.1', '0', 1390252248, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0a7cd20086af4f57ce0a788ca5cf9bb1', '127.0.0.1', '0', 1390253381, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0a85f5872b7de7f11752b51c42469324', '127.0.0.1', '0', 1390254225, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0a9ab429a4045c4c87f9f568ff864b08', '127.0.0.1', '0', 1390254321, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0a9ce57330f0d91930ee6e5fa1db3c59', '127.0.0.1', '0', 1390252341, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0aae9a2b7db6046a73b0afb3be9f940d', '127.0.0.1', '0', 1390252898, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0aba69269667b108999a89b088c5dbe9', '127.0.0.1', '0', 1390253968, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0ad115f18d5756160e6248ca0c24ef94', '127.0.0.1', '0', 1390256113, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0ad20ebe5a54c7640950f3ee3744d43b', '127.0.0.1', '0', 1390253382, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0ae0a8b33b751cee8689b7e61187957f', '127.0.0.1', '0', 1390249239, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0b06ba0d57c0905df05a2ffa6731c3fe', '127.0.0.1', '0', 1390252892, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0b10ccadccb2e2483b53e8a5590b51f8', '127.0.0.1', '0', 1390252771, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0b12cf707d8134d5811118c7a64c7fc5', '127.0.0.1', '0', 1390254323, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0b30f2bbd341b51fc38d7878c6fd9370', '127.0.0.1', '0', 1390252891, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0b43f4a02fe44b9cced0cd6f30f0e8e4', '127.0.0.1', '0', 1390252229, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0b79bf454e03494112bd083ccd904df1', '127.0.0.1', '0', 1390252773, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0b7b0114b12f01a9cabca3c7934fc9e8', '127.0.0.1', '0', 1390252231, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0b9fdac2569603c985d5feceac0706c1', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0ba50ca24c316a594082cb3b9017263e', '127.0.0.1', '0', 1390250296, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0bac1aa98990bb2cf96e07cbc0bcb35a', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390254220, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:16:"current_user_one";b:1;}'),
('0bc5c086c07cad49e2c8971b853ba61a', '127.0.0.1', '0', 1390250294, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0be35bd5e28024c6b56b1b02e9c0ede3', '127.0.0.1', '0', 1390252769, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0bef67cfe6bd9875476dcb073c7a429d', '127.0.0.1', '0', 1390256118, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0bf12ff659e02819e4396f5f6020f94e', '127.0.0.1', '0', 1390252346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0c0f10538d43efdfdd6d306b0f7b4661', '127.0.0.1', '0', 1390252894, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0c3bb29c237cfb1c60ea1348551cb105', '127.0.0.1', '0', 1390252895, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0ca322bbf44c177dd461023093fd30ed', '127.0.0.1', '0', 1390256118, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0ca8990d8a824b70ff77e12a82ac2511', '127.0.0.1', '0', 1390254368, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0cde2cf7f4c2f7eada9b508252b46986', '127.0.0.1', '0', 1390252770, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0ce7a4588ce493adaf086763ee060623', '127.0.0.1', '0', 1390252771, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0cebeea9edd865db61767af936311448', '127.0.0.1', '0', 1390252215, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0d5d5bf522f710f48b55e4a312f95dce', '127.0.0.1', '0', 1390254265, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0d647af0eb3fb137503229c1df03e80f', '127.0.0.1', '0', 1390253386, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0d741a90cd2b5631241a259fa76951a8', '127.0.0.1', '0', 1390256116, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0d7a5e4c4481a066f2666e2ccf63fe13', '127.0.0.1', '0', 1390256903, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0db1e3a7bf10dd9d03325f3dae5a54bc', '127.0.0.1', '0', 1390256227, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0dc29d5ff2050d829dc4a429b671e43b', '127.0.0.1', '0', 1390253690, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0dec919141b72e8c8ed5e063e2185ff4', '127.0.0.1', '0', 1390253255, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0e457310e3e73d023bd1793f21412742', '127.0.0.1', '0', 1390254234, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0e6431b799c52fb716cd9008a07f913a', '127.0.0.1', '0', 1390252337, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0e75dfb4e1c7b7fe9a7a5948cbbe100f', '127.0.0.1', '0', 1390251986, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0ea5e7b5f77798407a1ecbcf7212cdd5', '127.0.0.1', '0', 1390253388, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0ec0ebeac946e496026646fbd9ac0f57', '127.0.0.1', '0', 1390256116, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0ed715198ca3e0e00ba75288d7ef4711', '127.0.0.1', '0', 1390253695, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0ef61b36db96a5b9913af75f65262d50', '127.0.0.1', '0', 1390253271, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0f13979f0268b7f6e86e044e7c7524b2', '127.0.0.1', '0', 1390254373, ''),
('0f429b27a4081b41381c490530880a60', '127.0.0.1', '0', 1390252777, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0f6071959647c63c3ff1122b481d5f41', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.102 Safari/537.36', 1391093270, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0f66d2657efcd36338484c6db5888bfb', '127.0.0.1', '0', 1390256112, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0f8fc8015c29e2416bb14f5b62757fc2', '127.0.0.1', '0', 1390252779, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0fb1ee8a64be4af59164aa6457af7bd7', '127.0.0.1', '0', 1390252341, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0fc280920886361614742ddb1a8e75f9', '127.0.0.1', '0', 1390256004, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0fc832365131e5ff64fa2453425866e3', '127.0.0.1', '0', 1390256116, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0fda4cc366854d52dc00db2689a677f0', '127.0.0.1', '0', 1390253270, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0fdb5c6edabff9a36aeaefd63bdb9f94', '127.0.0.1', '0', 1390252227, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('0fde3946d4f35e2ecb696a64551b02db', '127.0.0.1', '0', 1390252903, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1018d7d6d93d32fd7156d47276f33f41', '127.0.0.1', '0', 1390253686, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('104ab8e21a4b8d27b5b71f29c8da6b7d', '127.0.0.1', '0', 1390252773, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1054eeb010b54b59daed0bbcd65119a3', '127.0.0.1', '0', 1390253258, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('108aed26c357e040e40ad24595d4ae65', '127.0.0.1', '0', 1390252892, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('10bf8f4d2b4e02978cc8546c0e223d72', '127.0.0.1', '0', 1390254326, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('10ce2f15ff753604c31385321ad360ac', '127.0.0.1', '0', 1390254359, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('10e8e513b1e526d2fed1799596bb0717', '127.0.0.1', '0', 1390253264, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('114f40299735e9421e423bddb3d7b48f', '127.0.0.1', '0', 1390252259, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1161c68963d7760727dd550296095aaf', '127.0.0.1', '0', 1390253963, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('118aeeaf3f09db15cb865a7f3057cf4c', '127.0.0.1', '0', 1390253978, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1198213e0b56c253629da2a02349b381', '127.0.0.1', '0', 1390256003, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('11b4700237440144f0d690deb1bcedfe', '127.0.0.1', '0', 1390254238, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('11f14770f968404603161de41cd694cf', '127.0.0.1', '0', 1390252222, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1204b4913829576652a39f6ac635f204', '127.0.0.1', '0', 1390253976, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('120f8ce730442f5bfaec0aa07aabe62c', '127.0.0.1', '0', 1390256115, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1229556e75d312b1b47cba34c1514488', '127.0.0.1', '0', 1390252903, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('122de8fbc8b5831b7ea5fecd428083b0', '127.0.0.1', '0', 1390252224, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('126805cd8e069b5ee2af7ae9d97a6a4d', '127.0.0.1', '0', 1390254221, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1275d4b694bc6ace79f54c6139b55d9c', '127.0.0.1', '0', 1390256111, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('12c794c9b7558314a603fd21f80bd4e9', '127.0.0.1', '0', 1390249430, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('12e8f2762678daeea34db6c27d3bfb6e', '127.0.0.1', '0', 1390249120, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1314fc17d46f20f7a669910ed9a52c24', '127.0.0.1', '0', 1390253691, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('13c25a489a1a28fde604c84109b046ee', '127.0.0.1', '0', 1390249430, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1436f2f5f7141a73116f54ed96c08dcf', '127.0.0.1', '0', 1390256115, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('144496fd4781e2db3b0e0840120b6511', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390852964, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:22:"juan.cantor@imagina.co";s:2:"id";s:2:"15";s:7:"user_id";s:2:"15";s:16:"current_user_one";b:1;}'),
('14694dfdf1224ac0b30480fd4bc8db49', '127.0.0.1', '0', 1390253381, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('14911bd070ad4fa1298f35412492e844', '127.0.0.1', '0', 1390253961, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('14c184ea3e9d4155884b9b500928e6b2', '127.0.0.1', '0', 1390254371, ''),
('14c4b72cb7d21254989d18eb0654bc25', '127.0.0.1', '0', 1390252216, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('14c585b803a279e868ef8be255feb292', '127.0.0.1', '0', 1390252776, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('150e7d5533709e0f8f431eaa448c468b', '127.0.0.1', '0', 1390252776, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('15503ca0e461771166fcc21dc9dc3798', '127.0.0.1', '0', 1390254372, ''),
('157e80e9b15564739a469c84053ecb2a', '127.0.0.1', '0', 1390252222, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1602eec5b91e1690bd78a7ecd5856188', '127.0.0.1', '0', 1390256114, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('161705b96640ffa25fb248e656fadc28', '127.0.0.1', '0', 1390252339, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('161d4ac111fdd9df5cd8442c45e389ac', '127.0.0.1', '0', 1390252891, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('163e0ca53d686eea3888fb941dae1829', '127.0.0.1', '0', 1390252243, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1641b55dc512459584f148cc1ca41c7f', '127.0.0.1', '0', 1390252900, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('168988f48b2b42708547e20680a940a2', '127.0.0.1', '0', 1390252245, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('16ae2a39ec8eda3e2eafdccd40cd282c', '127.0.0.1', '0', 1390256119, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('16bde57694daf77138b1f6851fd48151', '127.0.0.1', '0', 1390256233, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('16db859e2d094ad6bcde9d8c80c4af73', '127.0.0.1', '0', 1390253971, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('16fc7763c51051bd2a0b496149e92d44', '127.0.0.1', '0', 1390249237, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1735146857c806b06e78703f07b0049f', '127.0.0.1', '0', 1390253253, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1754d9f457ac9e23d8127f8e0cd57012', '127.0.0.1', '0', 1390252771, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('175b15ad40f2ff102b3fb2c27cb6bc01', '127.0.0.1', '0', 1390253681, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('175f41184ac506e12032a33c273ca6b1', '127.0.0.1', '0', 1390249556, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('178373b61c8636493363eecaefedd0b0', '127.0.0.1', '0', 1390252892, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('17b1d4cd7accb4ebaaf099c50fc017e7', '127.0.0.1', '0', 1390252238, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('17c52baca27e45b7f0a30673cc44811b', '127.0.0.1', '0', 1390253690, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('181ecb0088901305d22cb1c9aa084e59', '127.0.0.1', '0', 1390252246, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('183e6065a70add32849c92683bca42bd', '127.0.0.1', '0', 1390249426, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1850a64966f48d153b850d25e9de2c9f', '127.0.0.1', '0', 1390252775, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('18760f6c4f62c21607fef0dcea881fd6', '127.0.0.1', '0', 1390249077, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1903031848f8e6398c6720db6c096d91', '127.0.0.1', '0', 1390252899, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('19125d4066aa45558551b19fc5662853', '127.0.0.1', '0', 1390252771, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('191eb67071a934f06192a885a082c716', '127.0.0.1', '0', 1390253692, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('196474af503250349f5ba4e9a560f448', '127.0.0.1', '0', 1390252239, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('19c0981c633d1987f745bbb9250447a9', '127.0.0.1', '0', 1390253690, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('19c4029709cdacdc5081af0c623e6178', '127.0.0.1', '0', 1390253684, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('19d3bd1c274f040da0503bbfd7cfe6a9', '127.0.0.1', '0', 1390256234, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('19e96460c149d3aab5934af6a539b4ba', '127.0.0.1', '0', 1390252776, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1a0de85a322081d0bc64c30a3e06238c', '127.0.0.1', '0', 1390253699, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1a161f38b74a8c807ef724f622ff02b6', '127.0.0.1', '0', 1390256114, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1a17f621a1370395d274db00b40348fc', '127.0.0.1', '0', 1390249367, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1a2dc9c47c8d5ee0a719fcfe6c752d07', '127.0.0.1', '0', 1390252898, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1a349ae6c24585e426f2fa9ea679ee5e', '127.0.0.1', '0', 1390252778, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1a5e20720802b8506695423a69671323', '127.0.0.1', '0', 1390253697, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1a9b3173c972bc2fc838941de9a04772', '127.0.0.1', '0', 1390252775, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1a9f1d5274748720f97733ee1088706a', '127.0.0.1', '0', 1390252336, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1ad1181d3b2e5ad56aa2e3d659aa26b3', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1ad74cb6c8b950f31c4516ad500d35ad', '127.0.0.1', '0', 1390256899, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1b14d9e0d063a8321eace7cc5360920f', '127.0.0.1', '0', 1390252349, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1b723a9ae2987a018f4c50f88ab4d2a2', '127.0.0.1', '0', 1390249081, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1b791590d3fb8c48391f6301b8e183a2', '127.0.0.1', '0', 1390253982, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1ba64c2fa67a4c982970bf7216521cef', '127.0.0.1', '0', 1390251992, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1bb2cbbc2697798062b79f6a0d3861e7', '127.0.0.1', '0', 1390252773, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1bdedbb58504e88e08b02274453557b4', '127.0.0.1', '0', 1390253966, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1bfedf8e2de34bfa46b5327b2d74ef02', '127.0.0.1', '0', 1390252777, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1c38b2cf02e12011c19b0729f1d62618', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1391038147, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1c4b984af787eba6add728fded4694ec', '127.0.0.1', '0', 1390252338, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1d30a38b908ebc2fa069368f7ceefe42', '127.0.0.1', '0', 1390253255, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1d316c4d52d158834454a8dc817a592f', '127.0.0.1', '0', 1390256111, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1d57fe01e49b30fbd6db6caaa4191d58', '127.0.0.1', '0', 1390256118, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1d65318246d56d49739191050a09440f', '127.0.0.1', '0', 1390253968, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1d6d5f8c2e325fc1d93e3647651ca37e', '127.0.0.1', '0', 1390254223, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1db5a09b633a9b58cc51bd049581b309', '127.0.0.1', '0', 1390254323, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1dc417527d0356a4158d28cb1db15833', '127.0.0.1', '0', 1390253688, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1dd3e7860ff0a3af75cabd92e53bba64', '127.0.0.1', '0', 1390252223, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1e0f206f37e4455cd9d00a1927b83a62', '127.0.0.1', '0', 1390256418, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1e86cc28ca8a13b590cf9eed427aff87', '127.0.0.1', '0', 1390256227, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1e9dca218c460ce4a6dcdbd95d725b48', '127.0.0.1', '0', 1390249119, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1ea5ee3ce64892fd289ee694089fb84a', '127.0.0.1', '0', 1390256230, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1f03ba4df28722d4f307bc843f21dca7', '127.0.0.1', '0', 1390252774, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1f2e4b3e022c7f22b2b33798f42d672c', '127.0.0.1', '0', 1390250295, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1f497f1498f42aa0797109af738418f6', '127.0.0.1', '0', 1390252899, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1f6c6d8337f364bd0134e97e4e12b68f', '127.0.0.1', '0', 1390254366, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1f812d4eaf6171f870f99d2843291d5e', '127.0.0.1', '0', 1390256122, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1f830bb5fdd5694aaf01837842748790', '127.0.0.1', '0', 1390253387, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1f8ea771c7edb20d371740bcaef51088', '127.0.0.1', '0', 1390253272, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('1fd0c3b52c3c147f4ebd5c2ed81f8bf4', '127.0.0.1', '0', 1390248431, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('200314ed35c3d788366169cd639bcca8', '127.0.0.1', '0', 1390252348, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('205e7ccb922ce46c9525af9b4a22ecbf', '127.0.0.1', '0', 1390252896, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('20699472c4f3cf06acac18640b1cd34b', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('20811ce9935d2d80d85f0772219ab941', '127.0.0.1', '0', 1390252337, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('20c64d340647f7657f8406326b75713b', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390253249, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:16:"current_user_one";b:1;}'),
('20d0182bc69409d1bb15da0742b2206d', '127.0.0.1', '0', 1390252776, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('214823eda07879cc9403d1551bf458f1', '127.0.0.1', '0', 1390253967, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('214d48c567d093de8e68b7d1597cefb9', '127.0.0.1', '0', 1390252226, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2159d11e67bd27d094d05f6b9f296cac', '127.0.0.1', '0', 1390253274, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('217fa66d0c54c99c7994b1a565a5d8b4', '127.0.0.1', '0', 1390253693, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2193b289eab4f6e8589759c6f3dcbc8f', '127.0.0.1', '0', 1390256115, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('21a2ff475e9d1f9aec1eb14cc65b3f2b', '127.0.0.1', '0', 1390253272, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('21ca96d05d15f36bb6715529774ccdfa', '127.0.0.1', '0', 1390253978, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('21eb0fff39860eaec9a116f2b741b242', '127.0.0.1', '0', 1390252217, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('22215e1a3cf2023678bbab73ec5b8e97', '127.0.0.1', '0', 1390253379, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('223f872ca461c700796cb0b4385946f6', '127.0.0.1', '0', 1390249081, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2243efceee9da4ad5e5d9cbb0a54301c', '127.0.0.1', '0', 1390252898, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('224a05c037aabe0c2af8256dfd55c8d7', '127.0.0.1', '0', 1390256120, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('22d787e1967b0c57f510ae5528d417ef', '127.0.0.1', '0', 1390252335, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('22eae48493cb699c1f4faf3bae0f6074', '127.0.0.1', '0', 1390254368, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('22f2c911ec95cb3841c0d3c8f6b9aed3', '127.0.0.1', '0', 1390256114, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('232ea95770958b4bfbc85af5267a20c8', '127.0.0.1', '0', 1390252224, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('237183e0789be697d9bf8f32ad1c2839', '127.0.0.1', '0', 1390254368, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('23ca7241038d6ca85d2a077698f0ada2', '127.0.0.1', '0', 1390253261, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('241b4ca9ca2222209d0526e9b1a52803', '127.0.0.1', '0', 1390249369, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('24215034579f1fc1e527a9999e8dabdc', '127.0.0.1', '0', 1390254367, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('242dafa4e30497d22de0143a95548f21', '127.0.0.1', '0', 1390256417, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('242e74b1334382e8f5ab5026e291fdc8', '127.0.0.1', '0', 1390252896, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2435e0ad9e323242eaa0a45b6310bfb3', '127.0.0.1', '0', 1390254222, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2474f6b18b9a3fc9ebbeee871de80586', '127.0.0.1', '0', 1390254368, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2482ec635960c0f14f69ceabbc4e3fbe', '127.0.0.1', '0', 1390254322, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('24ab4040dca187a38d8d04ff0a61eacd', '127.0.0.1', '0', 1390251424, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('24aef92aedeb01754951152549d822df', '127.0.0.1', '0', 1390249077, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('24c079cc27452a95ace8fe38931b3128', '127.0.0.1', '0', 1390252902, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('24e84e5687e3e67f85ae000137c6c995', '127.0.0.1', '0', 1390253983, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('24e9a69632e47780083fb3ed37404ed6', '127.0.0.1', '0', 1390252222, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2517e5dfa561fd85d0bcd8f3e819215b', '127.0.0.1', '0', 1390254239, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('251875286165016fe71a0c6bd7a610c4', '127.0.0.1', '0', 1390254369, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('251f5bbf37ec648925e7eafb092058f3', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390852964, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:22:"juan.cantor@imagina.co";s:2:"id";s:2:"15";s:7:"user_id";s:2:"15";s:16:"current_user_one";b:1;}'),
('252a7877236e2736caa696d2b36659cd', '127.0.0.1', '0', 1390254270, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('25664883ba9282d6d44396be69723cf7', '127.0.0.1', '0', 1390253982, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('257cff7b0654bd1ea99aa17b8dd66152', '127.0.0.1', '0', 1390253969, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2587fdd99d9955187644e9f671bc34d5', '127.0.0.1', '0', 1390256118, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('25a0855f2e5d367eee9c19e0c57a7531', '127.0.0.1', 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)', 1390840276, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('25e36a0461bc8f4ce78a619ca6b36675', '127.0.0.1', '0', 1390255954, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('25f1cad813eee3efa2661e15330911ed', '127.0.0.1', '0', 1390256399, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('260dc8d990eb035cf71aa05df7b7cd3f', '127.0.0.1', '0', 1390254220, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('267d533debf0c81d46fb0be1ee307baa', '127.0.0.1', '0', 1390253263, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('26c58089bab5608488c158c4d2f9358b', '127.0.0.1', '0', 1390249122, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('26c62fe1e9580098f3478c104fb5ccb7', '127.0.0.1', '0', 1390249117, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('26f383ec75b991426ba5c723ac2855fb', '127.0.0.1', '0', 1390252889, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2714d658955009a9e7890719048d9796', '127.0.0.1', '0', 1390253258, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('276366ec710315226f4c16b370d6e9c9', '127.0.0.1', '0', 1390253969, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('27636f08790f118f02d1b1bb862ea537', '127.0.0.1', '0', 1390252336, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('27884456aa96074858e5f67864415a92', '127.0.0.1', '0', 1390251168, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('27d8b4406f4879950442b3a9a05de3ac', '127.0.0.1', '0', 1390256114, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2809e1295e5a9c3583618878bb2c858b', '127.0.0.1', '0', 1390253683, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('28339e69eeed8f07aacec69153581793', '127.0.0.1', '0', 1390252345, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2845128e023178c81048d98ae6727c95', '127.0.0.1', '0', 1390249117, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('28732be2ce463779649045e26bbe5d73', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390851600, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:22:"juan.cantor@imagina.co";s:2:"id";s:2:"15";s:7:"user_id";s:2:"15";s:16:"current_user_one";b:1;}'),
('28782c4d57cee54ae0e890ab6a959479', '127.0.0.1', '0', 1390254269, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('28a5ed9a7d755164d0bc47596d97e140', '127.0.0.1', '0', 1390253700, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2914c48f5e81d8d76b7c05e71223d9ac', '127.0.0.1', '0', 1390253975, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('292c6d8d1d6d5cc6c655cb1708401d04', '127.0.0.1', '0', 1390256113, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('296cb4156a8a275403bfd88cf8467767', '127.0.0.1', '0', 1390250296, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('297dd627959de32872dc854bd99b3147', '127.0.0.1', '0', 1390252900, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2983a984db06a8efcb55f39ed75e6454', '127.0.0.1', '0', 1390255952, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2a0a31486b707b49ee30b1c21765fb61', '127.0.0.1', '0', 1390254224, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2a421ee01e7059995649415e9fe5e4a8', '127.0.0.1', '0', 1390251988, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2a74182857a750c9f8a5317bce29292d', '127.0.0.1', '0', 1390248428, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2a787091f7da4a7ff044a358de44626c', '127.0.0.1', '0', 1390256121, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2a8a75cd7502526577d166f0eaec7201', '127.0.0.1', '0', 1390253974, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2ab15c32d74ff7c4b0236922d3030ba8', '127.0.0.1', '0', 1390252215, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2b0ea0aa414f4c645d619ba9ae624e1f', '127.0.0.1', '0', 1390252353, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2b0f9b957bb31be7d26a12008215d800', '127.0.0.1', '0', 1390252214, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2b53f1a05fe0ca02f80c2a4c8cea52e0', '127.0.0.1', '0', 1390253971, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2bc7116e5dc9a92ffa7a57bef65ab787', '127.0.0.1', '0', 1390252901, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2bccf6a9179080d352bdb9d7ef5b55c6', '127.0.0.1', '0', 1390253973, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2bcf533b63ae4686331f6607243cec1c', '127.0.0.1', '0', 1390253981, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2beb5073791ecb1427bab44d37ebd970', '127.0.0.1', '0', 1390256120, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2becff7ec7c822c5dda9e243fee344a4', '127.0.0.1', '0', 1390253965, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2cc289d296700e74a1a905aad83e0e97', '127.0.0.1', '0', 1390252348, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2ccdcd10dd18232afda22e7f5763d6fd', '127.0.0.1', '0', 1390254326, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2cda2b12e9619f99f7fcce3cddc81168', '127.0.0.1', '0', 1390254244, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2cde5bc967f13460934b39512b2fd3c3', '127.0.0.1', '0', 1390256419, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2d0aaccd632d145bc83e02578b0f2153', '127.0.0.1', '0', 1390252892, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2d16093b563eb81051d3caac04d2717b', '127.0.0.1', '0', 1390256400, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2d187bd724f860176b61aaf0398f5b7b', '127.0.0.1', '0', 1390252880, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2d2df0004a68462b92b08f9a9b5ea477', '127.0.0.1', '0', 1390252342, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2d4cedda19d22d75b84a1f05b268ce82', '127.0.0.1', '0', 1390256397, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2d626743102c1a04b715b155992ac251', '127.0.0.1', '0', 1390256002, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2d7f765e422a13dc8151e4fad2a8a4e3', '127.0.0.1', '0', 1390252339, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2d9435bb8b7517b921293419a05c0010', '127.0.0.1', '0', 1390252776, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2d9fdfa7c74b8691c841c4069a0b3c4a', '127.0.0.1', '0', 1390252900, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2da0afabf350a82b001097d8a09b36f3', '127.0.0.1', '0', 1390252778, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2db2249d646973bf8b30ca4045e39ee0', '127.0.0.1', '0', 1390253695, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2def32eb88210c3fa981e002c6577569', '127.0.0.1', '0', 1390254365, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2dfeaac058744464143fa0f782dfc03c', '127.0.0.1', '0', 1390252895, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2e0562b6e3340f8a04fee5c2a99de406', '127.0.0.1', '0', 1390252766, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2e09701e98ff5b8decb3cf67ed7572af', '127.0.0.1', '0', 1390252249, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2e67974fa64345ccbc82e91a16d3e18d', '127.0.0.1', '0', 1390252892, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2e71b2f1143b1fb867f4ba8b0775d552', '127.0.0.1', '0', 1390251428, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2ea778bb3330218ae35f05f2f28ee416', '127.0.0.1', '0', 1390253981, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2eb3fb76231642ece5027067d0153263', '127.0.0.1', '0', 1390253384, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2eb7f72b8863a847ed7cbd759271e971', '127.0.0.1', '0', 1390253681, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2f02f399ab3031d2cd4963fa6156ecbe', '127.0.0.1', '0', 1390256399, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2f204210095a2f54a068cc67d37289be', '127.0.0.1', '0', 1390253972, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2f21d455341a2e8cb4ddca336ad30c21', '127.0.0.1', '0', 1390253685, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2f268b1a5d46206470da0fed73e15889', '127.0.0.1', '0', 1390253966, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2f2e9e9df6e37e1c03d5952f564fe286', '127.0.0.1', '0', 1390252889, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2f35d6d5df4671361cf01d8eda952ade', '127.0.0.1', '0', 1390254370, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2fab28a7303f6e84673fe2894ab7ce29', '127.0.0.1', '0', 1390253260, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2fbe3e105df77ecd0f874eba5b4ffa41', '127.0.0.1', '0', 1390253975, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2fcc04d976cb2876ae0085b57587e962', '127.0.0.1', '0', 1390252902, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2fcc7ef21bdc82b8283e335372e26bbe', '127.0.0.1', '0', 1390252889, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2fd660ad2424dc0542c4ae12274bc4e2', '127.0.0.1', '0', 1390254222, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('2feffa2cbddff81ddb38165d1d196a72', '127.0.0.1', '0', 1390248429, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('30069c376436947484d97e5d07e62f71', '127.0.0.1', '0', 1390248356, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('301a6a6b2bf2743bb0f3cf7ed26e9851', '127.0.0.1', '0', 1390253977, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('30296d38813e34d6b02ecf5c25ca1080', '127.0.0.1', '0', 1390252217, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('304671593c5010486134f508faf2edab', '127.0.0.1', '0', 1390252897, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('307dc07affe806c75d2fb278ccba4517', '127.0.0.1', '0', 1390252770, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('30bfbea3e4db525948dfff86310e20c5', '127.0.0.1', '0', 1390249427, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('30c0d5048b13367b9d2bbdddf55b2f58', '127.0.0.1', '0', 1390256001, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('30da3b0baee4faf5a64b13e56d9b15ef', '127.0.0.1', '0', 1390253703, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3101fedec0b7245b265c35ec68f22f16', '127.0.0.1', '0', 1390254231, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('31070ec718eae37c9e19520126c413fb', '127.0.0.1', '0', 1390248356, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('312052b8e6f157d6fcc54cf5d4a62828', '127.0.0.1', '0', 1390252341, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('314e252e8917830ff4d784e69b85d23a', '127.0.0.1', '0', 1390253275, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('317e1519beeb280743100829e286c55c', '127.0.0.1', '0', 1390249427, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3185acf1298d8699381bc7bb93c347a3', '127.0.0.1', '0', 1390253691, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('318b19a4c6d21060d5a7e6753c568f4a', '127.0.0.1', '0', 1390255953, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3190c1a37cbe2f1f80a8763c2e325f7d', '127.0.0.1', '0', 1390256120, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}');
INSERT INTO `cms_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('31d066d51c3008fa671f522c3223a2d8', '127.0.0.1', '0', 1390256110, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('320443aacacad66d77b4731bdbd640ac', '127.0.0.1', '0', 1390252767, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('321e5459c6d92ea4bb35dece83c240b9', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('325c9d66ccc1722f3ef99ce03468e0a8', '127.0.0.1', '0', 1390253387, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('327ab35bd4099719e1525d846d071b9b', '127.0.0.1', '0', 1390253383, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('32c66a19b40e789a9a91f1889f49db82', '127.0.0.1', '0', 1390253970, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('32f0a48ef9f843488f50503182bb82d4', '127.0.0.1', '0', 1390256111, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('335d8eb18b9671827b02773708253288', '127.0.0.1', '0', 1390253975, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('338ba09aa4f8bbb7303fb8296e342304', '127.0.0.1', '0', 1390252772, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('33aa6d858d5cc26a55089ae1ef152425', '127.0.0.1', '0', 1390253252, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('33bf2bacb7df662ed010fc10d269577d', '127.0.0.1', '0', 1390253980, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3409a23f954225dc55f1095002ad73b4', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1391004810, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('340bc93d60d5b3640c08ccd4616ed162', '127.0.0.1', '0', 1390252896, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3417e83c534a923a0d245439c9bf5d3b', '127.0.0.1', '0', 1390253698, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3418a613ce291af0b38d3655cd2d1358', '127.0.0.1', '0', 1390249368, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('34275c32c75be01159a368d027ca6b73', '127.0.0.1', '0', 1390249079, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('348b231cb1824cf9c14c00e1402d4922', '127.0.0.1', '0', 1390254232, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('34c5d57a80441626421d861ca9c9cbb2', '127.0.0.1', '0', 1390253267, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('34cd2f8fccb39ae0289929426b4c4aab', '127.0.0.1', '0', 1390251423, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('34f494f2c215ae2c0168644a691edbcb', '127.0.0.1', '0', 1390252250, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('35071efc6a913029d541566308b1107e', '127.0.0.1', '0', 1390254269, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('352b4ed29441a72b0002950d4f0f1d78', '127.0.0.1', '0', 1390253966, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('352e11e01cae75485fedc6ee682d0948', '127.0.0.1', '0', 1390254372, ''),
('354adede808df0fced4a719e479d2308', '127.0.0.1', '0', 1390256117, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('355ef035e9b91d917f57059ce27ebe99', '127.0.0.1', '0', 1390253691, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('359341f3149b2b44dafa5014758c1087', '127.0.0.1', '0', 1390252337, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('35983e38193493e1cd012378ed3306c7', '127.0.0.1', '0', 1390255976, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('359d72f84c811890b7a61728eaca5e01', '127.0.0.1', '0', 1390255976, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('35d0a9f24c14213e91b5f9fe3b16004d', '127.0.0.1', '0', 1390256226, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('360cd412ed15d5b4b8167609d723788c', '127.0.0.1', '0', 1390252897, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('363fb5a3ac56704e7f9fc62d65e720b4', '127.0.0.1', '0', 1390249077, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('364078bcc5a34d99fc1a0e6b31353297', '127.0.0.1', '0', 1390256230, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('364365c8b8e4681e3f04ddae92fc9e52', '127.0.0.1', '0', 1390253964, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('369d1eb738d57a7e79c98eac6dba8ca3', '127.0.0.1', '0', 1390254227, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('36c7ac0f6d0faca8ac3451be2319f6f7', '127.0.0.1', '0', 1390254261, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('36cf022e85ac7f876584eb6506a1ed4d', '127.0.0.1', '0', 1390253961, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('36f8da9879ca172e5e13d42d272badb1', '127.0.0.1', '0', 1390253253, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('371228351b16a9f126aa717d5c9ff1fa', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36', 1389915982, 'a:20:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:20:"page_activity_levels";s:1:"1";s:16:"current_user_one";b:1;s:22:"page_activity_minutess";s:1:"1";s:24:"page_component_categorys";s:1:"1";s:16:"page_componentss";s:1:"1";s:22:"page_component_fieldss";s:1:"1";s:16:"page_field_types";s:1:"1";s:22:"page_diets_categoriess";s:1:"1";s:11:"page_dietss";s:1:"1";s:11:"page_mealss";s:1:"1";s:16:"page_meals_types";s:1:"1";s:14:"page_routiness";s:1:"1";s:9:"page_epss";s:1:"1";s:15:"page_users_epss";s:1:"1";s:13:"page_hobbiess";s:1:"1";s:16:"page_unity_types";s:1:"1";}'),
('37698a9d72d01cb48ae042643c0baa89', '127.0.0.1', '0', 1390252773, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('37a14fe8b1206cee929ed880b7dbd6c7', '127.0.0.1', '0', 1390252778, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('37c86e3346986136e4501fc1bfd92e48', '127.0.0.1', '0', 1390253982, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('37cf75d239dee236618a6894b423d5dc', '127.0.0.1', '0', 1390254322, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('37ddc2c9013b3165cc6d1b6650aebae4', '127.0.0.1', '0', 1390253700, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('38247bcf38b029cea71843d5e7701e58', '127.0.0.1', '0', 1390248431, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('38274378237e9c7ab9bef74c59a21ea2', '127.0.0.1', '0', 1390256114, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('384c6117648a2abf653846b75a35a908', '127.0.0.1', '0', 1390253381, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('390c38f2da7ed01b4cee2b7d0b6e70ee', '127.0.0.1', '0', 1390256003, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('396730ca203449a1031af40ddda09aeb', '127.0.0.1', '0', 1390256119, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('39eb3066e3d2030b1c0ca98ce970ac0c', '127.0.0.1', '0', 1390253972, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('39fea86ce1745f995379e12cec090157', '127.0.0.1', '0', 1390252901, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3a3a693edb7a46e3976b45f969d39200', '127.0.0.1', '0', 1390252773, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3a62cc3c355f7b8142ac52134ed5a2e4', '127.0.0.1', '0', 1390252227, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3a8d596014f198dae59bd7ffd7b4bd62', '127.0.0.1', '0', 1390252901, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3a917bc8a73753ceac5737e920342b08', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3aa12045623040123624ccb714e3b1df', '127.0.0.1', '0', 1390252225, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3aa410ef710719d815abdb54d8080cd0', '127.0.0.1', '0', 1390254324, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3aad4d18e3ed662f0354c34e62b1edd4', '127.0.0.1', '0', 1390254371, ''),
('3aae194fc763bb11851613151f5d8ea1', '127.0.0.1', '0', 1390252896, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3ad1b73ac270ceaa6353641d31e118c7', '127.0.0.1', '0', 1390252776, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3af8e068cfd8a8b1654d6fab1db82b09', '127.0.0.1', '0', 1390253384, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3b0ff71313add315bf9ce69c154a118e', '127.0.0.1', '0', 1390252345, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3b4ee4cdd93d04297bc3314b9a91213b', '127.0.0.1', '0', 1390253700, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3b78bd3c1f157ca6640b6483ed1eec37', '127.0.0.1', '0', 1390252335, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3b9890f273df972ac7b72d7a4a2e9fb9', '127.0.0.1', '0', 1390252777, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3ba86d3e20810de30e6b5e84131af576', '127.0.0.1', '0', 1390249428, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3c0be9a12f71fcf17391f6666f542055', '127.0.0.1', '0', 1390252774, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3c0fb9cb0ff53115126a082f7b130400', '127.0.0.1', '0', 1390253264, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3c1d42e314c8cb455bb32a79b47a6b53', '127.0.0.1', '0', 1390253979, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3c27c580162e92182567fad59b1e4816', '127.0.0.1', '0', 1390253255, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3c4efe57b442dd587b2def1a231ae0f7', '127.0.0.1', '0', 1390254365, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3c5ab637b3c6f8740e5e96e4998101a4', '127.0.0.1', '0', 1390253387, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3c63f99f60974f82b456aada19c114a3', '127.0.0.1', '0', 1390254224, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3c82c85b8d6e610bcf86f116ec00a7c9', '127.0.0.1', '0', 1390254326, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3ca6aad6a44434c30535dfff984c43f4', '127.0.0.1', '0', 1390254225, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3cbbaf2452bab8812af088b3acf6dc08', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3cf2991dc1bcc16bd40bf9e611f54ede', '127.0.0.1', '0', 1390256398, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3d003713e43f6365170a3d1b0b7ba04c', '127.0.0.1', '0', 1390250298, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3d08b39bcba0a7087b1a1c31594e15c4', '127.0.0.1', '0', 1390256008, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3d0d1a21e78d47df9dccbcc11cdba305', '127.0.0.1', '0', 1390252779, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3d31bef00fa17d8e4bb43d184596dd5f', '127.0.0.1', '0', 1390252351, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3d6bc5c0b26e18d884f3e5268b0b51bf', '127.0.0.1', '0', 1390256008, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3d7660891513761bc3938371b93364bb', '127.0.0.1', '0', 1390253682, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3dad3ee3216077d2f403c80ff2e805da', '127.0.0.1', '0', 1390250315, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3ddea0699fc2faf7b04d997461596e6c', '127.0.0.1', '0', 1390253689, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3e5cf00265ce430caf02cea431285dcb', '127.0.0.1', '0', 1390252900, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3e700c2ce23b7a321bd2bcd6a7e1d7b8', '127.0.0.1', '0', 1390254221, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3e7796e7d16b812036a5a00ce62751b9', '127.0.0.1', '0', 1390256112, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3e8d4ebbb8ff7761b8649616dae9a450', '127.0.0.1', '0', 1390256111, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3eac4e4141edf5306ab2b2c46cfd5cfa', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390852964, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:22:"juan.cantor@imagina.co";s:2:"id";s:2:"15";s:7:"user_id";s:2:"15";s:16:"current_user_one";b:1;}'),
('3eafbf590f2d7f9bb70a14a28b15f1db', '127.0.0.1', '0', 1390256417, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3efcea41953f5c645da7b84265a283af', '127.0.0.1', '0', 1390252777, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3f0010e2f6ba85628118b5132a4e9bfe', '127.0.0.1', '0', 1390252890, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3f0b409d022c369962466a61b5d99df1', '127.0.0.1', '0', 1390249556, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3f4a80b56f28abc0999eb3fba28f1c87', '127.0.0.1', '0', 1390252768, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3f586e4d488c83fae144d39a96798a02', '127.0.0.1', '0', 1390255951, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3f5a934d1671dc23f4f13c2836ed9e69', '127.0.0.1', '0', 1390255953, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3f708db8052b971f25dfffcd092370f5', '127.0.0.1', '0', 1390256114, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3f71068fce0d4570cf91ab56f1c5c2c9', '127.0.0.1', '0', 1390252899, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3f7284430c1e36877800b666386190fe', '127.0.0.1', '0', 1390253692, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3fc84e65f851a3525c1d961f8f82ad17', '127.0.0.1', '0', 1390256418, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('3fe4487b586124dbc45a823884edaa41', '127.0.0.1', '0', 1390252226, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4020cfb5050b60c50363f2c84cee54ff', '127.0.0.1', '0', 1390251164, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('404c2873f3137cd456718f293b0376cb', '127.0.0.1', '0', 1390252768, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('407944817a0980d154484f284cb5fdc9', '127.0.0.1', '0', 1390252233, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('40bcc96fb53aad0d163b9ee5c05a9754', '127.0.0.1', '0', 1390252774, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('40fbeaadf07456c92a29f4252307e856', '127.0.0.1', '0', 1390253700, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4108608a8dd7fb725552042d9db0819b', '127.0.0.1', '0', 1390249118, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('410bc6946311484951dd6c42c3d4c819', '127.0.0.1', '0', 1390252902, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('410f823d6b64ce51a90b3024cb320a06', '127.0.0.1', '0', 1390253693, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('411584d52e047c3c902f186efd535b84', '127.0.0.1', '0', 1390253694, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4115f82bc7322017d30bd3e3fda1f550', '127.0.0.1', '0', 1390249369, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('412ec5e55464af6e4cdd6ffa192f3101', '127.0.0.1', '0', 1390253977, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('413bf3b42703df92128ae32cced966da', '127.0.0.1', '0', 1390253686, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('41590ae9b773f7e4c1806478cbf3fb26', '127.0.0.1', '0', 1390253270, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('41736466a92502c39e788f7c574352ae', '127.0.0.1', '0', 1390252223, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('419a52d23de68c26346708541428a505', '127.0.0.1', '0', 1390252355, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('41ef76561009cbe9edbd67ab22ceeae5', '127.0.0.1', '0', 1390253965, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('420544442bcca8cedafe6e2abb04db06', '127.0.0.1', '0', 1390248427, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('421536fc575bcf275b2b94d870810a4b', '127.0.0.1', '0', 1390254225, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('423fb4afbc3d798f7aed6e0709a5f505', '127.0.0.1', '0', 1390251987, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('424457fa37473e809a41b14edfe4092a', '127.0.0.1', '0', 1390256228, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4248c13fd0a5ba6713d6e5a66dbb0fb5', '127.0.0.1', '0', 1390252248, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('42770f25e82f3020cdc753e6c77630ba', '127.0.0.1', '0', 1390253968, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('428edc61aa24e0d1cc48aa5953a66b8b', '127.0.0.1', '0', 1390256117, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('42ab6d87664e1f680b514ef8c2fa96bf', '127.0.0.1', '0', 1390252768, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('42bfff0d89e9a8c76aa9f8112e938724', '127.0.0.1', '0', 1390252895, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4326d5ee45267ad2ad5a40883f700364', '127.0.0.1', '0', 1390252779, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('433cae14cd4dca88171e6efe9ccfa40b', '127.0.0.1', '0', 1390256115, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('43ae78c447c77faf8f059ec76db0ea1e', '127.0.0.1', '0', 1390254229, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('43cc99f6a143fddcc0017cf1ea111170', '127.0.0.1', '0', 1390251163, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('440a1e8eec49b4420d84364789dc5eb9', '127.0.0.1', '0', 1390254372, ''),
('44199c30d2f8d61c95b5a6c035470f6f', '127.0.0.1', '0', 1390252774, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4434b595ad38009d96cd26012d16560c', '127.0.0.1', '0', 1390251990, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('443547cc16696c16dd2cf25537938de9', '127.0.0.1', '0', 1390252880, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('445965288d4d83a33df750a566de8410', '127.0.0.1', '0', 1390249236, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('44825daa9d89808658949b24bb7dafe5', '127.0.0.1', '0', 1390254368, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('44cb17b017cc64012ed11938fa782148', '127.0.0.1', '0', 1390252226, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('44e6dc2dd932724fa8c793b9040653d1', '127.0.0.1', '0', 1390254370, ''),
('44f070cf1eb85550b190ed6153c7d9f6', '127.0.0.1', '0', 1390254228, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('44f2c72b45616795a4452988d39050dc', '127.0.0.1', '0', 1390256123, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('44fc8a66040cf31c8085cdbe007dc349', '127.0.0.1', '0', 1390253259, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('45017d6094af54941aac3bc0187e0172', '127.0.0.1', '0', 1390252232, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('453619a15548fcdee9db7fad522b3706', '127.0.0.1', '0', 1390254240, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4559e0fc75a4deb017431c2b33868f88', '127.0.0.1', '0', 1390254321, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('455db375f272b122d9bc91bf7cafcc1a', '127.0.0.1', '0', 1390252895, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('458028304f6ebb5c9a43a3079e52e134', '127.0.0.1', '0', 1390253387, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('458a6ad682f7909972eb7b210cc38d19', '127.0.0.1', '0', 1390254223, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4591266d2bc69fa6ec10886de8bf4aad', '127.0.0.1', '0', 1390253263, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('45a3ca46e0135407db46e6a19162c7eb', '127.0.0.1', '0', 1390254221, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('45bc6144a4cd97accf81e3c2a94992f4', '127.0.0.1', '0', 1390256963, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('45c434202ec5dc36df8096d67db030f4', '127.0.0.1', '0', 1390256002, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('45d07a30041861f8e98a7b244e03e2a5', '127.0.0.1', '0', 1390252234, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('45f9282ecd4d1cd4e0bdd640e5142202', '127.0.0.1', '0', 1390253696, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('463f86c83ad21625e9680f9c02a1dfc3', '127.0.0.1', '0', 1390254327, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('467589b874f9c62aef29f5bb5821c01d', '127.0.0.1', '0', 1390252245, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('467fee4663862cdbc4e4d7187ccda9e0', '127.0.0.1', '0', 1390253971, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('46abdcf1a21c22c525dc7f06de6b25e0', '127.0.0.1', '0', 1390254368, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('46b80b32ed3a159a4a28dde4c1eff8ff', '127.0.0.1', '0', 1390252218, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('46d8955a45bb0f06804036c113de8d92', '127.0.0.1', '0', 1390254323, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('46eb08d69b924979c854fbc0d01fbb48', '127.0.0.1', '0', 1390254237, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('46fb59b16ccfe679b948edc3fbf3cc08', '127.0.0.1', '0', 1390252223, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('47026a76031248b57577d38764e57cf0', '127.0.0.1', '0', 1390252772, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4735e6aa87b761437cdd80f21cd5788c', '127.0.0.1', '0', 1390253697, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('473d2431ef87853b5c06cb0cb7f5720f', '127.0.0.1', '0', 1390256004, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4750c8da8b43ca31e44a77b14674b670', '127.0.0.1', '0', 1390252772, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4756d71b718c993e7be3dbc3f812d85e', '127.0.0.1', '0', 1390252767, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('475ae762a88396f328376c8e82ec90f6', '127.0.0.1', '0', 1390249372, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('476288a26e295dc43d167fdc707c67e7', '127.0.0.1', '0', 1390248106, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('476fd55102f58e6826eef4307de9cad9', '127.0.0.1', '0', 1390250315, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('47713148d818889e23df5f3ac18bbeeb', '127.0.0.1', '0', 1390252232, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('477133ee9f709b22843058b6eaf18595', '127.0.0.1', '0', 1390253980, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4771e6d93ca2e1efcd6a1982ab4f5aa8', '127.0.0.1', '0', 1390248431, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('47a90c0be722aa6e1fa87e8ab9dcfa0e', '127.0.0.1', '0', 1390253976, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('47ac294d2c24ca3ceaf306da18cf40f8', '127.0.0.1', '0', 1390252770, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('47cb5729253c9d9f979990481308fedf', '127.0.0.1', '0', 1390251427, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('48071e09207d536214a2d3ecd8a70074', '127.0.0.1', '0', 1390254272, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('48109f340fa1d14645d42daf81c8f19e', '127.0.0.1', '0', 1390254226, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4811ff4eb9f04dc5d01cea98a1563ae0', '127.0.0.1', '0', 1390252336, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('481d21f24d05c24c0f4f0bf7fcad8040', '127.0.0.1', '0', 1390251168, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4823546febd3cdca42a15b508c9532be', '127.0.0.1', '0', 1390256901, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4849ffb1a602e48c7ef02cb0b0f74b79', '127.0.0.1', '0', 1390252338, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('485083292398627ceb25c7ad4cd1abff', '127.0.0.1', '0', 1390256116, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('488dc6fa219e45439cc4a0d5d3175e65', '127.0.0.1', '0', 1390252773, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('48a26aa9800e16c4d555daa41323385a', '127.0.0.1', '0', 1390249239, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('48b8db8d2bade630f71c9d447059bdf3', '127.0.0.1', '0', 1390252345, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('48e986d6aefba93a62c7ba3329f7c5f6', '127.0.0.1', '0', 1390254235, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4910083d4b3061c3a994a4e7d42dec11', '127.0.0.1', '0', 1390252900, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('49162685390edb885a5aefd506d4e018', '127.0.0.1', '0', 1390252767, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('492f465d8f551a36de9f45f494410dcf', '127.0.0.1', '0', 1390252894, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4965d2786bd5ec59eb040d24b1edef8e', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390254220, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:16:"current_user_one";b:1;}'),
('498b182dce83c4d5874493d1a89fa610', '127.0.0.1', '0', 1390256398, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4990547d3ccdda82d34a9847a11acc30', '127.0.0.1', '0', 1390254371, ''),
('49c102b0c9b16ad1099b0f81b4d42cdf', '127.0.0.1', '0', 1390256121, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('49d96a284d293e84614f4912286b920a', '127.0.0.1', '0', 1390250296, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('49f72d4ca2a899eabc0d26bc5abb152d', '127.0.0.1', '0', 1390253981, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4a09548f965c51b7b62cd24cc3ed82b9', '127.0.0.1', '0', 1390252259, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4a25d7b261dd31dffe49abc6ca3005cb', '127.0.0.1', '0', 1390256123, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4a377dd3c715ac5dccbec803291ed196', '127.0.0.1', '0', 1390256121, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4a4ad63b66b68e5a6e954f51fa3f621a', '127.0.0.1', '0', 1390256113, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4a4d06571c7d421065eb251aa1975966', '127.0.0.1', '0', 1390256112, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4a4ff4e1734ce64901f4de546e380c91', '127.0.0.1', '0', 1390252772, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4a5a7e79bdbfe61fc1618db2863d710f', '127.0.0.1', '0', 1390253970, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4a5d13305d13e46a54638f8878bd2dcb', '127.0.0.1', '0', 1390253383, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4a61de273b90ec349afabf66ff46a898', '127.0.0.1', '0', 1390253264, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4afecff3f7686fc705f656baa6db95d5', '127.0.0.1', '0', 1390256118, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4b1bfcbaa045dea4529d96c08aa67261', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390250290, 'a:22:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:14:"page_routiness";s:1:"1";s:16:"current_user_one";b:1;s:15:"page_frontmenus";s:1:"1";s:21:"page_frontmenu_itemss";s:1:"1";s:16:"page_unity_types";s:1:"1";s:14:"page_no_unitss";s:1:"1";s:17:"page_periodicitys";s:1:"1";s:23:"page_medical_formulates";s:1:"1";s:19:"page_opening_hourss";s:1:"1";s:25:"page_users_opening_hourss";s:1:"1";s:23:"page_reported_activitys";s:1:"1";s:17:"page_specialtiess";s:1:"1";s:23:"page_users_specialtiess";s:1:"1";s:25:"page_routines_categoriess";s:1:"1";s:12:"page_stagess";s:1:"1";s:14:"page_workoutss";s:1:"1";s:11:"page_weekss";s:1:"1";s:12:"page_phasess";s:1:"1";}'),
('4b21f1259a4e7190d5d1785db55c3337', '127.0.0.1', '0', 1390251992, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4b32c291962b7fcc328239a438947129', '127.0.0.1', '0', 1390253982, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4b4b993992f995d0e13ab12eb82b839c', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390254220, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:16:"current_user_one";b:1;}'),
('4b52cd46990a3544498cdf3424981f62', '127.0.0.1', '0', 1390252774, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4b575b1d94843e24ad37ab0f82f64829', '127.0.0.1', '0', 1390256232, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4bbb333fca1e13f1932d863bc87e1295', '127.0.0.1', '0', 1390253268, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4bc5b60f380727fa9adbb973868297e1', '127.0.0.1', '0', 1390256120, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4beaf260f291acd1dd101ba6abba103f', '127.0.0.1', '0', 1390253971, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4bf2275281087e12bb52922f9b0b79f6', '127.0.0.1', '0', 1390252770, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4bf9efe83f3b06b5c8498ee1017727fb', '127.0.0.1', '0', 1390253703, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4c046ab5da9fa405038736b8a72d2ef6', '127.0.0.1', '0', 1390252348, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4c474ba335a7cbf7166dccb4553e45fa', '127.0.0.1', '0', 1390252340, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4c87833cef0bef86cc183dfd3d85c663', '127.0.0.1', '0', 1390252355, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4ca51ddaa6d9f0e42ae875ceeb5f0f91', '127.0.0.1', '0', 1390253684, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4cc8de9f17205fbcdd597ef9867f5b7c', '127.0.0.1', '0', 1390253252, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4cd5d938b6c75f25983dbcd4ecd360f0', '127.0.0.1', '0', 1390256119, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4ce42ffa2ee53f677708eb3089ba7efe', '127.0.0.1', '0', 1390249557, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4cec9bac4e845480944a7722349b5050', '127.0.0.1', '0', 1390253973, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4cfd207b2aaaa4d268ffab295a0bd66d', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4d0756a79c450c249d8f73e2c3b28fc1', '127.0.0.1', '0', 1390254324, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4d0bb04158e874f64eda4ed464f0f82b', '127.0.0.1', '0', 1390252253, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4d251dd55299c21afba7f8262d66578b', '127.0.0.1', '0', 1390253384, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4d38c49d5452f12656ad4de24140d371', '127.0.0.1', '0', 1390252894, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4d56761be31f1f50327c41fbc54a092e', '127.0.0.1', '0', 1390252772, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4d79fc04c4fe6172548e945f445ce495', '127.0.0.1', '0', 1390256122, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4d8090ce9ac058b55283c8883414fe99', '127.0.0.1', '0', 1390256113, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4d8203c69d776b3bd34066afc9480150', '127.0.0.1', '0', 1390254366, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4da7776cdb76d3b9ed061efe1aae9aff', '127.0.0.1', '0', 1390253380, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4df432c47502e3557910d6a367b36839', '127.0.0.1', '0', 1390253702, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4dfde27624b06150546a968b97b4ea10', '127.0.0.1', '0', 1390256009, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4e18491978c3e23f5a4febf751ffc83b', '127.0.0.1', '0', 1390252345, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4e24e65050e49b73ba78287c4fc56b8d', '127.0.0.1', '0', 1390252887, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4e2bf6123d5170866fddb0cb5f5b5c5d', '127.0.0.1', '0', 1390253262, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4e2e4092d4043d6fc244491abc11b97d', '127.0.0.1', '0', 1390253967, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4eab3bf2e40a755affb1f3e49dcc1e29', '127.0.0.1', '0', 1390255975, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4eb3966bec702c05e346dcf70726534f', '127.0.0.1', '0', 1390253681, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4eeddbcedbc30e4d60376d918f2066f5', '127.0.0.1', '0', 1390255977, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4effcd2aeea09d16b9d8f468e8cbf282', '127.0.0.1', '0', 1390252776, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4f2d7baa138aff963c6d857249507dce', '127.0.0.1', '0', 1390252773, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4f3ddff10223283f48fb3e0758a88c6f', '127.0.0.1', '0', 1390253384, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('4f86a2b7e021e0331d2e6a567a74c1b9', '127.0.0.1', '0', 1390249116, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('501964505e67ea8d2c5ae01d4eef1fd9', '127.0.0.1', '0', 1390256006, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('504cd66399ac6677a92b7b9b4c66a910', '127.0.0.1', '0', 1390255976, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5099b97badb7248ece75322cd4ca0486', '127.0.0.1', '0', 1390256113, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('50b668371ad6482f2251dd1b49ff88c4', '127.0.0.1', '0', 1390253380, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('50e57dff5dfe240c53e784a5e60566f9', '127.0.0.1', '0', 1390253962, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('510f98e5a9a1db3b2dec9736eb88f00e', '127.0.0.1', '0', 1390252337, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5112a7f19e97bba74358ec97223982ed', '127.0.0.1', '0', 1390252221, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5125420b38df905f932a6cdb677e38e5', '127.0.0.1', '0', 1390252240, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5174fa1dfb4cd571ccfced93b83ed215', '127.0.0.1', '0', 1390254325, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('517b992a010c646378f63605f0d8c526', '127.0.0.1', '0', 1390252890, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('51938fec3794de94d6e0bb618c6f650c', '127.0.0.1', '0', 1390248427, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('51aa08aa20e06bca74e2b9f91a8403ab', '127.0.0.1', '0', 1390253695, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('51bb4e34505932db4eb41e1ddf279c41', '127.0.0.1', '0', 1390254370, ''),
('51c1e53eee7ac190902b178b5dbd9ccf', '127.0.0.1', '0', 1390253690, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('52092f8932d1358134d7cc76248ce44c', '127.0.0.1', '0', 1390255954, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('52202ba45e8f4aae35803a94302fe556', '127.0.0.1', '0', 1390252899, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('523f9a56144f33342c900f70f7c8277b', '127.0.0.1', '0', 1390251987, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5299d2fbd7e90ccbfd5769998c1a849e', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', 1392724921, 'a:7:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;s:8:"identity";s:22:"juan.cantor@imagina.co";s:8:"username";s:20:"0.330176001391613110";s:5:"email";s:22:"juan.cantor@imagina.co";s:7:"user_id";s:2:"21";s:14:"old_last_login";s:4:"2014";}'),
('52a2c3a6ae90b262f24b84193caeca23', '127.0.0.1', '0', 1390256003, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('52b04b3a2c65f335fd33dcb0ddf4ec2b', '127.0.0.1', '0', 1390252771, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('52bb1697a4b3709134cff3eb092c47d3', '127.0.0.1', '0', 1390253383, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('531b9eb92f5bd5b189cc9e863473485e', '127.0.0.1', '0', 1390252258, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('534c9c176c56f0a2c1ff2a8954640b7c', '127.0.0.1', '0', 1390256230, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5357e98b297ef765b02523e7b9457647', '127.0.0.1', '0', 1390253270, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('535a154b7f08a0de058e7b5cbbf08604', '127.0.0.1', '0', 1390249119, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('53839e5c2b04fa41f1cdb99735461093', '127.0.0.1', '0', 1390249120, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('538f732def66c7202967e82a7689b2ec', '127.0.0.1', '0', 1390252770, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('53a0fac7095306f6ce92d12bf3edda79', '127.0.0.1', '0', 1390254359, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('53d42e6d9dab694c97e9cbef610c3afc', '127.0.0.1', '0', 1390254369, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('53e9c4b087bd93913566cc0086f1d157', '127.0.0.1', '0', 1390249116, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('53f336089ed17ac36f405dfeb0bb74d7', '127.0.0.1', '0', 1390256901, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('541430fffd39dd22c4319abdd27198fe', '127.0.0.1', '0', 1390250293, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('542218f5137b0978939f717513e9c678', '127.0.0.1', '0', 1390254246, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('546ed54ea74c4ce4751afff3b2fc0f70', '127.0.0.1', '0', 1390253382, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('54bfaac695002c7db6e8bbe32ffda434', '127.0.0.1', '0', 1390252901, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('54e697f811958a729db076201b0aca9c', '127.0.0.1', '0', 1390256398, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('54ee903dd463468bcac859f73751da22', '127.0.0.1', '0', 1390256007, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('550036431d32b434f6d1b7da5deffd4b', '127.0.0.1', '0', 1390249076, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('550acdc52b6bbc677fefe7a7c39bb842', '127.0.0.1', '0', 1390251984, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('551e5614c2b19376b01db021e8d11874', '127.0.0.1', '0', 1390252772, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('55204a0be08776724adea2609f6ddd29', '127.0.0.1', '0', 1390253689, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5536b6c76eb3c3452a19184bc468322a', '127.0.0.1', '0', 1390254245, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5550121781a4d22de360e3b0f5c12a54', '127.0.0.1', '0', 1390252771, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5559c6618fdc1164eec1579b76a31180', '127.0.0.1', '0', 1390252344, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('556631f3e18282c9486c410f3d4ce8e7', '127.0.0.1', '0', 1390250317, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('559382c4b560e7b3106850f6d5a4e1ba', '127.0.0.1', '0', 1390252900, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('55a626c0b7a421a63d47b2de8109b5c4', '127.0.0.1', '0', 1390249429, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5601f7e5b0001a686f4589a04f5837ce', '127.0.0.1', '0', 1390254365, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('563b6d35a479ea4a45c9f65dc0131601', '127.0.0.1', '0', 1390253976, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('566c8f9a5d900ace23eb298e741a8cef', '127.0.0.1', '0', 1390256229, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5675ab9555b7069aac5691e46db75af6', '127.0.0.1', '0', 1390253253, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('56aff33682adafe6dad2e55467c3ee24', '127.0.0.1', '0', 1390256901, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('56b5b25c7cb70b776c1d5d741b69f3fc', '127.0.0.1', '0', 1390253385, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('56d58a62cd09ab617c7c2ccfd2d89592', '127.0.0.1', '0', 1390253698, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('56ef9e69f6a5346b62ebea911737b21f', '127.0.0.1', '0', 1390253972, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('56f3783db1da44982bacec2ab617b4c0', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('57119962d2ce5eaf58cfa0b1cb161e01', '127.0.0.1', '0', 1390256119, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5729f8162adce8acd0927d38c99a07e1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390598002, ''),
('5740b8758122d7e5ea13a8f62bd27223', '127.0.0.1', '0', 1390253386, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5741ed55403518bd04a7b409f0a4a6ab', '127.0.0.1', '0', 1390256903, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('57473cbb1acfd8cdf687d06ba3f5d181', '127.0.0.1', '0', 1390256116, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5755f9c42d4733f9a89c9dfc48d68fe3', '127.0.0.1', '0', 1390250296, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('577acb042f9d761d2a3840050dfc0db3', '127.0.0.1', '0', 1390253269, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('578978aa93dd9beba74eabed91fb056d', '127.0.0.1', '0', 1390249556, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('57c769c951ebe27c9bd517b3b899418e', '127.0.0.1', '0', 1390253983, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('583d2a11e1b06e4a7165b56fb5ba16c0', '127.0.0.1', '0', 1390252338, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('58997e9dedd2de0e93bf529f4ddbf36c', '127.0.0.1', '0', 1390252335, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('58abeeeb6fe2c0ef69a078461202c0bf', '127.0.0.1', '0', 1390256007, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('58afd6384e860a0af684021d57ba5252', '127.0.0.1', '0', 1390253254, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('58b2c5ddaf9ad322295cd65c663cdcde', '127.0.0.1', '0', 1390253967, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('58d7499a4b15007487b7edb0457b5938', '127.0.0.1', '0', 1390255974, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('591ecd2123c1259f4961ae95409aa0ae', '127.0.0.1', '0', 1390252769, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('592aab8015c9bd3a2d682f0535b93a31', '127.0.0.1', '0', 1390252250, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('59593d15c14c88c8d68b230555188d9a', '127.0.0.1', '0', 1390254258, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('59755fdc8e46c9be6bfc8f8bb9881d90', '127.0.0.1', '0', 1390253386, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('59843d40557d1482bff39b63e77b4372', '127.0.0.1', '0', 1390256006, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('598daf5eaeadc74b4dd085f9718869cd', '127.0.0.1', '0', 1390253260, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('599b9a3e0eef9946dc20d6c022af8a83', '127.0.0.1', '0', 1390253686, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('59b9da5808e709d118b497755a2c3da3', '127.0.0.1', '0', 1390253702, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('59cec3adcfabcc395881c2e938bbe384', '127.0.0.1', '0', 1390254225, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5a27426d3ce13ad107ed5756586bfb69', '127.0.0.1', '0', 1390254367, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5ab59104887ef18bad2fa580761b5a68', '127.0.0.1', '0', 1390249119, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5acfd7ee3558a182699f85de1c27cc57', '127.0.0.1', '0', 1390251168, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5ad22c316ff53daa5c35d44047b51f73', '127.0.0.1', '0', 1390256122, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5ad2e9dfee3be6059ecec1a772fd6a36', '127.0.0.1', '0', 1390252771, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5b0fb0fec8710b4c8544305fe12cea21', '127.0.0.1', '0', 1390256399, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5b14e468a704e492d271fdc4b4f2b91e', '127.0.0.1', '0', 1390252215, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5b1c79cd045948b00a1950a7d8825100', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390953581, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5b2d4f7f7ef7bf45b06ca3db719166b2', '127.0.0.1', '0', 1390253700, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5b5d7a82531fcedb88f37c335620a33a', '127.0.0.1', '0', 1390252775, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5b5f917d026bbbd2300ad229742d6341', '127.0.0.1', '0', 1390254253, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5b897f27dbca4b61ff9d33b46c962028', '127.0.0.1', '0', 1390252228, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5bd4f85389d24d8b8a94e0ab40b70873', '127.0.0.1', '0', 1390253688, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5c01d48eb55a1c584318c631825b4a96', '127.0.0.1', '0', 1390249078, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5c18200e95124d2d7051eefdbe7d0ab3', '127.0.0.1', '0', 1390254242, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5c19821b51a940e66dd970a44e8caa50', '127.0.0.1', '0', 1390248427, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5c2954176c1a10da8ce93dcdd8356ac0', '127.0.0.1', '0', 1390254366, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5c37f606e70c9fee150af1e0d105376e', '127.0.0.1', '0', 1390253972, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5c5707373d07720ea3a248f18fc93b48', '127.0.0.1', '0', 1390253962, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5c6c414d4dda660935e6bee760efb236', '127.0.0.1', '0', 1390253272, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5c799cb44509dda661f1a2b5198a33d2', '127.0.0.1', '0', 1390253381, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5ca9233b9fc50a87f30464cb99592cce', '127.0.0.1', '0', 1390253686, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5cbc004545586775f320162d7b0640a4', '127.0.0.1', '0', 1390254240, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5d349af227fe216aaf29bc557026c1ac', '127.0.0.1', '0', 1390253700, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5d50a38c14f6d11f887175a1b537b83e', '127.0.0.1', '0', 1390253275, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5d6cad07793d35a6bd17c7dd561c6362', '127.0.0.1', '0', 1390253701, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5d868452aec7a3bfc08b1b88e1f511d9', '127.0.0.1', '0', 1390254321, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5d8f2e399898e93707f5468b0f9cc306', '127.0.0.1', '0', 1390254372, ''),
('5db25d544a25d087347b1eed889b8b23', '127.0.0.1', '0', 1390254247, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5dbaf5fe807d8ecab1c200e3ffc77a59', '127.0.0.1', '0', 1390253961, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5dcc8df563876a8924e1bad9feb8c6b1', '127.0.0.1', '0', 1390252774, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5df63c37d0c6ae10862cff10fe7cd35a', '127.0.0.1', '0', 1390252249, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5e02b3d535e85264a970b470b7d44841', '127.0.0.1', '0', 1390253685, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5e28b5749cda3bf22afe29a60bcef864', '127.0.0.1', '0', 1390252255, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5e34a6e7c671fab8d770e9a3fda1efd1', '127.0.0.1', '0', 1390254233, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5e3800851232fe367dc09c8a87174f8c', '127.0.0.1', '0', 1390249240, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5e88d5b834fdb701dc9c6b64b17e4928', '127.0.0.1', '0', 1390254322, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5eb6052b872b829dc75f3d086205d28d', '127.0.0.1', '0', 1390256009, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5ed0039a0d356ea788a3a992fdaa29c3', '127.0.0.1', '0', 1390254368, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5eda0c47a702190dc9618724388ee669', '127.0.0.1', '0', 1390253271, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5ee7bfa76a9d49c24f1abaae0250cce8', '127.0.0.1', '0', 1390256398, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5ef9a634990ab1ee7c6f20d78d02ce38', '127.0.0.1', '0', 1390253255, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5f15988213f6d7d7dc686f165cb21d0e', '127.0.0.1', '0', 1390252774, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5f206dfa8714f7197f23961858b6b7fd', '127.0.0.1', '0', 1390252339, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5f3809310b45f3db44491e983ff6dd3d', '127.0.0.1', '0', 1390253256, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5f5aae8cfb1a080e7a39e2e5d0262be6', '127.0.0.1', '0', 1390249238, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5f675ed09ef04cca94b23db5a60332ce', '127.0.0.1', '0', 1390254367, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5f897d45b4ce56a0bdc50b6489eaa687', '127.0.0.1', '0', 1390252228, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}');
INSERT INTO `cms_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('5f96256d49a090f3c8768917a76b589f', '127.0.0.1', '0', 1390252217, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5fb47f0573108a7e3b77e0dabbb74e6d', '127.0.0.1', '0', 1390250316, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5fd39d82ebe23c5448a8b7a0e0fd44ee', '127.0.0.1', '0', 1390256402, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('5fffccea514bf9a9edaefb1face23e7d', '127.0.0.1', '0', 1390256111, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('603d54a0c48d715ca066eadb866432bc', '127.0.0.1', '0', 1390254223, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6094c6edf5e739c40466a2d15c8c4b04', '127.0.0.1', '0', 1390256119, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('60c3a085c5bf0c87a9de3723e578b7aa', '127.0.0.1', '0', 1390253262, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('60e971d735e954643a9c8ea233266668', '127.0.0.1', '0', 1390255953, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('614622708968a3f501d94d9a73ab65e0', '127.0.0.1', '0', 1390252890, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('61591638198dcdd6737cc0ff2b85f4b2', '127.0.0.1', '0', 1390251166, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('617d97b08f93a315c3a2e7a879bcfe9a', '127.0.0.1', '0', 1390256005, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6183c4b75c0bf1615576e8bcdc47f750', '127.0.0.1', '0', 1390253974, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('618de3a816e302f49d4ac3d16a5a3ad6', '127.0.0.1', '0', 1390254221, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('618fdf15d782716433435821f28ae2e6', '127.0.0.1', '0', 1390253979, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('61d45b81ff909a6ff02e5bd6a3411689', '127.0.0.1', '0', 1390252254, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('61db534f6b2d55bb7805f4b6cd373725', '127.0.0.1', '0', 1390252903, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6208408d04a8c32efc543406c8f0ae6c', '127.0.0.1', '0', 1390252230, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('620dfd385cd4deb165fd25fc1d473001', '127.0.0.1', '0', 1390252354, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('620fbf305571432916af9d4d17e63acc', '127.0.0.1', '0', 1390256118, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('621d333216b62f391bd99d5f9dbf2adb', '127.0.0.1', '0', 1390254367, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('626eb53057a06ce60b39028f940aff9d', '127.0.0.1', '0', 1390254223, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('62a2e5494239ffba22fc94ba2b7de3c1', '127.0.0.1', '0', 1390256116, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('62bc176c5f2c3946b820c42e1d9a7e80', '127.0.0.1', '0', 1390249080, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('62bfe6de1de77088ab47a85f747298de', '127.0.0.1', '0', 1390250297, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('62c3054eb7d8a1db4a543f6501e7e293', '127.0.0.1', '0', 1390254367, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('62e0f3189d8f9d43546ed45331add84a', '127.0.0.1', '0', 1390256232, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('637b4365f0deb985e72ac87a97e2bd79', '127.0.0.1', '0', 1390254311, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('637b6db0f8a541a83b5d488d76bfbbec', '127.0.0.1', '0', 1390253978, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('63903d86c0746c7595e31c8b2bbff8db', '127.0.0.1', '0', 1390252338, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('63d3e2cff61f5d4e71da952df8ce93e4', '127.0.0.1', '0', 1390253687, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('63e539f73b7c3ce2826e5d73376ab7d0', '127.0.0.1', '0', 1390253265, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('63ea7852cb40575f3c8a65c0367369e5', '127.0.0.1', '0', 1390256120, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6415bcb895dd74edfad4b5210886c7dd', '127.0.0.1', '0', 1390256117, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('641b1a4bd48be4a05a1c67d3a89a8ca0', '127.0.0.1', '0', 1390252769, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('642b742964fcf058cbeab8be5cd729ad', '127.0.0.1', '0', 1390253970, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('642e9d7078a0be9e4cb5109f837f5cb5', '127.0.0.1', '0', 1390253694, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6446d577a4d5e68625f9e0de44987394', '127.0.0.1', '0', 1390253261, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('648a228080528be624c0c25fa2fce15c', '127.0.0.1', '0', 1390253263, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('649b883d747ff3a5ba4622ba57ef1cc2', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('64a5e5044ee67f834805ea22430c5965', '127.0.0.1', '0', 1390252232, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('64d4788079c2e96167f3ae2312a21173', '127.0.0.1', '0', 1390253977, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('64d890d3512ead30030f0f75a8a8b4e1', '127.0.0.1', '0', 1390252898, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('64e465d95abd0cb22903101484e91b40', '127.0.0.1', '0', 1390252778, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('64f49432bf63a737765757d5334e1210', '127.0.0.1', '0', 1390252220, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('653202ca807fe16b09e2dae9d4de19a9', '127.0.0.1', '0', 1390251426, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('658722559784f65120c27b577fb91846', '127.0.0.1', '0', 1390252251, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6587f8e63a2b9d2fb93556b9209dbb85', '127.0.0.1', '0', 1390250297, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('65b99678cccaf00f2855d34f37cfe4b2', '127.0.0.1', '0', 1390254262, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6605aa1744b9137588ec8c14ddd6c652', '127.0.0.1', '0', 1390256006, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('662268a987deec4666609e6e2dd47f27', '127.0.0.1', '0', 1390253983, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6672f20f5bf106d51e9e11247eabbdd6', '127.0.0.1', '0', 1390253694, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('66aff4b21e17725747ef6f1352fc9a5e', '127.0.0.1', '0', 1390249237, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('66c4581d8f0c740f120cbc08c51605d4', '127.0.0.1', '0', 1390252897, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('66cfe31f35bac44b10703f2796b8ecf2', '127.0.0.1', '0', 1390256120, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('672c5d11b84029fd9c1dff4c5ecf8e09', '127.0.0.1', '0', 1390252903, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('677956fd69a8ecdaf5e59a6f1385b7b9', '127.0.0.1', '0', 1390253971, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('677e115ef6f3e3c780e26b2aa5c45bdf', '127.0.0.1', '0', 1390256111, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('67b400ae3699455f05552b974aa55e1e', '127.0.0.1', '0', 1390252766, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('67b982b8dfa75b96fdbe962de1a86c0f', '127.0.0.1', '0', 1390250315, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('67c9a973f531e963e5344277a68b6127', '127.0.0.1', '0', 1390254238, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('67d43c651a30aa21ada943bbca0d46d0', '127.0.0.1', '0', 1390253980, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('680384d6529b8f03170c6d4ce3d34d3e', '127.0.0.1', '0', 1390252771, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6818604f0224182afc6384e34b257df7', '127.0.0.1', '0', 1390252899, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6831cbe1bd12a16c0dbed707144de8e6', '127.0.0.1', '0', 1390256022, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('68328b8c40064416a790aae07c02d2e1', '127.0.0.1', '0', 1390254327, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('68798b6e0f81463982234862b8cc0702', '127.0.0.1', '0', 1390252772, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('68b439e99340706a7d09fb47e7bcb1c4', '127.0.0.1', '0', 1390256123, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6909de0c264c75f846c25a823817dd10', '127.0.0.1', '0', 1390253699, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6928887f82065ec877787e4441d1fc87', '127.0.0.1', '0', 1390251166, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6946681d83602767f42287a4f3cbf4b6', '127.0.0.1', '0', 1390256122, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('694b6be0354f0145ff3c3eda97fdcb26', '127.0.0.1', '0', 1390253978, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('695c844e6e30859a03dbe6ac857ca3d6', '127.0.0.1', '0', 1390252903, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('69caae2399677b0285f62e811163e2c7', '127.0.0.1', '0', 1390254252, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('69e8ff1c13c79ba6f5dc243bc5e9ce8e', '127.0.0.1', '0', 1390253274, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('69ee58cde8403802c5203d645cddfcc8', '127.0.0.1', '0', 1390252891, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('69f393f2b7d8d647a3e82e1f7fc7875f', '127.0.0.1', '0', 1390251427, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('69fc8edb1ec962ad5379d134c59edf0d', '127.0.0.1', '0', 1390253263, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6a559ceadd900526eccd8db027f49933', '127.0.0.1', '0', 1390253973, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6a63721b238f3de6773eb3c527f2b044', '127.0.0.1', '0', 1390251985, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6a6a251cc6ac21c2e5b53e91e49e2c4c', '127.0.0.1', '0', 1390253683, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6a6cc253187c67aaaa5690fd3ac87d17', '127.0.0.1', '0', 1390256899, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6a77d7e7ca64575c252a8ce10e9494a9', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', 1392724800, 'a:8:{s:9:"user_data";s:0:"";s:8:"identity";s:18:"cms@imaginamos.com";s:8:"username";s:13:"administrator";s:5:"email";s:18:"cms@imaginamos.com";s:7:"user_id";s:1:"5";s:14:"old_last_login";s:4:"2014";s:13:"page_coachess";s:1:"1";s:24:"flash:old:alert_messages";s:121:"<script>$.sticky("Datos Guardados con exito...!", {autoclose : 5000, position: "top-center", type: "st-info" });</script>";}'),
('6a8d9d49f7cdb5182947daa63c22c4a0', '127.0.0.1', '0', 1390252778, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6a9f194f6b7feb875a768e9a126dae02', '127.0.0.1', '0', 1390256110, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6abdebf2f7bd75966ec0b775a8d006b5', '127.0.0.1', '0', 1390253701, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6ac95f4af2b4f870f92cbf832af65f61', '127.0.0.1', '0', 1390249079, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6afee361c63fe53c11d731bddeb6a024', '127.0.0.1', '0', 1390252221, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6b0f7828270e16a2df321556e51ebfca', '127.0.0.1', '0', 1390256963, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6b1ebec6508c470bb49c48712b72c3c8', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.102 Safari/537.36', 1391555510, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:16:"current_user_one";b:1;}'),
('6b20209954ac48e1ccc645f090f10af9', '127.0.0.1', '0', 1390249121, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6b558b3480a1f7cd1d4283a383dc915d', '127.0.0.1', '0', 1390252779, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6b7fcbef75136857bb028373af336fa1', '127.0.0.1', '0', 1390249430, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6bb98bd82279181dabdfce839bf729e9', '127.0.0.1', '0', 1390249241, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6bbf72b70fdad86d7016c7fc35a2fc66', '127.0.0.1', '0', 1390252897, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6c03119ff25e67b6559a7dec2df25bad', '127.0.0.1', '0', 1390253969, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6c10f2b7e554dcf8f2dee52868a28523', '127.0.0.1', '0', 1390254223, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6c45c0787121d3f07114ceef4c820685', '127.0.0.1', '0', 1390252341, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6c822236cbf965bc2fb01acb6a1e697f', '127.0.0.1', '0', 1390252775, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6c87fdfe67e2bc7065dc1b5225d08da7', '127.0.0.1', '0', 1390252775, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6c899edcd243a5b5992223616e0b06e5', '127.0.0.1', '0', 1390256005, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6caff6b57d54987f3c53c5a5073fd619', '127.0.0.1', '0', 1390252891, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6cbf564f49169987b257ffe9fb4689e2', '127.0.0.1', '0', 1390256232, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6cf0e907affbfef2189394fe5398cddd', '127.0.0.1', '0', 1390253693, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6cf8abe43caecbe93f8655012c185893', '127.0.0.1', '0', 1390252218, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6cfd27a6a64f218490903da75dbf3f50', '127.0.0.1', '0', 1390256118, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6d0c221880b8fa766759d7fcb58f8ab0', '127.0.0.1', '0', 1390253698, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6d110f37abf0d08b800bad7dd93cca50', '127.0.0.1', '0', 1390253683, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6d2709c34960a8492339353ccf6a846c', '127.0.0.1', '0', 1390254369, ''),
('6d3da9cb89bb229a6f9f74e4771509e5', '127.0.0.1', '0', 1390254222, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6d6d2a7b0c7e1d9deb9b50038d9463a4', '127.0.0.1', '0', 1390252341, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6d965a277b6c1cc52dcaa946567f7498', '127.0.0.1', '0', 1390252355, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6dc431244e806603b77114d39e6d1b95', '127.0.0.1', '0', 1390252779, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6dc6646d11c32e287d0f79623e25e180', '127.0.0.1', '0', 1390250294, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6dc8fb47ce7b97e935e6639b3f0366c8', '127.0.0.1', '0', 1390254272, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6de5b51e4d4d71dfe7e61ff1545f4c42', '127.0.0.1', '0', 1390253267, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6e0da481f9018c35c4e7dcd2c3dd64be', '127.0.0.1', '0', 1390252894, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6e1d46c189dfef11b156decf7893606a', '127.0.0.1', '0', 1390252769, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6e3e7e3689f6d9dfe37c6e23a63f63ba', '127.0.0.1', '0', 1390252900, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6e48923c6bda03060f7bb58f46fe78ee', '127.0.0.1', '0', 1390256232, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6e705fba89e6bb217f8cd245ab1d4406', '127.0.0.1', '0', 1390252216, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6e7e30f6d52f1b6a0bc960332d123130', '127.0.0.1', '0', 1390250316, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6e8062f02d1478248f146dccb4e196c4', '127.0.0.1', '0', 1390252770, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6ec472723f72456e38acfb117d3657f3', '127.0.0.1', '0', 1390252227, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6ecddf6b6ba6f183f8af8ed1be53dd28', '127.0.0.1', '0', 1390255952, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6f0a5d2bcd66b978f5489d228ac72dce', '127.0.0.1', '0', 1390248429, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6f0c5b97388adfc07f93f3fdca816ebc', '127.0.0.1', '0', 1390249556, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6f25a1f367ecf277803b290e7716b549', '127.0.0.1', '0', 1390256112, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6f29249c0cd72831f52c508c122a10f7', '127.0.0.1', '0', 1390253382, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6f3721fa2dba00963e5c511aa78252a5', '127.0.0.1', '0', 1390252227, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6f3ef39e48209be9c9c8b40af6dbbdc1', '127.0.0.1', '0', 1390253270, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6f46f1c0cb20752c8ce4929e1b6a95da', '127.0.0.1', '0', 1390253274, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6f724faf1617d7497053c46089703e1e', '127.0.0.1', '0', 1390251988, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6fbe787ad7a14d24e4b46384cf3508d5', '127.0.0.1', '0', 1390248356, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6fc9e4493807193ec1b8817ff51d0555', '127.0.0.1', '0', 1390252774, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('6fe6de8c53ff511bafb6bd067e2cac30', '127.0.0.1', '0', 1390252773, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('701918ca9be3bf53070cdb389ead846b', '127.0.0.1', '0', 1390253700, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('70345aa5ea6dc8229830b5a46f93a431', '127.0.0.1', '0', 1390251985, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7055ea3d299f2fabb25e2f386e7df9fe', '127.0.0.1', '0', 1390252219, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('705cca92a4b252ad037c6a9d96c0843e', '127.0.0.1', '0', 1390253264, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7061541e7322055092fb625dd5275771', '127.0.0.1', '0', 1390252902, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7099cdfff90dc5537997ce3aa15cc23c', '127.0.0.1', '0', 1390249557, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('70cee5f7b82b1e27d47929cd9fe86351', '127.0.0.1', '0', 1390249557, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('70da1d8384908fca7c3d2fd9566028be', '127.0.0.1', '0', 1390253979, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('70df2f1a8c34e077776aa4d0d0afd80e', '127.0.0.1', '0', 1390253965, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('70ee1ddfa54742c80824c3685a27e8f1', '127.0.0.1', '0', 1390253263, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('70f2e6c81fc67e03cfa5144132db0841', '127.0.0.1', '0', 1390254223, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('711218dea0ede740933b764a3a5a10a9', '127.0.0.1', '0', 1390254229, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('71224154c000be9dae1fdaf8823e85ab', '127.0.0.1', '0', 1390252768, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('717ff1c97a036e8c5e75c5b7803a60b9', '127.0.0.1', '0', 1390252893, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('71824d6af07897a5150e6e115941ee3b', '127.0.0.1', '0', 1390252220, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('719581f8b9bfa8e72c239d4ab15075dc', '127.0.0.1', '0', 1390253697, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('71a7c03b70000533d2c7df306be3370e', '127.0.0.1', '0', 1390253699, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('71d0d9266754d61a69e2bde353e7d3fa', '127.0.0.1', '0', 1390254368, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7259f1a342b64f7e84f7c228c11dd00f', '127.0.0.1', '0', 1390253387, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('72788a7b90ac7a676b044c61a1aaead9', '127.0.0.1', '0', 1390252898, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('72e3a98074ff5f956b20efed0afd4059', '127.0.0.1', '0', 1390253964, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7335c922afea810156b4ff500e7af2b5', '127.0.0.1', '0', 1390248426, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7393845e8b616ef8bb91da651da77009', '127.0.0.1', '0', 1390252776, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('739a551042311ae057d62db6bbcc8008', '127.0.0.1', '0', 1390252241, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('73d0da8968317226fd797ae245e4844d', '127.0.0.1', '0', 1390253270, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('73df42e81512e48c4b02b71460c8c190', '127.0.0.1', '0', 1390253983, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('742a981971a77e22360f1a81eea09385', '127.0.0.1', '0', 1390254322, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('742db19feecf46acbdc23f2314d59202', '127.0.0.1', '0', 1390254232, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('74368295c2e2c1e7236d75bc5b878ed3', '127.0.0.1', '0', 1390253964, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7438f794114a5194ed3ca1f789e59e42', '127.0.0.1', '0', 1390253974, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('744a111adad96f3114e60bac16134a2e', '127.0.0.1', '0', 1390253703, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('74507b17e4f658bd278e7111e0accded', '127.0.0.1', '0', 1390256115, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('74690df9dac3e5de56633567c3e793ab', '127.0.0.1', '0', 1390251427, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('747b3c4e754546d7d02bbd4d82ba2d8e', '127.0.0.1', '0', 1390252226, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('74a91db9c0453bb5124e13c0a7fb4e8e', '127.0.0.1', '0', 1390254369, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('74b9c49fadc4a5af31f1fe0a7c0067be', '127.0.0.1', '0', 1390254236, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('74c6d5d7524ea61ebf9b5edb0bf30ecb', '127.0.0.1', '0', 1390248356, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('74f140634e1eaccec812024f9927c5cb', '127.0.0.1', '0', 1390254237, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('756f9efd3c021f70bd38086db6b49ccc', '127.0.0.1', '0', 1390256115, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('75e1068dec51988bcc8cf56d74abe467', '127.0.0.1', '0', 1390249242, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('75ff489d9267b09a4350451f2dc6eae6', '127.0.0.1', '0', 1390253687, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('760c0f148f48c4952ad897b8044190df', '127.0.0.1', '0', 1390252900, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('76300cf8da7bf8377d0231240aa1bb7d', '127.0.0.1', '0', 1390253266, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7671c2d635b02ca43858d5efbe779a57', '127.0.0.1', '0', 1390253265, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('768bd58233ea283e55452bf9ab0eaf31', '127.0.0.1', '0', 1390253979, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('76be82b84b3a7455ac587386ec0f7679', '127.0.0.1', '0', 1390253703, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('76d4dcfe4e1a47ab5b3a41cfc27cadce', '127.0.0.1', '0', 1390254269, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('76ff222c8b081959276158741bd55ea7', '127.0.0.1', '0', 1390254359, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('772a340bf2948539ee0afa62555f1152', '127.0.0.1', '0', 1390254227, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7759e300fab788711414a0cc98f2ac9c', '127.0.0.1', '0', 1390254361, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('775cf43464b9cf3ec6bf280dd8bca6ee', '127.0.0.1', '0', 1390254369, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('77f439a4db7514ec64043bcdc6d8c273', '127.0.0.1', '0', 1390253975, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('77ffb46ab76f12d7f77a5f11cbd55d15', '127.0.0.1', '0', 1390252778, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('78314966bcac232f8ea9e4cff131f4fd', '127.0.0.1', '0', 1390251167, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('78364dba1bf8a3dfa96009b2428f8c16', '127.0.0.1', '0', 1390253386, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('783b368b77d17e4bff0b0185e49415cd', '127.0.0.1', '0', 1390253689, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7847cf723b3b05032c1f1d582f33b1c2', '127.0.0.1', '0', 1390249079, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('784f1feb43d7de1baad9570923054cda', '127.0.0.1', '0', 1390252775, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7851a52a19a272f0e8f386f24856d026', '127.0.0.1', '0', 1390253384, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7859ee60bf619240912f2507a0f68eba', '127.0.0.1', '0', 1390249078, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('789a0f8b79eefb426f6d959742daf176', '127.0.0.1', '0', 1390253687, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('78a10c1f46968752c5f1304de36e6a9e', '127.0.0.1', '0', 1390252244, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('78c58891f3b474ba6c737f8d288b3b81', '127.0.0.1', '0', 1390252335, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('78db3ba0e3d6f330a704c1adf65fe73a', '127.0.0.1', '0', 1390253980, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('78e6d68cc758402f52a068e15432f35c', '127.0.0.1', '0', 1390253251, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('792d5bf3fea46cc5827f091adf327c41', '127.0.0.1', '0', 1390256231, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7937cc640091bd745d7b790882cd484d', '127.0.0.1', '0', 1390252238, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('793e892cd48952fac7c35979d2f8e598', '127.0.0.1', '0', 1390253963, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('794a75485f2c48e322a37e9028333f05', '127.0.0.1', '0', 1390252775, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('795db89b2671eb78f6c61a2a85b1fbb1', '127.0.0.1', '0', 1390253689, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('79a6c2f50027e414d4f5933b56075ae4', '127.0.0.1', '0', 1390253687, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('79bf00c4bc11307f43fd2caca5102108', '127.0.0.1', '0', 1390251985, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('79d1b7633c313d43aec95933a8748328', '127.0.0.1', '0', 1390254234, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('79ed4a77eb33fb6801e30afbfa9c80d7', '127.0.0.1', '0', 1390253698, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7a07679b898a81fa080ecd2bb7cf52fe', '127.0.0.1', '0', 1390253697, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7a20a5013d870f9df89c9995a6f9bd89', '127.0.0.1', '0', 1390253963, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7a3f5bf3b445a976dd4c2eb6d5e41d20', '127.0.0.1', '0', 1390256401, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7a8313773d1dfa369640829ea67f109d', '127.0.0.1', '0', 1390253970, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7a8b41a3018e6374f78b2c9a82675391', '127.0.0.1', '0', 1390256006, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7aa406d9325293664091cd9be057aacc', '127.0.0.1', '0', 1390253266, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7abe795a62e4cdd42c1460223354f401', '127.0.0.1', '0', 1390249366, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7ac04149dc1285f4d6622201b2d59d88', '127.0.0.1', '0', 1390252349, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7ac6d98c93e1cc049aae614d9c7e84b7', '127.0.0.1', '0', 1390254225, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7b1673e240042c06bc842725a1f0cd77', '127.0.0.1', '0', 1390252900, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7b409e7bd5ec3f9837d0121e1fd6c480', '127.0.0.1', '0', 1390252777, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7b4b1e15e556b84e04937107e3b221c0', '127.0.0.1', '0', 1390256401, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7b71ee00907634f26804b1a3e09e3fa8', '127.0.0.1', '0', 1390249427, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7bbfcfcf604039d8b09283f137346476', '127.0.0.1', '0', 1390254367, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7bf9329117caeb6d4c9a826610992e6d', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7c312dc7c3487ecf3b94e569082d28de', '127.0.0.1', '0', 1390252346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7c43413572597360dffc08ddb2ca07c9', '127.0.0.1', '0', 1390252770, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7c4976f23ec4c33cde1448ffc9b61c6f', '127.0.0.1', '0', 1390253000, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7c4a13085f0a9b1e4cf13de7d9e3b4c4', '127.0.0.1', '0', 1390256111, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7c4e02d01d86bd3cb923e056fa31f115', '127.0.0.1', '0', 1390252769, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7c5702754696dee639964e7bed28d113', '127.0.0.1', '0', 1390254326, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7c7102d51386a04a7cc36e3577db5726', '127.0.0.1', '0', 1390248357, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7c72338878902bb47ae91b9951c32f7f', '127.0.0.1', '0', 1390252778, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7c734661db3acfc3744f9af110902a67', '127.0.0.1', '0', 1390253261, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7c83cd7e2af25e95a17ffca1c87aa045', '127.0.0.1', '0', 1390254264, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7ca2da521e6bfa99388995feb4c38abd', '127.0.0.1', '0', 1390254222, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7ca74160511ad74bf02b6a58d96dcef0', '127.0.0.1', '0', 1390254370, ''),
('7caee0bda47899616c8e4b886f5c92e7', '127.0.0.1', '0', 1390256111, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7ceb6e19bab1f6a6328c2c8e1f6ebdd1', '127.0.0.1', '0', 1390249080, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7db0a7abc0dc655d78c5d07ef6c13335', '127.0.0.1', '0', 1390252342, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7db3a36045dd0e630407c67151aeb87b', '127.0.0.1', '0', 1390252218, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7dbfca925b17458c4578336a6dcbe456', '127.0.0.1', '0', 1390254248, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7df1e055703521c67d95ea7dbf833855', '127.0.0.1', '0', 1390252214, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7dfb974a04c7759540eb5b96cefb4441', '127.0.0.1', '0', 1390252776, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7e13d7a99a2fe92e859e337793b71612', '127.0.0.1', '0', 1390253703, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7e1701bbd765fe09b1abecc7c8710e42', '127.0.0.1', '0', 1390256117, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7e31bd8258da472045e9a9d781f0a7bd', '127.0.0.1', '0', 1390252772, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7e66a5579c194d57017d0613d40f0473', '127.0.0.1', '0', 1390254224, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7e68dde5f3fe2cbcc6b9aef5baba724d', '127.0.0.1', '0', 1390253385, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7eaeb9b8a18965d9a992c49682dc69a9', '127.0.0.1', '0', 1390256900, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7ede3275d298be215194074d76e98cf8', '127.0.0.1', '0', 1390248431, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7f1a34fd622a00e5c41bbc034d974f3a', '127.0.0.1', '0', 1390256004, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7f297c225f775f8ab619f348c3dc24a8', '127.0.0.1', '0', 1390256888, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7f2f3983ee528ae2acc1d6d584f064d3', '127.0.0.1', '0', 1390253687, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7f4fe8b3531be56d707839911d46038e', '127.0.0.1', '0', 1390252334, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7f57dbd28baba8a1ff2c202126105446', '127.0.0.1', '0', 1390249240, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7fc0eb90cff5e80aa8abcb131f0db743', '127.0.0.1', '0', 1390254228, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7fc1b53b8f930e68241c4825469af176', '127.0.0.1', '0', 1390251162, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('7ff9a60c76520df31618fc0741e101c2', '127.0.0.1', '0', 1390253699, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8027c5de78cc937b950fffaa6ac5f1d7', '127.0.0.1', '0', 1390253684, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('803f20824bb04c53193117ec87622e82', '127.0.0.1', '0', 1390253383, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('804f263246e301da5a5085974c1e08d7', '127.0.0.1', '0', 1390249238, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('805a85c9ced417f4223fafe0306ce0f0', '127.0.0.1', '0', 1390253258, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('80685f09996093de3807d16f1358342f', '127.0.0.1', '0', 1390253963, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8069a798d582e3155e0b5335dcd4a9a2', '127.0.0.1', '0', 1390253381, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('806d50a91b3f8b2b2e17a10d4ebcafcb', '127.0.0.1', '0', 1390253976, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('808a805afd7749638c9ff3b85060c450', '127.0.0.1', '0', 1390253690, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('80ad61558f16f1fc06449cdf0923fc7e', '127.0.0.1', '0', 1390252349, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('80de8ebfefd313c26db377814ac48b9d', '127.0.0.1', '0', 1390253264, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('80e019ceffd379448134957710346204', '127.0.0.1', '0', 1390253688, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('80f509a06398e569ff62eb447595d3a1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390851600, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:22:"juan.cantor@imagina.co";s:2:"id";s:2:"15";s:7:"user_id";s:2:"15";s:16:"current_user_one";b:1;}'),
('81224f3d93c559e1819fa9190f0bb1c0', '127.0.0.1', '0', 1390253966, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('815a33686a5d2e4c9adfc11dad0fce82', '127.0.0.1', '0', 1390256110, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('815bba0952583895463b023d91d3877c', '127.0.0.1', '0', 1390256227, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('81b5bbea462a0fecd2cd5cf51cb3b712', '127.0.0.1', '0', 1390250316, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('81e6517599045b01ce10396111cb2bef', '127.0.0.1', '0', 1390252339, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('81e7d6588441a3a47d186bf283f97a60', '127.0.0.1', '0', 1390253696, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('81eb75a1bf8ca582b2c033e9bfe008f9', '127.0.0.1', '0', 1390252767, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('81f008d4982bdc9ddeda52e08f957e7c', '127.0.0.1', '0', 1390252900, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('81f2d865cd2953e98cef4015f647df69', '127.0.0.1', '0', 1390253256, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('81f812dc7ad24f095f1c276471916bae', '127.0.0.1', '0', 1390253274, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8208297cf9c8ac846ef973c2cdedab87', '127.0.0.1', '0', 1390253682, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('820e3ad21cf948e87db357edf843f05f', '127.0.0.1', '0', 1390253962, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8227c809f3474578ce259d3d0d9943d0', '127.0.0.1', '0', 1390249241, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('82305c39667b2f82f5bce98ebee1cb46', '127.0.0.1', '0', 1390254324, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('823762dd21aea7f162863e5c1c374bbb', '127.0.0.1', '0', 1390253259, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8276b4695fece6558484b327e081a869', '127.0.0.1', '0', 1390254228, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('82839f11562183e9d309ef89e03af867', '127.0.0.1', '0', 1390256113, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('82f5159c362560fbb5eb75fa60cda856', '127.0.0.1', '0', 1390256117, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('82fc7f676dd19f889720ab8934d9aa75', '127.0.0.1', '0', 1390248106, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('830679cba1767d4f2cee4497e301e890', '127.0.0.1', '0', 1390249369, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8314680220dadc84e008c0a0d9f2be46', '127.0.0.1', '0', 1390253694, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('831a51d69a63f8c9c0f0c11aad5ea3d1', '127.0.0.1', '0', 1390252346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('832e072823eb61a681c469358f360c28', '127.0.0.1', '0', 1390254252, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('834656886091ea9bfded246b911483c3', '127.0.0.1', '0', 1390254327, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('835e00ed7aa7246c9ec522d198c5186d', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', 1392157243, 'a:7:{s:9:"user_data";s:0:"";s:8:"identity";s:18:"cms@imaginamos.com";s:8:"username";s:13:"administrator";s:5:"email";s:18:"cms@imaginamos.com";s:7:"user_id";s:1:"5";s:14:"old_last_login";s:4:"2014";s:10:"page_tipss";s:1:"1";}'),
('836ea04a423b8e5195303ccbc4a69e3f', '127.0.0.1', '0', 1390252340, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('838fe667666b7875bc6cb0fd6ff0c88c', '127.0.0.1', '0', 1390249371, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('83c8bb49e90540affc4e366110734ed8', '127.0.0.1', '0', 1390256004, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('83f53b83e8ec464a18f4310ae43a8496', '127.0.0.1', '0', 1390254361, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8434d4e9e42ba6e44caade7b02801320', '127.0.0.1', '0', 1390256123, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('84406986fd93b181e3fd8066911b3a74', '127.0.0.1', '0', 1390256008, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('844d76736d05bf32b6f27f7b7ca5e94b', '127.0.0.1', '0', 1390253967, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('845fa33027679806a9c0549cde181353', '127.0.0.1', '0', 1390253385, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8479d514cec8cddf1c9124910cf086cc', '127.0.0.1', '0', 1390253695, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('84cf0b8e50ae3a09c5fea1090e2ef5cd', '127.0.0.1', '0', 1390252254, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('84f17ceab9a6b8cf13bec9c45bb9e4c7', '127.0.0.1', '0', 1390252350, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('855f6ac874c1dc6910e592902bc18c30', '127.0.0.1', '0', 1390254224, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8583114cd3c0a878329aaac63722014d', '127.0.0.1', '0', 1390254366, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8595e5b8ad3a6a897315cb78e2517541', '127.0.0.1', '0', 1390256401, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('85c7209a34c6058e1e696668cdb74c15', '127.0.0.1', '0', 1390253703, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('85d18a8264ac876c6eead21743251abd', '127.0.0.1', '0', 1390256118, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('85ea4a0bc05c27a625e93dd7f954dc77', '127.0.0.1', '0', 1390253976, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('862050cb7cab98b8fbe0b132addb251b', '127.0.0.1', '0', 1390252896, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('86849000a8bfdc002d4ff869a7b617a8', '127.0.0.1', '0', 1390249080, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('86af137785deb5c6ca3c9c4716478e10', '127.0.0.1', '0', 1390254237, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('86e471c85504cb01c7d5c591bd9d4f62', '127.0.0.1', '0', 1390253686, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('870a4ac49a893c503137cbe426ef1f38', '127.0.0.1', '0', 1390252890, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8737dce3fa6e2063db44ec02d230b8e2', '127.0.0.1', '0', 1390249429, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8761109964e2b3d4655673af6f706d19', '127.0.0.1', '0', 1390253259, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('87620629111209ecc5e88d20884f3f3c', '127.0.0.1', '0', 1390249557, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8774eddda16247d110a26005a40353a5', '127.0.0.1', '0', 1390253257, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('87ea0196cb76be40f47fd4fa30f7e7d8', '127.0.0.1', '0', 1390256117, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('88178c0f526e91b000593548b6cb3ba2', '127.0.0.1', '0', 1390253380, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('882593cfd9328a45868a9d5d171402bb', '127.0.0.1', '0', 1390256003, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('882ada819f3902b4531b2731b4be4466', '127.0.0.1', '0', 1390253967, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8837f11f529225dad0bacd21a918d19a', '127.0.0.1', '0', 1390253257, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('884bee7228af810a0a524ca044e1a1a1', '127.0.0.1', '0', 1390252347, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('88681e2eed7398be3347524618b9fd85', '127.0.0.1', '0', 1390250297, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('88c3fa350333be659ebd2f21ec875ab2', '127.0.0.1', '0', 1390252774, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('88d1232b322bd91a2d2af1ea4dcb5e08', '127.0.0.1', '0', 1390252243, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('890bc5183711447ba2cefd2ca50a9908', '127.0.0.1', '0', 1390253269, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8910c3eda9afd8a68874f9e57d196dd0', '127.0.0.1', '0', 1390256110, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('894a5d983d68808e61916cd8b1f79083', '127.0.0.1', '0', 1390256005, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('894d624aa152ee992395c28c35dde413', '127.0.0.1', '0', 1390256112, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('895bf20ee29c2a2b560d106a922e5adc', '127.0.0.1', '0', 1390252778, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('89649f8f67c4bda5f10cb67aaa3a165d', '127.0.0.1', '0', 1390254371, ''),
('897cdf8c9e50d664d43eef598a0dd867', '127.0.0.1', '0', 1390253692, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8983e3b4e084bcebd148d1145928c2e9', '127.0.0.1', '0', 1390253695, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('89a31d1bb502416a8c496b323230d727', '127.0.0.1', '0', 1390254347, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('89d6befde1cab52bcfe275ec8a03d653', '127.0.0.1', '0', 1390249241, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8a0fa6d5f01e1ab365b6f0bfa0e5e539', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390316708, 'a:7:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:16:"current_user_one";b:1;i:0;s:12:"current_page";i:1;s:4:"home";}'),
('8a4649e78d3f14626f691225df930181', '127.0.0.1', '0', 1390253262, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8a4885098d251f0a71339b305ae8f4a4', '127.0.0.1', '0', 1390253268, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8a4bdebbaf9240e1cacc9972821d3017', '127.0.0.1', '0', 1390254256, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8ab56fcfa0a51aaef1f4bcdb06b51a17', '127.0.0.1', '0', 1390254366, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8af89f352cce7787a70d0710debd675b', '127.0.0.1', '0', 1390252894, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8b1a9e1754a81e2aad81d45ce34f9276', '127.0.0.1', '0', 1390248429, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8b241a40f167e039343943b9239c4c03', '127.0.0.1', '0', 1390251990, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8b767366d57eb174ece62dca4618b326', '127.0.0.1', '0', 1390253703, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8b91df2f59e78c32e65a72f61a6ea264', '127.0.0.1', '0', 1390252895, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8b92d674a8e81372eb5093efd6eb707e', '127.0.0.1', '0', 1390251990, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8c670d84b612889c6d0d91cd09e324ee', '127.0.0.1', '0', 1390253969, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8c79dfc7d6e7068c9219e3c964277f02', '127.0.0.1', '0', 1390256402, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8c899c64ccf53392e39189e6d3de5099', '127.0.0.1', '0', 1390252887, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8c8e0648c56ba98f644223ac00aac682', '127.0.0.1', '0', 1390248107, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8cc63e222b67d1e1f5300c9b4dc89eb3', '127.0.0.1', '0', 1390253984, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8d0be58dc48aa3397c06b697fd989019', '127.0.0.1', '0', 1390252252, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8d19b8daa9be2d428fc93ea5ccfea743', '127.0.0.1', '0', 1390252344, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8d2c61de849437a90e28f7af29e7de93', '127.0.0.1', '0', 1390256120, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8d57962877333c9227e93aaf31139e96', '127.0.0.1', '0', 1390253963, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8d91c582ea9648e75b9fb5c534da5d58', '127.0.0.1', '0', 1390253969, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8d921a3d2198ba0e453fbd134608b807', '127.0.0.1', '0', 1390252353, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8d9394d312e3cd075fe1088e3c023a26', '127.0.0.1', '0', 1390252893, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8d98324c615f0ad386bf0bbe37852f99', '127.0.0.1', '0', 1390254372, ''),
('8d9ff7be1d61bda0a38f01ca27abe88c', '127.0.0.1', '0', 1390254236, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8de4b395e7695b2c0b77a6da53e2f619', '127.0.0.1', '0', 1390253383, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8e0c41d1e5fcc1e2d7e01e83f732a780', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390254220, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:16:"current_user_one";b:1;}'),
('8e403d7d17a628178ab340c46726e392', '127.0.0.1', '0', 1390250298, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8e48f16ebdfa7c6b32d59947575c686b', '127.0.0.1', '0', 1390253688, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8e615c1fed0c377ed6e1bf5d1d2d8831', '127.0.0.1', '0', 1390252775, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8e8c28107b29e4e7141dfa85a7d53ec3', '127.0.0.1', '0', 1390253385, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8eadba0bb3fec3882071b3cc066ac09b', '127.0.0.1', '0', 1390254325, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8f1f3bab2fa82a20ea8ed2597eb4019c', '127.0.0.1', '0', 1390251423, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8f1f5e3dda6474bd9fa7ea9831a56bb5', '127.0.0.1', '0', 1390254324, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8f2b93a87609cc32333ff10bf63b85df', '127.0.0.1', '0', 1390254365, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8f416fbcf17b9c143fa689766298c1c1', '127.0.0.1', '0', 1390253266, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8f4d68137f3f31bf6eecedb5e8f4e184', '127.0.0.1', '0', 1390252220, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8f4d9d5feacac83f9ae67863fd79728a', '127.0.0.1', '0', 1390252227, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8f75580021ed4cab2dbc499e4e39bbb0', '127.0.0.1', '0', 1390252247, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8f80336fad9c197a4612f8dc1eea7192', '127.0.0.1', '0', 1390252899, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8f9ea0f5f259d584e33fffe961059d5a', '127.0.0.1', '0', 1390256006, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8f9ef9787b1e2deadafaf7981dbf0494', '127.0.0.1', '0', 1390253983, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}');
INSERT INTO `cms_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('8fa61f8c78d96848796348a416b8cb12', '127.0.0.1', '0', 1390252223, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8fb12132f2277d65b9223df53a0cab05', '127.0.0.1', '0', 1390256418, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8fdaaf47df69d622df91e445c9b502c3', '127.0.0.1', '0', 1390252892, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8febf11a3c5eb38699b3b04034a7f4cd', '127.0.0.1', '0', 1390254325, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('8ffadcf382728cc571842258d0db1a7c', '127.0.0.1', '0', 1390253263, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('900d58a66224dc4f4ffe75e22513b9f7', '127.0.0.1', '0', 1390254321, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('90143254043467cb90c3d8c2a58e51c8', '127.0.0.1', '0', 1390254260, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9014eb37e989ec3a54d632516489de10', '127.0.0.1', '0', 1390252890, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('902fca03eb471d37d6e2cfda2348172e', '127.0.0.1', '0', 1390252244, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('908ccabbe4375060bebe7db787f8c7d6', '127.0.0.1', '0', 1390252896, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('90a0713f26766374314f2735868fd4e3', '127.0.0.1', '0', 1390252353, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('90bccdca8cd8004eecf34525172e690b', '127.0.0.1', '0', 1390252769, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('90cf2ff6c1602c97705bae3b9bc43e9a', '127.0.0.1', '0', 1390254347, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('90cf4a2be42d43eda99f23843efdca30', '127.0.0.1', '0', 1390249240, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('90f1c840871aeda919136feeffca236c', '127.0.0.1', '0', 1390252239, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9103a1e93683a6bfb765d009953e3dea', '127.0.0.1', '0', 1390252891, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9143ad218626db4aab0c5abfe7b88d78', '127.0.0.1', '0', 1390253693, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9154ebeec24d23d1f0fe1afbafc71d02', '127.0.0.1', '0', 1390256120, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('917a4ab58e705fdfebda8ffd613533d4', '127.0.0.1', '0', 1390256112, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('91821919dcbfb280bb45269bd098ba4d', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9193e623886cc6711aa252145ba0c413', '127.0.0.1', '0', 1390253275, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('91f459c7184f3c3ff53faa95f1f887e0', '127.0.0.1', '0', 1390253970, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('91f721e5371d6bcc9bae4fc59b06e5c4', '127.0.0.1', '0', 1390252343, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('91fe98b4de98125bfbb8733a098cc33b', '127.0.0.1', '0', 1390256008, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9242ba3867927cba3f117338462771c9', '127.0.0.1', '0', 1390254324, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9276681bc62d9acb36d81948813bd476', '127.0.0.1', '0', 1390252347, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('928be7392aa1dd4aaa716273812d55ad', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('92ca7fee27def3b11e832ec3f862da8a', '127.0.0.1', '0', 1390252225, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('92f223c832e00e98eaa2f9be2bdbafde', '127.0.0.1', '0', 1390252247, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('92f864a556614c95b9a7c64dab771636', '127.0.0.1', '0', 1390253269, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('930d0715d6ff28df5bb789bab2654499', '127.0.0.1', '0', 1390254235, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('931b037e6bef619e49ba0adf41fd2091', '127.0.0.1', '0', 1390253981, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('932ae7cc6cb9ae20f2ae0bf78e56f652', '127.0.0.1', '0', 1390249118, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9350a680f38326526acc2cf073c6e0ec', '127.0.0.1', '0', 1390253264, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('936d88b31f0b024067f60fcafd0fdddb', '127.0.0.1', '0', 1390254365, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('93793f4a21173bb13083a11f2806ec96', '127.0.0.1', '0', 1390253267, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('93a7143f4ca25924d533fb6b8875ebef', '127.0.0.1', '0', 1390256007, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('93ae9419494e3cd568cde736e06825e9', '127.0.0.1', '0', 1390253382, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('93bf7c5064aebb29b6f7e9481cbbfb3f', '127.0.0.1', '0', 1390253983, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('940215c199efa0063f4f6968b6cb624d', '127.0.0.1', '0', 1390252342, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('940727c139546069e3bcb7aa21384e3f', '127.0.0.1', '0', 1390254244, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('940f3b34f46d87062e20573423b0be65', '127.0.0.1', '0', 1390256121, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('951014388e5f13ecb976b6b2ac8b49e8', '127.0.0.1', '0', 1390253701, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('953ecb2cd5811507d31b651b53bd165e', '127.0.0.1', '0', 1390254236, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9567f7e8b40a2cb280e59133d39ae327', '127.0.0.1', '0', 1390255951, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('95d4827b79ca314f1b42e98782d86177', '127.0.0.1', '0', 1390252234, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('95d716d2bb8c2ebe2d4a1e9864319d9c', '127.0.0.1', '0', 1390251165, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('960679184cf7d35414f24ca217f04593', '127.0.0.1', '0', 1390253266, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('963503ac276e267038d227b796946779', '127.0.0.1', '0', 1390256220, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('96542d34c67946e4aaa0adbfc789aaa2', '127.0.0.1', '0', 1390253681, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9656766ee0c11054288eaf7215ba0a69', '127.0.0.1', '0', 1390252778, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('965f7df859ccf888f1a481f0f1908f3a', '127.0.0.1', '0', 1390256122, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9677d1341a2049f1b954f8ec9dfb0731', '127.0.0.1', '0', 1390253380, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('968ee3021c915e2b5602de836690664a', '127.0.0.1', '0', 1390248430, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('96f78390ec49f021bb5c6ac40077f31f', '127.0.0.1', '0', 1390253383, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('96fa268b10ee2c28327f2973c9b68dc2', '127.0.0.1', '0', 1390252890, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('970fdb96b20d89e3903df8a17477a430', '127.0.0.1', '0', 1390254369, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9717e054ba36e184ef676b205b9d92de', '127.0.0.1', '0', 1390252773, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9732d876be66e420a479e2d0e74cb2bd', '127.0.0.1', '0', 1390251424, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('97476f53a07bd25955d8300f14453ce4', '127.0.0.1', '0', 1390253252, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('979dde1f71d01e2c65108b504b02bc01', '127.0.0.1', '0', 1390253966, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('97a89017e9f4a40c27d1d20cf2118adf', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('97d1dd0adc84d3131807e29fc7da1051', '127.0.0.1', '0', 1390252899, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('97d4d75b42924a84691c7e5e7a14d178', '127.0.0.1', '0', 1390253696, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('97fa9a35eaa34e21c784242448bf8dc6', '127.0.0.1', '0', 1390256115, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9836b827fc473808d200f72b612572be', '127.0.0.1', '0', 1390256005, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9855cf8468529487bf629c69ccd3c85a', '127.0.0.1', '0', 1390253977, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('987b70720bf8af52952d07c40ff8ed0f', '127.0.0.1', '0', 1390253385, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('98be78996fda4aa9602aede0eab4ab60', '127.0.0.1', '0', 1390249120, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('98ed3cd5acad1f9533e9668ac7c89fea', '127.0.0.1', '0', 1390256114, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('994d160f9841fece2e1fcf969eefc6d9', '127.0.0.1', '0', 1390254233, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('99587afac90bb38942b5a07cb36312d9', '127.0.0.1', '0', 1390256003, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('995d7a1e7c6928ba92eb38f6f37ad1be', '127.0.0.1', '0', 1390252220, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('99c54b99735c6d5fc156be4f3caa54a3', '127.0.0.1', '0', 1390254233, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9a08f62d4ee60bb7f77a111212efe12f', '127.0.0.1', '0', 1390254367, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9a2051c7c434cd7c0f7b13a051b29dda', '127.0.0.1', '0', 1390253973, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9a541c93b209f37af48360e833c0a2e5', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390250293, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:16:"current_user_one";b:1;}'),
('9a599d3c9ace61303d8f53a760f14de2', '127.0.0.1', '0', 1390253262, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9a9d25b33d5b35b50da55ab9bc1ffbf5', '127.0.0.1', '0', 1390248430, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9abc5fd6a95bf998c468c5ac59a72dcf', '127.0.0.1', '0', 1390256008, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9ad7b02f377f60132436e7a80366f544', '127.0.0.1', '0', 1390248426, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9b243f1b1ad572c482c016f7a78b64e5', '127.0.0.1', '0', 1390253272, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9b2f4991887e78157443f5c216659210', '127.0.0.1', '0', 1390254325, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9b4a72ed97d124767028f14d9bd9ece9', '127.0.0.1', '0', 1390249237, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9b5bafcc5502e739dd5fce4a56dd3c73', '127.0.0.1', '0', 1390254259, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9b7191ff52d18e2886252cc89f3d7790', '127.0.0.1', '0', 1390252216, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9befe5f6aac2a1a7d28c22caf3a468c1', '127.0.0.1', '0', 1390252770, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9c018a4dfb7d058bdc75572aeaf6e949', '127.0.0.1', '0', 1390256002, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9c15fcdfafd2ed3be3d07ffdae695b04', '127.0.0.1', '0', 1390252340, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9c70c1b7754b634dbcf83bfb8539e8ca', '127.0.0.1', '0', 1390252341, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9c8bdcee0125a119a0791bf59cbbd967', '127.0.0.1', '0', 1390253982, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9ccb432e1ba9099f3d3221d31dfcdcb4', '127.0.0.1', '0', 1390252254, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9cd155693d864ea51d38718ca748539a', '127.0.0.1', '0', 1390252336, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9ce678612d1bcdcb1fe7c76fd332669b', '127.0.0.1', '0', 1390252235, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9d06a67c19a002587f88be3eb7d4a0f7', '127.0.0.1', '0', 1390254233, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9d3231eb4d1f5752a8040491514467b5', '127.0.0.1', '0', 1390256226, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9d48eb2f7136825479152d60e6a76c0c', '127.0.0.1', '0', 1390256902, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9d69bfc949b6c589f596d0c5711c72cc', '127.0.0.1', '0', 1390256116, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9db05f271a0ff1cf54797bf69607953a', '127.0.0.1', '0', 1390252218, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9dc3eb03b84b86d4064e3005a52b7c41', '127.0.0.1', '0', 1390248106, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9e138757e1d1f4950b8c05ba91d8dd56', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9e2878fb2d827062499cbc8e3d037a31', '127.0.0.1', '0', 1390251168, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9e2bdc240d2cfc23bbc6e4146c4844cd', '127.0.0.1', '0', 1390252356, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9e4234d98d1f934b5a84ad38408159d5', '127.0.0.1', '0', 1390253257, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9e5fee31097b974c707f7f93eb7727d3', '127.0.0.1', '0', 1390254236, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9e6218be0c3562064d45a1c1b5ee4ae8', '127.0.0.1', '0', 1390252248, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9e63bbf068f5f921a802fab4e4e63df0', '127.0.0.1', '0', 1390256234, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9e70a0126be5a39ce1ddf73083ea38e8', '127.0.0.1', '0', 1390253254, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9e835290a63c59d21a9d9ddfe1607eaa', '127.0.0.1', '0', 1390256118, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9e8b7d5f195df47aa3f21589301a5dbd', '127.0.0.1', '0', 1390253385, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9eb5aec767fdd30cc1fc9361302aa937', '127.0.0.1', '0', 1390256900, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9f1f6f8e7e63dd251c2f246a45bdc204', '127.0.0.1', '0', 1390251162, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9f33e7e7e39c144731f0693779f88e7b', '127.0.0.1', '0', 1390251165, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9f3704dede6c079ebc44a0290af1ad19', '127.0.0.1', '0', 1390252897, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9f54a21595f96bfbad71763e26c404dc', '127.0.0.1', '0', 1390253964, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9fb9c3b66302676c81b8788c54f19bfe', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390851600, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:22:"juan.cantor@imagina.co";s:2:"id";s:2:"15";s:7:"user_id";s:2:"15";s:16:"current_user_one";b:1;}'),
('9fd0e91df840b66348bb9a163379f862', '127.0.0.1', '0', 1390256111, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('9fd571ded90ee7b0f756d460b475388f', '127.0.0.1', '0', 1390253702, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a00b11caaa5e93443743b6a4f487df7f', '127.0.0.1', '0', 1390252770, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a00ed38a30fe01970ddd13ae09adc915', '127.0.0.1', '0', 1390252222, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a037ab3cd3ddaf23be9de2925281295c', '127.0.0.1', '0', 1390251425, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a03d29d455a1625213a77850331dbecf', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a0461cbac06b519bf1a36e7f224c833c', '127.0.0.1', '0', 1390251984, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a04c0235cbe61875d4c350915d77c35a', '127.0.0.1', '0', 1390249370, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a0663d761b4b4ac97edefa610c3bfa09', '127.0.0.1', '0', 1390253268, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a08ba4c0c0a19e2c73dae9de679c1596', '127.0.0.1', '0', 1390256114, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a0981eb41d2d4667de4c9c3779acf8b4', '127.0.0.1', '0', 1390253692, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a0fbd49304b2b521dafc3e4c78271efd', '127.0.0.1', '0', 1390252768, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a10af009d84fde7e224909c8e5668724', '127.0.0.1', '0', 1390253259, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a11a4ef3fce44254f18f1fdfbb369903', '127.0.0.1', '0', 1390256114, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a13b2ed38126dc6b4e34e261d5c233ae', '127.0.0.1', '0', 1390249078, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a15f25dcf7f051e1d3df1041ad7efd83', '127.0.0.1', '0', 1390253962, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a17522f0c76980f945695cda221b9c9e', '127.0.0.1', '0', 1390252352, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a1ce259d2017d16145077b30b9015440', '127.0.0.1', '0', 1390252889, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a1d3c2219e58437d13fca60f7e30de3f', '127.0.0.1', '0', 1390256111, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a1efe7786ab8813669ced3c1f4029265', '127.0.0.1', '0', 1390249430, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a204b9361d106b21632ecef96c52521b', '127.0.0.1', '0', 1390256234, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a2264165f95455afde0ee64b7cecdd09', '127.0.0.1', '0', 1390256902, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a2299cf1455452d362b8884c8b02cd2f', '127.0.0.1', '0', 1390251426, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a2378f4f67a9f7cfa21d137855fee5f2', '127.0.0.1', '0', 1390256115, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a24b5e8e1146eb471d75d9db7f0c3b35', '127.0.0.1', '0', 1390250297, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a260ed2699d2c6be6e5cae678b8540b9', '127.0.0.1', '0', 1390253253, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a26bb970f4c23dcc4104d05befa13890', '127.0.0.1', '0', 1390252219, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a26cc24ab69fa7cb2f001690ce6f75e3', '127.0.0.1', '0', 1390256117, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a29ee56718199004b70bd2fa9644f298', '127.0.0.1', '0', 1390256899, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a2a80d190c9c5af3cef639d7c4647b8d', '127.0.0.1', '0', 1390253253, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a2c932c883db1ac7217d03932c823eec', '127.0.0.1', '0', 1390253966, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a30cf19a902a9c0402ae8eccc5be8e31', '127.0.0.1', '0', 1390253977, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a3342d8e8e3b811d704a8b37b5c1e89c', '127.0.0.1', '0', 1390252778, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a35b43d8aa07488029e6fd3543684cc3', '127.0.0.1', '0', 1390256110, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a368687218d75aa847eb0982f90f43ef', '127.0.0.1', '0', 1390252889, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a38c329df80e6b0d869265b4ed80794f', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390840740, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:22:"juan.cantor@imagina.co";s:2:"id";s:2:"15";s:7:"user_id";s:2:"15";s:16:"current_user_one";b:1;}'),
('a392a6f416faa6427812a42f558aaedd', '127.0.0.1', '0', 1390248429, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a3ef4546e26af6252ceca00c762f3640', '127.0.0.1', '0', 1390256002, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a3f42eb559a238a409c90c806fb51cf7', '127.0.0.1', '0', 1390253381, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a4575d18a71c70ab5a54ff0f8ad1e774', '127.0.0.1', '0', 1390253696, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a484f63e55ba47d90fff57ab5c642573', '127.0.0.1', '0', 1390252231, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a48b4b313c78cd272626823ca95f110e', '127.0.0.1', '0', 1390253264, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a4b97dbe4e61a4734ad297d439faeb3b', '127.0.0.1', '0', 1390252771, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a4c3fdadf5a439094ae0e672fd9bfeeb', '127.0.0.1', '0', 1390253272, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a4cabf53069f266d310c7670c8d7fc4b', '127.0.0.1', '0', 1390256118, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a4d8df6827432f8baf2667893ec7d629', '127.0.0.1', '0', 1390253254, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a4f0b612b2241dcbc9344878bc55971b', '127.0.0.1', '0', 1390256900, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a5029d78185f93a1364a19975ab97280', '127.0.0.1', '0', 1390252767, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a54dde4451543f8f3a3d460854b42a2c', '127.0.0.1', '0', 1390256009, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a552a61e3891c9edb55b2c3b7cb87ac6', '127.0.0.1', '0', 1390252901, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a57eedb11aee04653069e7683a1d91b6', '127.0.0.1', '0', 1390252352, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a5a05c70ff09b33afd8e06df6f1f34fe', '127.0.0.1', '0', 1390252216, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a5a3687c79a7080347b017d0198f5961', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390840740, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:22:"juan.cantor@imagina.co";s:2:"id";s:2:"15";s:7:"user_id";s:2:"15";s:16:"current_user_one";b:1;}'),
('a5e5d5f8bf5d0b24e10f79a0de2671c5', '127.0.0.1', '0', 1390256230, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a5f5ace9ca921b68a6e1247acdcf80b9', '127.0.0.1', '0', 1390253684, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a65e5d2e1882fedfe749510fbef355d3', '127.0.0.1', '0', 1390252888, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a6b608a18d0e6a1776a6592e1d377257', '127.0.0.1', '0', 1390254366, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a6d9bfef9dfb945e90453f16c5082bad', '127.0.0.1', '0', 1390253266, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a6fc4f73d43a16ac1595e84c20eb22e3', '127.0.0.1', '0', 1390252246, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a736fdfc917ff655449f27c65a9064bb', '127.0.0.1', '0', 1390256006, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a74631d7815f9a99be0042d26e1a5143', '127.0.0.1', '0', 1390248108, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a775e4a292933093afd9015ef7bbe149', '127.0.0.1', '0', 1390251986, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a784c507854626fff7bc89b289c9f4c7', '127.0.0.1', '0', 1390254253, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a7bcd23fba53cbd0e77a1654c40dba0b', '127.0.0.1', '0', 1390249077, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a818c5f0810070370c845c3451e99f24', '127.0.0.1', '0', 1390253384, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a852b63a61d3beeb59272ecd83df7997', '127.0.0.1', '0', 1390251424, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a861dd2d2d17118ba8ce9e7142539577', '127.0.0.1', '0', 1390251425, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a884c1c6fa30e44299f523e18875b627', '127.0.0.1', '0', 1390252889, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a8981082876a039909c834c3ae6f97d5', '127.0.0.1', '0', 1390253981, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a8b8437d0cbf7001b8aa02bfb8bcd30f', '127.0.0.1', '0', 1390252235, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a8e2d656e41341432723df88fc670ee2', '127.0.0.1', '0', 1390253690, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a90237ed93cdf8f04d6988f9a0ff4b3c', '127.0.0.1', '0', 1390252892, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a910c9f242b3746765f44b3c1a56b5bf', '127.0.0.1', '0', 1390253685, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a95217991c5a59016d2d590e97096840', '127.0.0.1', '0', 1390253383, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a96c57cae300506ca48a14890bfe0c0d', '127.0.0.1', '0', 1390256117, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a97d99a1debfcca75453789ff6b5d577', '127.0.0.1', '0', 1390252769, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('a993ac1b84294a4d47059e3cfc495d10', '127.0.0.1', '0', 1390252225, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('aa019ba3e47c70dc362dc0b78d66368b', '127.0.0.1', '0', 1390253979, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('aa05dfc157dd474d8992204bbdd14771', '127.0.0.1', '0', 1390252773, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('aa1854a539109cedf96c2fb4ce01ec68', '127.0.0.1', '0', 1390252344, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('aa274391e8bbad492d3efe4829e12ed2', '127.0.0.1', '0', 1390252902, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('aa4ad913645d2a45d3d55329bb7e5eb1', '127.0.0.1', '0', 1390253966, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('aa8adc041cd73a5a2ccfdea142631220', '127.0.0.1', '0', 1390253260, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('aa8da51707dd6d44c15e3b3e578d03a0', '127.0.0.1', '0', 1390252356, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('aab96b393bd05cf324074bb8c1e942a2', '127.0.0.1', '0', 1390256119, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('aac9a3c69cdd9aeb75a9dcdc9d7961fa', '127.0.0.1', '0', 1390256233, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('aad22598435254b6ac67cbe2238de2e4', '127.0.0.1', '0', 1390252891, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('aafd04d026e7fe9b6fee17859b2b8106', '127.0.0.1', '0', 1390256005, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ab113167d7a2cddd3d3ed206c709693a', '127.0.0.1', '0', 1390249428, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ab30d5601829e70690f4b06d261cf8a9', '127.0.0.1', '0', 1390253268, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ab4f46f5819f06a5d9097725b8fa237e', '127.0.0.1', '0', 1390249426, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ab5479475ee9236aa79075d4b75659fb', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ab62348aac1e385cbefc1c6dc5919461', '127.0.0.1', '0', 1390252221, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ab7a81f3f9d7ac917719286a3484cc73', '127.0.0.1', '0', 1390256121, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ab92a6eb1937a19b754e956f031193bb', '127.0.0.1', '0', 1390254371, ''),
('aba046729365a622ab32d6bfbc31a7d3', '127.0.0.1', '0', 1390254232, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('abb53640648fb5485bfa880181aaba96', '127.0.0.1', '0', 1390253256, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('abf2afcf4aaa9fc0e7927be5f062c799', '127.0.0.1', '0', 1390253382, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ac0640257c94ba5bbfc79e551e025cb4', '127.0.0.1', '0', 1390252230, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ac29a588eccb63d835e854115ce0f418', '127.0.0.1', '0', 1390253251, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ac2a3bc4aa7b536c855ae23244f56b20', '127.0.0.1', '0', 1390253682, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ac560998d09d5575503cf5416d785c2d', '127.0.0.1', '0', 1390249236, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ac5db856a529f25256d8d4f7b0510f51', '127.0.0.1', '0', 1390251422, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ac882a8fbc72653436aad57548f4cbcd', '127.0.0.1', '0', 1390256120, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ac9a1f7588550678eea56218a21055ec', '127.0.0.1', '0', 1390253271, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('acaf90f4ac41baf7d93f87d43ba17f09', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ad13188a72fb80ebdab0e9dd12137f55', '127.0.0.1', '0', 1390256112, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ad4ac7dd8228c87bda37b88f62fe33c1', '127.0.0.1', '0', 1390253380, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ad5990db0612a2108b7e22f83db2874f', '127.0.0.1', '0', 1390253970, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ad682790c76f1750c4d4b960ea249f94', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ad8179ba380e090f411fca8ae4ad354b', '127.0.0.1', '0', 1390252767, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('adaa1f07b3a5d0e459ed5c35956f2e0e', '127.0.0.1', '0', 1390253683, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ade96cc18d7186300e8a1c13c308fabb', '127.0.0.1', '0', 1390256002, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ae091360777b9759b4b25d628270f185', '127.0.0.1', '0', 1390251166, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ae09cdd89be24be6225371607773b19b', '127.0.0.1', '0', 1390256228, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ae172ca47ab933bed12ff399a188265c', '127.0.0.1', '0', 1390253260, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ae70523b815d44c5d841215d90c011b5', '127.0.0.1', '0', 1390253691, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('aea5c832f46eb419f7415dcc4b816513', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390953581, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('aeade1535ecc9060cbf260bed98f9d7d', '127.0.0.1', '0', 1390256121, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('aef5280257500872cb16c9133da99211', '127.0.0.1', '0', 1390252770, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('af2a2cd8cbd5aa32f4522ffa83347f07', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', 1392609756, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:22:"juan.cantor@imagina.co";s:2:"id";s:2:"21";s:7:"user_id";s:2:"21";s:16:"current_user_one";b:1;}'),
('af398c5e944ec4df6399eeb438ec9246', '127.0.0.1', '0', 1390254322, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('af4df5eff3478d442b042a50fb7db468', '127.0.0.1', '0', 1390253961, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('af890f1c249a4a1032b07e71073de027', '127.0.0.1', '0', 1390252236, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('afaea6052bd781384bb90ccb5024e896', '127.0.0.1', '0', 1390254322, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('afc013b800058c52831e728518f3d217', '127.0.0.1', '0', 1390252898, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('afcb72b93ecea423544eae13563fc50a', '127.0.0.1', '0', 1390253977, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b0050d3825c2af3431fcac8a641e38cc', '127.0.0.1', '0', 1390249370, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b034f0a74af46c22f979bf3634700f3b', '127.0.0.1', '0', 1390253972, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b083dc26939759f848a35c3e4f316730', '127.0.0.1', '0', 1390250294, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b0910e509572c17ed2639bc8861ea3de', '127.0.0.1', '0', 1390253702, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b0c1621d12ed2824fdb6671fda2b6d48', '127.0.0.1', '0', 1390252245, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b0c4c242954647d2ffba6cba080ad9f9', '127.0.0.1', '0', 1390253270, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b0f38d71f2e9ca65f6c95fed2d5ed154', '127.0.0.1', '0', 1390250295, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b1104e3b25b1193abe8760cf2fc8b4f7', '127.0.0.1', '0', 1390253261, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b140abbfa3aa606c083c1b4facca6d9c', '127.0.0.1', '0', 1390254323, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b1494ff73ff5fd9a3c3db3aeb1516cef', '127.0.0.1', '0', 1390252215, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b16b5b9bcb4ffa975e71b9a28b5289c4', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390252887, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:16:"current_user_one";b:1;}'),
('b18b45a0a9c8d6f11790337b746c84cd', '127.0.0.1', '0', 1390255952, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b191a947c78549edd433496da7ca9f2a', '127.0.0.1', '0', 1390256119, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b1be84530fc48aab9cf40c4964bd15d3', '127.0.0.1', '0', 1390256008, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b20988535d0320c623851a7889fbe140', '127.0.0.1', '0', 1390249426, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b219f1e1a0c85a858c754740ec913c1f', '127.0.0.1', '0', 1390253693, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b22fcd287fb65c8e150713f8f82d3f36', '127.0.0.1', '0', 1390253685, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b233ae617a2cb5f416837ff53ed3c239', '127.0.0.1', '0', 1390256112, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b298a57347d0b35d72bd488344771e41', '127.0.0.1', '0', 1390252239, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b29d2a440238b876f2c4c57b836fd24b', '127.0.0.1', '0', 1390252887, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b2afdb12ff7556226c23edd48431c3e6', '127.0.0.1', '0', 1390249427, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b2d85d55c2dde6351140a52778d5d94e', '127.0.0.1', '0', 1390249431, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b2dcd19b7c9c82a9d6dfd9eb5d47a26a', '127.0.0.1', '0', 1390254324, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b2df86c50e66e8b77cd6164926294bb8', '127.0.0.1', '0', 1390254367, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b39638c9b7259e968229cee2ef98490e', '127.0.0.1', '0', 1390253273, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b3b0bac42356bc9ff1a7f65375f53c44', '127.0.0.1', '0', 1390253266, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b3d5459e595fa6bb243d0a530f43de5d', '127.0.0.1', '0', 1390255977, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b3da0af816770ca0d0fa15424d6438bf', '127.0.0.1', '0', 1390252234, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b43e8a41a3b072860d628a0b41909a73', '127.0.0.1', '0', 1390251165, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b46c4505d4436c80e3eddd6fc210a85d', '127.0.0.1', '0', 1390256119, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b47a8568479ed3afb1b41c77ee63ad5a', '127.0.0.1', '0', 1390256229, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b497240500123b9232b275c9549395b6', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36', 1389894795, 'a:18:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:25:"page_routines_categoriess";s:1:"1";s:16:"current_user_one";b:1;s:14:"page_routiness";s:1:"1";s:12:"page_stagess";s:1:"1";s:11:"page_weekss";s:1:"1";s:12:"page_phasess";s:1:"1";s:14:"page_workoutss";s:1:"1";s:11:"page_dietss";s:1:"1";s:22:"page_diets_categoriess";s:1:"1";s:11:"page_mealss";s:1:"1";i:0;s:12:"current_page";i:1;s:4:"home";s:20:"page_activity_levels";s:1:"1";s:22:"page_activity_minutess";s:1:"1";}'),
('b4a7a97dfa620f1c7448d7d2f4d48e2d', '127.0.0.1', '0', 1390252901, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b4aa4379199205b94cfdf62e0b9efe3b', '127.0.0.1', '0', 1390249076, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b4af40ff6bab814e31a63064fb6da565', '127.0.0.1', '0', 1390252770, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b4cc27f46e5b36bf34e357625d0f464e', '127.0.0.1', '0', 1390253982, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b500d21eba3e809e5582161ccfb4971d', '127.0.0.1', '0', 1390253965, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b51062fe3a06e79493a7f3bcff1bc0fc', '127.0.0.1', '0', 1390252348, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b551b38681d2b821c508369b4e409bb0', '127.0.0.1', '0', 1390253979, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b5b34e6c99c22274e3d5e69a422ab04a', '127.0.0.1', '0', 1390252230, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b5e509dac7f6c2852946565417927b1c', '127.0.0.1', '0', 1390251984, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b5fc4924a9dad433144261c6ade37883', '127.0.0.1', '0', 1390256113, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b645065e68be9c2df4f38457fc95cd8c', '127.0.0.1', '0', 1390253691, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b6a1520c6cf12a76839d74d2b71d0b40', '127.0.0.1', '0', 1390254242, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b6b7a6ef0eb8da8c81e77c2a365c5025', '127.0.0.1', '0', 1390252258, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b6cb75f8ceacd847ec82db42e9ddb105', '127.0.0.1', '0', 1390252350, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b72109834553aeb4dbcd324559f147ed', '127.0.0.1', '0', 1390253388, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b7602f977f8dae9981322f70b1019c39', '127.0.0.1', '0', 1390252216, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b7927f76c4db6a89ef0fc4ae0e52bd84', '127.0.0.1', '0', 1390254241, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b7a89872303c530e33a3c20e7d498fcf', '127.0.0.1', '0', 1390254239, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b7db113930624aeb0032b0aeb8a2dd40', '127.0.0.1', '0', 1390252888, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b7ddf785dea39c9c7ddfb4313be5f688', '127.0.0.1', '0', 1390256418, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b7e57b8158f5e8e2be8ef379bbca963d', '127.0.0.1', '0', 1390253702, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b7ee9458e142f68cf6fa91380533a0ed', '127.0.0.1', '0', 1390252347, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b810a3f40f25fc3f430f21ddc7d91fa6', '127.0.0.1', '0', 1390254366, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b81ce0190687109aba44e7d05095b057', '127.0.0.1', '0', 1390253702, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b84616311024673278c30c7bab927361', '127.0.0.1', '0', 1390251426, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b88da5935a9baa231a09aeadf232a528', '127.0.0.1', '0', 1390252347, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b8d1e582927c90c5dc456a42df14b171', '127.0.0.1', '0', 1390256006, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b8d39343fb91709163a4e30d318f24bb', '127.0.0.1', '0', 1390253961, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b90156cd8c8fdf82c6f1dd3f58b49871', '127.0.0.1', '0', 1390254365, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b91b86c388b9344a7dfa64c293745d36', '127.0.0.1', '0', 1390253975, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b9466916d38bb6ee9cd85c43ec08103c', '127.0.0.1', '0', 1390253386, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b95d44a86602e1f745f9f8b90415b464', '127.0.0.1', '0', 1390253979, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b9a99bdb6b146999c89aca459a9c0155', '127.0.0.1', '0', 1390256110, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b9c01f7a438db1f5768dcd9fb26c0b53', '127.0.0.1', '0', 1390252901, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('b9ceb93e41986729884590b8c69339b8', '127.0.0.1', '0', 1390249371, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ba0372afa1445bae9516cc0c3860d27a', '127.0.0.1', '0', 1390253980, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ba1bda344e334289874ae6d11cf8a223', '127.0.0.1', '0', 1390253265, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ba1df30342cdb9a9405d4edc02103783', '127.0.0.1', '0', 1390253258, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ba1ef9c63736fadc64869e40722d8f1a', '127.0.0.1', '0', 1390253261, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ba21d9eaf817bcc62bc9067e5d454109', '127.0.0.1', '0', 1390252902, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ba282ab44df70f394aa92c30a5cc90bd', '127.0.0.1', '0', 1390253683, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ba7ee58eb67c27aa9e99505c791f7270', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('baa306da4be53a4d9dedf95280ec17b6', '127.0.0.1', '0', 1390256113, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bac8f6f11a7daa6683dfb1fa1af7f6b0', '127.0.0.1', '0', 1390252888, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bb347de0d733be4797423dd9261b1742', '127.0.0.1', '0', 1390253684, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bb34a5b9e4e3d28af1d9659f68201db6', '127.0.0.1', '0', 1390254225, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bb39e59d81e1946a27eda6bc4e93e5fe', '127.0.0.1', '0', 1390252767, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bb4ee212d23f1bab5d34d09a74257ae4', '127.0.0.1', '0', 1390256116, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bb6b675c732fb4724eceb90f0e46ac8f', '127.0.0.1', '0', 1390254325, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bb827a41792b8ecf84d86c3edf23aa0a', '127.0.0.1', '0', 1390249121, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bb8bd8dfb859a620fca2cbe2a0dbdc60', '127.0.0.1', '0', 1390256119, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bbbe2cbc3990a6b2c8746635c0d46c5c', '127.0.0.1', '0', 1390250296, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bbf9007c234334507e50cb6c27fa3eaa', '127.0.0.1', '0', 1390251422, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bc02f4d83add0127a05c0e63717c18d3', '127.0.0.1', '0', 1390248428, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bc060e7f9c62e5edaff7bd6442e6977a', '127.0.0.1', '0', 1390256005, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bc3707e994a1f2813660868696637c75', '127.0.0.1', '0', 1390256003, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bca2687917b6a3e18022d9463a593603', '127.0.0.1', '0', 1390250296, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bca653104c2af9816ab9297968966ab4', '127.0.0.1', '0', 1390253268, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bcb0b74248d99e33d2c906e5be39b2b6', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bcbb71ce51d49f3293304515b42eec22', '127.0.0.1', '0', 1390252340, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bcbf4326633250ceeb2245796b0c786f', '127.0.0.1', '0', 1390255950, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bcc5775ab9cf8960885834198d347fb5', '127.0.0.1', '0', 1390253269, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bd28d9f62da57bf6ee7416e4f057f4b1', '127.0.0.1', '0', 1390251991, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bd602ab13d47f199a4c955f77cf8c09e', '127.0.0.1', '0', 1390256901, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bdc2a8a3da15237a52f84c8a9154c83f', '127.0.0.1', '0', 1390252898, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('be0a43bc1181b6f5918d3abe5feb3cac', '127.0.0.1', '0', 1390256117, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('be28689858d83128c0986a891338ef2f', '127.0.0.1', '0', 1390251986, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('be7007cb8223ed5d1e1eda87ef5ba13e', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390852964, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:22:"juan.cantor@imagina.co";s:2:"id";s:2:"15";s:7:"user_id";s:2:"15";s:16:"current_user_one";b:1;}'),
('be917ebffbf3667e4dd9cbe00068e741', '127.0.0.1', '0', 1390255953, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bed1d594132fe5c73eaef2f58f7f0150', '127.0.0.1', '0', 1390252769, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bedae118feb39b16b722534d757d113b', '127.0.0.1', '0', 1390256119, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bef02bf9cc5f4450bd532c88ed123320', '127.0.0.1', '0', 1390252342, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bef29c1fc33ce137abcabe245bd76fb5', '127.0.0.1', '0', 1390249078, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bf063843bce56a84004d8c61ce857eff', '127.0.0.1', '0', 1390253692, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bf23300be18a2e7cc3e3ed958d8f99a2', '127.0.0.1', '0', 1390254226, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bf48159a970d8d01f13a764d03372bf3', '127.0.0.1', '0', 1390253684, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bf5a5daa27811326a62d7d421d1081c8', '127.0.0.1', '0', 1390252216, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bf5e8c4d6a91192b610926ba21da0c69', '127.0.0.1', '0', 1390249241, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bf67e06474899ca3c2437456ab4dfd84', '127.0.0.1', '0', 1390253965, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bf85c365e224bcd67a494bf54c5262a0', '127.0.0.1', '0', 1390254368, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('bf89704ba85227f32c1e05ebbd606180', '127.0.0.1', '0', 1390256401, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c03ccdb79c53bc05ad56ff6301b674ce', '127.0.0.1', '0', 1390252769, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c1306c84a57cc025cf535797f55bc95f', '127.0.0.1', '0', 1390256116, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c170dba1faf17b56ac632dfddb00bc72', '127.0.0.1', '0', 1390252903, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c211997a8efa4e1f8bf8e480e39a1e5a', '127.0.0.1', '0', 1390252777, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c242c5b49fb5eff3897470a7be871aab', '127.0.0.1', '0', 1390252771, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c26a537370303ddb37f8f1fed54911a3', '127.0.0.1', '0', 1390249372, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c28a0dfff405e9d538dc9aad28d75882', '127.0.0.1', '0', 1390256115, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c28e039baf52223ce0fe2880abcfa0b4', '127.0.0.1', '0', 1390256114, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c29b5194b9f599d4add3d6a6c6ac826f', '127.0.0.1', '0', 1390252219, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c2fb4bd5fd17e1a9f87cb8a731460c91', '127.0.0.1', '0', 1390248426, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c37c9cade85a8831e43b515671202856', '127.0.0.1', '0', 1390252771, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c38c1f627f055914faf1b9b2639c28e2', '127.0.0.1', '0', 1390253262, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c38e98da29dcaa5388f50fcee00c23af', '127.0.0.1', '0', 1390254231, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c3ca5eee5412c01ca0c4ef8e59366eb3', '127.0.0.1', '0', 1390251992, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c3f243664151efe905918b8674e17581', '127.0.0.1', '0', 1390253381, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c45b50d5c338ed7393872851bcfa4cec', '127.0.0.1', '0', 1390256225, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c46b426607a296f4ae4423cec7a8b0e2', '127.0.0.1', '0', 1390252350, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c49e23c037aa3d4038c7eb5fbf23f69d', '127.0.0.1', '0', 1390252224, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c4e69191dfb34be7bf5e94f2a57cf020', '127.0.0.1', '0', 1390248108, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c51c36b241f32f356b6ff6862126a5db', '127.0.0.1', '0', 1390254227, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c545b8d40fababda72c1e2bdec8766a5', '127.0.0.1', '0', 1390252773, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}');
INSERT INTO `cms_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('c58052891aa708838cda911a2276fbc5', '127.0.0.1', '0', 1390253258, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c589aed751d4fc278e53dd731a20ceeb', '127.0.0.1', '0', 1390254272, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c58c2e1cb4777d363cbbdfe4d3229f9c', '127.0.0.1', '0', 1390252893, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c5917b25a583e35f4a9fc1d033169278', '127.0.0.1', '0', 1390252889, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c59b277e9d23caa2ecc0015d4a007cdb', '127.0.0.1', '0', 1390256120, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c5c832decc5bcbfc778e513d13b7eb06', '127.0.0.1', '0', 1390256119, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c5f3027becf33001daa0651df43bff29', '127.0.0.1', '0', 1390253682, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c607a9b352e492fecb8f248095ca3727', '127.0.0.1', '0', 1390256009, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c617484a69ba0f7aff7aa0a993eb75c9', '127.0.0.1', '0', 1390252222, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c61d5e5dd57498e1157b838f2627ae11', '127.0.0.1', '0', 1390256113, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c63bc84f46adf57c31006b405f955df7', '127.0.0.1', '0', 1390252769, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c65916b4c697c355f94553e0cfe0ddde', '127.0.0.1', '0', 1390248430, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c670fd986ba3075fe38a64d6bb3f9f7f', '127.0.0.1', '0', 1390252240, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c6a4fc31ea14cc9946e513456f8a8803', '127.0.0.1', '0', 1390252895, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c70564d59260f945582314d6fbe05513', '127.0.0.1', '0', 1390252901, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c718f655c71b89c622df4655fcdfb1b9', '127.0.0.1', '0', 1390252777, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c72958b3159b2732d45e47b9126d9f59', '127.0.0.1', '0', 1390256007, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c7401f3f9636d24bf28dd663b9bac0b4', '127.0.0.1', '0', 1390252222, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c7b2ccef10b232bfe0330777fe11e3da', '127.0.0.1', '0', 1390256122, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c7c7bd6678d9dca86a111114b8f76070', '127.0.0.1', '0', 1390256119, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c7d18799b854fd2e967a6cdc71110bf9', '127.0.0.1', '0', 1390254263, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c830f61c052d8e199f143bc9f9d4dfb2', '127.0.0.1', '0', 1390252772, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c83511cb83677b978300123f58b921fc', '127.0.0.1', '0', 1390256231, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c87b3693ca857f37ab6a904875d6a25a', '127.0.0.1', '0', 1390256419, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c882bb74370cbab3af5c720e239620f9', '127.0.0.1', '0', 1390254223, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c8ad74b970deebf793c32e9de801fc93', '127.0.0.1', '0', 1390256231, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c8d0551db77b26a782f1142d5fd1f195', '127.0.0.1', '0', 1390254326, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c8ed8dcff9acb00e9615c2c270e63ebd', '127.0.0.1', '0', 1390249367, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c8edb5701e2b6c15a27c1156666aa97e', '127.0.0.1', '0', 1390253981, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c9052179358d9133a54358f3dbbc999a', '127.0.0.1', '0', 1390251164, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c96e274f2b0343e283f86f297f82e7ac', '127.0.0.1', '0', 1390254229, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c970981ae11f5fe006da5b5e340c045c', '127.0.0.1', '0', 1390253972, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('c9fac6f03a65b9b5e1562cc3d4682b61', '127.0.0.1', '0', 1390254345, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ca3f0477f6e145201e170fee8c7a346c', '127.0.0.1', '0', 1390252901, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ca5857e4636d06ebbee6fac5cd380a91', '127.0.0.1', '0', 1390252889, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('caad1fbd83931072ab73db2c00cd3637', '127.0.0.1', '0', 1390251422, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cabae9bf30c1548673e9be71f57c8f28', '127.0.0.1', '0', 1390254371, ''),
('cac319ad48c73d143d9574e152fef7de', '127.0.0.1', '0', 1390253273, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cb29fd8850a34b78b6d7107c8ccea01c', '127.0.0.1', '0', 1390256120, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cb2d3f8963b88afe7195cc469ed27aeb', '127.0.0.1', '0', 1390252888, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cb475ff0429495962c763ef9c6f20bbd', '127.0.0.1', '0', 1390252335, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cb47b35ddefbf490bed788ef217d0160', '127.0.0.1', '0', 1390252217, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cb55ef82c117ef5397d9649f546c36a7', '127.0.0.1', '0', 1390254366, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cb66de61dca9b16c5d661b8f241d9163', '127.0.0.1', '0', 1390253270, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cb91141972d6cce6cd105a3dc21cb896', '127.0.0.1', '0', 1390253696, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cbf0510098ca724df8ea0618cda5d216', '127.0.0.1', '0', 1390252770, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cc782530e4087e0028316b3113ce18f1', '127.0.0.1', '0', 1390252778, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cc86ff80f5ad383f36d57aef0f64c0a8', '127.0.0.1', '0', 1390253693, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cc931defed9d6a590f56310493e62b58', '127.0.0.1', '0', 1390253968, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ccb240541c4c2ff4cabd21d7e0f736e5', '127.0.0.1', '0', 1390253961, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ccc3798f6c4f97dd192c6a009e72f6c7', '127.0.0.1', '0', 1390256121, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cd23bcc8e69b427eb8edd03f9aead55c', '127.0.0.1', '0', 1390251428, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cd39ec0d4845664c7248db44f3545ad6', '127.0.0.1', '0', 1390253256, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cd635898323aeb1f3688d8c94ac5dde1', '127.0.0.1', '0', 1390251989, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cd67071a171245c10d55ec92591b9aac', '127.0.0.1', '0', 1390254229, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cd6f5ccaa440bbe2664a5cf9e8976c1d', '127.0.0.1', '0', 1390252768, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cd82f19af6130efacf9e0c22270ddfb5', '127.0.0.1', '0', 1390253689, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cdd8c2497af0ac014fe228363f3446fe', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390851601, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:22:"juan.cantor@imagina.co";s:2:"id";s:2:"15";s:7:"user_id";s:2:"15";s:16:"current_user_one";b:1;}'),
('cdfc6a3e855978ad416913e31da94866', '127.0.0.1', '0', 1390256001, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ce13c22d8815f3e548028fdd9b45e13a', '127.0.0.1', '0', 1390254324, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ce1b6f7bb50d7d5a30c786ab1b45dd88', '127.0.0.1', '0', 1390254367, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ce68b0a7eb7187c28f9c3a4918cad705', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ceaa851e6abd6f2aff5af95add4bce1c', '127.0.0.1', '0', 1390252338, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cedcb16fa3734c153e7c36fca2829d1e', '127.0.0.1', '0', 1390252346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cee7757074869cb69796608d71e937d4', '127.0.0.1', '0', 1390253969, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cf1047822e7ea18f04cf9af49e071c92', '127.0.0.1', '0', 1390249118, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cf25e6dfbe488f2552e7b12a30955952', '127.0.0.1', '0', 1390249118, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cf34d247a2d1cc9c18fff1f5aa7eccca', '127.0.0.1', '0', 1390253260, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cf477e90a1659db63d216fe5fbf8ae46', '127.0.0.1', '0', 1390253962, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cf51991e2eb64d8c6ec58fa6cf2f1cb0', '127.0.0.1', '0', 1390252777, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cf62e79dd420f1ca073c32980b82df8d', '127.0.0.1', '0', 1390253269, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cf7d5422a86ee1b9a24eb3594ec778ce', '127.0.0.1', '0', 1390252241, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cf99c5ebb8e07806b72cdc49a6bb0cf2', '127.0.0.1', '0', 1390249431, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('cfb49914ea1c5b7590bdf700cd9398b7', '127.0.0.1', '0', 1390252242, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d00e975dc190967af151c345731b833b', '127.0.0.1', '0', 1390256898, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d049f39b90629e5ba49cb0f28f446821', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', 1391613110, 'a:7:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;s:8:"identity";s:22:"juan.cantor@imagina.co";s:8:"username";s:20:"0.330176001391613110";s:5:"email";s:22:"juan.cantor@imagina.co";s:7:"user_id";s:2:"21";s:14:"old_last_login";N;}'),
('d04a9bf099128891d1ea18a857f6db8f', '127.0.0.1', '0', 1390253254, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d05b8b4a1b0ec60d49a8467fca5c3c35', '127.0.0.1', '0', 1390252221, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d06a2ef77fa04f87e67f759e09365e6c', '127.0.0.1', '0', 1390248428, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d083d7fc335b52db41c6f903e76fec33', '127.0.0.1', '0', 1390253701, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d0a688ccf4d897d27ef2d55525e6d8b5', '127.0.0.1', '0', 1390252888, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d0c682c785f66f3e6db9d0aa5f20215f', '127.0.0.1', '0', 1390252768, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d0d0f66f7296abb080988288c483ccad', '127.0.0.1', '0', 1390253964, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d0da7d63d3a1ea6935144b60471d4101', '127.0.0.1', '0', 1390252250, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d12cc9faab05a5b19f6dc1c5d6d89bcd', '127.0.0.1', '0', 1390252224, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d13e92194fcc50e54979c3d0fe47df63', '127.0.0.1', '0', 1390248428, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d15319d975f30696358be8f196a2f2b3', '127.0.0.1', '0', 1390254226, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d18e4ae93b5378aa3093df30ceba0e30', '127.0.0.1', '0', 1390256005, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d1d87a69ce2239974111667ed8e5a097', '127.0.0.1', '0', 1390253256, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d2023a4220b3c7281dd98aa547381c84', '127.0.0.1', '0', 1390252343, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d2062f3928bbe32cd52eafb9bdea75da', '127.0.0.1', '0', 1390249076, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d20934e452febc2cc944c07fac15ac92', '127.0.0.1', '0', 1390253973, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d2102698b2000acb836576f802cc61b0', '127.0.0.1', '0', 1390252229, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d241bd183fd71940cfb543193584fc50', '127.0.0.1', '0', 1390253253, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d267eeb8958c7a92bbac896116a01dc9', '127.0.0.1', '0', 1390252900, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d31a5ae88d01d5eae47d4c3357a2b5d9', '127.0.0.1', '0', 1390256003, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d35484ad471ed03136cee269698c7800', '127.0.0.1', '0', 1390256400, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d3808bb5e0ac4795b61ac17608e5e37c', '127.0.0.1', '0', 1390252891, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d3883931a61cbcdf751ce13d0e7143aa', '127.0.0.1', '0', 1390249429, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d38a6c890a4a25df2c286959f7640375', '127.0.0.1', '0', 1390253689, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d392988c6bcf309a652dee91818739f8', '127.0.0.1', '0', 1390253258, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d39e6267f6db2602edb49e3f0182eab1', '127.0.0.1', '0', 1390253273, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d3bedda891fdb3c31aa40d5859ca417f', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d3ca23da5259d71f018b12fe8b55a770', '127.0.0.1', '0', 1390251988, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d3ff0a1e0cebd6ff7f3153ae8758ca93', '127.0.0.1', '0', 1390250315, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d41024c5b0da664139df950adf73c3fe', '127.0.0.1', '0', 1390252221, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d430e2fa11000cc045282b73db27f613', '127.0.0.1', '0', 1390253682, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d4496eb09e452fcbe335df787d266495', '127.0.0.1', '0', 1390253387, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d46e109fed485467221a0f9ddc2ed8b9', '127.0.0.1', '0', 1390252897, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d49ad13a192bde497348bc955c8475ea', '127.0.0.1', '0', 1390256123, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d4ba88cb2cbd290dc8e76b3a73a10fbb', '127.0.0.1', '0', 1390254323, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d51340874cededf93d3552cde256ea2c', '127.0.0.1', '0', 1390255975, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d51d3f143756650e8391c886adeb1fa5', '127.0.0.1', '0', 1390252890, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d559d4ca0670cc555ca352c4e55f7d5f', '127.0.0.1', '0', 1390256903, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d5615447d2e5dd85208262fc9bc66b70', '127.0.0.1', '0', 1390253965, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d59d1614c8f66c133154e6b9462abcb4', '127.0.0.1', '0', 1390253255, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d5b3460ee0407937269e91c6b2166879', '127.0.0.1', '0', 1390252215, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d6053c3f4c4aa5f5b067fcbe3de3ee69', '127.0.0.1', '0', 1390252902, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d65838f1fd16daae360795089cd379db', '127.0.0.1', '0', 1390253269, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d65c7da3f9cb16a968fe434512411366', '127.0.0.1', '0', 1390254256, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d670172b3e4ee6408b9979f993178f3f', '127.0.0.1', '0', 1390254368, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d675f7a620ff03a652ec6aa1b7dde01f', '127.0.0.1', '0', 1390252900, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d67e4742d7cdca8cb7117b20b6cafd60', '127.0.0.1', '0', 1390252767, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d680049279ec537476f8e47140cc7ce4', '127.0.0.1', '0', 1390249558, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d680c48bdde1314f020402c83565e0f2', '127.0.0.1', '0', 1390256122, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d68e0a87303aa12c1f989cfbc9185d85', '127.0.0.1', '0', 1390252340, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d6e23f93cf3db236c1fb467a2bed080c', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390853812, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:22:"juan.cantor@imagina.co";s:2:"id";s:2:"15";s:7:"user_id";s:2:"15";s:16:"current_user_one";b:1;}'),
('d6f9effea2669ad5317a912794947451', '127.0.0.1', '0', 1390253260, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d71942ca1daa42efda83449fae9afb67', '127.0.0.1', '0', 1390252772, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d74b600456095721fa5d593f6f5ee709', '127.0.0.1', '0', 1390254241, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d74ddc53ed200f632157cedaa7ab35da', '127.0.0.1', '0', 1390252894, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d767d5888eb876b20e8b9eb8e75ce229', '127.0.0.1', '0', 1390256113, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d77bdc138afa99957ab13060ba0bec58', '127.0.0.1', '0', 1390253968, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d7aad8016ac3419ecdcf9211bb2db2c2', '127.0.0.1', '0', 1390252899, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d7e8e2081087ba1246ae1b4d20f04392', '127.0.0.1', '0', 1390253974, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d862e185ee9e4f8aa3d9ced0dbe32ff4', '127.0.0.1', '0', 1390252887, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d87567aa47b28705133171b3e99dbe1a', '127.0.0.1', '0', 1390252896, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d8b564392fa9dc9a547aafd67fd2741a', '127.0.0.1', '0', 1390252235, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d8d31b832dabfeebaaaa2eb2afe7879c', '127.0.0.1', '0', 1390252342, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d91cf2d614fc34eeed4b9d1206a8597c', '127.0.0.1', '0', 1390253382, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d92e6270004f5d64a4f958a5910ebe4e', '127.0.0.1', '0', 1390254231, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d9613ba3a90e91a9648fdbd0ddd3ef90', '127.0.0.1', '0', 1390254235, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d96172cb51cdd593640710b242f7d91a', '127.0.0.1', '0', 1390252902, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d9863420711b736d730b2eb658883a20', '127.0.0.1', '0', 1390249119, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d9da3dd09caf55d941884505410624fd', '127.0.0.1', '0', 1390253971, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('d9e8a0bcc18404fa4458e48648615718', '127.0.0.1', '0', 1390252217, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('da148e3d94cc7e96b96e2e1025951e95', '127.0.0.1', '0', 1390249429, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('da1cf7fd252238900fb0f6c1d0adff7d', '127.0.0.1', '0', 1390249368, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('da329ccb789117a812e51abfcde8f9e2', '127.0.0.1', '0', 1390253962, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('da3437cfa0fa1c2185668981e8fed535', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', 1392304564, 'a:7:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;s:8:"identity";s:22:"juan.cantor@imagina.co";s:8:"username";s:20:"0.330176001391613110";s:5:"email";s:22:"juan.cantor@imagina.co";s:7:"user_id";s:2:"21";s:14:"old_last_login";s:4:"2014";}'),
('da5737ffa4be473ea65aa03ed4b6eb82', '127.0.0.1', '0', 1390254347, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('da69ebb8495250017b5f3ab7b5475510', '127.0.0.1', '0', 1390256121, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('daa5273cc0e4f25beaa134386ed91fca', '127.0.0.1', '0', 1390253681, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dab2df055ba47c6de8e18939664e2ae2', '127.0.0.1', '0', 1390252901, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dabd205c334cab775f4cba99eb64bcbc', '127.0.0.1', '0', 1390256899, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dadc6c8efff2f0b79a8018308a854c45', '127.0.0.1', '0', 1390249366, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('daf25bc72e2c1e5000fff5aeb40e5fce', '127.0.0.1', '0', 1390252218, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('daf67fcf25d7d0c4abd7970c829e4132', '127.0.0.1', '0', 1390252893, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('db2404fa34abbbfc2ee2e09a8857fd93', '127.0.0.1', '0', 1390253964, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('db37b95206cdff4563021cf10ebff3ea', '127.0.0.1', '0', 1390254367, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('db436bddf7175fd73321458cbb9fb29c', '127.0.0.1', '0', 1390252356, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('db5585e1ed3315b6822aae151a21dd9e', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', 1392300958, 'a:7:{s:9:"user_data";s:0:"";s:8:"identity";s:18:"cms@imaginamos.com";s:8:"username";s:13:"administrator";s:5:"email";s:18:"cms@imaginamos.com";s:7:"user_id";s:1:"5";s:14:"old_last_login";s:4:"2014";s:13:"page_recipess";s:1:"1";}'),
('db5c2661104ef90a83fb97367dc6f873', '127.0.0.1', '0', 1390252246, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('db5ef8f21ce1d8ea6df8099f6391d751', '127.0.0.1', '0', 1390252892, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('db84231c3ddc4562bdf477c2e2bc339b', '127.0.0.1', '0', 1390252339, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dbacf4c7c1631584a83f1251575e976b', '127.0.0.1', '0', 1390253372, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dbb2d875d6dca2dbb58a4ccdc9350b14', '127.0.0.1', '0', 1390256121, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dbf39663b2ac967c41063382dd2e1b0d', '127.0.0.1', '0', 1390252247, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dbf6fc1edba5e60449d966bed9869257', '127.0.0.1', '0', 1390253254, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dc01b06bb6d0111375a1f60725440ea1', '127.0.0.1', '0', 1390256115, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dc1c0e31be4a7f0fbc6c3226f9e4748b', '127.0.0.1', '0', 1390252227, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dc42aa97ab7c480dc698f1988c5de181', '127.0.0.1', '0', 1390256007, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dc54ba39f5c14739ffe8c07b492ba394', '127.0.0.1', '0', 1390249373, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dc62a425646fb1c026344c2fc7fa96f3', '127.0.0.1', '0', 1390253984, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dc699d24ef5679e21896e7dcde8e8dc2', '127.0.0.1', '0', 1390253271, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dc78353c07a67661b1f02c853d0eb00d', '127.0.0.1', '0', 1390252767, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dc878f88003a377d0f091fe4eb049671', '127.0.0.1', '0', 1390255976, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dc9a9169173833b8b04d5d397e3377ad', '127.0.0.1', '0', 1390252766, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dc9e06be2ff550cba9cc0d4e68ceecb4', '127.0.0.1', '0', 1390253685, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dcafe2e37230efd10496372f13a3cf40', '127.0.0.1', '0', 1390251424, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dd0b43954ddc90e961cd405ef8535604', '127.0.0.1', '0', 1390252336, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dd3d4db5847937731f623d57c01544ba', '127.0.0.1', '0', 1390253686, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dd3f5e353cb75890668d067a1c0358f7', '127.0.0.1', '0', 1390251989, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dd843eab2beceebea238c6fdb29d0498', '127.0.0.1', '0', 1390256008, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dd879cb4398f03e9a270c137b88013b5', '127.0.0.1', '0', 1390248430, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dd9eb1318b1aab33dcd560a17a6738e0', '127.0.0.1', '0', 1390254327, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ddabfa836629a42787735e1c87942dfe', '127.0.0.1', '0', 1390256988, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ddafbb79293f3bb75df19e328ce5090b', '127.0.0.1', '0', 1390249558, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ddfdf8b4d55ef198a19e70128446f083', '127.0.0.1', '0', 1390253698, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('de08ebf2a538cac4466b8112431bfef0', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390851600, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:22:"juan.cantor@imagina.co";s:2:"id";s:2:"15";s:7:"user_id";s:2:"15";s:16:"current_user_one";b:1;}'),
('de4087d80aad8fcf66d787664faa7cc7', '127.0.0.1', '0', 1390252251, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('de496c7385dc5206fe8b5e453de970d6', '127.0.0.1', '0', 1390248357, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('de5c144ca6ca845f5adbcff1faaf59de', '127.0.0.1', '0', 1390252776, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('de848f6a3720aabdb7abf4799e86d1f0', '127.0.0.1', '0', 1390253682, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('de95597f99e1434cf99dc13fb1b43555', '127.0.0.1', '0', 1390254371, ''),
('de96114b4e937681ff70f9b04cea90a8', '127.0.0.1', '0', 1390254326, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('deaafcc393c9072fabcb051d3afb7df7', '127.0.0.1', '0', 1390253386, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('deadb5c2b547e4291453ff5681fd2de8', '127.0.0.1', '0', 1390252233, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dec7082f51214b0af802ba49d929dd13', '127.0.0.1', '0', 1390253978, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('decc72afb753aa6aa7b30e7addd2fc4b', '127.0.0.1', '0', 1390252902, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dedb53e485b6123dedc8d508361a19f2', '127.0.0.1', '0', 1390248357, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dede088bfb8351a4d86202dd493e2e36', '127.0.0.1', '0', 1390252890, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dee863352622cbc6fc9943d3aa640c25', '127.0.0.1', '0', 1390254323, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('def735631db42746b94a7ed32bd34c0a', '127.0.0.1', '0', 1390253380, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('df2aa2ad779d1f1a2eff1a15056787ce', '127.0.0.1', '0', 1390256117, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('df63f982a940c5dc774c6fe9d582e99f', '127.0.0.1', '0', 1390252336, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('df677f562692465dcf57e419ffe7d725', '127.0.0.1', '0', 1390253695, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dfabe73e09f67a25d8f3ed0a4e46e39e', '127.0.0.1', '0', 1390254226, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dfbb35ceadee3b4fd37902417aaecf42', '127.0.0.1', '0', 1390253253, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dfcc15949d7da6d8a0331edb0931510b', '127.0.0.1', '0', 1390249367, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dfd6fc3cf27d65b3cd67c44037143514', '127.0.0.1', '0', 1390253271, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('dfe6f15c7822c253c02548a5bdfe1618', '127.0.0.1', '0', 1390254372, ''),
('e0549949165302a58fe62ca13c80fdc0', '127.0.0.1', '0', 1390253688, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e05c33d9770882ab5e5af46bc08289d0', '127.0.0.1', '0', 1390253267, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e06fe43cedede7097e8a820f9bef5c20', '127.0.0.1', '0', 1390252898, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e075c319cda1a96613e35153d07c6d29', '127.0.0.1', '0', 1390252220, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e09999641447a38433f2339b92b9b701', '127.0.0.1', '0', 1390251423, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e09e7ef8e7f98d1ac73fc026d7f9a22a', '127.0.0.1', '0', 1390253697, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e0adf3c39a6d0b2eabcc8cdd0e6a8d29', '127.0.0.1', '0', 1390255951, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e0b426cf89aca467c10b32449df7a962', '127.0.0.1', '0', 1390253379, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e0c665123bfc174021fc2d9c4fa27cea', '127.0.0.1', '0', 1390256121, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e10796b67bbfeeae42f9ebec821c3317', '127.0.0.1', '0', 1390251164, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e17903f47fce3d4f70d7904bd7a38bb3', '127.0.0.1', '0', 1390249076, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e18e83c1723c9b8f9d62a81fcfbbb209', '127.0.0.1', '0', 1390251991, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e19c54283ad218f7d47ff257aafe2551', '127.0.0.1', '0', 1390254326, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e1df7030fc57cff79c214da606620d43', '127.0.0.1', '0', 1390254368, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e23b4489d0624503f2df24e871819828', '127.0.0.1', '0', 1390254369, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e24efaa78561d75ed921b3558de5a663', '127.0.0.1', '0', 1390252219, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e25b8d8ef91fad52217e6ce5c9f4023e', '127.0.0.1', '0', 1390256113, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e2d8fb47662b9e73be8bf43e3c5f1c9b', '127.0.0.1', '0', 1390254254, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e2d97afef0762b2a38031a9573b3278e', '127.0.0.1', '0', 1390251164, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e312f158f64d19698af65588f2986736', '127.0.0.1', '0', 1390253681, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e38584769fc8fb81fda8c2dc66499aaa', '127.0.0.1', '0', 1390252893, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e3a51edf03eecf2ffc33d8bcb0126cb4', '127.0.0.1', '0', 1390256006, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e3b39c334d761ae9fb1e34e707457270', '127.0.0.1', '0', 1390253963, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e3e165a9928ee52a78b9eb501c02480e', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390852964, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:22:"juan.cantor@imagina.co";s:2:"id";s:2:"15";s:7:"user_id";s:2:"15";s:16:"current_user_one";b:1;}'),
('e3ebcd379fbb6ecc62b206cf042a5127', '127.0.0.1', '0', 1390256114, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e40046e4bb86b39f14e075a2433a0969', '127.0.0.1', '0', 1390256901, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e402d6acec382996d7f486032bb447a5', '127.0.0.1', '0', 1390255975, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e4045863a164bf3fae013c9b5db5e01e', '127.0.0.1', '0', 1390252777, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e407fa1cd6ddbe373e14cc5877f40254', '127.0.0.1', '0', 1390252346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e40bfbbcbc3f1522be4dca43c5b8d671', '127.0.0.1', '0', 1390252901, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e471b5f4bd22c485ed4bc7bf6126d488', '127.0.0.1', '0', 1390253690, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e4783bc40a768ddceef5cbace9201a46', '127.0.0.1', '0', 1390252887, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e522e7755bca3915e81c76b21cf211fc', '127.0.0.1', '0', 1390252343, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e53b832c3651171ed947789d54fa2d26', '127.0.0.1', '0', 1390249079, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e54ba565c0a5fe01d097779fce366167', '127.0.0.1', '0', 1390254230, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e5554307a09463b1d96551aab2bc8814', '127.0.0.1', '0', 1390256008, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e5730dbf502450a8fac80f5c04e3d285', '127.0.0.1', '0', 1390252228, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e575cdafa9c22e9004f52976470331cc', '127.0.0.1', '0', 1390252895, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e592135a377acff4375d49c276c162a4', '127.0.0.1', '0', 1390254235, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e5bdebde810fbfac6d5830927c304d5d', '127.0.0.1', '0', 1390253259, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e5d5ed27c60a8605f6e6e94986d10d2c', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390254220, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:16:"current_user_one";b:1;}'),
('e6028f5c20d7027817c0bfb8bf6cc1a0', '127.0.0.1', '0', 1390256112, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e61cfab1f008612aae5522b5cb91487d', '127.0.0.1', '0', 1390254234, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e61f345e4770e46312612fecfbb6bdfc', '127.0.0.1', '0', 1390252901, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e62c459cfcc1452342cb61a3eed570a3', '127.0.0.1', '0', 1390256009, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e63dc9b1b15c48a7ff25224e3a99a2bf', '127.0.0.1', '0', 1390253256, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e6465db84255e9de72d27ec028c79424', '127.0.0.1', '0', 1390253266, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e647b2819112a6817e188290cb54a93a', '127.0.0.1', '0', 1390256005, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e67703c1b9abd982eccc04ccca682038', '127.0.0.1', '0', 1390253258, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e67c49fac3ddad2f18f332931c4a81ee', '127.0.0.1', '0', 1390249236, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e6946613f510c49deed341fd42b769e6', '127.0.0.1', '0', 1390253686, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e6b80084732720de7be3b8f0b5b7043b', '127.0.0.1', '0', 1390252897, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e6ba0b6fde87c664c84ae4d94ff568c3', '127.0.0.1', '0', 1390252775, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e6c6466c5cede4697640dc0c0bf6b7d1', '127.0.0.1', '0', 1390256112, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e6ca87147b7e6ac3ad5eea83c33b337e', '127.0.0.1', '0', 1390254371, ''),
('e6cf4385decbef9dd86424ab3fa9867f', '127.0.0.1', '0', 1390254373, ''),
('e6df33db738cf60acafccb37f02bf9c2', '127.0.0.1', '0', 1390252354, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e6e9017ab8f666d3395cf922e227c8ea', '127.0.0.1', '0', 1390253684, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e6f97bcce440aa499f433bd1450fec36', '127.0.0.1', '0', 1390252776, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e7a5120ab2a7471e0d0bb8da114f332e', '127.0.0.1', '0', 1390254363, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e7b9ffd4dd6d6e909a6f38f89c362c7e', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.102 Safari/537.36', 1391551535, 'a:7:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;s:8:"identity";s:18:"cms@imaginamos.com";s:8:"username";s:13:"administrator";s:5:"email";s:18:"cms@imaginamos.com";s:7:"user_id";s:1:"5";s:14:"old_last_login";s:4:"2014";}'),
('e7c24bab5f56c9cefd8191f161bcb654', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', 1391611104, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e7cf9e38ea5ff11c66520b619d176902', '127.0.0.1', '0', 1390250316, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e7e8fd7b6b709e99d9fb27521cb8c725', '127.0.0.1', '0', 1390253268, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e8030c0cde4aad733ee9deb00560432c', '127.0.0.1', '0', 1390249080, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e809b3a365fc39ced41efdce2d3afe2f', '127.0.0.1', '0', 1390250317, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e850b50fde55036562e43381152dc3d0', '127.0.0.1', '0', 1390253683, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e851e2b728a4afc067fb9a9586a7b1c4', '127.0.0.1', '0', 1390253265, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e87985d2f51e2ed2ff9b0724e1e792df', '127.0.0.1', '0', 1390252220, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e87dcf8b125636f3680f683fb88f1779', '127.0.0.1', '0', 1390254221, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e8ba460478e790b6429ccbbd05f3993f', '127.0.0.1', '0', 1390256902, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e8e1067d75dbcd2072a0a1f63fd64458', '127.0.0.1', '0', 1390253698, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e90a49b1e6ec25ebae14498b0d4f8c23', '127.0.0.1', '0', 1390253265, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e92023dbdfa5e8aeb468b4d04d9c71b8', '127.0.0.1', '0', 1390252893, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e93d50e1635526aee3a915055e125a06', '127.0.0.1', '0', 1390253976, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e9553a218e937d12cd3400b3071730fd', '127.0.0.1', '0', 1390254359, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e96ee2c9fbd738e4b6620482d0aa83d7', '127.0.0.1', '0', 1390256123, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e989ac04f1d181bc295e130445bf8da9', '127.0.0.1', '0', 1390252775, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e98c6baa1009ed8bf19946f0b8d6e4fe', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e9a89af8a7a35e30b164387c869eb206', '127.0.0.1', '0', 1390254231, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e9d0746d14f9cfc7b02517ff6a05d0ed', '127.0.0.1', '0', 1390254366, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('e9ed683c44c18e8cf0abe7a4959146bf', '127.0.0.1', '0', 1390253384, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ea1a667f873cde2e07449892b2a76f41', '127.0.0.1', '0', 1390253967, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ea227a7e5dea52ddda64fdec82196eef', '127.0.0.1', '0', 1390254325, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ea3adb31a7c4e2cff710d81ebc4904d9', '127.0.0.1', '0', 1390254222, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ea5387af0f3f17edc193fcc1356806fc', '127.0.0.1', '0', 1390252777, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ea71f7a57f1adc9ad33db3a99e2c3a02', '127.0.0.1', '0', 1390251167, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ea9e4f03bd43b12fb5c6f202fd2b681a', '127.0.0.1', '0', 1390251987, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('eaa1cdb2b493161cf1d7ae1882978055', '127.0.0.1', '0', 1390254237, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('eacacd1e4137a7c2e8eb66c9ad30972f', '127.0.0.1', '0', 1390251990, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('eaf4bc94b9cb238631ab688391c238bb', '127.0.0.1', '0', 1390251163, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('eb3172b2022b8cd0cbaaeef6150e9e7b', '127.0.0.1', '0', 1390249122, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('eb65918ed92700cf4a897259f9741a6c', '127.0.0.1', '0', 1390249366, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('eb703b030bc518961c134c0245318792', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('eb7265c074d59989408f15f0d8020686', '127.0.0.1', '0', 1390256113, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('eb8d1d4a325e9a989d0c5296527b9bd5', '127.0.0.1', '0', 1390253699, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('eba884e7a77a0d5deed88041a19f4852', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('eba8ca088c72627ec47c7e48f0a0eac3', '127.0.0.1', '0', 1390253974, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ebb03315ba51f1d346bc3addd5fe5187', '127.0.0.1', '0', 1390256117, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ebde78747888c59d3131646ec938da8a', '127.0.0.1', '0', 1390252351, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ec25e58e5f498e46f7ec37e077bad092', '127.0.0.1', '0', 1390253974, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ec29f488fd9ae4e3bc23cc37b25cc00d', '127.0.0.1', '0', 1390255964, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ec4c12bb032d9a94c32ec95abc5035fb', '127.0.0.1', '0', 1390251991, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ecaa8449580c3004d009c11441da0e86', '192.168.169.26', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.102 Safari/537.36', 1391019395, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ecb1bc519bce6514180d4c56f11a6053', '127.0.0.1', '0', 1390253694, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ecbb83db22f86f63fe69dd8fadec1f6e', '127.0.0.1', '0', 1390254230, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('eceb65f90583a330b0dee71cce4adaec', '127.0.0.1', '0', 1390253263, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ecebbc501894a16797b425af89675edb', '127.0.0.1', '0', 1390254367, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ecf4d5816f9d460370da2184b5fed84b', '127.0.0.1', '0', 1390252901, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ed13b8a7f4f9c6bf55877226ba08e9dd', '127.0.0.1', '0', 1390252901, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ed16f052bace10dba127a33aabc0c8cf', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390853812, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:22:"juan.cantor@imagina.co";s:2:"id";s:2:"15";s:7:"user_id";s:2:"15";s:16:"current_user_one";b:1;}'),
('ed1d83e2d05c3ef3f9cb2ee89e6c4a8b', '127.0.0.1', '0', 1390253984, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ed1da97f8ac75a15bf8dcb0881d7e5ff', '127.0.0.1', '0', 1390252768, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ed6a7a4954c0626860ca09a5f17606f5', '127.0.0.1', '0', 1390251989, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ed77e4e91768ea7c99afab7ca3d8bae4', '127.0.0.1', '0', 1390252231, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('eda1a4b6dbd36f96d59c88fcf9f35512', '127.0.0.1', '0', 1390252217, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('edd6cc5c039ea73a00fb3b520bd7aade', '127.0.0.1', '0', 1390252340, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('edd8653bf030cae28921f25e757211b4', '127.0.0.1', '0', 1390250295, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ee066108d4f5c2287c31d25a56909e6a', '127.0.0.1', '0', 1390248357, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ee2f79a6d4652903291b9774a7f151c8', '127.0.0.1', '0', 1390250295, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ee4e75ae56e283154e30d72c9ac58458', '127.0.0.1', '0', 1390253694, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ee9128ab28d2fbb864d0c9cb0716d7eb', '127.0.0.1', '0', 1390252772, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('eeb09704dbed6bc5fdd0747edc9ba53a', '127.0.0.1', '0', 1390251427, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('eeb549cd018e929fd8ac468bb5fe4800', '127.0.0.1', '0', 1390254366, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('eebfe41a52435651c674fb0381ecfc4d', '127.0.0.1', '0', 1390254359, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('eed7f8c63f958bd782898a78480ab756', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('eef90055c69abd7825c8f744ac81f870', '127.0.0.1', '0', 1390249431, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ef1f2dc5f9bc8472154969ee5083db81', '127.0.0.1', '0', 1390253274, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ef769a2780806729ad158640cf6067bc', '127.0.0.1', '0', 1390253274, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('efabac1fc6e18808cdadaee1de4f5cee', '127.0.0.1', '0', 1390252775, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('efde6fb6cd6e7c7345397d0088cd349b', '127.0.0.1', '0', 1390252902, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f0152b648098c9d3cedc703f3561c277', '127.0.0.1', '0', 1390252777, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f01801bf7c5da9567e23f5aba74f0f69', '127.0.0.1', '0', 1390253261, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f034665ceb34eb5844d02290dee2d9a4', '127.0.0.1', '0', 1390256902, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f04e6111136a7e31fc5ad5514f8b6881', '127.0.0.1', '0', 1390254366, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f08f5d210d17c4c178c1b2d284791059', '127.0.0.1', '0', 1390252337, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f0e9835e94f45f39ae26ef9ca8071693', '127.0.0.1', '0', 1390254327, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f109cf4761c43b0cd14883eb7a55e897', '127.0.0.1', '0', 1390252347, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f148caeb861eed0365706d9e2e83541f', '127.0.0.1', '0', 1390252340, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f15f2e05a99d22de611582a6cdac1a92', '127.0.0.1', '0', 1390254221, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f15fbda9a5e136d92194b3d78080453d', '127.0.0.1', '0', 1390252767, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f1745e74b7f3c72fd33b2fcff4b963c5', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.102 Safari/537.36', 1391556823, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:16:"current_user_one";b:1;}'),
('f1bbc31c48f66d09db3f635a1fd835e4', '127.0.0.1', '0', 1390252894, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f1cfca4c95521e3de8a1c2987fb60333', '127.0.0.1', '0', 1390254223, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f1ddaaaa83db029a96ab3e89749ad967', '127.0.0.1', '0', 1390253262, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f1eacb328a8a07a2397f47665b005c79', '127.0.0.1', '0', 1390256009, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f1fdcb33333dcc0569f211b42a0625b4', '127.0.0.1', '0', 1390252215, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f200ef71cd5cad31f2a190599b773537', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390252757, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:16:"current_user_one";b:1;}'),
('f22beeb8549ae2dc2ffe28642d71cac5', '127.0.0.1', '0', 1390253691, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f2381569a695ad1174897d5dce302d50', '127.0.0.1', '0', 1390252889, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f277335daf1194349437b4ed49be108e', '127.0.0.1', '0', 1390252236, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f281fcd7d1fd4e4b30beb3b15444a1b6', '127.0.0.1', '0', 1390253682, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f283d4084eacd0d85446d2807fb5bc49', '127.0.0.1', '0', 1390254236, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f289be8ee8eb83821ef3e1114043f442', '127.0.0.1', '0', 1390249372, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f2a96703d0a9d72fb391f2c44c88ed5c', '127.0.0.1', '0', 1390251166, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f2c1b72e7c2540889d2b0daba7d99884', '127.0.0.1', '0', 1390253267, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f2c27c45cb18a3dacdf48b801d6d07ec', '127.0.0.1', '0', 1390252345, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f2d2e837fa635f98ce9a1be5f3f8fe4d', '127.0.0.1', '0', 1390252774, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f2e91204b3ea2e4245066b91f201453c', '127.0.0.1', '0', 1390253269, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f306c65a38f627b1718a342de8303e5d', '127.0.0.1', '0', 1390254370, ''),
('f309057da4965e3bbc03bc29d1e69342', '127.0.0.1', '0', 1390256118, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}');
INSERT INTO `cms_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('f323e70b1b6c369abe740ba92607c579', '127.0.0.1', '0', 1390253688, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f32e1e49dc419657afde313d752d54a3', '127.0.0.1', '0', 1390253257, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f33776f0488f5ff64b135a8799d6b782', '127.0.0.1', '0', 1390255950, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f33983960677c17dfbf2f95efb29feb5', '127.0.0.1', '0', 1390249370, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f347361347bf2c973b5aac0b26341d96', '127.0.0.1', '0', 1390252214, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f36f1dca810e86473708703ab90732cc', '127.0.0.1', '0', 1390252226, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f3785529c858f24c027f2422fd45bd2c', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390952663, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f3a3f172549c79996b6f060800afe1a9', '127.0.0.1', '0', 1390248107, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f3f6ce68e9c47b5916f6ce620e87a35f', '127.0.0.1', '0', 1390249431, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f4020346419c1de698b95bef7312a7d7', '127.0.0.1', '0', 1390253974, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f4498fa2abae77a134f30abaa39bb2a4', '127.0.0.1', '0', 1390254372, ''),
('f46bcb85f9ae875353e0e549ee9d6f3e', '127.0.0.1', '0', 1390256400, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f476d092e188122f648498292fee330a', '127.0.0.1', '0', 1390253265, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f47fd7f9312ebfb5e706ff72ae4fc409', '127.0.0.1', '0', 1390253975, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f4b26ef93d9955477dd961b996d4ab63', '127.0.0.1', '0', 1390252224, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f4c97fca95ebf10267d2c7d2d856535e', '127.0.0.1', '0', 1390253268, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f4cd656fbbb047b8b8aabcf674c8e778', '127.0.0.1', '0', 1390254366, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f5568ddeda0f364fd9f08e7f7afb9773', '127.0.0.1', '0', 1390251425, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f579eeb2c295a7047497a21424104b28', '127.0.0.1', '0', 1390252767, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f58642d9f9d0ab40121e7cb734c193cc', '127.0.0.1', '0', 1390252347, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f5981776825418b43e9987575466c7d0', '127.0.0.1', '0', 1390252342, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f5a02318f77a62ee302ef94728eceb9d', '127.0.0.1', '0', 1390252900, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f5fe24f374aea155c57d288df828dfa2', '127.0.0.1', '0', 1390253693, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f615bd7da90d15961951f28d835fb1c4', '127.0.0.1', '0', 1390252225, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f6185797933deaac28cf3fa653dd86fa', '127.0.0.1', '0', 1390254363, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f62753de9f58183be7a46bcc746fd175', '127.0.0.1', '0', 1390256111, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f63d004abada16343698afca8b34dfb9', '127.0.0.1', '0', 1390252769, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f677d218a958bd398015d40f5bcd33c9', '127.0.0.1', '0', 1390254368, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f67f83d4b32174d7f3ea85f2981d0d05', '127.0.0.1', '0', 1390254269, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f6b870cb7b2f3f713309cd8051824739', '127.0.0.1', '0', 1390253271, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f6ce9435c920786e91291458d32a184b', '127.0.0.1', '0', 1390252778, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f6d1eb20f459d118cb890d4738d2c16a', '127.0.0.1', '0', 1390252892, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f6dc06a648d92e630f8e0fce89c8d6ec', '127.0.0.1', '0', 1390255951, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f708feb80a3e700423ae6a40235883c7', '127.0.0.1', '0', 1390256007, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f728d9b8170e0766de6a971be571e8b0', '127.0.0.1', '0', 1390251987, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f72c1001a9485489df6a08757eeb7db2', '127.0.0.1', '0', 1390252888, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f73bc2679f70319d5f778bacbf0c617c', '127.0.0.1', '0', 1390249239, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f76173e79e414e5660dfae9475bbc671', '127.0.0.1', '0', 1390253983, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f7af6aaf40b69b986a2c8d98fb1dd3c3', '127.0.0.1', '0', 1390252893, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f7fcdc602ce13c9115a6a59616a851b1', '127.0.0.1', '0', 1390253685, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f8116d66948bae74f806cc1d1758f259', '127.0.0.1', '0', 1390254368, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f8344ccc8cb9ef8bdcc686c86f9e396d', '127.0.0.1', '0', 1390253976, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f83caa98147c95172da84886b601c9a0', '127.0.0.1', '0', 1390253978, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f88506036bdaef9fdd8182599f5729c7', '127.0.0.1', '0', 1390249371, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f889f637828b43c17fcff5905fb6d215', '127.0.0.1', '0', 1390253701, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f88ad17f7db11da00bd27822d4a658ff', '127.0.0.1', '0', 1390253252, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f88ebc1c37cc9359d8fc984ad59b1c1d', '127.0.0.1', '0', 1390252766, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f92798f575d031cbd0a731e711c23f0c', '127.0.0.1', '0', 1390253260, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f948270cd6603a79952db40705c7045f', '127.0.0.1', '0', 1390255952, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f97696808686f907cab2dac2c9ed7126', '127.0.0.1', '0', 1390248107, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f977e055c50a8b5bb0d4e31ca04bf628', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390853811, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:22:"juan.cantor@imagina.co";s:2:"id";s:2:"15";s:7:"user_id";s:2:"15";s:16:"current_user_one";b:1;}'),
('f9906db43ba2608265b3052b2475fd36', '127.0.0.1', '0', 1390256118, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f99323aae1f7a331e0ae0e4cc54277f9', '127.0.0.1', '0', 1390256122, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f9a23804d6b948cb20a8c237d62b21b3', '127.0.0.1', '0', 1390254346, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f9adabb44ef03336acb4efa7b8fc963f', '127.0.0.1', '0', 1390255975, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('f9ef63aec4bccae213725db4bc08e550', '127.0.0.1', '0', 1390253257, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fa843fa6accf2911b1d75a659db065f4', '127.0.0.1', '0', 1390251426, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fa95ede34d7574330532f3a523ad564b', '127.0.0.1', '0', 1390252767, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('faa0fe1a4d63d6fc9c522f5c532539b5', '127.0.0.1', '0', 1390254369, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('faad9988ff8c6ee9c45444bdd6ba75b2', '127.0.0.1', '0', 1390256231, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('faaf60097f6cad41f1f766d8f0dcf871', '127.0.0.1', '0', 1390254247, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fab5396f6fa266557cd90d770d3bb98b', '127.0.0.1', '0', 1390249367, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fab54d4374f1c8cf9bb15f8eb6ad1fe0', '127.0.0.1', '0', 1390252772, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fad4deda43a485ae0a1d68bd33842b47', '127.0.0.1', '0', 1390254257, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fadcb0c5eaae53fbb410768ed4532435', '127.0.0.1', '0', 1390252902, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fae5e052332e2ef3c315e930c23e756b', '127.0.0.1', '0', 1390248427, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fb825f73ea7af1d6c0ee535306ebe3bc', '127.0.0.1', '0', 1390252777, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fbba8c127169a92dd8cadb4d07f9f6c5', '127.0.0.1', '0', 1390253688, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fc14063acc819c95e81f8b7a8c3df8f2', '127.0.0.1', '0', 1390254359, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fc3139c1ba56e33f5c7d90770896f400', '127.0.0.1', '0', 1390249372, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fc5e1698d919761b20966d6d31b7b12c', '127.0.0.1', '0', 1390256002, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fc69f8a8b86de82c036c3d5a68d622a1', '127.0.0.1', '0', 1390253970, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fcb88d52ca6ad8f6eadd8e0af2837f70', '127.0.0.1', '0', 1390252768, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fcc56441e10435360ac41312807664c7', '127.0.0.1', '0', 1390252773, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fcd36d25cc5f859d42132836d988325e', '127.0.0.1', '0', 1390253000, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fcecce0c818de23fd6fa84e27abeb047', '127.0.0.1', '0', 1390253973, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fd0597460c265e9bd91e976e6669780d', '127.0.0.1', '0', 1390254264, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fd0964acae0f965d03f750c493a8ee35', '127.0.0.1', '0', 1390253385, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fd0e9b5872183fc3bc50dc41a1abc8c1', '127.0.0.1', '0', 1390251163, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fd26f14745c87f7388a5637e036a269f', '127.0.0.1', '0', 1390252221, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fd361ff029a27add13f37d17e6808a24', '127.0.0.1', '0', 1390252899, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fd770f7324a95c1138f1ce1b1af05b02', '127.0.0.1', '0', 1390252337, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fda7925d9f75bf234551619c8a853baf', '127.0.0.1', '0', 1390252335, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fdb81851a04a51bf22dd35b99dfe95f4', '127.0.0.1', '0', 1390253691, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fdf8e99481759861ae06b0663c6fff86', '127.0.0.1', '0', 1390252354, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fdfb50a0c06ad501978958ef75f72718', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390408326, 'a:28:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:16:"page_componentss";s:1:"1";s:24:"page_component_categorys";s:1:"1";s:22:"page_component_fieldss";s:1:"1";s:15:"page_frontmenus";s:1:"1";s:16:"current_user_one";b:1;s:21:"page_frontmenu_itemss";s:1:"1";s:22:"page_diets_categoriess";s:1:"1";s:11:"page_dietss";s:1:"1";s:11:"page_mealss";s:1:"1";s:25:"page_routines_categoriess";s:1:"1";s:14:"page_routiness";s:1:"1";s:12:"page_stagess";s:1:"1";s:11:"page_weekss";s:1:"1";s:12:"page_phasess";s:1:"1";s:14:"page_workoutss";s:1:"1";s:20:"page_activity_levels";s:1:"1";s:13:"page_hobbiess";s:1:"1";s:17:"page_specialtiess";s:1:"1";s:9:"page_epss";s:1:"1";s:19:"page_opening_hourss";s:1:"1";s:16:"page_unity_types";s:1:"1";s:14:"page_no_unitss";s:1:"1";s:17:"page_periodicitys";s:1:"1";s:16:"page_meals_types";s:1:"1";}'),
('fe1211b6155e468c339baf028ce397b4', '127.0.0.1', '0', 1390254372, ''),
('fe1758a4bdf8b61868295f708ee93de3', '127.0.0.1', '0', 1390254256, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fe259ad302a158a2bbaeeef69b01cdfe', '127.0.0.1', '0', 1390254237, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fe29b0eb7bcfa9d810aa528c0a02d055', '127.0.0.1', '0', 1390252218, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fe3cd27f522559a999ff6eed29962300', '127.0.0.1', '0', 1390253701, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fe4d4d91e3a4fabe58ef35bd208e8efb', '127.0.0.1', '0', 1390249428, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fe6c31db9f0c1544273c9bd238866c06', '127.0.0.1', '0', 1390252219, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fe7b6526e28e66a3c73c1d50eec74bbf', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36', 1390412081, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:16:"current_user_one";b:1;}'),
('fe87f0846ec39a564479f99fae47461a', '127.0.0.1', '0', 1390256111, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fe92036a4be70fee8be8d4a0111c4430', '127.0.0.1', '0', 1390252775, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fe998f09cf2280dfc461afe9ebb2b983', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', 1392126093, 'a:7:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;s:8:"identity";s:22:"juan.cantor@imagina.co";s:8:"username";s:20:"0.330176001391613110";s:5:"email";s:22:"juan.cantor@imagina.co";s:7:"user_id";s:2:"21";s:14:"old_last_login";s:4:"2014";}'),
('fec9e5fa8e77262cc952dd98c58f5671', '127.0.0.1', '0', 1390252225, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('fed2ddb87c0e2d6ff54082002662ad93', '127.0.0.1', '0', 1390254222, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ff23cfb4a1c35434ae37266d6191b738', '127.0.0.1', '0', 1390256120, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ff523c1b953af9047c9f96fb9a662280', '127.0.0.1', '0', 1390253252, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ff6cfb3808946cb792a0ceccfce1d644', '127.0.0.1', '0', 1390254222, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ff6eab705d3359b130adde10cbea05f6', '127.0.0.1', '0', 1390256116, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}'),
('ffd0b6ff6fb495c92d7f45da7f0e991f', '127.0.0.1', '0', 1390249117, 'a:2:{s:9:"user_data";s:0:"";s:16:"current_user_one";b:1;}');

-- --------------------------------------------------------

--
-- Table structure for table `cms_specialties`
--

CREATE TABLE IF NOT EXISTS `cms_specialties` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Áreas#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar las áreas de especialidad disponibles /* comment truncated */ /* para el registro de médicos*/' AUTO_INCREMENT=22 ;

--
-- Dumping data for table `cms_specialties`
--

INSERT INTO `cms_specialties` (`id`, `name`) VALUES
(1, 'Cabeza'),
(2, 'Cuello'),
(3, 'Mano'),
(4, 'Corazón'),
(5, 'Sistema nervioso'),
(6, 'Cerebro'),
(7, 'Cadera'),
(8, 'Rodilla'),
(9, 'Sistema reproductor'),
(10, 'Piernas'),
(11, 'Pie'),
(12, 'Brazo'),
(13, 'Cara'),
(14, 'Piel'),
(15, 'Sangre'),
(16, 'Enfermedades huérfanas'),
(17, 'Neuronal'),
(18, 'Dolor'),
(19, 'Trastorno mental'),
(20, 'Sistema hepático'),
(21, 'Ocnología');

-- --------------------------------------------------------

--
-- Table structure for table `cms_stages`
--

CREATE TABLE IF NOT EXISTS `cms_stages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `routines_id` int(4) NOT NULL COMMENT 'combox|view|label#Rutinar#',
  `name` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Nombre#',
  `type` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Tipo#',
  `description` text COMMENT 'input|view|label#Descripción#',
  PRIMARY KEY (`id`),
  KEY `fk_workouts_routines1_idx` (`routines_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tablas para administrar las etapas de una rutina de ejericio' AUTO_INCREMENT=9 ;

--
-- Dumping data for table `cms_stages`
--

INSERT INTO `cms_stages` (`id`, `routines_id`, `name`, `type`, `description`) VALUES
(1, 1, 'Etapa 1', 'Lorem ipsum no sé que ', 'Moar Lorem ipsum no sé que'),
(2, 1, 'Etapa 2', 'Lorem ipsum no sé que ', 'Moar Lorem ipsum no sé que'),
(3, 2, NULL, NULL, NULL),
(4, 2, NULL, NULL, NULL),
(5, 2, NULL, NULL, NULL),
(6, 2, NULL, NULL, NULL),
(7, 2, NULL, NULL, NULL),
(8, 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_tips`
--

CREATE TABLE IF NOT EXISTS `cms_tips` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'input|view|label#Nombre#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `cms_tips`
--

INSERT INTO `cms_tips` (`id`, `name`) VALUES
(1, 'Las manzanas, y no la cafeína, son más eficientes para despertarte en la mañana. '),
(2, 'Ejercitar yoga beneficia a muchos problemas digestivos'),
(3, 'Ejercitar hasta sudar por una hora a la semana reduce el riesgo de un ataque al corazón'),
(4, 'Dormir bien disminuye el riesgo de padecer cáncer de mama');

-- --------------------------------------------------------

--
-- Table structure for table `cms_unity_type`
--

CREATE TABLE IF NOT EXISTS `cms_unity_type` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Tipo de unidad#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar la información de la lista "Tipo de u /* comment truncated */ /*nidad" del módulo de fórmula médica*/' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `cms_unity_type`
--

INSERT INTO `cms_unity_type` (`id`, `name`) VALUES
(1, 'Tableta'),
(2, 'Cucharada'),
(3, 'Inyección');

-- --------------------------------------------------------

--
-- Table structure for table `cms_users`
--

CREATE TABLE IF NOT EXISTS `cms_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `activation_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forgotten_password_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sex` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'inputradio|view|label#Sexo#',
  `birthday` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'input|view|label#Fecha de nacimiento#',
  `doctor` tinyint(1) DEFAULT NULL COMMENT 'inputcheckbox|view|label#¿Es ustde médico?#',
  `height` int(3) DEFAULT NULL COMMENT 'input|view|label#Altura#',
  `weight` int(3) DEFAULT NULL COMMENT 'input|view|label#Peso#',
  `pre_weight` int(3) DEFAULT NULL,
  `activity_level_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#Nivel de actividad física que realizas#',
  `cities_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#Ciudad#',
  `imagen_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_users_activity_level1_idx` (`activity_level_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=87 ;

--
-- Dumping data for table `cms_users`
--

INSERT INTO `cms_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `sex`, `birthday`, `doctor`, `height`, `weight`, `pre_weight`, `activity_level_id`, `cities_id`, `imagen_id`) VALUES
(5, '\0\0', 'administrator', '092e624ccaf41c1b9c0dd32a1041043a82507bc7', 'e0efe63787', 'cms@imaginamos.com', NULL, NULL, NULL, '1218e83c71363e71c292b071dace76d3f56b47af', 1392595200, 2014, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(6, '\0\0', 'Admin CMS', '092e624ccaf41c1b9c0dd32a1041043a82507bc7', 'e0efe63787', 'admin@imaginamos.com', NULL, NULL, NULL, '1218e83c71363e71c292b071dace76d3f56b47af', 1392595200, 2013, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(21, '', '0.330176001391613110', '7ff47f9c5db1d82974f55cd9108a284dae4cafd3', '19ab5c1fdb', 'juan.cantor@imagina.co', NULL, NULL, NULL, 'ec0b44566c23d92284434bbedd99f4f50760d5bd', 1392595200, 2014, 1, 'Juan', 'Cantor', NULL, '23456432', 'm', '1991-10-04', NULL, 178, 57, 60, 2, 1, 61),
(22, '', '0.940020001391717417', 'c30f42413f83832510fa53a95be36997a2c9712f', '6fbb34eb3e', 'wqsqdapan2584@hotmail.com', NULL, NULL, NULL, NULL, 1392595200, NULL, 1, 'Armando', 'Perez', NULL, '2345678', 'f', '2078-10-01', NULL, 178, 50, NULL, 2, 1, NULL),
(23, '', '0.077549001391718416', 'bcb409ab9777971eb4b4a8688480249a0c62ec73', '8b8e115f89', 'dapacsaxan2584@hotmail.com', NULL, NULL, NULL, NULL, 1392595200, NULL, 1, 'Pedro', 'Ramirez', NULL, '23456765', 'f', '1991-10-05', NULL, 178, 60, NULL, 2, 1, NULL),
(24, '', '0.179144001391718459', '2584ec3af33ea1fbcafcf0eb98e089bb301a16bb', '64ca8ddb9e', 'dapacsaxant4@hotmail.com', NULL, NULL, NULL, NULL, 1392595200, NULL, 1, 'Lucía', 'Días', NULL, '23456765', 'f', '1991-10-05', NULL, 178, 60, NULL, 2, 1, NULL),
(25, '', '0.646004001391718520', '6fffe91bd150373409853957e6a97ba94adf8842', '82e1c95fd4', '45643saxant4@hotmail.com', NULL, NULL, NULL, NULL, 1392595200, NULL, 1, 'Andres', 'Arteaga', NULL, '23456765', 'f', '1991-10-05', NULL, 178, 60, NULL, 2, 1, NULL),
(26, '', '0.744118001391718557', '8dae1d2ea03cfa6745e1ea2b73be165be9bf91aa', 'caef4c4d31', '45643vesdat4@hotmail.com', NULL, NULL, NULL, NULL, 1392595200, NULL, 1, 'Bernardo', 'Silva', NULL, '23456765', 'f', '1991-10-05', NULL, 178, 60, NULL, 2, 1, NULL),
(27, '', '0.969047001391790208', '0881b9ccb57c8199dc78747940a730d6911e944b', '86e73bf695', 'farizasdffth89@hotmail.com', NULL, NULL, NULL, NULL, 1392595200, NULL, 1, 'Mario', 'Castillo', NULL, '234567865', 'f', '1989-12-23', NULL, 169, 70, NULL, 2, 2, NULL),
(28, '', '0.721946001391790245', 'cf4aa8f68d6840206ff559946b53832c7dbe2d73', 'ba79433b4f', 'farizassadadffth89@hotmail.com', NULL, NULL, NULL, NULL, 1392595200, NULL, 1, 'Mario', 'Castillo', NULL, '234567865', 'f', '1989-12-23', NULL, 169, 70, NULL, 2, 2, 5),
(31, '', '0.223046001391802083', 'b26e5cfc7b5cd2281fa3f654c018b5cdfde58f6c', '3dfae54382', 'jesdasfdsafau22@hotmail.com', NULL, NULL, NULL, NULL, 1392595200, NULL, 1, 'Manuel', 'Hernandez', NULL, '3456543', 'm', '1991-11-06', NULL, 178, 60, NULL, 2, 1, 8),
(32, '', '0.306541001391805805', '7e37704bc266c9bc9bb24f97d3d7b15b5283e577', '543b24ad56', 'ivuothks@sharklasers.com', NULL, NULL, NULL, NULL, 1392595200, NULL, 1, 'Juan', 'Perez', NULL, '567876543', 'm', '1989-02-07', NULL, 170, 60, NULL, 3, 1, NULL),
(33, '', '0.281740001391805864', '12cb0ca4ee86f57cb93e21f5cca60bacc522fe94', '6815daaba0', 'Eary1990@rhyta.com', NULL, NULL, NULL, 'ad88c51d93e06f33d4c493e6f8077b92b4b06a99', 1392595200, 2014, 1, 'Juan', 'Perez', NULL, '567876543', 'm', '1989-02-07', NULL, 170, 60, NULL, 3, 2, 10),
(34, '', '0.952082001391808004', 'a2f206b4ac9dffb6e66d326478c0c82f2c497b49', 'a465363ea6', 'sdfghjjjdfasd@yopmail.com', NULL, NULL, NULL, NULL, 1392595200, NULL, 1, 'Martha ', 'Ramirez', NULL, '19373932', 'f', '1970-04-06', NULL, 178, 70, NULL, 4, 2, NULL),
(35, '', '0.377074001391808069', '2a5dbfbabee317e13cc6fdf035abc66022d1a34a', '409f41dcba', 'florecita@yopmail.com', NULL, NULL, NULL, '9839f717f29c94a88b837f86ac21c08339282fbe', 1392595200, 2014, 1, 'Martha ', 'Ramirez', NULL, '19373932', 'f', '1970-04-06', NULL, 178, 70, NULL, 4, 2, 11),
(40, '', '0.488410001391866023', '4cddfe3616820e33cebe670bffdf0624b95457d2', 'e9af6fb241', 'test_jessicapatu22@hotmail.com', NULL, NULL, NULL, NULL, 1392595200, NULL, 1, 'Jessica', 'Guzmán', NULL, '3143454463', 'f', '1992-05-22', NULL, 148, 50, NULL, 2, 1, 15),
(85, '', '0.267346001391981768', 'bbc2b77e7462a5088c9098445e6520aedb08ba08', '44d842668f', 'dapan25dsfsdfs84@hotmail.com', NULL, NULL, NULL, NULL, 1392595200, NULL, 1, 'dsgs', 'fsdg', NULL, '23456', 'm', '2014-02-07', NULL, 167, 60, NULL, 5, 4, 59),
(86, '', '0.112907001392154013', '77c0ee819792913498d94ee618f82871eb9ddb14', '80e738eb45', 'ilvxyrvq@sharklasers.com', NULL, NULL, NULL, 'ebd0219ef483a975f0ad3ca5e78ee2eb414bc135', 1392595200, 2014, 1, 'Fulano', 'De tal', NULL, '23423', 'm', '2014-12-31', NULL, 160, 60, NULL, 5, 1, 60);

-- --------------------------------------------------------

--
-- Table structure for table `cms_users_activity`
--

CREATE TABLE IF NOT EXISTS `cms_users_activity` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `users_id` int(10) unsigned NOT NULL,
  `action` varchar(80) DEFAULT NULL COMMENT 'input|view|label#Acción#',
  `module` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Módulo#',
  `description` text COMMENT 'input|view|label#Descripción#',
  `related_id` int(10) DEFAULT NULL COMMENT 'input|view|label#Relacionado#',
  `date` varchar(19) DEFAULT NULL COMMENT 'input|view|label#Fecha#',
  PRIMARY KEY (`id`),
  KEY `fk_users_activity_cms_users1_idx` (`users_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla para recolectar información de la intereacción del usu /* comment truncated */ /*ario con el sistema para la sección "Actividad reciente" */' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cms_users_activity`
--

INSERT INTO `cms_users_activity` (`id`, `users_id`, `action`, `module`, `description`, `related_id`, `date`) VALUES
(1, 21, 'update', 'users', 'Juan ha cambiado su foto de perfil.', NULL, '2014-12-02');

-- --------------------------------------------------------

--
-- Table structure for table `cms_users_diets`
--

CREATE TABLE IF NOT EXISTS `cms_users_diets` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `users_id` int(10) unsigned NOT NULL,
  `diets_id` int(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_users_diets_cms_users1_idx` (`users_id`),
  KEY `fk_cms_users_diets_cms_diets1_idx` (`diets_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='tabla intermedia para romper relación de muchos a muchos ent /* comment truncated */ /*re dietas y usuarios*/' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cms_users_diets`
--

INSERT INTO `cms_users_diets` (`id`, `users_id`, `diets_id`) VALUES
(1, 21, 3),
(2, 86, 3);

-- --------------------------------------------------------

--
-- Table structure for table `cms_users_doc_profile`
--

CREATE TABLE IF NOT EXISTS `cms_users_doc_profile` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `users_id` int(10) unsigned NOT NULL,
  `profession` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Profesión#',
  `school` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Universidad#',
  `profesional_register` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Tarjeta profesional#',
  `graduation_year` int(4) NOT NULL COMMENT 'input|view|label#Año de graduación#',
  `specialty` varchar(54) NOT NULL COMMENT 'input|view|label#Especialidad#',
  `specialty_school` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Universidad (especialización)#',
  `medical_register` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Registro médico#',
  `society` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Sociedad a la que pertenece#',
  `work_company1` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Empresa donde trabaja 1#',
  `work_company2` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Empresa donde trabaja 2#',
  `work_company3` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Empresa donde trabaja 3#',
  `work_company4` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Empresa donde trabaja 4#',
  `address` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Dirección de contacto#',
  `average_consulting_cost` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Costo promedio de consulta#',
  PRIMARY KEY (`id`),
  KEY `fk_users_doc_profile_users_idx` (`users_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla para almacenar información extra de usuario (en el cas /* comment truncated */ /*o de los usuarios médicos)*/' AUTO_INCREMENT=16 ;

--
-- Dumping data for table `cms_users_doc_profile`
--

INSERT INTO `cms_users_doc_profile` (`id`, `users_id`, `profession`, `school`, `profesional_register`, `graduation_year`, `specialty`, `specialty_school`, `medical_register`, `society`, `work_company1`, `work_company2`, `work_company3`, `work_company4`, `address`, `average_consulting_cost`) VALUES
(4, 20, 'Ingeniero de sistemas', 'Universidad San Martín', '32456432', 1940, 'Codos', 'Univwersidad San Martín', '12393487', 'Ninguna', 'Imaginamos', '', '', '', 'Cra 4 # 4-18', ''),
(5, 21, 'Ingeniero ', 'San Martín', '234567890', 1940, 'Codos', 'San Martín', '23456975', 'Ninguna', 'Imaginamos', 'Otro..', '', '', 'calle 45 #3-45', '100000'),
(6, 22, 'Ingeniero', 'Samartín', '12345675', 2010, 'Codos', 'Sa', '235632456', 'ninguna', 'Imaginamos', '', '', '', 'sdfg6543', '2345323'),
(7, 23, 'Ingeniero', 'San martín', '89754', 2013, 'Codos', 'San Martín', '78942542', 'Ninguna', 'Imginamso', '', '', '', 'calle 45#45-45', '100000'),
(8, 24, 'Ingeniero', 'San martín', '89754', 2013, 'Codos', 'San Martín', '78942542', 'Ninguna', 'Imginamso', '', '', '', 'calle 45#45-45', '100000'),
(9, 25, 'Ingeniero', 'San martín', '89754', 2013, 'Codos', 'San Martín', '78942542', 'Ninguna', 'Imginamso', '', '', '', 'calle 45#45-45', '100000'),
(10, 26, 'Ingeniero', 'San martín', '89754', 2013, 'Codos', 'San Martín', '78942542', 'Ninguna', 'Imginamso', '', '', '', 'calle 45#45-45', '100000'),
(11, 28, 'Urólogo', 'La Sabana', '3r43r3432', 2014, 'Andrología', 'La sabana', '4308347', 'Ninguna', 'Imaginasmo', '', '', '', 'calle 45 #45-45', '2000000'),
(12, 29, 'Ocnologo', 'no aplica', '324543242', 2011, 'Ocnología', 'Ninguna', '34567654', 'Ninguna', 'Ninguno', '', '', '', 'cadc 345#343', '150000'),
(13, 30, 'Ocnologo', 'no aplica', '324543242', 2011, 'Ocnología', 'Ninguna', '34567654', 'Ninguna', 'Ninguno', '', '', '', 'cadc 345#343', '150000'),
(14, 31, 'Ocnologo', 'no aplica', '324543242', 2011, 'Ocnología', 'Ninguna', '34567654', 'Ninguna', 'Ninguno', '', '', '', 'cadc 345#343', '150000'),
(15, 35, 'Médico cirujano', 'Pontificia Universidad  Javeriana', '45677543434', 1995, 'Pulmones', 'Universidad Nacional', '34587533', 'Asociación de medicina Colombiana', 'Clínica Santa Fe', 'Consultorio propio', '', '', 'No disponible', '45000');

-- --------------------------------------------------------

--
-- Table structure for table `cms_users_eps`
--

CREATE TABLE IF NOT EXISTS `cms_users_eps` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `users_id` int(10) unsigned NOT NULL COMMENT 'combox|view|label#Usuario#',
  `eps_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#EPS#',
  PRIMARY KEY (`id`),
  KEY `fk_users_eps_cms_users1_idx` (`users_id`),
  KEY `fk_users_eps_eps1_idx` (`eps_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla intermedia entre usuarios y EPS (muchos a muchos en ca /* comment truncated */ /*so de usuario médico (que EPS attiende))*/' AUTO_INCREMENT=86 ;

--
-- Dumping data for table `cms_users_eps`
--

INSERT INTO `cms_users_eps` (`id`, `users_id`, `eps_id`) VALUES
(58, 22, 5),
(59, 22, 3),
(60, 23, 5),
(61, 23, 4),
(62, 24, 5),
(63, 24, 4),
(64, 25, 3),
(65, 25, 2),
(66, 25, 1),
(67, 26, 5),
(68, 26, 1),
(69, 25, 5),
(70, 26, 5),
(77, 28, 5),
(80, 35, 2),
(81, 35, 1),
(84, 21, 5),
(85, 21, 4);

-- --------------------------------------------------------

--
-- Table structure for table `cms_users_groups`
--

CREATE TABLE IF NOT EXISTS `cms_users_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_users_groups` (`user_id`),
  KEY `group_users_groups` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `cms_users_groups`
--

INSERT INTO `cms_users_groups` (`id`, `user_id`, `group_id`) VALUES
(5, 5, 1),
(6, 6, 2);

-- --------------------------------------------------------

--
-- Table structure for table `cms_users_hobbies`
--

CREATE TABLE IF NOT EXISTS `cms_users_hobbies` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `users_id` int(10) unsigned NOT NULL COMMENT 'combox|view|label#Usuario#',
  `hobbies_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#Pasatiempos#',
  PRIMARY KEY (`id`),
  KEY `fk_users_hobbies_cms_users1_idx` (`users_id`),
  KEY `fk_users_hobbies_hobbies1_idx` (`hobbies_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla intermedia entre usuarios y los hobbies o pasatiempos' AUTO_INCREMENT=110 ;

--
-- Dumping data for table `cms_users_hobbies`
--

INSERT INTO `cms_users_hobbies` (`id`, `users_id`, `hobbies_id`) VALUES
(63, 22, 8),
(64, 22, 7),
(65, 22, 6),
(66, 23, 8),
(67, 23, 7),
(68, 23, 6),
(69, 24, 8),
(70, 24, 7),
(71, 24, 6),
(72, 25, 8),
(73, 25, 7),
(74, 25, 6),
(75, 25, 5),
(76, 26, 8),
(77, 26, 7),
(78, 26, 6),
(79, 26, 5),
(86, 27, 3),
(87, 28, 3),
(90, 32, 8),
(91, 32, 7),
(92, 32, 6),
(96, 33, 8),
(97, 33, 7),
(98, 33, 6),
(99, 33, 3),
(100, 34, 8),
(101, 34, 7),
(102, 34, 6),
(103, 35, 8),
(104, 35, 7),
(105, 35, 6),
(108, 21, 2),
(109, 21, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_users_opening_hours`
--

CREATE TABLE IF NOT EXISTS `cms_users_opening_hours` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `opening_hours_id` int(10) NOT NULL COMMENT 'combox|view|label#Horario de atención#',
  `users_id` int(10) unsigned NOT NULL COMMENT 'input|view|label#Usuario#',
  PRIMARY KEY (`id`),
  KEY `fk_users_opening_hours_opening_hours1_idx` (`opening_hours_id`),
  KEY `fk_users_opening_hours_cms_users1_idx` (`users_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla intermedia entre usuarios y horarios (para casos de us /* comment truncated */ /*uario médico (En que horarios atiende))*/' AUTO_INCREMENT=87 ;

--
-- Dumping data for table `cms_users_opening_hours`
--

INSERT INTO `cms_users_opening_hours` (`id`, `opening_hours_id`, `users_id`) VALUES
(57, 3, 22),
(58, 2, 22),
(59, 3, 23),
(60, 2, 23),
(61, 3, 24),
(62, 2, 24),
(63, 3, 25),
(64, 2, 25),
(65, 3, 26),
(75, 1, 28),
(79, 3, 35),
(80, 2, 35),
(84, 3, 21),
(85, 2, 21),
(86, 1, 21);

-- --------------------------------------------------------

--
-- Table structure for table `cms_users_routines`
--

CREATE TABLE IF NOT EXISTS `cms_users_routines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `routines_id` int(4) NOT NULL COMMENT 'combox|view|label#Rutina#',
  `users_id` int(10) unsigned NOT NULL COMMENT 'input|view|label#Usuario#',
  PRIMARY KEY (`id`),
  KEY `fk_users_routines_routines1_idx` (`routines_id`),
  KEY `fk_users_routines_cms_users1_idx` (`users_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla intermedia entre usuarios y rutinas' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cms_users_routines`
--

INSERT INTO `cms_users_routines` (`id`, `routines_id`, `users_id`) VALUES
(1, 2, 21),
(2, 1, 21);

-- --------------------------------------------------------

--
-- Table structure for table `cms_users_specialties`
--

CREATE TABLE IF NOT EXISTS `cms_users_specialties` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `specialties_id` int(10) NOT NULL,
  `users_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_users_specialties_cms_specialties1_idx` (`specialties_id`),
  KEY `fk_cms_users_specialties_cms_users1_idx` (`users_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla intermedia para romper la relación muchos a muchos ent /* comment truncated */ /*re áreas de especialidad y usuarios (médicos)*/' AUTO_INCREMENT=97 ;

--
-- Dumping data for table `cms_users_specialties`
--

INSERT INTO `cms_users_specialties` (`id`, `specialties_id`, `users_id`) VALUES
(60, 21, 22),
(61, 20, 22),
(62, 21, 23),
(63, 20, 23),
(64, 19, 23),
(65, 18, 23),
(66, 21, 24),
(67, 20, 24),
(68, 19, 24),
(69, 18, 24),
(70, 21, 25),
(71, 21, 26),
(72, 20, 26),
(73, 18, 26),
(74, 16, 26),
(75, 15, 26),
(76, 14, 26),
(86, 21, 28),
(90, 4, 35),
(94, 21, 21),
(95, 20, 21),
(96, 19, 21);

-- --------------------------------------------------------

--
-- Table structure for table `cms_weeks`
--

CREATE TABLE IF NOT EXISTS `cms_weeks` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Nombre#',
  `description` text,
  `stages_id` int(10) NOT NULL COMMENT 'combox|view|label#Etapa#',
  `phases_id` int(10) NOT NULL COMMENT 'combox|view|label#Tipo de fase#',
  PRIMARY KEY (`id`),
  KEY `fk_weeks_stages1_idx` (`stages_id`),
  KEY `fk_weeks_phases1_idx` (`phases_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla para dministrar las semanas de una fase, de una rutina /* comment truncated */ /* de ejercicios*/' AUTO_INCREMENT=27 ;

--
-- Dumping data for table `cms_weeks`
--

INSERT INTO `cms_weeks` (`id`, `name`, `description`, `stages_id`, `phases_id`) VALUES
(1, 'Semana 1', NULL, 1, 1),
(2, 'Semana 2', NULL, 2, 1),
(3, 'Semana 1', NULL, 3, 1),
(4, 'Semana 2', NULL, 3, 1),
(5, 'Semana 3', NULL, 3, 1),
(6, 'Semana 4', NULL, 3, 1),
(7, 'Semana 1', NULL, 4, 1),
(8, 'Semana 2', NULL, 4, 1),
(9, 'Semana 3', NULL, 4, 1),
(10, 'Semana 4', NULL, 4, 1),
(11, 'Semana 1', NULL, 5, 1),
(12, 'Semana 2', NULL, 5, 1),
(13, 'Semana 3', NULL, 5, 1),
(14, 'Semana 4', NULL, 5, 1),
(15, 'Semana 1', NULL, 6, 1),
(16, 'Semana 2', NULL, 6, 1),
(17, 'Semana 3', NULL, 6, 1),
(18, 'Semana 4', NULL, 6, 1),
(19, 'Semana 1', NULL, 7, 1),
(20, 'Semana 2', NULL, 7, 1),
(21, 'Semana 3', NULL, 7, 1),
(22, 'Semana 4', NULL, 7, 1),
(23, 'Semana 1', NULL, 8, 1),
(24, 'Semana 2', NULL, 8, 1),
(25, 'Semana 3', NULL, 8, 1),
(26, 'Semana 4', NULL, 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_workouts`
--

CREATE TABLE IF NOT EXISTS `cms_workouts` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Título#',
  `description` text COMMENT 'input|view|label#Descripción#',
  `imagen_id` int(11) DEFAULT NULL COMMENT 'input|view|label#Imagen#',
  `weeks_id` int(10) NOT NULL COMMENT 'combox|view|label#Semana#',
  PRIMARY KEY (`id`),
  KEY `fk_workouts_weeks1_idx` (`weeks_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar las sesiones de ejercicios de las rut /* comment truncated */ /*inas */' AUTO_INCREMENT=171 ;

--
-- Dumping data for table `cms_workouts`
--

INSERT INTO `cms_workouts` (`id`, `name`, `description`, `imagen_id`, `weeks_id`) VALUES
(1, 'Sesión de prueba', 'Sesión de ejericios de prueba', 1, 1),
(2, 'Sesión de prueba 2', 'Sesión de ejericios de prueba 2', 2, 1),
(3, 'Sesión 1', NULL, NULL, 3),
(4, 'Sesión 2', NULL, NULL, 3),
(5, 'Sesión 3', NULL, NULL, 3),
(6, 'Sesión 4', NULL, NULL, 3),
(7, 'Sesión 5', NULL, NULL, 3),
(8, 'Sesión 6', NULL, NULL, 3),
(9, 'Sesión 7', NULL, NULL, 3),
(10, 'Sesión 1', NULL, NULL, 4),
(11, 'Sesión 2', NULL, NULL, 4),
(12, 'Sesión 3', NULL, NULL, 4),
(13, 'Sesión 4', NULL, NULL, 4),
(14, 'Sesión 5', NULL, NULL, 4),
(15, 'Sesión 6', NULL, NULL, 4),
(16, 'Sesión 7', NULL, NULL, 4),
(17, 'Sesión 1', NULL, NULL, 5),
(18, 'Sesión 2', NULL, NULL, 5),
(19, 'Sesión 3', NULL, NULL, 5),
(20, 'Sesión 4', NULL, NULL, 5),
(21, 'Sesión 5', NULL, NULL, 5),
(22, 'Sesión 6', NULL, NULL, 5),
(23, 'Sesión 7', NULL, NULL, 5),
(24, 'Sesión 1', NULL, NULL, 6),
(25, 'Sesión 2', NULL, NULL, 6),
(26, 'Sesión 3', NULL, NULL, 6),
(27, 'Sesión 4', NULL, NULL, 6),
(28, 'Sesión 5', NULL, NULL, 6),
(29, 'Sesión 6', NULL, NULL, 6),
(30, 'Sesión 7', NULL, NULL, 6),
(31, 'Sesión 1', NULL, NULL, 7),
(32, 'Sesión 2', NULL, NULL, 7),
(33, 'Sesión 3', NULL, NULL, 7),
(34, 'Sesión 4', NULL, NULL, 7),
(35, 'Sesión 5', NULL, NULL, 7),
(36, 'Sesión 6', NULL, NULL, 7),
(37, 'Sesión 7', NULL, NULL, 7),
(38, 'Sesión 1', NULL, NULL, 8),
(39, 'Sesión 2', NULL, NULL, 8),
(40, 'Sesión 3', NULL, NULL, 8),
(41, 'Sesión 4', NULL, NULL, 8),
(42, 'Sesión 5', NULL, NULL, 8),
(43, 'Sesión 6', NULL, NULL, 8),
(44, 'Sesión 7', NULL, NULL, 8),
(45, 'Sesión 1', NULL, NULL, 9),
(46, 'Sesión 2', NULL, NULL, 9),
(47, 'Sesión 3', NULL, NULL, 9),
(48, 'Sesión 4', NULL, NULL, 9),
(49, 'Sesión 5', NULL, NULL, 9),
(50, 'Sesión 6', NULL, NULL, 9),
(51, 'Sesión 7', NULL, NULL, 9),
(52, 'Sesión 1', NULL, NULL, 10),
(53, 'Sesión 2', NULL, NULL, 10),
(54, 'Sesión 3', NULL, NULL, 10),
(55, 'Sesión 4', NULL, NULL, 10),
(56, 'Sesión 5', NULL, NULL, 10),
(57, 'Sesión 6', NULL, NULL, 10),
(58, 'Sesión 7', NULL, NULL, 10),
(59, 'Sesión 1', NULL, NULL, 11),
(60, 'Sesión 2', NULL, NULL, 11),
(61, 'Sesión 3', NULL, NULL, 11),
(62, 'Sesión 4', NULL, NULL, 11),
(63, 'Sesión 5', NULL, NULL, 11),
(64, 'Sesión 6', NULL, NULL, 11),
(65, 'Sesión 7', NULL, NULL, 11),
(66, 'Sesión 1', NULL, NULL, 12),
(67, 'Sesión 2', NULL, NULL, 12),
(68, 'Sesión 3', NULL, NULL, 12),
(69, 'Sesión 4', NULL, NULL, 12),
(70, 'Sesión 5', NULL, NULL, 12),
(71, 'Sesión 6', NULL, NULL, 12),
(72, 'Sesión 7', NULL, NULL, 12),
(73, 'Sesión 1', NULL, NULL, 13),
(74, 'Sesión 2', NULL, NULL, 13),
(75, 'Sesión 3', NULL, NULL, 13),
(76, 'Sesión 4', NULL, NULL, 13),
(77, 'Sesión 5', NULL, NULL, 13),
(78, 'Sesión 6', NULL, NULL, 13),
(79, 'Sesión 7', NULL, NULL, 13),
(80, 'Sesión 1', NULL, NULL, 14),
(81, 'Sesión 2', NULL, NULL, 14),
(82, 'Sesión 3', NULL, NULL, 14),
(83, 'Sesión 4', NULL, NULL, 14),
(84, 'Sesión 5', NULL, NULL, 14),
(85, 'Sesión 6', NULL, NULL, 14),
(86, 'Sesión 7', NULL, NULL, 14),
(87, 'Sesión 1', NULL, NULL, 15),
(88, 'Sesión 2', NULL, NULL, 15),
(89, 'Sesión 3', NULL, NULL, 15),
(90, 'Sesión 4', NULL, NULL, 15),
(91, 'Sesión 5', NULL, NULL, 15),
(92, 'Sesión 6', NULL, NULL, 15),
(93, 'Sesión 7', NULL, NULL, 15),
(94, 'Sesión 1', NULL, NULL, 16),
(95, 'Sesión 2', NULL, NULL, 16),
(96, 'Sesión 3', NULL, NULL, 16),
(97, 'Sesión 4', NULL, NULL, 16),
(98, 'Sesión 5', NULL, NULL, 16),
(99, 'Sesión 6', NULL, NULL, 16),
(100, 'Sesión 7', NULL, NULL, 16),
(101, 'Sesión 1', NULL, NULL, 17),
(102, 'Sesión 2', NULL, NULL, 17),
(103, 'Sesión 3', NULL, NULL, 17),
(104, 'Sesión 4', NULL, NULL, 17),
(105, 'Sesión 5', NULL, NULL, 17),
(106, 'Sesión 6', NULL, NULL, 17),
(107, 'Sesión 7', NULL, NULL, 17),
(108, 'Sesión 1', NULL, NULL, 18),
(109, 'Sesión 2', NULL, NULL, 18),
(110, 'Sesión 3', NULL, NULL, 18),
(111, 'Sesión 4', NULL, NULL, 18),
(112, 'Sesión 5', NULL, NULL, 18),
(113, 'Sesión 6', NULL, NULL, 18),
(114, 'Sesión 7', NULL, NULL, 18),
(115, 'Sesión 1', NULL, NULL, 19),
(116, 'Sesión 2', NULL, NULL, 19),
(117, 'Sesión 3', NULL, NULL, 19),
(118, 'Sesión 4', NULL, NULL, 19),
(119, 'Sesión 5', NULL, NULL, 19),
(120, 'Sesión 6', NULL, NULL, 19),
(121, 'Sesión 7', NULL, NULL, 19),
(122, 'Sesión 1', NULL, NULL, 20),
(123, 'Sesión 2', NULL, NULL, 20),
(124, 'Sesión 3', NULL, NULL, 20),
(125, 'Sesión 4', NULL, NULL, 20),
(126, 'Sesión 5', NULL, NULL, 20),
(127, 'Sesión 6', NULL, NULL, 20),
(128, 'Sesión 7', NULL, NULL, 20),
(129, 'Sesión 1', NULL, NULL, 21),
(130, 'Sesión 2', NULL, NULL, 21),
(131, 'Sesión 3', NULL, NULL, 21),
(132, 'Sesión 4', NULL, NULL, 21),
(133, 'Sesión 5', NULL, NULL, 21),
(134, 'Sesión 6', NULL, NULL, 21),
(135, 'Sesión 7', NULL, NULL, 21),
(136, 'Sesión 1', NULL, NULL, 22),
(137, 'Sesión 2', NULL, NULL, 22),
(138, 'Sesión 3', NULL, NULL, 22),
(139, 'Sesión 4', NULL, NULL, 22),
(140, 'Sesión 5', NULL, NULL, 22),
(141, 'Sesión 6', NULL, NULL, 22),
(142, 'Sesión 7', NULL, NULL, 22),
(143, 'Sesión 1', NULL, NULL, 23),
(144, 'Sesión 2', NULL, NULL, 23),
(145, 'Sesión 3', NULL, NULL, 23),
(146, 'Sesión 4', NULL, NULL, 23),
(147, 'Sesión 5', NULL, NULL, 23),
(148, 'Sesión 6', NULL, NULL, 23),
(149, 'Sesión 7', NULL, NULL, 23),
(150, 'Sesión 1', NULL, NULL, 24),
(151, 'Sesión 2', NULL, NULL, 24),
(152, 'Sesión 3', NULL, NULL, 24),
(153, 'Sesión 4', NULL, NULL, 24),
(154, 'Sesión 5', NULL, NULL, 24),
(155, 'Sesión 6', NULL, NULL, 24),
(156, 'Sesión 7', NULL, NULL, 24),
(157, 'Sesión 1', NULL, NULL, 25),
(158, 'Sesión 2', NULL, NULL, 25),
(159, 'Sesión 3', NULL, NULL, 25),
(160, 'Sesión 4', NULL, NULL, 25),
(161, 'Sesión 5', NULL, NULL, 25),
(162, 'Sesión 6', NULL, NULL, 25),
(163, 'Sesión 7', NULL, NULL, 25),
(164, 'Sesión 1', NULL, NULL, 26),
(165, 'Sesión 2', NULL, NULL, 26),
(166, 'Sesión 3', NULL, NULL, 26),
(167, 'Sesión 4', NULL, NULL, 26),
(168, 'Sesión 5', NULL, NULL, 26),
(169, 'Sesión 6', NULL, NULL, 26),
(170, 'Sesión 7', NULL, NULL, 26);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cms_components`
--
ALTER TABLE `cms_components`
  ADD CONSTRAINT `fk_cms_components_cms_component_category1` FOREIGN KEY (`component_category_id`) REFERENCES `cms_component_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cms_components_cms_users1` FOREIGN KEY (`users_id`) REFERENCES `cms_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_component_fields`
--
ALTER TABLE `cms_component_fields`
  ADD CONSTRAINT `fk_cms_component_fields_cms_components1` FOREIGN KEY (`components_id`) REFERENCES `cms_components` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cms_component_fields_cms_field_type1` FOREIGN KEY (`field_type_id`) REFERENCES `cms_field_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_diets`
--
ALTER TABLE `cms_diets`
  ADD CONSTRAINT `fk_cms_diets_cms_diets_categories1` FOREIGN KEY (`diets_categories_id`) REFERENCES `cms_diets_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_frontmenu_items`
--
ALTER TABLE `cms_frontmenu_items`
  ADD CONSTRAINT `fk_cms_frontmenu_items_cms_menu1` FOREIGN KEY (`frontmenu_id`) REFERENCES `cms_frontmenu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_groups_permissions`
--
ALTER TABLE `cms_groups_permissions`
  ADD CONSTRAINT `fk_cms_groups_permissions_cms_groups1` FOREIGN KEY (`group_id`) REFERENCES `cms_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cms_groups_permissions_cms_permissions1` FOREIGN KEY (`permission_id`) REFERENCES `cms_permissions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_meals`
--
ALTER TABLE `cms_meals`
  ADD CONSTRAINT `fk_cms_meals_cms_diets1` FOREIGN KEY (`diets_id`) REFERENCES `cms_diets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cms_meals_cms_meals_type1` FOREIGN KEY (`meals_type_id`) REFERENCES `cms_meals_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_medical_formulate`
--
ALTER TABLE `cms_medical_formulate`
  ADD CONSTRAINT `fk_cms_medical_formulate_cms_no_units1` FOREIGN KEY (`no_units_id`) REFERENCES `cms_no_units` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cms_medical_formulate_cms_periodicity1` FOREIGN KEY (`periodicity_id`) REFERENCES `cms_periodicity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cms_medical_formulate_cms_unity_type1` FOREIGN KEY (`unity_type_id`) REFERENCES `cms_unity_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cms_medical_formulate_cms_users1` FOREIGN KEY (`users_id`) REFERENCES `cms_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_municipio`
--
ALTER TABLE `cms_municipio`
  ADD CONSTRAINT `fk_cms_municipio_cms_departamento1` FOREIGN KEY (`departamento_id`) REFERENCES `cms_departamento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_reported_activity`
--
ALTER TABLE `cms_reported_activity`
  ADD CONSTRAINT `fk_cms_reported_activity_cms_activity_minutes1` FOREIGN KEY (`activity_minutes_id`) REFERENCES `cms_activity_minutes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cms_reported_activity_cms_users1` FOREIGN KEY (`users_id`) REFERENCES `cms_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cms_reported_activity_cms_weeks1` FOREIGN KEY (`weeks_id`) REFERENCES `cms_weeks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_routines`
--
ALTER TABLE `cms_routines`
  ADD CONSTRAINT `fk_cms_routines_routines_cms_categories1` FOREIGN KEY (`routines_categories_id`) REFERENCES `cms_routines_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_stages`
--
ALTER TABLE `cms_stages`
  ADD CONSTRAINT `fk_cms_workouts_cms_routines1` FOREIGN KEY (`routines_id`) REFERENCES `cms_routines` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
