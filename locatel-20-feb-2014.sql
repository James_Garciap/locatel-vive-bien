-- MySQL dump 10.13  Distrib 5.5.33, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: locatel
-- ------------------------------------------------------
-- Server version	5.5.33-1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cms_activity_level`
--

DROP TABLE IF EXISTS `cms_activity_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_activity_level` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(75) DEFAULT NULL COMMENT 'input|view|label#Descripción#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar la información de la lista "¿Qué nive /* comment truncated */ /*l de actividad físíca realizas?"  en el registro de usuario*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_activity_minutes`
--

DROP TABLE IF EXISTS `cms_activity_minutes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_activity_minutes` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(5) DEFAULT NULL COMMENT 'input|view|label#Minutos#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar la lista "Minutos de actividad" en el /* comment truncated */ /* formulario " Reporta tu actividad\n- Actualmente en desuso, por definir si el campo apunta a esta lista o se deja abierto*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_api_oauth`
--

DROP TABLE IF EXISTS `cms_api_oauth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_api_oauth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `strategy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `api_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `api_secret` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `scope` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `active_oauth` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_calendar`
--

DROP TABLE IF EXISTS `cms_calendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_calendar` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT 'input|view|label#Nombre del evento#',
  `description` text NOT NULL COMMENT 'input|view|label#Descripción#',
  `date` date NOT NULL COMMENT 'input|view|label#Fecha#',
  `users_id` int(10) NOT NULL,
  `imagen_id` int(11) DEFAULT NULL COMMENT 'input|view|label#Imagen#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_captcha`
--

DROP TABLE IF EXISTS `cms_captcha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `captcha_time` int(10) unsigned NOT NULL,
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=InnoDB AUTO_INCREMENT=236 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_cities`
--

DROP TABLE IF EXISTS `cms_cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_cities` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(75) NOT NULL COMMENT 'input|view|label#Nombre#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_coaches`
--

DROP TABLE IF EXISTS `cms_coaches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_coaches` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT 'input|view|label#Nombre#',
  `email` varchar(150) NOT NULL COMMENT 'input|view|label#Email#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_component_category`
--

DROP TABLE IF EXISTS `cms_component_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_component_category` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Nombre#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar las categorías en las que se pueden c /* comment truncated */ /*lasificar los componentes*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_component_fields`
--

DROP TABLE IF EXISTS `cms_component_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_component_fields` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `components_id` int(10) NOT NULL COMMENT 'combox|view|label#Componente#',
  `value` text COMMENT 'input|view|label#Valor#',
  `field` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Campo#',
  `type` varchar(20) DEFAULT NULL,
  `field_type_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#Tipo de campo#',
  PRIMARY KEY (`id`),
  KEY `fk_component_fields_components1_idx` (`components_id`),
  KEY `fk_cms_component_fields_cms_field_type1_idx` (`field_type_id`),
  CONSTRAINT `fk_cms_component_fields_cms_components1` FOREIGN KEY (`components_id`) REFERENCES `cms_components` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_component_fields_cms_field_type1` FOREIGN KEY (`field_type_id`) REFERENCES `cms_field_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COMMENT='Tabla para almacenar los campos correspondientes a cada comp /* comment truncated */ /*onente*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_components`
--

DROP TABLE IF EXISTS `cms_components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_components` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `component_category_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#Tipo de componente#',
  `name` varchar(50) DEFAULT NULL COMMENT 'input|view|label#Nombre#',
  `description` text COMMENT 'input|view|label#Descripción#',
  `parent` int(10) DEFAULT NULL COMMENT 'input|view|label#Relacionado..#',
  `users_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_components_component_category1_idx` (`component_category_id`),
  KEY `fk_cms_components_cms_users1_idx` (`users_id`),
  CONSTRAINT `fk_cms_components_cms_component_category1` FOREIGN KEY (`component_category_id`) REFERENCES `cms_component_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_components_cms_users1` FOREIGN KEY (`users_id`) REFERENCES `cms_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 COMMENT='Tabla para dministrar los componentes del FrontEnd del sitio /* comment truncated */ /* (banners, footers, etc..)*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_contacto`
--

DROP TABLE IF EXISTS `cms_contacto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_contacto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `direccion` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `celular` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ciudad` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cordenada_x` double NOT NULL DEFAULT '0',
  `cordenada_y` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_departamento`
--

DROP TABLE IF EXISTS `cms_departamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_departamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_diets`
--

DROP TABLE IF EXISTS `cms_diets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_diets` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `diets_categories_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#Categorías#',
  `name` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Nombre#',
  `description` text COMMENT 'input|view|label#Descripción#',
  PRIMARY KEY (`id`),
  KEY `fk_diets_diets_categories1_idx` (`diets_categories_id`),
  CONSTRAINT `fk_cms_diets_cms_diets_categories1` FOREIGN KEY (`diets_categories_id`) REFERENCES `cms_diets_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='Tabla para dministrar las dietas que se crearn';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_diets_categories`
--

DROP TABLE IF EXISTS `cms_diets_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_diets_categories` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Nombre#',
  `description` text COMMENT 'input|view|label#Descripción#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='Tabla apra administrar las categorías a las que puede perten /* comment truncated */ /*cer una dieta, dependiendo de los resultados de las fórmulas suministradas*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_eps`
--

DROP TABLE IF EXISTS `cms_eps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_eps` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Nombre#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar las listas de EPS en el registro de u /* comment truncated */ /*suarios básicos y de usuarios médicos*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_field_type`
--

DROP TABLE IF EXISTS `cms_field_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_field_type` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT 'input|view|label#Tipo de campo#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='Tabla de especialización para los tipos de campos en los com /* comment truncated */ /*ponentes*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_frontmenu`
--

DROP TABLE IF EXISTS `cms_frontmenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_frontmenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Nombre#',
  `description` text COMMENT 'input|view|label#Descripción#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Tabla que contiene los menús que se usarán en la aplicación  /* comment truncated */ /*(la información de los items se almacena en otra tabla (menu_items) )\n*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_frontmenu_items`
--

DROP TABLE IF EXISTS `cms_frontmenu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_frontmenu_items` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `frontmenu_id` int(4) NOT NULL COMMENT 'combox|view|label#Menú superiorr#',
  `label` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Etiqueta#',
  `value` varchar(250) DEFAULT NULL COMMENT 'input|view|label#Valor (link)#',
  `parent` int(4) DEFAULT NULL COMMENT 'input|view|label#Parent#',
  PRIMARY KEY (`id`),
  KEY `fk_frontmenu_items_menu1_idx` (`frontmenu_id`),
  CONSTRAINT `fk_cms_frontmenu_items_cms_menu1` FOREIGN KEY (`frontmenu_id`) REFERENCES `cms_frontmenu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Tabla donde se alamcenan todos los items de menús, si un ite /* comment truncated */ /*m es un child y tiene un parent, este es buscado en la misma tabla*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_groups`
--

DROP TABLE IF EXISTS `cms_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_groups_permissions`
--

DROP TABLE IF EXISTS `cms_groups_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_groups_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL,
  `permission_id` int(11) NOT NULL,
  `view` tinyint(1) DEFAULT '0',
  `create` tinyint(1) DEFAULT '0',
  `update` tinyint(1) DEFAULT '0',
  `delete` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_cms_users_permissions_cms_permissions1_idx` (`permission_id`),
  KEY `fk_cms_groups_permissions_cms_groups1_idx` (`group_id`),
  CONSTRAINT `fk_cms_groups_permissions_cms_groups1` FOREIGN KEY (`group_id`) REFERENCES `cms_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_groups_permissions_cms_permissions1` FOREIGN KEY (`permission_id`) REFERENCES `cms_permissions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_hobbies`
--

DROP TABLE IF EXISTS `cms_hobbies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_hobbies` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL COMMENT 'input|view|label#Pasatiempo#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COMMENT='Tabla para almacenar los hobbies que puede tener el usuario  /* comment truncated */ /*a momento del registro*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_imagen`
--

DROP TABLE IF EXISTS `cms_imagen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_imagen` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'inputhidden|none',
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_login_attempts`
--

DROP TABLE IF EXISTS `cms_login_attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_login_attempts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_meals`
--

DROP TABLE IF EXISTS `cms_meals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_meals` (
  `id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `diets_id` int(4) NOT NULL COMMENT 'combox|view|label#Dieta N|#',
  `description` text COMMENT 'input|view|label#Descripción#',
  `meals_type_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#Tipo de comida#',
  PRIMARY KEY (`id`),
  KEY `fk_meals_diets1_idx` (`diets_id`),
  KEY `fk_cms_meals_cms_meals_type1_idx` (`meals_type_id`),
  CONSTRAINT `fk_cms_meals_cms_diets1` FOREIGN KEY (`diets_id`) REFERENCES `cms_diets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_meals_cms_meals_type1` FOREIGN KEY (`meals_type_id`) REFERENCES `cms_meals_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar las comidas pretenecientes a una diet /* comment truncated */ /*a (desaynos, almuerzos, cena, etc..)*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_meals_type`
--

DROP TABLE IF EXISTS `cms_meals_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_meals_type` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Tipo de comida#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_medical_formulate`
--

DROP TABLE IF EXISTS `cms_medical_formulate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_medical_formulate` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `users_id` int(10) unsigned NOT NULL,
  `medicine` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Medicina#',
  `description` text COMMENT 'input|view|label#Datos importantes#',
  `mail_notificaction` tinyint(1) DEFAULT NULL COMMENT 'input|view|label#Recibir recordatorio en el correo electrónico#',
  `unity_type_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#Tipo de unidad#',
  `no_units_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#N° de unidades#',
  `periodicity_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#Periodicidad de la fórmula#',
  PRIMARY KEY (`id`),
  KEY `fk_medical_formulate_cms_users1_idx` (`users_id`),
  KEY `fk_medical_formulate_unity_type1_idx` (`unity_type_id`),
  KEY `fk_medical_formulate_no_units1_idx` (`no_units_id`),
  KEY `fk_medical_formulate_periodicity1_idx` (`periodicity_id`),
  CONSTRAINT `fk_cms_medical_formulate_cms_no_units1` FOREIGN KEY (`no_units_id`) REFERENCES `cms_no_units` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_medical_formulate_cms_periodicity1` FOREIGN KEY (`periodicity_id`) REFERENCES `cms_periodicity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_medical_formulate_cms_unity_type1` FOREIGN KEY (`unity_type_id`) REFERENCES `cms_unity_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_medical_formulate_cms_users1` FOREIGN KEY (`users_id`) REFERENCES `cms_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='Tabla para el módulo de Fórmula médica (administrar informac /* comment truncated */ /*ión de formulación médica del usuario)*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_menu`
--

DROP TABLE IF EXISTS `cms_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_short` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `image` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_municipio`
--

DROP TABLE IF EXISTS `cms_municipio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_municipio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `departamento_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_municipio_cms_departamento1` (`departamento_id`),
  CONSTRAINT `fk_cms_municipio_cms_departamento1` FOREIGN KEY (`departamento_id`) REFERENCES `cms_departamento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1113 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_no_units`
--

DROP TABLE IF EXISTS `cms_no_units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_no_units` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(4) DEFAULT NULL COMMENT 'input|view|label#Cantidad#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar la información de la lista "Número de /* comment truncated */ /* unidades" del módulo de fórmula médica*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_oauth_config`
--

DROP TABLE IF EXISTS `cms_oauth_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_oauth_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_opening_hours`
--

DROP TABLE IF EXISTS `cms_opening_hours`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_opening_hours` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(8) DEFAULT NULL COMMENT 'input|view|label#Hora#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar la información de las opciones de "Ho /* comment truncated */ /*rarios de atención" en el formulario de registro de usuarios*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_periodicity`
--

DROP TABLE IF EXISTS `cms_periodicity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_periodicity` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL COMMENT 'input|view|label#Periodicidad#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar la información de la lista "Periodici /* comment truncated */ /*dad" del módulo de fórmula médica*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_permissions`
--

DROP TABLE IF EXISTS `cms_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` enum('module','function','component') COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_phases`
--

DROP TABLE IF EXISTS `cms_phases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_phases` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL COMMENT 'input|view|label#Nombre#',
  `description` text COMMENT 'input|view|label#Descripción#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar los tipos de  fases de rutinas dispob /* comment truncated */ /*ibles*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_posts`
--

DROP TABLE IF EXISTS `cms_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_posts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT 'input|view|label#Nombre#',
  `description` text NOT NULL COMMENT 'input|view|label#Descripción#',
  `date` date NOT NULL COMMENT 'input|view|label#Fecha#',
  `users_id` int(10) NOT NULL,
  `imagen_id` int(11) DEFAULT NULL COMMENT 'input|view|label#Imagen#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_prepaid_health`
--

DROP TABLE IF EXISTS `cms_prepaid_health`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_prepaid_health` (
  `prepaid_health_id` int(11) NOT NULL AUTO_INCREMENT,
  `name_prepaid_health` varchar(45) NOT NULL,
  `logic_state` int(11) NOT NULL,
  `date_insert` date NOT NULL,
  PRIMARY KEY (`prepaid_health_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_recipes`
--

DROP TABLE IF EXISTS `cms_recipes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_recipes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT 'input|view|label#Titulo de la receta#',
  `description` text NOT NULL COMMENT 'input|view|label#Descripción#',
  `date` date DEFAULT NULL COMMENT 'input|view|label#Fecha#',
  `users_id` int(10) NOT NULL,
  `imagen_id` int(11) DEFAULT NULL COMMENT 'input|view|label#Imagen#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_redes_sociales`
--

DROP TABLE IF EXISTS `cms_redes_sociales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_redes_sociales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `red_social` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_red` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_reported_activity`
--

DROP TABLE IF EXISTS `cms_reported_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_reported_activity` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `users_id` int(10) unsigned NOT NULL COMMENT 'combox|view|label#Usuario#',
  `weight` varchar(5) DEFAULT NULL COMMENT 'input|view|label#Peso#',
  `time` varchar(8) DEFAULT NULL COMMENT 'input|view|label#Tiempo#',
  `description` text COMMENT 'input|view|label#Descripción#',
  `activity_minutes_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#Minutos de actividad#',
  `weeks_id` int(10) NOT NULL COMMENT 'combox|view|label#Semana#',
  PRIMARY KEY (`id`),
  KEY `fk_reported_activity_cms_users1_idx` (`users_id`),
  KEY `fk_reported_activity_actitity_minutes1_idx` (`activity_minutes_id`),
  KEY `fk_reported_activity_weeks1_idx` (`weeks_id`),
  CONSTRAINT `fk_cms_reported_activity_cms_activity_minutes1` FOREIGN KEY (`activity_minutes_id`) REFERENCES `cms_activity_minutes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_reported_activity_cms_users1` FOREIGN KEY (`users_id`) REFERENCES `cms_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cms_reported_activity_cms_weeks1` FOREIGN KEY (`weeks_id`) REFERENCES `cms_weeks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1 COMMENT='Tabla apra administrar la información del módulo "Reporta tu /* comment truncated */ /* actividad"*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_routines`
--

DROP TABLE IF EXISTS `cms_routines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_routines` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `routines_categories_id` int(4) NOT NULL COMMENT 'combox|view|label#Categoríar#',
  `name` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Nombre#',
  `description` text COMMENT 'input|view|label#Descripción#',
  PRIMARY KEY (`id`),
  KEY `fk_routines_routines_categories1_idx` (`routines_categories_id`),
  CONSTRAINT `fk_cms_routines_routines_cms_categories1` FOREIGN KEY (`routines_categories_id`) REFERENCES `cms_routines_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Taba para administrar las rutinas de ejercicios que manejará /* comment truncated */ /* la aplicación*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_routines_categories`
--

DROP TABLE IF EXISTS `cms_routines_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_routines_categories` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Nombre#',
  `description` text COMMENT 'input|view|label#Description#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Tabla para adminsitrar las categorías de rutinas disponibles /* comment truncated */ /* */';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_sessions`
--

DROP TABLE IF EXISTS `cms_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_sessions` (
  `session_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `user_agent` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_specialties`
--

DROP TABLE IF EXISTS `cms_specialties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_specialties` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Áreas#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar las áreas de especialidad disponibles /* comment truncated */ /* para el registro de médicos*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_stages`
--

DROP TABLE IF EXISTS `cms_stages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_stages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `routines_id` int(4) NOT NULL COMMENT 'combox|view|label#Rutinar#',
  `name` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Nombre#',
  `type` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Tipo#',
  `description` text COMMENT 'input|view|label#Descripción#',
  PRIMARY KEY (`id`),
  KEY `fk_workouts_routines1_idx` (`routines_id`),
  CONSTRAINT `fk_cms_workouts_cms_routines1` FOREIGN KEY (`routines_id`) REFERENCES `cms_routines` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COMMENT='Tablas para administrar las etapas de una rutina de ejericio';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_tips`
--

DROP TABLE IF EXISTS `cms_tips`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_tips` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'input|view|label#Nombre#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_unity_type`
--

DROP TABLE IF EXISTS `cms_unity_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_unity_type` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Tipo de unidad#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar la información de la lista "Tipo de u /* comment truncated */ /*nidad" del módulo de fórmula médica*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_users`
--

DROP TABLE IF EXISTS `cms_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `activation_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forgotten_password_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sex` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'inputradio|view|label#Sexo#',
  `birthday` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'input|view|label#Fecha de nacimiento#',
  `doctor` tinyint(1) DEFAULT NULL COMMENT 'inputcheckbox|view|label#¿Es ustde médico?#',
  `height` int(3) DEFAULT NULL COMMENT 'input|view|label#Altura#',
  `weight` int(3) DEFAULT NULL COMMENT 'input|view|label#Peso#',
  `pre_weight` int(3) DEFAULT NULL,
  `activity_level_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#Nivel de actividad física que realizas#',
  `cities_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#Ciudad#',
  `imagen_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_users_activity_level1_idx` (`activity_level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_users_activity`
--

DROP TABLE IF EXISTS `cms_users_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_users_activity` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `users_id` int(10) unsigned NOT NULL,
  `action` varchar(80) DEFAULT NULL COMMENT 'input|view|label#Acción#',
  `module` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Módulo#',
  `description` text COMMENT 'input|view|label#Descripción#',
  `related_id` int(10) DEFAULT NULL COMMENT 'input|view|label#Relacionado#',
  `date` varchar(19) DEFAULT NULL COMMENT 'input|view|label#Fecha#',
  PRIMARY KEY (`id`),
  KEY `fk_users_activity_cms_users1_idx` (`users_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Tabla para recolectar información de la intereacción del usu /* comment truncated */ /*ario con el sistema para la sección "Actividad reciente" */';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_users_diets`
--

DROP TABLE IF EXISTS `cms_users_diets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_users_diets` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `users_id` int(10) unsigned NOT NULL,
  `diets_id` int(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_users_diets_cms_users1_idx` (`users_id`),
  KEY `fk_cms_users_diets_cms_diets1_idx` (`diets_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='tabla intermedia para romper relación de muchos a muchos ent /* comment truncated */ /*re dietas y usuarios*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_users_doc_profile`
--

DROP TABLE IF EXISTS `cms_users_doc_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_users_doc_profile` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `users_id` int(10) unsigned NOT NULL,
  `profession` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Profesión#',
  `school` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Universidad#',
  `profesional_register` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Tarjeta profesional#',
  `graduation_year` int(4) NOT NULL COMMENT 'input|view|label#Año de graduación#',
  `specialty` varchar(54) NOT NULL COMMENT 'input|view|label#Especialidad#',
  `specialty_school` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Universidad (especialización)#',
  `medical_register` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Registro médico#',
  `society` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Sociedad a la que pertenece#',
  `work_company1` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Empresa donde trabaja 1#',
  `work_company2` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Empresa donde trabaja 2#',
  `work_company3` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Empresa donde trabaja 3#',
  `work_company4` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Empresa donde trabaja 4#',
  `address` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Dirección de contacto#',
  `average_consulting_cost` varchar(45) DEFAULT NULL COMMENT 'input|view|label#Costo promedio de consulta#',
  PRIMARY KEY (`id`),
  KEY `fk_users_doc_profile_users_idx` (`users_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1 COMMENT='Tabla para almacenar información extra de usuario (en el cas /* comment truncated */ /*o de los usuarios médicos)*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_users_eps`
--

DROP TABLE IF EXISTS `cms_users_eps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_users_eps` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `users_id` int(10) unsigned NOT NULL COMMENT 'combox|view|label#Usuario#',
  `eps_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#EPS#',
  PRIMARY KEY (`id`),
  KEY `fk_users_eps_cms_users1_idx` (`users_id`),
  KEY `fk_users_eps_eps1_idx` (`eps_id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=latin1 COMMENT='Tabla intermedia entre usuarios y EPS (muchos a muchos en ca /* comment truncated */ /*so de usuario médico (que EPS attiende))*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_users_groups`
--

DROP TABLE IF EXISTS `cms_users_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_users_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_users_groups` (`user_id`),
  KEY `group_users_groups` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_users_hobbies`
--

DROP TABLE IF EXISTS `cms_users_hobbies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_users_hobbies` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `users_id` int(10) unsigned NOT NULL COMMENT 'combox|view|label#Usuario#',
  `hobbies_id` tinyint(2) NOT NULL COMMENT 'combox|view|label#Pasatiempos#',
  PRIMARY KEY (`id`),
  KEY `fk_users_hobbies_cms_users1_idx` (`users_id`),
  KEY `fk_users_hobbies_hobbies1_idx` (`hobbies_id`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=latin1 COMMENT='Tabla intermedia entre usuarios y los hobbies o pasatiempos';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_users_opening_hours`
--

DROP TABLE IF EXISTS `cms_users_opening_hours`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_users_opening_hours` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `opening_hours_id` int(10) NOT NULL COMMENT 'combox|view|label#Horario de atención#',
  `users_id` int(10) unsigned NOT NULL COMMENT 'input|view|label#Usuario#',
  PRIMARY KEY (`id`),
  KEY `fk_users_opening_hours_opening_hours1_idx` (`opening_hours_id`),
  KEY `fk_users_opening_hours_cms_users1_idx` (`users_id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=latin1 COMMENT='Tabla intermedia entre usuarios y horarios (para casos de us /* comment truncated */ /*uario médico (En que horarios atiende))*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_users_prepaid_health`
--

DROP TABLE IF EXISTS `cms_users_prepaid_health`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_users_prepaid_health` (
  `users_prepaid_health_id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `prepaid_health_id` int(11) NOT NULL,
  `logic_state` int(11) DEFAULT NULL,
  `date_insert` date DEFAULT NULL,
  PRIMARY KEY (`users_prepaid_health_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_users_routines`
--

DROP TABLE IF EXISTS `cms_users_routines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_users_routines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `routines_id` int(4) NOT NULL COMMENT 'combox|view|label#Rutina#',
  `users_id` int(10) unsigned NOT NULL COMMENT 'input|view|label#Usuario#',
  PRIMARY KEY (`id`),
  KEY `fk_users_routines_routines1_idx` (`routines_id`),
  KEY `fk_users_routines_cms_users1_idx` (`users_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Tabla intermedia entre usuarios y rutinas';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_users_specialties`
--

DROP TABLE IF EXISTS `cms_users_specialties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_users_specialties` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `specialties_id` int(10) NOT NULL,
  `users_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_users_specialties_cms_specialties1_idx` (`specialties_id`),
  KEY `fk_cms_users_specialties_cms_users1_idx` (`users_id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=latin1 COMMENT='Tabla intermedia para romper la relación muchos a muchos ent /* comment truncated */ /*re áreas de especialidad y usuarios (médicos)*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_weeks`
--

DROP TABLE IF EXISTS `cms_weeks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_weeks` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Nombre#',
  `description` text,
  `stages_id` int(10) NOT NULL COMMENT 'combox|view|label#Etapa#',
  `phases_id` int(10) NOT NULL COMMENT 'combox|view|label#Tipo de fase#',
  PRIMARY KEY (`id`),
  KEY `fk_weeks_stages1_idx` (`stages_id`),
  KEY `fk_weeks_phases1_idx` (`phases_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1 COMMENT='Tabla para dministrar las semanas de una fase, de una rutina /* comment truncated */ /* de ejercicios*/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cms_workouts`
--

DROP TABLE IF EXISTS `cms_workouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_workouts` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL COMMENT 'input|view|label#Título#',
  `description` text COMMENT 'input|view|label#Descripción#',
  `imagen_id` int(11) DEFAULT NULL COMMENT 'input|view|label#Imagen#',
  `weeks_id` int(10) NOT NULL COMMENT 'combox|view|label#Semana#',
  PRIMARY KEY (`id`),
  KEY `fk_workouts_weeks1_idx` (`weeks_id`)
) ENGINE=InnoDB AUTO_INCREMENT=171 DEFAULT CHARSET=latin1 COMMENT='Tabla para administrar las sesiones de ejercicios de las rut /* comment truncated */ /*inas */';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-02-20 14:11:18
