<section>
  <aside>
    <div class="fondos-right" style="background-image: url(assets/img/fondos/fondo-2.png);">
      <div class="mask-right"> 
        <a href="#contactar-modal" class="contactar pull-right margin-left llama_modal">Contactar entrenador</a> 
        <a href="#cont8" class="enlace-8 articulo pull-right escribir_articulo">Escribir artículo</a>
        <div class="clear e10"></div>
        <div class="fondo-articulos">
          <h2 class="ribon-negro">Mis artículos</h2>
          <div class="e10"></div>
          <ol class="lista_numerica">
            <li>Influencia de la obesidad en el ritmo cardiaco  13/07/2013</li>
            <li>¿Cómo los alimentos influyen en nuestro comportamiento?  11/07/2013</li>
            <li>La reducción de calorías y la de masa corporal 21/06/2013</li>
            <li>Tres dietas para pacientes diabéticos  28/05/2013</li>
            <li>Una aplicación que le ayudará a cdomprender como funciona su cuerpo  13/05/2013</li>
          </ol>
          <div class="e10 clear"></div>
        </div>
        <div class="e20 clear"></div>
        <div class="col-left50 pull-left">
          <div class="fondo-blanco">
            <h2 class="ribon-verde ">Mi dieta</h2>
            <div class="e5 clear"></div>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
          </div>
          <div class="e5 clear"></div>
          <a class="bt_amarillo pull-right miplan">Ver más</a> </div>
        <div class="col-left50 pull-right">
          <div class="fondo-blanco">
            <h2 class="ribon-verde ">Mi rutina</h2>
            <div class="e5 clear"></div>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
          </div>
          <div class="e5 clear"></div>
          <a class="bt_amarillo pull-right rutina">Ver más</a> </div>
        <div class="e10 clear"></div>
        <div class="fondo-negro">
          <h2 class="ribon-amarillo ">Actividad reciente</h2>
          <div class="e5 clear"></div>
          <ol class="lista_numerica">
            <li>Mariana García cambió su perfil</li>
            <li>Mariana García publicó un artículo</li>
            <li>Mariana García cambió su foto de perfil</li>
          </ol>
        </div>
        <div class="e5 clear"></div>
      </div>
    </div>
  </aside>
</section>
