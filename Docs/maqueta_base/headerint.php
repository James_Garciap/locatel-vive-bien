<nav class="navint clearfix">
  <ul class="lista_navint" id="navint">
    <li>
      <a href="#cont1">
        <p>Mi perfil</p>
        <div class="circulo_navprin"></div>
      </a>
    </li>
    <li>
      <a href="#cont2">
        <p>Mi directorio médico</p>
        <div class="circulo_navprin"></div>
      </a>
    </li>
    <li>
      <a href="#cont3">
        <p>Mi plan Vive Bien</p>
        <div class="circulo_navprin"></div>
      </a>
    </li>
    <li>
      <a href="#cont4">
        <p>Mis tips y recetas</p>
        <div class="circulo_navprin"></div>
      </a>
    </li>
    <li>
      <a href="#cont5">
        <p>Mis artículos de interés</p>
        <div class="circulo_navprin"></div>
      </a>
    </li>
    <li>
      <a href="#cont6">
        <p>Mi formula médica</p>
        <div class="circulo_navprin"></div>
      </a>
    </li>
    <li>
      <a href="#cont7">
        <p>Mi calendario Vive Bien</p>
        <div class="circulo_navprin"></div>
      </a>
    </li>
    <li>
      <a href="#cont8">
        <p>Mi ingreso artículos</p>
        <div class="circulo_navprin"></div>
      </a>
    </li>
    <li>
      <a href="#cont9">
        <p>Mi Ciudad Locatel</p>
        <div class="circulo_navprin"></div>
      </a>
    </li>
  </ul>  
</nav>

<div id="mobile-header">
  <a id="responsive-menu-button" href="#"></a>
  <div class="logo_responsivo">
    <a class="inline" href="index.php"><img src="assets/img/logotipo-responsivo.png"></a>
  </div>
  <a id="responsive-menu-buttonperfil" href="#"></a>
</div>

<div class="bgcom"></div>

<nav class="nav_responsivo nav_responsivointernas" id="nav_responsivointernas">
  <ul>
    <li>
      <a href="#cont1">
        Mi perfil
      </a>
    </li>
    <li>
      <a href="#cont2">
        Mi directorio médico
      </a>
    </li>
    <li>
      <a href="#cont3">
        Mi plan Vive Bien
      </a>
    </li>
    <li>
      <a href="#cont4">
        Mis tips y recetas
      </a>
    </li>
    <li>
      <a href="#cont5">
        Mis artículos de interés
      </a>
    </li>
    <li>
      <a href="#cont6">
        Mi formula médica
      </a>
    </li>
    <li>
      <a href="#cont7">
        Mi calendario Vive Bien
      </a>
    </li>
    <li>
      <a href="#cont8">
        Mi ingreso artículos
      </a>
    </li>
    <li>
      <a href="#cont9">
        Mi Ciudad Locatel
      </a>
    </li>
  </ul>
  <div class="cerrar_menumod inline">
    <img src="assets/img/iconos/cerrarmod2.png">
  </div>
</nav>

<nav class="footer_responsivo" id="footer_responsivo">
  <div class="logotipo"></div>
  <div class="clear"></div>
  <a class="bt_ingresar ingresar_footerres llama_modal" href="#ingreso-modal">Ingresar</a>
  <a href="#registro-modal" class="registrarse_footerres llama_modal">Registrase</a>
  <div class="clear"></div>
  <ul>
    <li><a class="mapa" href="#">Mapa de navegación </a></li>
    <li><a class="politicas" href="#">Políticas de calidad</a></li>
    <li><a class="terminos" href="#">Términos y condiciones</a></li>
  </ul>
  <p>© 2013 <strong>VIVEBIEN</strong> - Todos los derechos reservados - Prohibida su reproducción parcial o total </p>
  <div class="footer-ahorranito"></div>
  <div class="cerrar_footermod inline">
    <img src="assets/img/iconos/cerrarmod1.png">
  </div>
</nav>

