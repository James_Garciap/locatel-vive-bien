<div class="fondos-left" style="background: #00874F;">
      <div class="mask-left clearfix">
        <h1>Mi dieta</h1>
        <div class="e10"></div>
        <div class="fondo-blanco">
          <p>Este régimen es para que una mujer que no hace regularmente ejercicios pierda un kilo a la semana sin pasar hambre.</p>
          <p>Se puede elegir entre diversas alternativas de desayuno, almuerzo y comida, siempre respetando la rutina de comer 4 veces por día y privilegiando la ingesta de calorías entre el desayuno y el almuerzo.</p>
        </div>
        <div class="e20"></div>
        <div class="col-left50 pull-left">
          <div class="fondo-blanco clearfix">
            <h2 class="ribon-negro ">Mis logros</h2>
            <div class="clear e10"></div>
            <div class="con_cont50 text_centrado">
              <p>Pasaste de:</p>
              <p class="pkilos inline bg_naranja"><span>74</span> <br>
                Kilos</p>
              <p class="inline separador-a">a</p>
              <p class="pkilos inline bg_verde"><span>69</span> <br>
                Kilos</p>
            </div>
            <div class="clear e10"></div>
            <div class="con_cont50 text_centrado">
              <p><strong>¡Felicidades Mariana!</strong> <br>
                Has logrado un progreso 
                destacado en tu plan vive bien. Sigue progresando.</p>
            </div>
          </div>
          <div class="clear e10"></div>
          <div class="compartir float_left inline">
            <div class="fb-like" data-href="http://developers.facebook.com/docs/reference/plugins/like" data-send="true" data-layout="button_count" data-width="300" data-show-faces="false" data-font="segoe ui"></div>
            <div id="fb-root"></div>
            <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/es_ES/all.js#xfbml=1";
                fjs.parentNode.insertBefore(js, fjs);
              }(document, 'script', 'facebook-jssdk'));
            </script> 
          </div>
        </div>
        <div class="col-left50 pull-right">
          <div class="fondo-negro">
            <h2 class="ribon-amarillo ">Tips</h2>
            <div class="e5 clear"></div>
            <p>ara obtener mayor energía hay que comer mas alimentos ricos en vitamina B2 tiamina y riboflavina.</p>
          </div>
          <div class="clear e10"></div>
          <div class="compartir float_left inline">
            <div class="fb-like" data-href="http://developers.facebook.com/docs/reference/plugins/like" data-send="true" data-layout="button_count" data-width="300" data-show-faces="false" data-font="segoe ui"></div>
            <div id="fb-root"></div>
            <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/es_ES/all.js#xfbml=1";
                fjs.parentNode.insertBefore(js, fjs);
              }(document, 'script', 'facebook-jssdk'));
            </script> 
          </div>
          <a class="bt_amarillo pull-right inline" href="#">Otro Tip</a> </div>
      </div>
</div>

