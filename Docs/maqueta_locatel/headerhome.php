
<div id="mobile-header">
  <a id="responsive-menu-button" href="#"></a>
  <div class="logo_responsivo">
    <img src="assets/img/logotipo-responsivo.png">
  </div>
  <a id="responsive-menu-buttonperfil" href="#"></a>
</div>

<div class="bgcom"></div>

<nav class="nav_responsivo" id="nav_responsivohome">
  <ul>
    <li>
      <a href="contenidos.php#cont1">
        Mi perfil
      </a>
    </li>
    <li>
      <a href="contenidos.php#cont2">
        Mi directorio médico
      </a>
    </li>
    <li>
      <a href="contenidos.php#cont3">
        Mi plan Vive Bien
      </a>
    </li>
    <li>
      <a href="contenidos.php#cont4">
        Mis tips y recetas
      </a>
    </li>
    <li>
      <a href="contenidos.php#cont5">
        Mis artículos de interés
      </a>
    </li>
    <li>
      <a href="contenidos.php#cont6">
        Mi formula médica
      </a>
    </li>
    <li>
      <a href="contenidos.php#cont7">
        Mi calendario Vive Bien
      </a>
    </li>
    <li>
      <a href="contenidos.php#cont8">
        Mi ingreso artículos
      </a>
    </li>
    <li>
      <a href="contenidos.php#cont9">
        Mi Ciudad Locatel
      </a>
    </li>
  </ul>
  <div class="cerrar_menumod inline">
    <img src="assets/img/iconos/cerrarmod2.png">
  </div>
</nav>

<nav class="footer_responsivo" id="footer_responsivo">
  <div class="logotipo"></div>
  <div class="clear"></div>
  <a class="bt_ingresar ingresar_footerres llama_modal" href="#ingreso-modal">Ingresar</a>
  <a href="#registro-modal" class="registrarse_footerres llama_modal">Registrase</a>
  <div class="clear"></div>
  <ul>
    <li><a class="mapa" href="#">Mapa de navegación </a></li>
    <li><a class="politicas" href="#">Políticas de calidad</a></li>
    <li><a class="terminos" href="#">Términos y condiciones</a></li>
  </ul>
  <p>© 2013 <strong>VIVEBIEN</strong> - Todos los derechos reservados - Prohibida su reproducción parcial o total </p>
  <div class="footer-ahorranito"></div>
  <div class="cerrar_footermod inline">
    <img src="assets/img/iconos/cerrarmod1.png">
  </div>
</nav>




