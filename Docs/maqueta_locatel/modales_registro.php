<div class="ingreso-modal clearfix" id="ingreso-modal">
  <div class="e20"></div>
  <h1 class="center cverde">Accede a tu cuenta</h1>
  <div class="e20"></div>
  <form class="centrado">
    <div class="input-prepend">
      <span class="add-on"><i class="icon-user"></i></span>
      <input type="text" placeholder="Usuario">
    </div>
    <div class="e10"></div>
    <div class="input-prepend">
      <span class="add-on"><i class="icon-user"></i></span>
      <input type="password" placeholder="Contraseña">
    </div>
    <div class="e10"></div>
    <a class="olvido llama_modal" href="#olvido-modal">Olvidáste tu contraseña</a>
    <div class="e5"></div>
    <a class="olvido llama_modal" href="#olvido-modal">Olvidáste tu usuario</a>
    <div class="e5"></div>
    <a class="olvido llama_modal" href="#registro-modal">Registrarse</a>
    <div class="e20 "></div>
    <a class="facebook pull-left"></a>
    <button class="pull-right boton " type="submit" onclick="location.href = 'internas.php'">Ingresar</button>
  </form>
</div>

<div class="registro-modal clearfix" id="registro-modal">
  <div class="e20"></div>
  <h1 class="center cverde">Regístrate</h1>
  <div class="e20"></div>
  <div class="row-fluid">
  <div class="colum-left"><h2>Usuario Vive Bien</h2>
    <div class="e10"></div>
  <p class="parrafo">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
  consequat. Duis aute irure dolor...</p>
  <div class="e20 "></div>
  <a class="boton left100" href="registrese.php">Registar</a>
  </div>
  <div class="separador-vertical"></div>
  <div class="colum-right"><h2>Medico Vive Bien</h2>
    <div class="e10"></div>
  <p class="parrafo">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
  consequat. Duis aute irure dolor...</p>
  <div class="e20 "></div>
  <a class="boton left100" href="registrese.php">Registar</a>
  </div>
  </div>
</div>

<div class="olvido-modal clearfix" id="olvido-modal">
  <div class="e20"></div>
  <h1 class="center">Olvidaste tus datos</h1>
  <div class="e10"></div>
  <form class="centrado">
  <p class="parrafo">Ingresa tu e-mail y recibiras tus datos de acceso lo mas pronto posible</p>
  <div class="e20"></div>
  <div class="input-prepend">
    <span class="add-on"><i class="icon-envelope"></i></span>
    <input type="text" placeholder="E-mail">
  </div>
  <div class="e20"></div>
  <button class="pull-right boton" type="submit">Enviar</button>
  </form>
</div>

<div class="actualizar-modal clearfix" id="actualizar-modal"> 
  <h2>Actualizar información</h2>
  <div class="e10"></div>
  <p>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
  </p>
  <form class="formulario">
    <input type="text" placeholder="Nombre">
    <input type="text" placeholder="Cargo">
    <div class="fileupload fileupload-new" data-provides="fileupload">
      <div class="input-append">
        <div class="uneditable-input span3"><span class="fileupload-preview">Subir archivo</span></div>
        <span class="btn btn-file"><span class="fileupload-new"></span><input type="file" /></span>
      </div>
    </div>
    <div class="clear"></div>
    <button class="pull-right boton" type="submit">Actualizar</button>
  </form>
</div>
