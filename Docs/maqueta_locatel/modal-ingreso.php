
<?php include("header.php"); ?>

<div class="ingreso-modal clearfix">
  <div class="e20"></div>
  <h1 class="center cverde">Accede a tu cuenta</h1>
  <div class="e20"></div>
  <form class="centrado">
    <div class="input-prepend">
      <span class="add-on"><i class="icon-user"></i></span>
      <input type="text" placeholder="Usuario">
    </div>
    <div class="e10"></div>
    <div class="input-prepend">
      <span class="add-on"><i class="icon-user"></i></span>
      <input type="password" placeholder="Contraseña">
    </div>
    <div class="e10"></div>
    <a class="olvido" href="">Olvidáste tu contraseña</a>
    <div class="e5"></div>
    <a class="olvido" href="">Olvidáste tu usuario</a>
    <div class="e20 "></div>
    <a class="facebook pull-left"></a>
    <button class="pull-left boton " type="submit" onclick="location.href = 'internas.php'">Ingresar</button>
  </form>
</div>

</body>
</html>
