<section>
    <article>
        <div class="dieta-left" style="background: #00874F;">
            <div class="mask-left">
                <h1>Mi rutina</h1>
                <div class="e10"></div>
                <div class="fondo-blanco ">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vitae magna mi. Nullam augue ipsum, vehicula sed leo eget, mollis commodo orci. Vivamus condimentum diam massa, tincidunt egestas mauris fermentum ut. Curabitur nec lacus vulputate, feugiat augue ac, vestibulum ligula. Aenean in nulla leo. In ultricies molestie ipsum nec lacinia. Aliquam auctor luctus nibh, ut commodo dui sollicitudin sit amet. Morbi pellentesque mauris sit amet turpis euismod ultrices. Duis ut lorem libero. </p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vitae magna mi. Nullam augue ipsum, vehicula sed leo eget, mollis commodo orci. Vivamus condimentum diam massa, tincidunt egestas mauris fermentum ut. </p>
                </div>
                <div class="e20"></div>
                <div class="cont_botonesrutina"> <a class="bt_amarillo inline bt_rituna reportar" href="#" id="">Reporta <br />
                    tu Actividad</a> <a class="bt_amarillo inline bt_rituna conoce" href="#" id="">Conoce <br />
                    tu Avance</a> <a class="bt_amarillo inline bt_rituna tusarticulos" href="#" id="">Artículos<br />
                    para ti</a> </div>
            </div>
        </div>
    </article>
</section>
<section>
    <article>
        <div class="dieta-right" style="background-image: url(assets/img/fondos/fondo-3.png)">
            <div class="mask-right">
                <h2 class="ribon-slider">Lorem Ipsum Dolor Sit amet</h2>
                <ul class="slider-recetas">
                    <li>
                        <figure><img src="assets/img/tips.png"></figure>
                    </li>
                    <li>
                        <figure><img src="assets/img/tips.png"></figure>
                    </li>
                    <li>
                        <figure><img src="assets/img/tips.png"></figure>
                    </li>
                    <li>
                        <figure><img src="assets/img/tips.png"></figure>
                    </li>
                    <li>
                        <figure><img src="assets/img/tips.png"></figure>
                    </li>
                </ul>
                <div class="e10"></div>
                <div class="fondo-blanco nopad">
                    <p>Etapa: <strong>Estabilidad</strong></p>
                    <p>Semana: <strong>10</strong></p>
                    <p>Días: <strong>20</strong></p>
                </div>
                <div class="scroll-ingredientes scroll">
                    <article class="fondo-scroll">
                        <h3>Estabilidad en espalda y cadera</h3>
                        <div class="e10"></div>
                        <p> tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </article>
                </div>
            </div>
        </div>
    </article>
</section>
