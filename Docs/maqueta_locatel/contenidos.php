
<?php include("header.php"); ?>
<?php include("menu.php"); ?>
<?php include("headerint.php"); ?>
<?php include("modales_registro.php"); ?>
<?php include("modales.php"); ?>


<div class="dieta_izq">
  <?php include("mi_dieta.php"); ?>
</div>

<div class="dieta_der">
  <?php include("mi_dieta2.php"); ?>
</div>

<div class="rutina_izq">
  <?php include("mi_rutina.php"); ?>
</div>

<div class="rutina_der">
  <?php include("mi_rutina2.php"); ?>
</div>



<!-- contenido -->

  <div  id="container">

      <!-- left column is the master -->
      <div id="leftColumn">

      <div class="cont1 content_panel scrollable cont_contenidos" id="cont1">
        <?php include("perfil_medico.php"); ?>
      </div>

      <div class="cont2 content_panel scrollable cont_contenidos" id="cont2">
        <?php include("directorio-medico.php"); ?>
      </div>

      <div class="cont3 content_panel scrollable cont_contenidos" id="cont3">
        <?php include("mi_plan.php"); ?>
      </div>

      <div class="cont1 content_panel scrollable cont_contenidos" id="cont4">
        <?php include("tips-recetas.php"); ?>
      </div>

      <div class="cont2 content_panel scrollable cont_contenidos" id="cont5">
        <?php include("noticias.php"); ?>
      </div>

      <div class="cont3 content_panel scrollable cont_contenidos" id="cont6">
        <?php include("formula-medica.php"); ?>
      </div>

      <div class="cont1 content_panel scrollable cont_contenidos" id="cont7">
        <?php include("eventos.php"); ?>
      </div>

      <div class="cont2 content_panel scrollable cont_contenidos" id="cont8">
        <?php include("ingreso-articulos.php"); ?>
      </div>

      <div class="cont3 content_panel scrollable cont_contenidos" id="cont9">
        <?php include("ciudad-locatel.php"); ?>
      </div>

      </div>

      <div id="rightColumn">
        
      <div class="cont3 content_panel scrollable cont_contenidos" id="cont9-1">
        <?php include("ciudad-locatel2.php"); ?>
      </div>

      <div class="cont2 content_panel scrollable cont_contenidos" id="cont8-1">
        <?php include("ingreso-articulos2.php"); ?>
      </div>

      <div class="cont1 content_panel scrollable cont_contenidos" id="cont7-1">
        <?php include("eventos2.php"); ?>
      </div>

      <div class="cont3 content_panel scrollable cont_contenidos" id="cont5-1">
        <?php include("formula-medica2.php"); ?>
      </div>

      <div class="cont2 content_panel scrollable cont_contenidos" id="cont5-1">
        <?php include("noticias2.php"); ?>
      </div>

      <div class="cont1 content_panel scrollable cont_contenidos" id="cont4-1">
        <?php include("tips-recetas2.php"); ?>
      </div>

      <div class="cont3 content_panel scrollable cont_contenidos" id="cont3-1">
        <?php include("mi_plan2.php"); ?>
      </div>

      <div class="cont2 content_panel scrollable cont_contenidos" id="cont2-1"> 
        <?php include("directorio-medico2.php"); ?>
      </div>

      <div class="cont1 content_panel scrollable cont_contenidos" id="cont1-1">
        <?php include("perfil_medico2.php"); ?>
      </div>


      </div>

  </div>

  <?php include("footer.php"); ?>
  <?php include("footer2.php"); ?>


