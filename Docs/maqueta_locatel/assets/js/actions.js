
$(document).ready(function() {


/*Menu home*/

$('ul.menu-home li:first').before($('ul.menu-home li:last'));


$('.arrow_abajo1').click(function (event) {
event.preventDefault();
var item_width = $('ul.menu-home li').outerWidth();
var left_indent = parseInt($('ul.menu-home').css('left')) - item_width;
$('ul.menu-home:not(:animated)').animate({'left' : left_indent},500,function(){
$('ul.menu-home li:last').after($('ul.menu-home li:first'));
$('ul.menu-home').css({'left' : - item_width});
});
});


$('.arrow_abajo2').click(function (event) {
event.preventDefault();
var item_width = $('ul.menu-home li').outerWidth();
var left_indent = parseInt($('ul.menu-home').css('left')) + item_width;
$('ul.menu-home:not(:animated)').animate({'left' : left_indent},500,function(){
$('ul.menu-home li:first').before($('ul.menu-home li:last'));
$('ul.menu-home').css({'left' : - item_width});
});
});




/*Mover menu home con scroll*/

var mousewheelevt = (/Firefox/i.test(navigator.userAgent)) ? "DOMMouseScroll" : "mousewheel" //FF doesn't recognize mousewheel as of FF3.x
$('body').bind(mousewheelevt, function(e){

    var evt = window.event || e //equalize event object     
    evt = evt.originalEvent ? evt.originalEvent : evt; //convert to originalEvent if possible               
    var delta = evt.detail ? evt.detail*(-40) : evt.wheelDelta //check for detail first, because it is used by Opera and FF

    if(delta > 0) {
     
      $(".arrow_abajo1").click();
        
    }
    else{
        
      $(".arrow_abajo2").click();
    }   
});


/*loader*/

$(window).bind("load", function(){jQuery("#loader").fadeOut("slow");});

/*footer*/

$('.slide-up').toggle(
  function () {
    $(".slide-up").attr('class', 'slide-down');
    $("footer").animate({height:'115'},"600");
  },
  function () {
    $(".slide-down").attr('class', 'slide-up');
    $("footer").animate({height:'65'},"1200");
  }
);


/*
  $(".miplan").click(function(event) {
    event.preventDefault();
    $(".dieta_izq").stop(true).animate({left: 0}, 600, function(){
      $("body").css( "overflow", "hidden") ;
    });
    $(".dieta_der").animate({right: 0}, 600);
  });

  $(".rutina").click(function(event) {
    event.preventDefault();
    $(".rutina_izq").stop(true).animate({left: 0}, 600, function(){
      $("body").css( "overflow", "hidden") ;
    });
    $(".rutina_der").animate({right: 0}, 600);
  });
*/


  $(".miplan").click(function(event) {
    event.preventDefault();
    $(".dieta_izq").css("left", "0");
    $(".dieta_der").css("right", "0");
  });

  $(".rutina").click(function(event) {
    event.preventDefault();
    $(".rutina_izq").css("left", "0");
    $(".rutina_der").css("right", "0");
  });

  $("a.cerrar_emerlados").click(function(event) {
    event.preventDefault();
      $(".dieta_izq").css("left", "-55%");
      $(".dieta_der").css("right", "-55%");
      $(".rutina_izq").css("left", "-55%");
      $(".rutina_der").css("right", "-55%");
    });

  $("#responsive-menu-button").click(function(event) {
    event.preventDefault();
    $('.bgcom').css({'opacity':'1','display':'block'});
    $("nav.nav_responsivo").css("left", "0");
  });

  $(".bgcom, .cerrar_menumod, nav.nav_responsivointernas ul li a").click(function(event) {
    event.preventDefault();
    $(".bgcom").css({'opacity':'0','display':'none'});
    $("nav.nav_responsivo").css("left", "-55%");
  });

  $("#responsive-menu-buttonperfil").click(function(event) {
    event.preventDefault();
    $('.bgcom').css({'opacity':'1','display':'block'});
    $(".footer_responsivo").css({'right':'0','opacity':'1'});
  });

  $(".bgcom, .cerrar_footermod").click(function(event) {
    event.preventDefault();
    $(".bgcom").css({'opacity':'0','display':'none'});
    $(".footer_responsivo").css("right", "-55%");
  });

/*navegacion*/
  /*
  var contregresa1 = $('#cont1').offset().top;
  var cont1 = $('#cont1');
  var cont1_offset = cont1.offset();
  var contregresa2 = $('#cont2').offset().top;
  var cont2 = $('#cont2');
  var cont2_offset = cont2.offset();
  var contregresa3 = $('#cont3').offset().top;
  var cont3 = $('#cont3');
  var cont3_offset = cont3.offset();
  var cont4 = $('#cont4');
  var cont4_offset = cont4.offset();
  var cont5 = $('#cont5');
  var cont5_offset = cont5.offset();
  var cont6 = $('#cont6');
  var cont6_offset = cont6.offset();
  var cont7 = $('#cont7');
  var cont7_offset = cont7.offset();
  var cont8 = $('#cont8');
  var cont8_offset = cont8.offset();
  var cont9 = $('#cont9');
  var cont9_offset = cont9.offset();

  console.log(contregresa1);
  console.log(contregresa2);

  window.onscroll = scroll; //cuando hacemos scroll llamamos a la función
  function scroll(){
    var sc = window.pageYOffset;
    console.log('window.pageYOffset = '+sc);
    if(sc > contregresa1){ //si racemes scroll más de 100 pixeles
      setTimeout(function() {$("html, body").delay(5000).stop(true).animate({scrollTop: $("#cont1").offset().top})} , 1500);  
    }

    if(sc > contregresa2){ //si racemes scroll más de 100 pixeles
      setTimeout(function() {$("html, body").delay(5000).stop(true).animate({scrollTop: $("#cont2").offset().top})} , 1500);  
    }

    if(sc > contregresa3){ //si racemes scroll más de 100 pixeles
      setTimeout(function() {$("html, body").delay(5000).stop(true).animate({scrollTop: $("#cont3").offset().top})} , 1500);  
    }
  }

  jQuery(window).bind('scrollstop', function(e){
        console.log(conteo)
        //setTimeout(function() {$("html, body").delay(50000).stop(true).animate({scrollTop: $("#cont2").offset().top})} , 1500);      
      });

  */

$(window).resize(function(){

  var cont1 = $('#cont1');
  var cont1_offset = cont1.offset();
  var cont2 = $('#cont2');
  var cont2_offset = cont2.offset();
  var cont3 = $('#cont3');
  var cont3_offset = cont3.offset();
  var cont4 = $('#cont4');
  var cont4_offset = cont4.offset();
  var cont5 = $('#cont5');
  var cont5_offset = cont5.offset();
  var cont6 = $('#cont6');
  var cont6_offset = cont6.offset();
  var cont7 = $('#cont7');
  var cont7_offset = cont7.offset();
  var cont8 = $('#cont8');
  var cont8_offset = cont8.offset();
  var cont9 = $('#cont9');
  var cont9_offset = cont9.offset();

  $(window).on('scroll', function() {

    if($(window).scrollTop() >= cont1_offset.top -100) {
      $("ul.lista_navint li a").removeClass('activo_nav');
      $("ul.lista_navint li a:eq(0)").addClass('activo_nav');
      $("nav.nav_responsivo ul li a").removeClass('activo_navresp');
      $("nav.nav_responsivo ul li a:eq(0)").addClass('activo_navresp');
    } 
    if($(window).scrollTop() >= cont2_offset.top -100) {
      var conteo = 2;
      $("ul.lista_navint li a").removeClass('activo_nav');
      $("ul.lista_navint li a:eq(1)").addClass('activo_nav');
      $("nav.nav_responsivo ul li a").removeClass('activo_navresp');
      $("nav.nav_responsivo ul li a:eq(1)").addClass('activo_navresp');
    } 
    if($(window).scrollTop() >= cont3_offset.top -100) {
      $("ul.lista_navint li a").removeClass('activo_nav');
      $("ul.lista_navint li a:eq(2)").addClass('activo_nav');
      $("nav.nav_responsivo ul li a").removeClass('activo_navresp');
      $("nav.nav_responsivo ul li a:eq(2)").addClass('activo_navresp');
      console.log("funciona")
    } 
    if($(window).scrollTop() >= cont4_offset.top -100) {
      $("ul.lista_navint li a").removeClass('activo_nav');
      $("ul.lista_navint li a:eq(3)").addClass('activo_nav');
      $("nav.nav_responsivo ul li a").removeClass('activo_navresp');
      $("nav.nav_responsivo ul li a:eq(3)").addClass('activo_navresp');
      console.log("funciona")
    } 
    if($(window).scrollTop() >= cont5_offset.top -100) {
      $("ul.lista_navint li a").removeClass('activo_nav');
      $("ul.lista_navint li a:eq(4)").addClass('activo_nav');
      $("nav.nav_responsivo ul li a").removeClass('activo_navresp');
      $("nav.nav_responsivo ul li a:eq(4)").addClass('activo_navresp');
      console.log("funciona")
    } 
    if($(window).scrollTop() >= cont6_offset.top -100) {
      $("ul.lista_navint li a").removeClass('activo_nav');
      $("ul.lista_navint li a:eq(5)").addClass('activo_nav');
      $("nav.nav_responsivo ul li a").removeClass('activo_navresp');
      $("nav.nav_responsivo ul li a:eq(5)").addClass('activo_navresp');
      console.log("funciona")
    } 
    if($(window).scrollTop() >= cont7_offset.top -100) {
      $("ul.lista_navint li a").removeClass('activo_nav');
      $("ul.lista_navint li a:eq(6)").addClass('activo_nav');
      $("nav.nav_responsivo ul li a").removeClass('activo_navresp');
      $("nav.nav_responsivo ul li a:eq(6)").addClass('activo_navresp');
      console.log("funciona")
    } 
    if($(window).scrollTop() >= cont8_offset.top -100) {
      $("ul.lista_navint li a").removeClass('activo_nav');
      $("ul.lista_navint li a:eq(7)").addClass('activo_nav');
      $("nav.nav_responsivo ul li a").removeClass('activo_navresp');
      $("nav.nav_responsivo ul li a:eq(7)").addClass('activo_navresp');
      console.log("funciona")
    } 
    if($(window).scrollTop() >= cont9_offset.top -100) {
      $("ul.lista_navint li a").removeClass('activo_nav');
      $("ul.lista_navint li a:eq(8)").addClass('activo_nav');
      $("nav.nav_responsivo ul li a").removeClass('activo_navresp');
      $("nav.nav_responsivo ul li a:eq(8)").addClass('activo_navresp');
      console.log("funciona")
    } 
  });

});

// Ejecutamos la función
$(window).resize();


/*scroll*/

$('ul.lista_navint li a, nav.nav_responsivointernas ul li a, a.escribir_articulo').click(function(event) {
    event.preventDefault();
    enlace  = $(this).attr('href');
    $('html, body').animate({
    scrollTop: $(enlace).offset().top
  }, 1000);
});

/*centrar botones inicio*/

  $(window).resize(function(){

        $('.navint').css({
            position:'fixed',
            top: ($(window).height() - $('.navint').outerHeight())/2,
         });

        /*
        $(".cont_texto").each(function() {
          var alto_papa = $(this).parent("li").height();
          var alto_this = $(this).height();
            $(this).css({
              position:'absolute',
              top: (alto_papa - $(this).outerHeight())/2,
            });
        });
*/


        $(".mask-left").each(function() {
          var alto_papa = $(this).parent(".fondos-left").height();
          var alto_this = $(this).height();
          //console.log(alto_papa);
          //console.log(alto_this);

          if( alto_papa > alto_this +50 ){

            $(this).css({
                 position:'absolute',
                 top: (alto_papa - $(this).outerHeight())/2,
            });

          } else{
            $(this).css({
                 position:'relative',
                 marginTop: 60,
                 marginBottom: 60
            });
          }


        });

        $(".mask-right").each(function() {
          var alto_papa = $(this).parent(".fondos-right").height();
          var alto_this = $(this).height();
          //console.log(alto_papa);
          //console.log(alto_this);

          if( alto_papa > alto_this +50){

            $(this).css({
                 position:'absolute',
                 top: (alto_papa - $(this).outerHeight())/2,
            });

          } else{
             $(this).css({
                 position:'relative',
                 marginTop: 60,
                 marginBottom: 60
            });
          }


        });

  });

  // Ejecutamos la función
  $(window).resize();

});


